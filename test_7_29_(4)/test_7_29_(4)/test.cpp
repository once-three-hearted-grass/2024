#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
    int singleNumber(vector<int>& nums) {
        //依次遍历数组的每个二进制之和，%3就是那个答案的一个二进制
        //空间复杂度为o（n）
        int answer = 0;
        for (int i = 0; i < 32; i++)
        {
            int sum = 0;
            for (int j = 0; j < nums.size(); j++)
            {
                if (((nums[j] >> i) & 1) == 0)
                {
                    sum += 0;
                }
                else
                {
                    sum += 1;
                }
            }
            sum = sum % 3;//这个就是答案的第i个二进制
            answer = (answer | (sum << i));
        }
        return answer;
    }
};