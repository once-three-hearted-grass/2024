#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <math.h>


int main() {
    double a, b, c, p, s;
    scanf("%lf %lf %lf", &a, &b, &c);
    if (a <= 0 || b <= 0 || c <= 0)
    {
        printf("This isn��t a triangle.\n");
        return 0;
    }
    if (b > a) {
        double t = a;
        a = b;
        b = t;
    }
    if (c > a) {
        double t = a;
        a = c;
        c = t;
    }
    if (b + c > a) {
        if (a * a == b * b + c * c) {
            printf("This is a Right triangle.\n");
        }
        else if (a * a > b * b + c * c) {
            printf("This is an Obtuse triangle.\n");
        }//ppaaaaa����mmmssssssqqq������zzzsssqqqaaaAAaasssssszzzssssssaaasssssss
        else {
            printf("This is a Acute triangle.\n");
        }
        p = (a + b + c) / 2;
        s = sqrt(p * (p - a) * (p - b) * (p - c));
        printf("Area=%.3lf\n", s);
    }
    else {
        printf("This isn't a triangle.\n");
    }
    return 0;
}