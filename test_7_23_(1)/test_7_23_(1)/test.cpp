#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

void test1()
{
	//string s2("sdwjfhe");
	//cout << s2<<endl;
	//string s3("12345677", 3);
	//cout << s3<<endl;
	//string s4(4, '2');
	//cout << s4<<endl;
	//string s5;
	//s5 = s4;
	//cout << s5;
	string s6 = "sadijwhf";
	cout << s6<<endl;
	const string& s7 = "sadijwhf";
	cout << s7<<endl;
}

void test2()
{
	string s1("dxwhjcbjhc");
	for (int i = 0; i < s1.length(); i++)
	{
		cout << s1[i] << " ";
	}
	cout << endl;
}

void test3()
{
	string s1("dxwhjcbjhc");
	string::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << ' ';
		it++;
	}
	cout << endl;
	for (auto x : s1)
	{
		cout << x << ' ';
	}
}

void test4()
{
	const string s1("dxwhjcbjhc");
	//string::const_reverse_iterator it = s1.rbegin();
	auto it = s1.rbegin();
	while (it != s1.rend())
	{
		cout << *it << ' ';
		it++;
	}
}

//void test5()
//{
//	string s1("dxwhjcbjhc");
//	auto it1 = s1.begin();
//	auto it2 = s1.end();
//	sort(it1, it2);
//	cout << s1 << endl;
//}
//
//void test5()
//{
//	string s1("dxwhjcbjhc");
//	auto it1 = s1.begin();
//	auto it2 = s1.end();
//	sort(it1, it1+5);
//	cout << s1 << endl;
//}

void test6()
{
	string s1("111111");
	//s1.push_back('x');
	//s1.append("akdja");
	//cout << s1 << endl;
	//s1 = "111111";
	//string s2("000000");
	//s1.append(s2);
	//cout << s1 << endl;
	s1 += '2';
	cout << s1 << endl;
	s1 += "3333";
	cout << s1 << endl;
	string s2 = "4444";
	s1 += s2;
	cout << s1 << endl;
}

void test7()
{
	cout << typeid(string::iterator).name() << endl;
}

void test8()
{
	string s1 = "111111111111111";
	s1.assign("2222");
	cout << s1<<endl;
	string s2 = "111111111111111";
	string s3 = "2222";
	s2.assign(s3);
	cout << s2 << endl;
	string s4 = "111111111111111";
	s4.assign(6,'9');
	cout << s4 << endl;

}

void test9()
{
	string s1 = "111111111111111";
	s1.insert(0, "222222");
	cout << s1 << endl;
	string s2 = "333330";
	s1.insert(0, s2);
	cout << s1 << endl;
	s1.insert(0, 1, 'x');
	cout << s1 << endl;
	s1.insert(s1.begin(), 'y');
	cout << s1 << endl;
	s1.insert(s1.begin(),1, 'z');
	cout << s1 << endl;
	string s3 = "wwwwwwww";
	s1.insert(s1.begin(), s3.begin(), s3.end());
	cout << s1 << endl;
}

void test10()
{
	/*string s1 = "12345678";
	s1.erase(0, 3);
	cout << s1 << endl;*/
	//string s1 = "hello world";
	//s1.replace(5, 1, "%20");
	//cout << s1;
	//string s1("hello world hello bit");
	//for (int i = 0; i < s1.size(); i++)
	//{
	//	if (s1[i] == ' ')
	//	{
	//		s1.replace(i, 1, "%20");
	//	}
	//}
	//cout << s1;
	//string s1("hello world hello bit");
	//string s2;
	//for (auto x : s1)
	//{
	//	if (x == ' ')
	//	{
	//		s2 += "%20";
	//	}
	//	else
	//	{
	//		s2 += x;
	//	}
	//}
	//cout << s2;
}

void test11()
{
	//string s1;
	//for (int i = 0; i < 300; i++)
	//{
	//	if (s1.capacity() == s1.size())
	//	{
	//		cout << "capacity:" << s1.capacity() << endl;
	//	}
	//	s1 += '1';
	//}
	string s1 = "1234567";
	cout << s1.capacity() << endl;
	s1.reserve(200);
	cout << s1.capacity() << endl;
	s1.reserve(10);
	cout << s1.capacity() << endl;
}
void test12()
{
	//string s1;
	//cout << sizeof(s1) << endl;
	//cout << s1.max_size() << endl;
	//string s1;
	//cout << s1.size() << endl;
	//s1.resize(29);
	//cout << s1.size() << endl;
	//cout << s1.capacity() << endl;
	string s1 = "1234567";
	s1.resize(34, 'x');
	cout << s1 << endl;
	s1.resize(5);
	cout << s1.size() << endl;
}

void test13()
{
	//string s1 = "1234567";
	/*for (int i = 0; i < s1.size(); i++)
	{
		cout << s1.at(i) << ' ';
	}*/
	string s1 = "1234567";
	try
	{
		//s1[10];
		//s1.at(10);
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}
}

void test14()
{
	//string s1 = "test.cpp";
	//FILE* pf = fopen(s1.c_str(), "r");
	//char ch = fgetc(pf);
	//while (ch != EOF)
	//{
	//	cout << ch;
	//	ch = fgetc(pf);
	//}
	//string s1 = "test.cpp.zip";
	//int pos = s1.rfind('.');
	//string s2 = s1.substr(pos);
	//cout << s2 << endl;
	//string s1 = "https://gitee.com/bithange/112-issueTest.cpp";
	//int pos1 = s1.find(':');
	//string s2 = s1.substr(0, pos1 - 0);
	//cout << s2 << endl;
	//int pos2 = s1.find('/',pos1+3);
	//string s3 = s1.substr(pos1 + 3, pos2 - pos1 - 3);
	//cout << s3 << endl;
	//string s4 = s1.substr(pos2+1);
	//cout << s4 << endl;

	//string s1 = "1235768675246365768";
	//int pos = s1.find_first_of("34", 0);
	//cout << pos;
	//string s1 = "Please, replace the vowels in this sentence by asterisks.";
	//string s2 = "aeiou";
	//int pos = s1.find_first_of(s2, 0);
	//while (pos < s1.size())
	//{
	//	s1[pos] = '*';
	//	pos = s1.find_first_of(s2, pos + 1);
	//}
	//cout << s1 << endl;
	string s1 = "Please, replace the vowels in this sentence by asterisks.";
	cout << s1.npos<<endl;
	cout << std::string::npos<<endl;
	//	std::string str("please, replace the vowels in this sentence by asterisks.");
	//std::size_t found = str.find_first_of("aeiou");
	//while (found != std::string::npos)
	//{
	//	str[found] = '*';
	//	found = str.find_first_of("aeiou", found + 1);
	//}
	//cout << str<<endl;
	//cout << std::string::npos;
}

void test15()
{
	//string s1 = "1234";
	//string s3 = s1 + "678";
	//cout << s3 << endl;
	string s1 = "asdfwer";
	string s2 = "asdcv";
	cout << (s1 > s2) << endl;
}

void test16()
{
	/*string s1;
	string s2;
	cin >> s1>>s2;
	cout << s1<<endl;
	cout << s2<<endl;*/
	//string s1;
	//while (cin >> s1)
	//{
	//	cout << s1 << endl;
	//}

	//string s1;
	//getline(cin, s1);
	//cout << s1;

	//string s1 = "123456";
	//int ret = atoi("123456");
	//cout << ret << endl;

	//char arr[20] = { 0 };
	//itoa(1234567, arr, 10);
	//cout << arr << endl;

	//int a = 88888;
	//int b = 11111;
	//string s1 = to_string(a + b);
	//cout << s1 << endl;
	//string s1 = "123456";
	//int ret = stoi(s1);
	//cout << ret;
	//char arr1[] = "1234";
	//char arr2[] = "����";
	//cout << sizeof(arr1) << endl;
	//cout << sizeof(arr2) << endl;

	//string s1 = "123456";
	//cout << s1.empty() << endl;
	//s1.clear();
	//cout << s1<<endl;
	//cout << s1.empty() << endl;
	string s1 = "123456";
	cout << s1.back() << endl;
	cout << s1.front() << endl;
}

int main()
{
	test16();
	return 0;
}