#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdbool.h>
#include<math.h>
#define MAXSIZE 100          //假设非零元个数的最大值为100
#define MAX 10
typedef int ElemType;
typedef struct {
    int i, j;									//非零元的行下标和列下标，i 和 j 从 1 开始计数，与数学中矩阵元素的编号一致
    ElemType e;						//非零元的值
}Triple;

typedef struct {
    Triple data[MAXSIZE];			// 非零元三元组表
    int    m, n, len;							// 矩阵的行数、列数和非零元个数
}TSMatrix;


bool add_matrix(const TSMatrix* pM, const TSMatrix* pN, TSMatrix* pQ) {
    if (pM->m != pN->m || pM->n != pN->n)
    {
        return false;
    }

    //直接创建两个二维数组进行计算
    ElemType arr1[MAX][MAX] = {0};
    for (int sub = 0; sub < pM->len; sub++)
    {
        arr1[pM->data[sub].i][pM->data[sub].j] = pM->data[sub].e;
    }
    ElemType arr2[MAX][MAX] = { 0 };
    for (int sub = 0; sub < pN->len; sub++)
    {
        arr2[pN->data[sub].i][pN->data[sub].j] = pN->data[sub].e;
    }
    ElemType arr3[MAX][MAX] = { 0 };
    for (int sub1 = 0; sub1 < MAX; sub1++)
    {
        for (int sub2 = 0; sub2 < MAX; sub2++)
        {
            arr3[sub1][sub2] = arr1[sub1][sub2] + arr2[sub1][sub2];
        }
    }
    int count = 0;
    for (int sub1 = 0; sub1 < MAX; sub1++)
    {
        for (int sub2 = 0; sub2 < MAX; sub2++)
        {
            if (arr3[sub1][sub2] != 0)
            {
                pQ->data[count].i = sub1;
                pQ->data[count].j = sub2;
                pQ->data[count].e = arr3[sub1][sub2];
                count++;
            }
        }
    }
    pQ->len = count;
    pQ->m = MAX;
    pQ->n = MAX;

    return true;
}