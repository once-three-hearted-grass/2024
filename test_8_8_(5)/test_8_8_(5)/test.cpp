//#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//using namespace std;
//int main()
//{
//	printf("%d %d", 'A', 'a');
//}


#include <iostream>
#include<map>
#include<vector>
#include<algorithm>
using namespace std;

class my_compare
{
public:
    bool operator()(pair<string, int> p1, pair<string, int> p2)
    {
        return (p1.second > p2.second) || (p1.second == p2.second && p1.first < p2.first);//因为要降序//而且字节序小的在前面
        //这样写就是次数大的在前面，字节序小的在前面
    }
};

class Solution {
public:
    void topKFrequent(vector<string>& words) {
        //先把每个string统计好次数
        map<string, int> m;
        for (auto x : words)
        {
            ++m[x];
        }
        //这样就统计好次数了
        // 先存入vector中再排序//因为sort只能排连续的//所以传入pair
        vector<pair<string, int>> tmp;
        for (auto x : m)
        {
            tmp.push_back(x);
        }
        //接下来按照次数来排序//我们用库里面的排序算法
        //sort(tmp.begin(), tmp.end());//但这样不行，因为这样默认排的序是pair的first，如果first相同就比second，所以我们要自己写仿函数
        sort(tmp.begin(), tmp.end(), my_compare());//传入对象进去就可以了，这里是匿名对象
        //开始打印
        for (auto x : tmp)
        {
            cout << x.first << ":" << x.second << endl;
        }
    }
};

int main() {
    string str;
    vector<string> tmp;
    while (cin >> str) { // 注意 while 处理多个 case
        auto it = str.begin();
        while (it != str.end())
        {
            if ((*it) >= 'A' && (*it) <= 'Z')//大写转小写
            {
                (*it) += 32;
            }
            if ((*it) < 'a' || (*it) > 'z')//删除非字母
            {
                it = str.erase(it);//小心迭代器失效
            }
            else {
                it++;
            }
        }
        tmp.push_back(str);
    }
    Solution().topKFrequent(tmp);
}