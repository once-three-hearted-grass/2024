#define _CRT_SECURE_NO_WARNINGS 1
//模拟和实现strcat
//字符串拼接
#include<stdio.h>
#include<string.h>
#include<assert.h>
char* my_strcat(char* destination, const char* source)
{
	char* start = destination;
	assert(destination && source);
	while (*destination != '\0')
	{
		destination++;
	}
	while (*source != '\0')
	{
		*(destination++) = *(source++);
	}
	return start;

	
}
int main()
{
	char arr1[100] = "ckckyyds";
	char arr2[] = "hrtsb";
	my_strcat(arr1, arr2);
	//strcat(arr1, arr2);
	//拼接字符串直到拼到\0
	//char *strcat( char *strDestination, const char *strSource );
	printf("%s", arr1);
	return 0;
}