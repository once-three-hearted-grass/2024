#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int main() {//
    int a, b;
    char op;
    while (scanf("%d%c%d", &a, &op, &b) != EOF)
    {
        if (op == '+') {
            printf("%d+%d=%d\n", a, b, a + b);
        }
        else if (op == '-') {
            printf("%d-%d=%d\n", a, b, a - b);
        }
        else if (op == '*') {
            printf("%d*%d=%d\n", a, b, a * b);
        }
        else if (op == '/') {
            if (b == 0) {
                printf("Division by zero!\n");
            }
            else {
                printf("%d/%d=%d\n", a, b, a / b);
            }
        }
        else {
            printf("Invalid operator!\n");
        }

    }//

    return 0;
}