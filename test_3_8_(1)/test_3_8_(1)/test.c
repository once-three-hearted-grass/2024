#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//输入：[1, 1, 0, 1, 1, 1]
//输出：3
//解释：开头的两位和最后的三位都是连续 1 ，所以最大连续 1 的个数是 3.
int findMaxConsecutiveOnes(int* nums, int numsSize) {
    int max = 0;
    int first = 0;
    int end = 0;
    while (first < numsSize && end < numsSize)
    {
        if (nums[end] == 1)
        {
            end++;
        }
        else
        {
            max = (max > (end - first)) ? max : (end - first);
            end++;
            first = end;
        }
        if (end == numsSize)//最后一个是1进行求max
        {
            max = (max > (end - first)) ? max : (end - first);
        }
    }
    return max;
}
int main()
{
    int arr[] = {1,1,0,1,1,1};
    int sz = sizeof(arr) / sizeof(arr[0]);
    int ret = findMaxConsecutiveOnes(arr, sz);
    printf("%d", ret);
	return 0;
}