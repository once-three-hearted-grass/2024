#define _CRT_SECURE_NO_WARNINGS 1
//编写一个函数实现n的k次方，使用递归实现
#include<stdio.h>
double power(int n, int k)
{
	if (k > 0)
	{
		return n * power(n,k-1);
	}
	else if(k==0)
	{
		return 1;
	}
	else
	{
		return (1 / (double)n) * power(n, k + 1);
	}
}
int main()
{
	int n = 0;
	int k = 0;
	printf("输入底数:");
	scanf("%d", &n);
	printf("输入次方数:");
	scanf("%d", &k);
	double ret = power(n, k);
	printf("%d的%d次方为:%.2f",n,k,ret);
	return 0;
}