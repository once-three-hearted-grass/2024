#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<stdio.h>

//这里的所有逻辑一定要画图一定要画图画图画图！！！！！！！！！！！！！！

//这一个我们主要实现的就是平衡二叉树
typedef struct AVL
{
	int data;
	int height;
	struct AVL* left;
	struct AVL* right;
}Tree;

//定义一个求两个数最大值的函数，因为一个树的高度就是左右子树高度最大值加一
int GetMax(int a, int b)
{
	return a > b ? a : b;
}

//这里我们定义一个返回树的高度的函数
int GetHeight(Tree* root)
{
	if (root == NULL)
	{
		return 0;
	}
	return root->height;
}

//void AVLInsert(Tree** T, int key)
//{
//	//我们先实现二叉搜索树，就是顺便在以前的基础上添加了树的高度的实现
//	if ((*T) == NULL)
//	{
//		Tree* tmp = (Tree*)malloc(sizeof(Tree));
//		if (tmp == NULL)
//		{
//			exit(-1);
//		}
//		tmp->data = key;
//		tmp->left = NULL;
//		tmp->right = NULL;
//		tmp->height = 0;//这个写不写都无所谓
//		(*T) = tmp;
//	}
//	if (key < (*T)->data)
//	{
//		AVLInsert(&(*T)->left, key);
//	}
//	else if (key > (*T)->data)
//	{
//		AVLInsert(&(*T)->right, key);
//	}
//	else
//	{
//		return;
//	}
//	//这样的话，利用GetMax和GetHeight就可以求出所有树的的高度了
//	//总的思路就是插入完左右子树才能决定这个T的高度，所以放在最后来写
//	//就这最后一个一步，就造成了树的高度的实现，以后都可以这样干，边创造树，边得到树的高度，反正树的高度就是最后一步得到的
//	//先创建根，在创建左右子树，这样根的高度才可以得到哦
//	(*T)->height = GetMax(GetHeight((*T)->left), GetHeight((*T)->right)) + 1;//反正高度就是左右子树的最大值加一
//}

void LLAdjust(Tree**T)
{
	//LL调整的话，就只需要调整两个就可以了，一个是parent，也就是那个高度差为2的，就是T
	//还有一个是child,也就是parent的左孩子
	Tree* parent = (*T);
	Tree* child = parent->left;
	//怎么调整呢，先将child的右孩子变为parent的左孩子
	//在把parent变为child的右孩子
	parent->left = child->right;
	child->right = parent;
	//最后更改根节点
	*T = child;

	//调整完别忘了更改高度啊
	//主要高度变的只有parent和child
	//parent的高度就是左右子树高度最大值加一
	//child也是一样的
	//但要注意的是，一定要先求parent的高度，因为parent是child的孩子，所以要求child的高度就要先求parent的高度
	parent->height= GetMax(GetHeight(parent->left), GetHeight(parent->right)) + 1;
	child->height= GetMax(GetHeight(child->left), GetHeight(child->right)) + 1;
}

//顺便写出RR
void RRAdjust(Tree** T)
{
	//只需要把左换右，右换左就可以了

	Tree* parent = (*T);
	Tree* child = parent->right;

	parent->right = child->left;
	child->left = parent;

	*T = child;
	parent->height = GetMax(GetHeight(parent->left), GetHeight(parent->right)) + 1;
	child->height = GetMax(GetHeight(child->left), GetHeight(child->right)) + 1;
}

//void AVLInsert(Tree** T, int key)
//{
//	//现在已经实现了树的高度这个操作，最终实现平衡二叉树的操作还需要多做一步，就是在边插入，边检查是否平衡
//	//如果不平衡的话还要调整
//	if ((*T) == NULL)
//	{
//		Tree* tmp = (Tree*)malloc(sizeof(Tree));
//		if (tmp == NULL)
//		{
//			exit(-1);
//		}
//		tmp->data = key;
//		tmp->left = NULL;
//		tmp->right = NULL;
//		tmp->height = 0;//这个写不写都无所谓
//		(*T) = tmp;
//		//走到这并不能说明什么高度问题
//	}
//	if (key < (*T)->data)
//	{
//		AVLInsert(&(*T)->left, key);
//		//走到这一步就说明了已经插入到这个节点的左边了
//		//所以要看一下这个节点是不是满足平衡
//		//那就看左子树的高度减去右子树的高度是不是等于2了，如果等于2，说明要调整了，因为是往左边插入的，所以在已经是
//		//平衡二叉树的前提下，只能是左边子树的高度大于右边子树的高度
//		if (GetHeight((*T)->left) - GetHeight((*T)->right) == 2)
//			//进入这个判断条件说明要调整
//			//由于我们插入就是到了最底层的，所以递归出来的时候，一定是最小的树，下面的某个根不满足平衡二叉树了
//			//因为插入是递进去，然后是归出来，所以发现不平衡的节点，一定是最小的树的时候
//		{
//			//说明左边有问题了
//			//接下来就要判断到底是LL还是LR
//			if (key < (*T)->left->data)
//			{
//				//LL//比根的左孩子都还要小的话就是LL了
//				//就调用个函数就可以了
//				//只用传那个根节点的地址就可以了，这样既可以访问到后面子孙，又可以改变根
//				LLAdjust(T);
//			}
//			else if(key == (*T)->left->data)
//			{
//				return;//这里有问题，因为已经给(*T)赋值了,所以的话最后一层的节点永远走不到最后一步，高度永远为0，所以换个方式处理，大的逻辑用else括起来
//				//因为建造二叉搜索树时，最后对T没什么改动，所以才没事，或者一开始就给height赋值为1，算了，我还是用大else括起来感觉好点
//			}
//			else
//			{
//				//LR//比根的左孩子都还要大的话就是LR了
//				//LR的话，就是可以这样
//				//先用RR的函数，再用LL的函数，只不过调用的参数不一样
//				//RR的参数是&(*T)->left
//				//LL就是T
//				RRAdjust(&(*T)->left);
//				LLAdjust(T);
//			}
//		}
//	}
//	else if (key > (*T)->data)
//	{
//		AVLInsert(&(*T)->right, key);
//		if (GetHeight((*T)->right) - GetHeight((*T)->left) == 2)
//		{
//			if (key > (*T)->right->data)
//			{
//				RRAdjust(T);
//			}
//			else if (key == (*T)->right->data)
//			{
//				return;
//			}
//			else
//			{
//				//RL
//				//左换右右换左就可以了，很简单
//				LLAdjust(&(*T)->right);
//				RRAdjust(T);
//			}
//		}
//	}
//	else
//	{
//		return;
//	}
//	(*T)->height = GetMax(GetHeight((*T)->left), GetHeight((*T)->right)) + 1;//反正高度就是左右子树的最大值加一
//}

void AVLInsert(Tree** T, int key)
{
	//现在已经实现了树的高度这个操作，最终实现平衡二叉树的操作还需要多做一步，就是在边插入，边检查是否平衡
	//如果不平衡的话还要调整
	if ((*T) == NULL)
	{
		Tree* tmp = (Tree*)malloc(sizeof(Tree));
		if (tmp == NULL)
		{
			exit(-1);
		}
		tmp->data = key;
		tmp->left = NULL;
		tmp->right = NULL;
		tmp->height = 0;//这个写不写都无所谓
		(*T) = tmp;
		//走到这并不能说明什么高度问题
	}
	else
	{
		if (key < (*T)->data)
		{
			AVLInsert(&(*T)->left, key);
			//走到这一步就说明了已经插入到这个节点的左边了
			//所以要看一下这个节点是不是满足平衡
			//那就看左子树的高度减去右子树的高度是不是等于2了，如果等于2，说明要调整了，因为是往左边插入的，所以在已经是
			//平衡二叉树的前提下，只能是左边子树的高度大于右边子树的高度
			if (GetHeight((*T)->left) - GetHeight((*T)->right) == 2)
				//进入这个判断条件说明要调整
				//由于我们插入就是到了最底层的，所以递归出来的时候，一定是最小的树，下面的某个根不满足平衡二叉树了
				//因为插入是递进去，然后是归出来，所以发现不平衡的节点，一定是最小的树的时候
			{
				//说明左边有问题了
				//接下来就要判断到底是LL还是LR
				if (key < (*T)->left->data)
				{
					//LL//比根的左孩子都还要小的话就是LL了
					//就调用个函数就可以了
					//只用传那个根节点的地址就可以了，这样既可以访问到后面子孙，又可以改变根
					LLAdjust(T);
				}
				else if (key == (*T)->left->data)
				{
					return;//这里有问题，因为已经给(*T)赋值了,所以的话最后一层的节点永远走不到最后一步，高度永远为0，所以换个方式处理，大的逻辑用else括起来
					//因为建造二叉搜索树时，最后对T没什么改动，所以才没事，或者一开始就给height赋值为1，算了，我还是用大else括起来感觉好点
				}
				else
				{
					//LR//比根的左孩子都还要大的话就是LR了
					//LR的话，就是可以这样
					//先用RR的函数，再用LL的函数，只不过调用的参数不一样
					//RR的参数是&(*T)->left
					//LL就是T
					RRAdjust(&(*T)->left);
					LLAdjust(T);
				}
			}
		}
		else if (key > (*T)->data)
		{
			AVLInsert(&(*T)->right, key);
			if (GetHeight((*T)->right) - GetHeight((*T)->left) == 2)
			{
				if (key > (*T)->right->data)
				{
					RRAdjust(T);
				}
				else if (key == (*T)->right->data)
				{
					return;
				}
				else
				{
					//RL
					//左换右右换左就可以了，很简单
					LLAdjust(&(*T)->right);
					RRAdjust(T);
				}
			}
		}
		else
		{
			return;
		}
	}

	(*T)->height = GetMax(GetHeight((*T)->left), GetHeight((*T)->right)) + 1;//反正高度就是左右子树的最大值加一
}

//来写个中序遍历和先序遍历，看自己写的平衡二叉树对不对
void InOrder(Tree* root)
{
	if (root == NULL)
	{
		return;
	}
	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}

void PreOrder(Tree* root)
{
	if (root == NULL)
	{
		return;
	}
	printf("%d ", root->data);
	PreOrder(root->left);
	PreOrder(root->right);
}

int main()
{
	int arr[] = { 10,8,1,90,87};
	int sz = sizeof(arr) / sizeof(arr[0]);
	Tree* t=NULL;
	for (int i = 0; i < sz; i++)
	{
		AVLInsert(&t, arr[i]);
	}
	InOrder(t);
	printf("\n");
	PreOrder(t);
	return 0;
}