#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//给你一个整数数组 nums ，请计算数组的 中心下标 。
//数组 中心下标 是数组的一个下标，其左侧所有元素相加的和等于右侧所有元素相加的和。
//如果中心下标位于数组最左端，那么左侧数之和视为 0 ，因为在下标的左侧不存在元素。这一点对于中心下标位于数组最右端同样适用。
//如果数组有多个中心下标，应该返回 最靠近左边 的那一个。如果数组不存在中心下标，返回 - 1 。
int pivotIndex(int* nums, int numsSize) 
{
    int i = 0;
    int j = 0;
    for (i = 0; i < numsSize; i++)
    {
        int sum1 = 0;
        int sum2 = 0;
        sum1 = 0;
        sum2 = 0;
        if (i == 0)
        {
            for (j = 1; j < numsSize; j++)
            {
                sum2 += nums[j];
            }
            if (sum2 == 0)
            {
                return i;
            }
        }
        for (j = 0; j < i; j++)
        {
            sum1 += nums[j];
        }
        for (j = i + 1; j < numsSize; j++)
        {
            sum2 += nums[j];
        }
        if (sum1 == sum2)
        {
            return i;
        }
        if (i == numsSize - 1)
        {
            for (j = 0; j < numsSize - 1; j++)
            {
                sum1 += nums[j];
            }
            if (sum1 == 0)
            {
                return i;
            }
        }
    }
    return -1;
}
int main()
{
    int arr[6] = { 1,7,3,6,5,6 };
    printf("%d", pivotIndex(arr, 6));
	return 0;
}