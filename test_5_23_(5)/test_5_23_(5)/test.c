#define _CRT_SECURE_NO_WARNINGS 1
//描述
//编一个程序，读入用户输入的一串先序遍历字符串，根据此字符串建立一个二叉树（以指针方式存储）。 例如如下的先序遍历字符串：
//ABC##DE#G##F### 其中“#”表示的是空格，空格字符代表空树。建立起此二叉树以后，再对二叉树进行中序遍历，输出遍历结果。
//输入描述：
//输入包括1行字符串，长度不超过100。
//输出描述：
//可能有多组测试数据，对于每组数据， 输出将输入字符串建立二叉树后中序遍历的序列，每个字符后面都有一个空格。 每个输出结果占一行。

#include <stdio.h>
#include<stdlib.h>

struct TreeNode
{
    char val;
    struct TreeNode* left;
    struct TreeNode* right;
};

void PreOrderTree(struct TreeNode* tree)
{
    if (tree == NULL)
    {
        return;
    }
    PreOrderTree(tree->left);//左
    printf("%c ", tree->val);//根
    PreOrderTree(tree->right);//右
}
struct TreeNode* TreeCreat(struct TreeNode* tree, char* arr, int* i)
{
    //因为先序遍历，所以先序递归
    //先创建头，先创建根
    //再创建左右孩子，而且左右孩子的创建与根是一样的
    //最后再返回头

    //停止条件，第一NULL不为#创建，第二数组值为#，不创建，而且它肯定没子树了，
    //什么都不用干,但要返回NULL了，因为它是某个非空的孩子

    if (arr[*i] == '#')//
    {
        (*i)++;
        return NULL;
    }
    //创建根,但是要tree不为空才创建//因为先序遍历，所以有可能到因为创建过的节点
    if (tree == NULL)//创建
    {
        tree = (struct TreeNode*)malloc(sizeof(struct TreeNode));
        tree->val = arr[*i];
        (*i)++;
        tree->left = tree->right = NULL;//这样才可以递归去创建，不然为随机值不好弄
        tree->left = TreeCreat(tree->left, arr, i);
        tree->right = TreeCreat(tree->right, arr, i);
    }
    return tree;//左右都安排好了就直接返回了
}
int main() {
    //首先先创建这个数组
    char arr[100] = { 0 };
    scanf("%s", arr);
    //创建树
    struct TreeNode* tree;
    tree = NULL;
    //主要函数
    //因为要把数组的值打入创建树//所以还要传下标
    int i = 0;
    tree = TreeCreat(tree, arr, &i);
    PreOrderTree(tree);
    return 0;
}