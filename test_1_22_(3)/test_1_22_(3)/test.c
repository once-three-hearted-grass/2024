#define _CRT_SECURE_NO_WARNINGS 1
//求Sn = a + aa + aaa + aaaa + aaaaa的前5项之和，其中a是一个数字，
//例如：2 + 22 + 222 + 2222 + 22222
#include<stdio.h>
#include<math.h>
int main()
{
	int n = 0;
	while (scanf("%d", n) != EOF)
	{
		int i = 0;
		int get = n;
		int sum = 0;
		for (i = 0; i < 5; i++)
		{
			if (i > 0)
			{
				get = get + get * pow(10, i);
			}
			sum += get;
		}
	printf("%d+%d%d+%d%d%d+%d%d%d%d+%d%d%d%d%d=%d", n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, sum);
	}
	
	return 0;
}