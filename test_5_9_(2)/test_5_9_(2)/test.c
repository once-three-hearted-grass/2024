#define _CRT_SECURE_NO_WARNINGS 1
//顺序表 数据调整
//已知顺序表L中的数据元素类型为int。设计算法将其调整为左右两部分，左边的元素（即排在前面的）均为奇数，
//右边所有元素（即排在后面的）均为偶数，并要求算法的时间复杂度为O(n), 空间复杂度为O（1）。
//函数原型如下：
//相关定义如下：
typedef int ElemType;
struct _seqlist {
    ElemType  elem[24];
    int last;
};
typedef struct _seqlist SeqList;

#include<stdio.h>
//void odd_even(SeqList* L) {
//    //还是两个指针，一个left，一个right，left++，right--,直到遇到了right为奇数，left为偶数，这样的话，就交换，交换再++
//    int left = 0;
//    int right = L->last;
//    while (left < right)
//    {
//        while (left<L->last)//不能越界
//        {
//            if (L->elem[left] % 2 == 0)
//            {
//                break;
//            }
//            else//left为奇数
//            {
//                left++;
//            }
//        }
//        while (right>0)//不能越界
//        {
//            if (L->elem[right] % 2 == 1)
//            {
//                break;
//            }
//            else//right为偶数
//            {
//                right--;
//            }
//        }
//        //走到这里，说明left为偶数，right为奇数,或者马上越界了，如果马上越界的话，left是等于right的，所以没有关系，继续交换就是
//        if (left < right)//满足这个绝对可以交换
//        {
//            ElemType tmp = L->elem[left];
//            L->elem[left] = L->elem[right];
//            L->elem[right] = tmp;
//            left++;
//            right--;
//        }//
//    }
//}

void odd_even(SeqList* L) {//法二：只用一个循环
    //还是两个指针，一个left，一个right，left++，right--,直到遇到了right为奇数，left为偶数，这样的话，就交换，交换再++
    int left = 0;
    int right = L->last;
    while (left < right)
    {
        //如果left为偶数，right为奇数就交换
        if (L->elem[left] % 2 == 0 && L->elem[right] % 2 == 1)
        {
            ElemType tmp = L->elem[left];
            L->elem[left] = L->elem[right];
            L->elem[right] = tmp;
            left++;
            right--;
        }
        else if (L->elem[left] % 2 == 1)
            //有一个或一个不满足交换。就前进或后退
        {
            left++;
        }
        else if (L->elem[right] % 2 == 0)
        {
            right--;
        }
    }
}
int main()
{
    int arr[] = { 1,2,3 };
    SeqList a = { { 417,354,941,774,204,956,176,814,230,40,310,743,612,407,99,33,763,98,491,994,764,451,426,578 } ,23};//初始化要在定义的时候才行
    odd_even(&a);
	return 0;
}