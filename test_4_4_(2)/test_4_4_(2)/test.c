#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>
int main()
{
	char arr1[] = "abcdefg";
	char arr2[] = "abcdg";
	int ret = memcmp(arr1, arr2, 5);
	if (ret == 0)
	{
		printf("arr1=arr2\n");
	}
	else if(ret<0)
	{
		printf("arr1<arr2\n");
	}
	else
	{
		printf("arr1>arr2\n");
	}
	return 0;
}
#if 0
int main()
{
	int arr1[20] = { 1,2,3,4,5,6,7,8,9 };
	memset(arr1, 0, sizeof(arr1));
	for (int i = 0; i < 9; i++)
	{
		printf("%d ", arr1[i]);
	}
	return 0;
}
int main()
{
	char arr[] = "asjhasdjhifsd";
	memset(arr, 'x', 10);//将前十个字节也是前十个字符设置为'x'
	printf(arr);
	return 0;
}
int main()
{
	int arr1[20] = { 1,2,3,4,5,6,7,8,9 };
	memmove(arr1, arr1+2, 5 * sizeof(int));//将3，4，5，6，7拷贝到1，2，3，4，5上
	for (int i = 0; i < 9; i++)
	{
		printf("%d ", arr1[i]);
	}
	return 0;
}
//int main()
//{
//	int arr1[20] = { 1,2,3,4,5,6,7,8,9 };
//	int arr2[] = {1,1,1,1,1};
//	memcpy(arr1, arr2, 5 * sizeof(int));
//	for (int i = 0; i < 9; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}
int main()
{
	FILE* pf = fopen("test.txt", "r");
	if (pf == NULL)
	{
		printf("%s\n", strerror(errno));
	}
	return 0;
}
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		perror("pf");
//	}
//	return 0;
//}

int main()
{
	//打印0~10的错误码的错误
	for (int i = 0; i <= 10; i++)
	{
		printf("%s\n", strerror(i));
	}
	return 0;
}
#endif