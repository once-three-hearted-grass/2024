#define _CRT_SECURE_NO_WARNINGS 1
//使用联合体的知识，写一个函数判断当前机器是大端还是小端，如果是小端返回1，如果是大端返回0.
#include<stdio.h>
int big_or_small()
{
	union Un
	{
		int i;
		char c;
	};
	union Un un = { 0 };
	un.i = 1;
	//01 00 00 00
	return un.c;
}
int main()
{
	int ret = big_or_small();
	if (ret == 1)
	{
		printf("小端\n");
	}
	else
	{
		printf("大端\n");
	}
	return 0;
}