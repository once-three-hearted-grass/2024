#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

struct Date
{
	int _year;
	int _month;
	int _day;
	Date(int year=0,int month=0,int day=0)
		:_year(year)
		,_month(month)
		,_day(day)
	{}
	~Date()
	{
		cout << "~Date()" << endl;
	}
};

//void func()
//{
//	my_ptr<Date> p(new Date);
//	//throw 1;
//}

//int main()
//{
//	try
//	{
//		func();
//	}
//	catch (...)
//	{
//		cout << "ssss" << endl;
//	}
//	return 0;
//}

template<class T>
struct del
{
	void operator()(T* d)
	{
		delete[]d;
	}
};

#include<memory>
//int main()
//{
//	/*auto_ptr<Date> p1(new Date);
//	auto_ptr<Date> p2(p1);
//	p2->_day++;
//	p1->_day++;*/
//	/*unique_ptr<Date> p1(new Date);
//	unique_ptr<Date> p2(p1);*/
//	/*shared_ptr<Date> p1(new Date);
//	shared_ptr<Date> p2(p1);*/
//	/*shared_ptr<Date> p1(new Date[5], [](Date* p) {delete[]p; });*/
//
//	/*shared_ptr<Date> p1=new Date;
//	shared_ptr<Date> p2=p1;
//	shared_ptr<Date> p3(p1);
//	cout << p1.use_count() << endl;*/
//
//	shared_ptr<Date> p = make_shared<Date>(1, 1, 1);
//
//	return 0;
//}

#include"shared_ptr.h"
//int main()
//{
//	bit::my_ptr<Date> p1(new Date[5], [](Date* p) {delete[]p; });
//	bit::my_ptr<Date> p2(p1);
//	auto del = [](Date* p) { delete p; };
//	/*bit::my_ptr<Date> p3(new Date, del); */
//	/*p3 = p2;*/
//
//	return 0;
//}

class fun
{
public:
	fun()
	{

	}
	~fun()
	{
		cout << "~func()" << endl;
	}
	bit::weak_ptr<fun> next;
	bit::weak_ptr<fun> prev;
};

int main()
{
	/*bit::my_ptr<fun> p1(new fun);
	bit::my_ptr<fun> p2(new fun);
	p1->next = p2;
	p2->prev = p1;*/

	shared_ptr<Date> s1;
	weak_ptr<Date> s2;
	{
		shared_ptr<Date> s3(new Date);
		s2 = s3;
		cout << s2.expired() << endl;
		s1 = s2.lock();
	}
	cout << s2.expired() << endl;

	return 0;
}