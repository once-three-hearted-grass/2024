#pragma once
#include<functional>
using namespace std;

namespace bit
{
	template<class T>
	class my_ptr
	{
	public:

		//再添加删除器
		/*my_ptr(T* ptr=nullptr)
			:_ptr(ptr)
			,_count(new int(1))
		{
			if (ptr == nullptr)
			{
				*_count = 0;
			}
		}*/
		my_ptr(T* ptr = nullptr)
			:_ptr(ptr)
			, _count(new int(1))//会自动走初始化列表，没写的也会走初始化列表
		{
			if (ptr == nullptr)
			{
				*_count = 0;
			}
		}

		//要添加删除器的时候，必须还要留一个原来的一个参数的构造函数，因为构造是可以传一个参数和两个参数的（包含删除器）
		//如果总共就写一个构造函数，就是下面这个构造函数，那么就传一个参数的时候，d的类型无法推导，是不会走这个构造函数的，
		//相反还会走拷贝构造
		template<class d>//自己推类型的
		my_ptr(T* ptr, d del)
			:_ptr(ptr)
			,_del(del)
			, _count(new int(1))
		{
			if (ptr == nullptr)
			{
				*_count = 0;
			}
		}

		my_ptr(const my_ptr<T>& ptr)
		{
			_ptr = ptr._ptr;
			_count = ptr._count;
			++*(_count);
		}

		void release()//因为this指针一直传
		{
			--(*_count);
			if ((*_count) == 0)
			{
				_del(_ptr);//会调用_ptr的析构函数
				_ptr = nullptr;
			}
		}

		my_ptr&operator=(const my_ptr& ptr)
		{
			//要先对以前的资源处理
			release();
			if (_ptr != ptr._ptr)
			{
				_ptr = ptr._ptr;
				_count = ptr._count;
				++*(_count);
			}
			return (*this);
		}

		~my_ptr()
		{
			release();
		}

		T* operator->()
		{
			return _ptr;
		}

		T& operator*()
		{
			return *_ptr;
		}

		int use_count()
		{
			return *_count;
		}

		T* get()const
		//get是获得底层的那个指针
		{
			return _ptr;
		}

	private:
		T* _ptr;   
		int* _count;
		//接受删除器
		function<void(T* p)> _del = [](T* p) {delete p; };
	};

	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr()
		{
			;
		}
		weak_ptr(const my_ptr<T>&ptr)
			:_ptr(ptr.get())
		{}

		weak_ptr& operator=(const my_ptr<T>& ptr)
		{
			_ptr = ptr.get();
			return *this;
		}

	private:
		T* _ptr;
	};
}













