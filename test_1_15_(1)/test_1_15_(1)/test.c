#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int x = 5, y = 7;
void swap()
{
	//这里使用x，y就只能使用全局变量的x，y
	//所以x为5，y为7
	int z;
	z = x;
	x = y;
	y = z;
} 
int main()
{
	int x = 3, y = 8;
	swap();
	//这里的x，y的使用就只能是局部变量的，所以x为3，y为8
	printf("%d,%d\n",x, y);
	return 0;
}