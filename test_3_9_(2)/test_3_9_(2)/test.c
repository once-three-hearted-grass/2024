#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int* findErrorNums(int* nums, int numsSize, int* returnSize) {
    *returnSize = 2;
    int* ret = (int*)malloc(sizeof(int) * (*returnSize));
    if (ret == NULL)
    {
        return NULL;
    }
    // //法一：先排序，在遍历找相同元素，在相邻元素相减得另一个
    // //冒泡排序
    // for(int i=0;i<numsSize-1;i++)
    // {
    //     int flag=0;
    //     for(int j=0;j<numsSize-1-i;j++)
    //     {
    //         if(nums[j]>nums[j+1])
    //         {
    //             flag=1;
    //             int tmp=nums[j];
    //             nums[j]=nums[j+1];
    //             nums[j+1]=tmp;
    //         }
    //     }
    //     if(flag==0)
    //     {
    //         break;
    //     }
    // }
    // for(int i=0;i<numsSize-1;i++)
    // {
    //     if(nums[i]==nums[i+1])
    //     {
    //         ret[0]=nums[i];
    //     }
    //     if(nums[numsSize-1]!=numsSize)//尾部不对
    //     {
    //         ret[1]=numsSize;
    //     }
    //     else if(nums[0]!=1)//头部不对
    //     {
    //         ret[1]=1;
    //     }
    //     else
    //     {
    //         if(nums[i+1]-nums[i]>1)
    //     {
    //         ret[1]=nums[i]+1;
    //     }
    //     }
    // }
    //法二：位运算符，法一时间复杂度太长了,将这个数组看为两个单身狗，依次^i就可以了
    int sum = 0;
    for (int i = 0; i < numsSize; i++)
    {
        sum = sum ^ (i + 1) ^ nums[i];
    }
    //找出sum二进制为一的，就是两个数的区别
    int j = 0;
    while (((1 << j) & sum) == 0)//sum为1的，则&（1<<J）为1吗，不是，只能说不等于0而已罢了//&的优先级低于<<和！=，所以要打括号
    {
        j++;
    }
    int sum1 = 0;
    int sum2 = 0;
    for (int i = 0; i < numsSize; i++)
    {
        if (((1 << j) & nums[i]) == 0)//&的优先级低于<<和！=，所以要打括号
        {
            sum1 = sum1 ^ nums[i];
        }
        else
        {
            sum2 = sum2 ^ nums[i];
        }
        if (((1 << j) & (i + 1)) == 0)
        {
            sum1 = sum1 ^ (i + 1);
        }
        else
        {
            sum2 = sum2 ^ (i + 1);
        }
    }
    //这样num1与num2就是我们所求的两个数字，加上所有（i+1）就看为了两个单身狗问题
    //到底谁是谁呢，只需要挨个遍历就可以了
    for (int i = 0; i < numsSize; i++)
    {
        if (sum1 == nums[i])//sum1为重复数字
        {
            ret[0] = sum1;
            ret[1] = sum2;
        }
        else if (sum2 == nums[i])
        {
            ret[0] = sum2;
            ret[1] = sum1;
        }
    }
    return ret;
}
int main()
{
    int arr[] = {1,2,2,4};
    int sz = sizeof(arr) / sizeof(arr[0]);
    int ret = 0;
    findErrorNums(arr, sz, &ret);
	return 0;
}