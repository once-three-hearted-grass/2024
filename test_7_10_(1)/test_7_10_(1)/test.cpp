#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//namespace s
//{
//	int a = 0;
//}
//int a = 1;
//using namespace s;
//
//int main()
//{
//	printf("%d", a);
//	return 0;
//}

//void swap(int a, int b)
//{
//	cout << "void swap(int a, int b)" << endl;
//}
//
//void swap(double a, double b)
//{
//	cout << "void swap(double a, double b)" << endl;
//}
//
//
//int main()
//{
//	int a = 0, b = 0;
//	double c = 0.2, d = 4.5;
//	swap(a, b);
//	swap(c, d);
//	return 0;
//}

//void swap(int a, char b)
//{
//	cout << "void swap(int a, char b)" << endl;
//}
//
//void swap(char a, int b)
//{
//	cout << "void swap(char a, int b)" << endl;
//}
//
//
//int main()
//{
//	swap(1, '1');
//	swap('1', 1);
//	return 0;
//}

//void swap(int a, char b)
//{
//	cout << "void swap(int a, char b)" << endl;
//}
//
//int swap(int a, char b)
//{
//	cout << "int swap(int a, char b)" << endl;
//}
//
//
//int main()
//{
//	swap(1, '1');
//	swap('1', 1);
//	return 0;
//}

//void swap(int a, int b)
//{
//	cout << "void swap(int a, int b)" << endl;
//}
//
//void swap(int a, int b,int c)
//{
//	cout << "void swap(int a, int b,int c)" << endl;
//}
//
//
//int main()
//{
//	swap(1, 1);
//	swap(1, 1, 1);
//	return 0;
//}

//void swap()
//{
//	cout << "void swap()" << endl;
//}
//
//void swap(int a=1)
//{
//	cout << "void swap(int a=1)" << endl;
//}
//
//int main()
//{
//	swap();
//	return 0;
//}

//namespace p1
//{
//	void swap(char a)
//	{
//		cout << "void swap(char a)" << endl;
//	}
//}
//
//namespace p2
//{
//	void swap(int a)
//	{
//		cout << "void swap(int a)" << endl;
//	}
//}
//
//using namespace p1;
//using namespace p2;
//
//int main()
//{
//	swap(1);
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int& c = a;
//	int b = 1;
//	 c = b;
//	b++;
//	printf("b=%d a=%d c=%d", b, a,c);
//	return 0;
//}

//int main()
//{
//	const int a = 10;
//	//int& ra = a; // 该语句编译时会出错，a为常量
//	//因为这个是权限的放大，本来是不能改变a空间的值的，现在可以了，这就是权限的放大，这是不行的
//	const int& ra = a;//这个就可以，因为这个是权限的平移
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	const int& ra = a;
//	return 0;
//}

//typedef struct ListNode
//{
//	int val;
//	struct ListNode* next;
//}LTNode, * PNode;
//
//void ListPushBack(PNode& phead, int x)
//{
//	if (phead == NULL)
//	{
//		// phead = newnode;
//	}
//}

//int main()
//{
//	const int a = 0;
//	const int* p1 = &a;
//	int* const p2 = &a;
//	int* p3 = &a;
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	const int* p1 = &a;
//	int* const p2 = &a;
//	int* p3 = &a;
//	return 0;
//}

//int main()
//{
//	int& p1 = 10;
//	const int& p2 = 10;
//	int& p3 = 10+3;
//	const int& p3 = 10+3;
//	return 0;
//}

//int main()
//{
//	double a = 10.34;
//	int& p1 = (int)a;
//	const int& p2 = (int)a;
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int& p1 = a;
//	int* p2 = &a;
//	cout << sizeof(p1) << endl;
//	return 0;
//}
//int main()
//{
//	int* p = NULL;
//	int& p2 = *p;
//	//cout << *p << endl;
//	return 0;
//}

//int main()
//{
//	int* p = NULL;
//	int&& p2 = *p;
//	return 0;
//}
//
//inline int add(int a, int b)
//{
//	return a + b;
//}
//int main()
//{
//	add(1, 2);
//	return 0;
//}

//int main()
//{
//	auto a = 10;
//	return 0;
//}
//
//typedef char* pstring;
//int main()
//{
//	const pstring p1; // 编译成功还是失败？
//	const pstring* p2; // 编译成功还是失败？
//	return 0;
//}

//void TestAuto()
//{
//	auto a = 1, b = 2;
//	auto c = 3, d = 4.0; // 该行代码会编译失败，因为c和d的初始化表达式类型不同
//}
//int main()
//{
//	int a = 0;
//	auto p = &a;
//	cout << typeid(a).name() << endl;
//	cout << typeid(p).name() << endl;
//	return 0;
//}

//void TestAuto(auto a)
//{}

//int main()
//{
//	auto arr[] = { 1,2,3 };
//	return 0;
//}

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8 };
//	for (auto e : arr)
//	{
//		cout << e << ' ';
//	}
//	cout << endl;
//	for (auto& e : arr)
//	{
//		e *= 2;
//	}
//	for (auto e : arr)
//	{
//		cout << e << ' ';
//	}
//	return 0;
//}


void f(int a)
{
	cout << "void f(int a)" << endl;
}
void f(int* a)
{
	cout << "void f(int* a)" << endl;
}
int main()
{
	f(NULL);//
	return 0;
}