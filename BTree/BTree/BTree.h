#pragma once
#include<stdio.h>
#include<stdlib.h>
typedef struct BTreeNode
{
	char data;
	struct BTreeNode* left;
	struct BTreeNode* right;
	int flag;//实现非递归后序遍历
}Tree;

//边输入边创建
void CreatTree1(Tree** T);

//先序遍历
void PreTree(Tree* T);

//先全输入再创建
void CreatTree2(Tree** T,char*arr,int*i);

//层次遍历
void LevelTraversal(Tree* T);

//非递归先序遍历
void PreOrder1(Tree* T);

//非递归中序遍历
void InOrder2(Tree* T);

//非递归先序遍历
void PreOrder2(Tree* T);

//非递归后续遍历
void PostOrder(Tree* t);
