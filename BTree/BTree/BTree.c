#define _CRT_SECURE_NO_WARNINGS 1
#include"BTree.h"

void CreatTree1(Tree** T)
{
	//我们这里用边输入边创建二叉树的方式创建二叉树
	//我们这里创建二叉树的方式都是先序遍历，#代表NULL
	char ch;
	scanf("%c", &ch);
	getchar();//因为有换行符
	if (ch == '#')
	{
		*T = NULL;
	}
	else
	{
		//就要创建一个节点
		Tree* newnode = (Tree*)malloc(sizeof(Tree));
		if (newnode == NULL)
		{
			perror("CreatTree1:malloc:");
			return;
		}
		*T = newnode;
		(*T)->data = ch;
		//在创建左子树
		CreatTree1(&(*T)->left);
		//右子树
		CreatTree1(&(*T)->right);
	}
}

//先序遍历
void PreTree(Tree* T)
{
	if (T == NULL)
	{
		return;
	}
	printf("%c", T->data);
	PreTree(T->left);
	PreTree(T->right);
}

//先全输入再创建
void CreatTree2(Tree** T, char* arr, int* i)//方法一还是太麻烦了，要不先把先序序列输入到数组，再创建
{
	//为什么要用#呢，如果不用#的话，那么只给一个先序数列是无法确定树的
	if (arr[*i] == '#')
	{
		(*i)++;
		return;
	}
	else
	{
		//创建节点
		Tree* newnode = (Tree*)malloc(sizeof(Tree));
		if (newnode == NULL)
		{
			perror("CreatTree1:malloc:");
			return;
		}
		*T = newnode;
		(*T)->data = arr[*i];
		(*i)++;
		(*T)->left = NULL;
		(*T)->right = NULL;
		(*T)->flag = 0;//实现非递归后序遍历
		//接下来创建左子树，右子树
		CreatTree2(&(*T)->left,arr,i);
		CreatTree2(&(*T)->right,arr,i);
	}
}
#include"Queue.h"
//层次遍历
void LevelTraversal(Tree* T)
{
	//要实现层次遍历的话，就要用栈
	Queue q;
	QueueInit(&q);
	//先将根入栈
	if (T)
	{
		QueuePush(&q, T);
	}
	else
	{
		return;
	}
	while (!QueueEmpty(&q))
	{
		//先取出一个
		Tree*tmp=QueueFront(&q);
		printf("%c", tmp->data);
		QueuePop(&q);
		//再依次将左右孩子入队列
		if (tmp->left)
		{
			QueuePush(&q, tmp->left);
		}
		if (tmp->right)
		{
			QueuePush(&q, tmp->right);
		}
	}
	QueueDestroy(&q);
}

#include"Stack.h"
//非递归先序遍历
void PreOrder1(Tree* T)
{
	//方法一：先入根，再出栈，出栈就先入右孩子，再入左孩子
	Stack s;
	StackInit(&s);
	if (T)
	{
		StackPush(&s, T);
	}
	else
	{
		return;
	}
	while (!StackEmpty(&s))
	{
		//先出栈
		Tree* tmp = StackTop(&s);
		StackPop(&s);
		printf("%c", tmp->data);
		//再入左右孩子
		if (tmp->right)
		{
			StackPush(&s,tmp->right);
		}
		if (tmp->left)
		{
			StackPush(&s,tmp->left);
		}
	}
	StackDestroy(&s);
}

//非递归先序遍历
void PreOrder2(Tree* T)
{
	//这个方法也是教材上的方法
	//先将根入栈
	//再把根的左孩子一直入栈，直到左孩子为空
	//再出栈，出栈的时候再把这个出栈的数据的右孩子入栈
	//这个右孩子入栈的话，再像以前一样，循环入左孩子，就这样一直循环

	//就这个过程就相当于沿着这棵树挨着走
	//那么先序遍历就相当于第一次遇到，也就是入栈的时候，中序遍历就相当于第二次遇到，也就是出栈的时候，因为总共只有两次遇到的机会，所以后续遍历先不管嘛
	Stack s;
	StackInit(&s);
	if (T)
	{
		StackPush(&s, T);
		printf("%c", T->data);
	}
	else
	{
		return;
	}
	//因为要一直访问左孩子，所以要有一个指向的指针
	Tree* node = T->left;
	while (node||!StackEmpty(&s))//这个循环条件不能这样，因为就算栈为空，如果node不为空说明还有入栈的东西，因为根节点的右孩子不为空，栈为空退出来了
		//就有问题
	{
		//如果左孩子不为空，就入栈，为空退出
		while (node)
		{
			StackPush(&s, node);
			printf("%c", node->data);//因为先序遍历就是在入栈的时候也就是第一次遇到的时候访问
			node = node->left;
		}
		//走到这里，说明node作为上一个节点的左孩子为空了，这时候就应该出栈了
		Tree* tmp = StackTop(&s);
		StackPop(&s);
		//这时候又要把tmp的右孩子入栈了，然后就是一直入左孩子，所以要改变node的指向
		node = tmp->right;
	}
}

//非递归中序遍历
void InOrder2(Tree* T)
{
	//这个方法也是教材上的方法
	//先将根入栈
	//再把根的左孩子一直入栈，直到左孩子为空
	//再出栈，出栈的时候再把这个出栈的数据的右孩子入栈
	//这个右孩子入栈的话，再像以前一样，循环入左孩子，就这样一直循环

	//就这个过程就相当于沿着这棵树挨着走
	//那么先序遍历就相当于第一次遇到，也就是入栈的时候，中序遍历就相当于第二次遇到，也就是出栈的时候，因为总共只有两次遇到的机会，所以后续遍历先不管嘛
	Stack s;
	StackInit(&s);
	if (T)
	{
		StackPush(&s, T);
	}
	else
	{
		return;
	}
	//因为要一直访问左孩子，所以要有一个指向的指针
	Tree* node = T->left;
	while (node || !StackEmpty(&s))//这个循环条件不能这样，因为就算栈为空，如果node不为空说明还有入栈的东西，因为根节点的右孩子不为空，栈为空退出来了
		//就有问题
	{
		//如果左孩子不为空，就入栈，为空退出
		while (node)
		{
			StackPush(&s, node);
			node = node->left;
		}
		//走到这里，说明node作为上一个节点的左孩子为空了，这时候就应该出栈了
		Tree* tmp = StackTop(&s);
		printf("%c", tmp->data);//因为中序遍历就是在入栈的时候也就是第二次遇到的时候访问
		StackPop(&s);
		//这时候又要把tmp的右孩子入栈了，然后就是一直入左孩子，所以要改变node的指向
		node = tmp->right;
	}
}

//非递归后续遍历
void PostOrder(Tree* t)
{
	Tree* node = t;
	Stack s;
	StackInit(&s);
	while (node||!StackEmpty(&s))
	{
		while (node)
		{
			StackPush(&s, node);
			node = node->left;
		}
		//当node为空时，就取栈顶元素，看栈顶元素是否有右孩子，如果有右孩子的话，就把
		//右孩子入栈，node变右孩子的左孩子，，，，如果没有右孩子的话，就可以直接出栈了，出栈就访问，这也相当于三次访问了
		Tree* top = StackTop(&s);
		if (top->right&&top->right->flag==0)//而且右孩子没有被访问
		{
			StackPush(&s, top->right);
			node = top->right->left;
		}
		else
		{
			printf("%c", top->data);
			StackPop(&s);
			top->flag = 1;
		}
	}
	StackDestroy(&s);
}

