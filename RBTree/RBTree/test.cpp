#define _CRT_SECURE_NO_WARNINGS 1

#include"BRTree.h"

int main()
{
	BRTree<int> br;
	//int arr[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15,12,34,67,34 };
	//int arr[] = { 20,10,5,30,40,57,3,2,4,35,25,18,22,23,24,19,18};
	int arr[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16,14 };
	for (auto x : arr)
	{
		br.insert(x);
		//br.PreOrder();
	}
	br.InOrder();
	br.PreOrder();
	cout << br.IsBalance() << endl;
	//检查我们的平衡树的插入是否正常除了先序遍历的方法，我们还可以写一个程序的方法
	return 0;
}