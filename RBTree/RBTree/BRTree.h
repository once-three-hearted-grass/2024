#pragma once
#include<iostream>
#include<assert.h>
using namespace std;

enum color
{
	Red,
	Black
};

//实现AVL树
template<class K>
struct BRNode
{
	BRNode<K>* _left;
	BRNode<K>* _right;
	BRNode<K>* _parent;
	K _key;
	color _color;
	typedef BRNode<K> Node;
	BRNode(const K& key)
		:_key(key)
		, _left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		,_color(Red)
	{}
};

template<class K>
class BRTree
{
public:
	typedef BRNode<K> Node;

	bool insert(const K& key)
	{
		if (_root == nullptr)
		{
			_root = new Node(key);
			_root->_color = Black;
			return true;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}
		cur = new Node(key);
		//但要注意_root==nullptr的时候
		if (key > parent->_key)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}

		//插入完成，开始检查是否平衡
		while (parent&&parent->_color==Red)//红黑树总共有四个特性，
			//左小于根小于右（左跟右），一条路黑相同（黑路同），根为黑（根叶黑），然后就是连续的不为红（不红红）
		//插入只会不红红特性
		{
			//先找到叔叔parent->_color==Red说明一定有爷
			Node* Grandparent = parent->_parent;
			if (parent == Grandparent->_right)
			{
				//叔叔在左边
				Node* uncle = Grandparent->_left;
				//开始讨论叔叔为红还是黑//叔叔为空默认为黑
				if (uncle && uncle->_color == Red)
				{
					//只需要将叔叔父亲变黑，爷爷变红就可以了
					uncle->_color = Black;
					parent->_color = Black;
					Grandparent->_color = Red;
					//然后将Grandparent看为cur继续循环
					cur = Grandparent;
					parent = cur->_parent;
					//当parent为红就继续循环
					//如果为空，说明cur为根，所以直接退出循环变黑，并增加条件parent不为空
				}
				else if (!uncle || uncle->_color == Black)//叔叔为空或者为黑//而且叔叔为黑这种情况只可能是由叔叔为红变过来的，不可能一开始插入就变成这样了
					//因为不满足黑路同，所以cur还有子树，后面还有黑节点
				{
					//接下来就来看是什么类型的旋转
					if (cur == parent->_right)//RR
					{
						RR(Grandparent);
						//然后再让原来的爷爷变红，原来的父亲变黑，原来的父亲就变为爷爷
						Grandparent->_color = Red;
						parent->_color = Black;
						//因为原来的父亲就变为爷爷，爷爷变黑，所以黑色节点每条路还是相同的，反正就是旋转之后就已经满足了红黑树了
						//不需要在继续循环了
						break;
					}
					else//RL
					{
						LL(parent);
						RR(Grandparent);
						//cur变黑，Grandparent变红
						cur->_color = Black;
						Grandparent->_color = Red;
						break;
					}
				}
			}
			else
			{
				Node* uncle = Grandparent->_right;
				//开始讨论叔叔为红还是黑//叔叔为空默认为黑
				if (uncle && uncle->_color == Red)
				{
					//只需要将叔叔父亲变黑，爷爷变红就可以了
					uncle->_color = Black;
					parent->_color = Black;
					Grandparent->_color = Red;
					//然后将Grandparent看为cur继续循环
					cur = Grandparent;
					parent = cur->_parent;
					//当parent为红就继续循环
					//如果为空，说明cur为根，所以直接退出循环变黑，并增加条件parent不为空
				}
				else if (!uncle || uncle->_color == Black)//叔叔为空或者为黑//而且叔叔为黑这种情况只可能是由叔叔为红变过来的，不可能一开始插入就变成这样了
					//因为不满足黑路同，所以cur还有子树，后面还有黑节点
				{
					//接下来就来看是什么类型的旋转
					if (cur == parent->_left)//LL
					{
						LL(Grandparent);
						//然后再让原来的爷爷变红，原来的父亲变黑，原来的父亲就变为爷爷
						Grandparent->_color = Red;
						parent->_color = Black;
						//因为原来的父亲就变为爷爷，爷爷变黑，所以黑色节点每条路还是相同的，反正就是旋转之后就已经满足了红黑树了
						//不需要在继续循环了
						break;
					}
					else//LR
					{
						RR(parent);
						LL(Grandparent);
						//cur变黑，Grandparent变红
						cur->_color = Black;
						Grandparent->_color = Red;
						break;
					}
				}
			}
		}
		_root->_color = Black;
		return true;
	}

	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	void PreOrder()
	{
		_PreOrder(_root);
		cout << endl;
	}

	bool IsBalance()
	{
		return _IsBalance(_root);
	}
	//搜索某个数据
	Node* Search(const K& key)
	{
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return cur;
			}
		}
		return nullptr;
	}

	//删除某个数据
	bool Erase(const K& key)
	{
		if (_root == nullptr)
		{
			return false;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				//找到了要删除的数据
				//删除的话就分为三种//
				//第一种就是没有孩子直接删除
				//第二种就是有一个孩子的话就直接连在父亲的后面
				//第三种就是有两个孩子
				//其中第一种和第二种可以合并。因为可以把没有孩子当做空指针的孩子
				if (cur->_left == nullptr)
				{
					if (parent == nullptr)//说明删的是根
					{
						_root = cur->_right;
						delete cur;
						return true;
					}
					//先看cur在parent的左还是右
					if (cur->_key < parent->_key)
					{
						parent->_left = cur->_right;
					}
					else
					{
						parent->_right = cur->_right;
					}
					delete cur;
					return true;
				}
				else if (cur->_right == nullptr)
				{
					if (parent == nullptr)//说明删的是根
					{
						_root = cur->_left;
						delete cur;
						return true;
					}
					if (cur->_key < parent->_key)
					{
						parent->_left = cur->_left;
					}
					else
					{
						parent->_right = cur->_left;
					}
					delete cur;
					return true;
				}
				else//现在左右孩子都不为空
				{
					//如果是头结点，也不用单独考虑，我们这样设计的话
					//我们直接找到右孩子的最小值放在cur，然后删除这个最小值节点就可以了,最小值就是一直往左走就可以了
					Node* min = cur->_right;
					Node* min_parent = cur;
					while (min->_left)
					{
						min_parent = min;
						min = min->_left;
					}
					cur->_key = min->_key;
					//删除min节点
					//因为min的左孩子一定为空，所以很好删除
					if (min_parent == cur)//说明没走
					{
						min_parent->_right = min->_right;
					}
					else
					{
						min_parent->_left = min->_right;
					}
					delete min;
					return true;
				}
			}
		}
		return false;
	}

private:
	void RR(Node* parent)
	{
		Node* parent_parent = parent->_parent;
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		//开始链接左右孩子
		parent->_right = subRL;
		subR->_left = parent;
		if (parent_parent)
		{
			if (parent_parent->_left == parent)
			{
				parent_parent->_left = subR;
			}
			else
			{
				parent_parent->_right = subR;
			}
		}
		else//说明parent_parent是nullptr，parent就是_root了
		{
			_root = subR;
		}
		//开始链接父亲节点
		if (subRL)//防止RL的时候调用为空，因为RL的时候调用的parent不是2或-2
			subRL->_parent = parent;
		parent->_parent = subR;
		subR->_parent = parent_parent;
	}

	void LL(Node* parent)//就是在RR的核心逻辑上right改left，left改right
	{
		Node* parent_parent = parent->_parent;
		Node* subR = parent->_left;
		Node* subRL = subR->_right;
		//开始链接左右孩子
		parent->_left = subRL;
		subR->_right = parent;
		if (parent_parent)
		{
			if (parent_parent->_left == parent)
			{
				parent_parent->_left = subR;
			}
			else
			{
				parent_parent->_right = subR;
			}
		}
		else//说明parent_parent是nullptr，parent就是_root了
		{
			_root = subR;
		}
		//开始链接父亲节点
		if (subRL)
			subRL->_parent = parent;
		parent->_parent = subR;
		subR->_parent = parent_parent;
	}

	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_InOrder(root->_left);
		cout << root->_key << " ";
		_InOrder(root->_right);
	}

	void _PreOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		cout << root->_key << " ";
		_PreOrder(root->_left);
		_PreOrder(root->_right);
	}

	bool _Judge1(Node* root, int min, int max)
	{
		if (root == nullptr)
		{
			return true;
		}
		if (!(root->_key >= min && root->_key <= max))
		{
			return false;
		}
		return _Judge1(root->_left, min, root->_key) && _Judge1(root->_right, root->_key, max);//往左走min保留，往右走max保留
	}

	bool Judge1(Node* root)
	{
		if (root == nullptr)
		{
			return true;
		}
		//这个就是判断一个树是不是搜索树罢了
		//直接先序遍历，传入区间进去，值不满足上个父亲传下来的区间就是false
		return _Judge1(root->_left,INT_MIN, root->_key) && _Judge1(root->_right, root->_key, INT_MAX);//这里的区间很难处理，所以我们
		//定义一个很大的数，很很小的数
	}

	//bool Judge24(Node* root, int BlackNum, int num)
	//{
	//	//我们用一个BlackNum来记录根节点到此节点的黑色节点数量
	//	//如何检验黑路同呢
	//	//对于一个节点来讲，它到任意叶子的黑色数量相同，然后左右子树也是这样，终止条件就是为null时，看数量是不是num
	//	if (root == nullptr)
	//	{
	//		if (num != BlackNum)
	//		{
	//			return false;
	//		}
	//		return true;
	//	}
	//	if (root->_color == Black)
	//	{
	//		BlackNum++;
	//	}
	//	return Judge24(root->_left, BlackNum, num) && Judge24(root->_right, BlackNum, num);
	//}
	// 或许我们可以这样理解，我们来实现一个先序遍历，一边遍历，一遍统计到此节点的黑色数量，到了空节点就判断一下，然后只要有一个地方不满足，就总的是
	// false了，所以先序遍历间用的是&&
	//然后我们在增加不红红的判断，就是在遍历的过程中看看如果自己为红，再看看父亲为不为红

	bool Judge24(Node* root, int BlackNum, int num)
	{
		//我们用一个BlackNum来记录根节点到此节点的黑色节点数量
		//如何检验黑路同呢
		//对于一个节点来讲，它到任意叶子的黑色数量相同，然后左右子树也是这样，终止条件就是为null时，看数量是不是num
		if (root == nullptr)
		{
			if (num != BlackNum)
			{
				return false;
			}
			return true;
		}
		if (root->_color == Black)
		{
			BlackNum++;
		}
		else
		{
			if (root->_parent && root->_parent->_color == Red)
			{
				return false;
			}
		}
		return Judge24(root->_left, BlackNum, num) && Judge24(root->_right, BlackNum, num);
	}

	bool _IsBalance(Node* root)//这个函数来判断是不是红黑树
		//红黑树总共有四个特性，挨个挨个判断就可以了
		//左小于根小于右（左跟右），一条路黑相同（黑路同），根为黑（根叶黑），然后就是连续的不为红（不红红）
	{
		//根叶黑
		if (_root->_color == Red)
		{
			return false;
		}

		//先统计好根到随便一个叶子结点的黑色节点数目
		int num = 0;
		Node * cur = _root;
		while (cur)
		{
			if (cur->_color == Black)
			{
				num++;
			}
			cur = cur->_left;
		}

		return Judge24(root,0,num)&& Judge1(root);
	}

	Node* _root = nullptr;
};
