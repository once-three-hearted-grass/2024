#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int convertInteger(int A, int B) {
    int C = A ^ B;
    //相同取0；相异取1
    int count = 0;
    for (int i = 0; i < 32; i++)
    {
        if ((C >> i) & 1 == 1)
        {
            count++;
        }
    }
    return count;
}

int main()
{
	int A = 0;
	int B = 0;
    scanf("%d%d", &A, &B);
    printf("只需要改变%d个二进制位就可以把%d变为%d", convertInteger(A, B), A, B);
	return 0;
}