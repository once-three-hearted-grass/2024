#define _CRT_SECURE_NO_WARNINGS 1
//实现一个函数，可以左旋字符串中的k个字符。
//例如
//ABCD左旋一个字符得到BCDA
//ABCD左旋两个字符得到CDAB
#include<stdio.h>
#include<string.h>

//void Spin(char* arr1, int k, int sz)
//{
//	//法一：创建临时数组
//  k=k%sz;
//	char arr2[100] = { 0 };
//	for (int i = 0; i < sz; i++)
//	{
//		arr2[i] = arr1[i];
//	}
//	for (int i = 0; i < sz; i++)
//	{
//		arr1[i] = arr2[(i+k)%sz];
//	}
//}



//void Spin(char* arr1, int k, int sz)
//{
//	//法二：直接一个一个转
//	k = k % sz;
//	int i = 0;
//	for(i=0;i<k;i++)//左移一步这个动作重复k次
//	{
//		//for (int j = 0; j < sz-1; j++)
//		//{
//		//	int tmp = arr1[j];
//		//	arr1[j] = arr1[j + 1];
//		//	arr1[j + 1] = tmp;
//		//}//这一步就是左移一个
//		//或者这样
//		int tmp = arr1[0];
//		int j = 0;
//		for (; j < sz - 1; j++)
//		{
//			arr1[j] = arr1[j + 1];
//		}
//		arr1[j] = tmp;
//	}
//}




//void Turn(char *arr,int start,int end)
//{
//	while (start < end)
//	{
//		int tmp = arr[start];
//		arr[start] = arr[end];
//		arr[end] = tmp;
//		start++;
//		end--;
//	}
//}
//void Spin(char* arr, int k, int sz)
//{
//	//法三:三次旋转，旋转整体，旋转两个小的结构
//	Turn(arr,0, sz - 1);
//	Turn(arr, 0, sz-k - 1);
//	Turn(arr,sz-k, sz - 1);
//}




void Spin(char* arr, int k, int sz)
{
	//法四:利用string的函数
	/*strcmp;
	strncat;
	strcat;
	strcpy;*/
	char arr1[100] = { 0 };
	strcpy(arr1, arr + k);//从arr+k开始的字符串全覆盖arr1
	strncat(arr1, arr, k);//拼接前k个，在arr1的后面接上arr的前k个//strcat直接拼接全部
	strcpy(arr, arr1);
}



void Print(char* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%c ", arr[i]);
	}
}


int main()
{
	char arr[100] = { 0 };
	printf("请输入字符串:");
	scanf("%s", arr);
	int sz = strlen(arr);
	int k = 0;
	printf("请输入k的值:");
	scanf("%d",&k);
	Spin(arr, k,sz);
	Print(arr, sz);
	return 0;
}