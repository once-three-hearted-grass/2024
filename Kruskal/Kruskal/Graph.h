#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

//prim算法的话
#define MAX 32326

typedef struct Edge {
	int start;
	int end;
	int weight;
}Edge;

typedef struct Graph
{
	char* vex;
	int** arc;
	int vexNUM;
	int arcNUM;
}Graph;

//初始化图
Graph* InitGraph(int vexnum);

//创造图
void CreatGraph(Graph* G, char* vex,int*arc);

//深度优先遍历图
void DFS(Graph* G, int* visited, int index);

Edge* InitEdge(Graph* G);

//对这个边的数组排序
void SortEdge(Graph* G, Edge* e);

//克鲁斯卡尔算法生成最小生成树
void Kruskal(Graph* G);
