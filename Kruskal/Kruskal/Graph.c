#define _CRT_SECURE_NO_WARNINGS 1
#include"Graph.h"

//初始化图
Graph* InitGraph(int vexnum)
{
	Graph* G = (Graph*)malloc(sizeof(Graph));
	G->vex = (char*)malloc(sizeof(char) * vexnum);
	//arc是一个二级指针数组，每个元素指向一个一维数组，也相当于一个二维数组
	G->arc = (int**)malloc(sizeof(int*) * vexnum);
	G->vexNUM = vexnum;
	G->arcNUM = 0;
	for (int i = 0; i < vexnum; i++)
	{
		G->arc[i] = (int*)malloc(sizeof(int) * vexnum);
	}
	return G;
}

//创造图
void CreatGraph(Graph* G, char* vex, int* arc)//第二个参数传的是顶点，第三个传的是边，强制类型转换而来的
{
	for (int i = 0; i < G->vexNUM; i++)
	{
		G->vex[i] = vex[i];//赋值顶点
		for (int j = 0; j < G->vexNUM; j++)//赋值边
		{
			G->arc[i][j] = arc[i * G->vexNUM + j];
			if (G->arc[i][j] >0&& G->arc[i][j]!=MAX)
			{
				G->arcNUM++;
			}
		}
	}
	G->arcNUM /= 2;//因为是无向图
}

//深度优先遍历图
void DFS(Graph* G, int* visited, int index)//第二个参数是看这个顶点是否被访问了,第二个参数是看从哪个顶点开始访问
{
	//先访问传入的这个顶点
	printf("v%c ", G->vex[index]);
	//标记
	visited[index] = 1;//表示被访问过了
	for (int i = 0; i < G->vexNUM; i++)
	{
		if (G->arc[index][i]>0 && G->arc[index][i] !=MAX&& visited[i] == 0)//有这条边并且这个顶点未被访问
		{
			//那就沿着这条边去访问这个顶点，这个顶点的访问都一样的
			DFS(G, visited, i);
		}
	}
}


Edge* InitEdge(Graph* G)
{
	//这个数组的容量就是边的个数
	Edge* e = (Edge*)malloc(sizeof(Edge) * (G->arcNUM));
	int index = 0;//记录e的下标
	for (int i = 0; i < G->vexNUM; i++)//在赋值的时候就只需要赋值数组右上部分的就可以了
	{
		for (int j = i + 1; j < G->vexNUM; j++)
		{
			if (G->arc[i][j] != MAX)
			{
				e[index].start = i;
				e[index].end = j;
				e[index].weight = G->arc[i][j];
				index++;
			}
		}
	}
	return e;
}

//对这个边的数组排序
void SortEdge(Graph* G, Edge* e)
{
	for (int i = 0; i < G->arcNUM - 1; i++)
	{
		for (int j = 0; j < G->arcNUM - 1 - i; j++)
		{
			if (e[j].weight > e[j + 1].weight)
			{
				Edge tmp = e[j];
				e[j] = e[j + 1];
				e[j + 1] = tmp;
			}
		}
	}
}

//克鲁斯卡尔算法生成最小生成树
void Kruskal(Graph* G)
{
	Edge* e = InitEdge(G);
	SortEdge(G, e);
	//定义一个记录连通分量的数组
	int* connected = (int*)malloc(sizeof(int) * G->vexNUM);
	//初始化连通分量
	for (int i = 0; i < G->vexNUM; i++)
	{
		connected[i] = i;
	}
	//选边的话就只需要选n-1个就可以了,虽然是这样，但是万一选到了可以成环的边，那就完了，所以遍历全部边
	for (int i = 0; i < G->arcNUM; i++)
	{
		//定义某一个边的起始与终点的联通分量
		int start = connected[e[i].start];
		int end = connected[e[i].end];
		if (start != end)//选到这个边，而且将终点所属的连通分量全部改为起点的
		{
			printf("v%c->v%c,weight:%d\n", G->vex[e[i].start], G->vex[e[i].end], e[i].weight);
			for (int j = 0; j < G->vexNUM; j++)
			{
				if (connected[j] == end)
				{
					connected[j] = start;
				}
			}
		}
	}
}