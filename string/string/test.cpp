#define _CRT_SECURE_NO_WARNINGS 1
#include"string.h"

//void test1()
//{
//	bit::string s1("123456");//为了防止与库里的string矛盾，所以这样
//	cout << s1.c_str() << endl;//对于字符串的地址，会直接打印字符串，而不是地址
//	bit::string s2;
//	cout << s2.c_str() << endl;
//	bit::string s3(s1);
//	cout << s3.c_str() << endl;
//	bit::string s4 = "12367890";//隐式类型转换
//	cout << s4.c_str() << endl;
//	s4 = s1;
//	cout << s4.c_str() << endl;
//}
namespace bit
{
	void test2()
	{
		//str/*ing s1("1234567");*/
		//string::iterator it = s1.begin();
		//while (it != s1.end())
		//{
		//	cout << (*it) << ' ';
		//	it++;
		//}
		//cout << endl;
		//for (auto x : s1)
		//{
		//	cout << x << ' ';
		//}
		//s1[2] += 4;
		//cout << s1[2] << endl;
		//s1.reserve(10);
		//cout << s1.c_str() << endl;
		//cout << s1.size() << endl;
		//cout << s1.capacity() << endl;
		//s1 += 'x';
		//s1 += 'x';
		//s1 += 'x';
		//s1.push_back('x');
		//s1.push_back('x');
		//s1.push_back('x');
		//cout << s1.c_str() << endl;
		//cout << s1.size() << endl;
		//cout << s1.capacity() << endl;
		////s1 += "yyyyyyyy";
		//s1.append("yyyyy");
		//cout << s1.c_str() << endl;
		//cout << s1.size() << endl;
		//cout << s1.capacity() << endl;
		//s1.insert(0, "xxx");
		//s1.insert(0, "xxx");
		//s1.insert(1, "yyy");
		//string s2("xxxxx");
		//s1.replace(1, 2, "yyyy");
		//cout << s1.c_str() << endl;
		//cout << s1.size() << endl;
		//cout << s1.capacity() << endl;
		/*s1.erase(2);*/
		/*cout << s1.c_str() << endl;*/
		//string s1("sdfgg");
		//string s2("111111");
		//s1.swap(s2);
		//cout << s1.c_str() << endl;
		//cout << s2.c_str() << endl;
		//swap(s1, s2);
		//cout << s1.c_str() << endl;
		//cout << s2.c_str() << endl;

		//bit::swap(s1, s2);

		string s1("1234567");
		//cout << s1.find(2, '3') << endl;
		//cout << s1.find(2, "456") <<endl ;
		string s2("1234899");
		//s2=s1.substr(1, 10);
		//cout << s2.c_str() << endl;
		//cout << (s1 <= s2) << endl;
		//cout << s1 << endl<<s2<<endl;
		//cin >> s1;
		//cout << s1<<endl;
		s1 = s2;
		cout << s1 << endl;
		cout << s2 << endl;
	}
}
int main()//主函数不能放在命名空间里
{
	//int a = 0, b = 2;
	//std::swap(a, b);
	//cout << a;
	bit::test2();
	return 0;
}