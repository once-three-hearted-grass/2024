#pragma once
#include<iostream>
#include<assert.h>
#include<algorithm>
using namespace std;

namespace bit
{
	class string
	{
	public:
		string(const char* arr="");
		string(const string& s);
		char* c_str();
		~string();
		string& operator=(const string& s);
		typedef char* iterator;//因为迭代器类似于指针，所以在string中，可以定义为指针
		typedef const char* const_iterator;//因为迭代器类似于指针，所以在string中，可以定义为指针
		iterator begin();
		iterator end();
		const_iterator begin()const;
		const_iterator end()const;//这个是针对const定义的string
		size_t size();
		size_t capacity();
		void resize(size_t n);
		void reserve(size_t n);
		void clear();
		char& operator[](size_t n);
		string& operator+=(const string& s);
		string& operator+=(char c);
		void append(const string& str);
		void push_back(char c);
		void insert(size_t pos, char c);
		void insert(size_t pos, const string&s);
		void erase(size_t pos = 0, size_t len = npos);
		void replace(size_t pos, size_t len, const string& s);
		void swap(string& s);  //这里你是要交换的为什么要加一个const呢
		size_t find(size_t pos, char c);
		size_t find(size_t pos, string s);
		string substr(size_t pos = 0, size_t len = npos) const;//获取一个子串，并返回这个子串
		bool operator<(string& s)const;
		bool operator<=(string& s)const;
		bool operator>(string& s)const;
		bool operator>=(string& s)const;
		bool operator!=(string& s)const;
		bool operator==(string& s)const;
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		const static size_t npos;
	};

	void swap(string& s1, string& s2);  
	ostream& operator<<(ostream&out,string& s);//流提取和流输入都定义在类外面
	istream& operator>>(istream&in,string& s);
}
