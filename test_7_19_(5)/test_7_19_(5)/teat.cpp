#define _CRT_SECURE_NO_WARNINGS 1
//根据输入的日期，计算是这一年的第几天。
//输入描述：
//输入一行，每行空格分割，分别是年，月，日
//输出描述：
//输出是这一年的第几天
//示例1
//输入：
//2012 12 31
//复制
//输出：
//366
#include <iostream>
using namespace std;

int main() {
    int arr[] = { 0,31,59,90,120,151,181,212,243,273,304,334,365 };
    //先定义一个数组来存储到某个月的所有天数
    int year = 0;
    int month = 0;
    int day = 0;
    cin >> year >> month >> day;
    int sum = arr[month - 1] + day;
    if (month > 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
    {
        ++sum;
    }
    cout << sum;
    return 0;
}