#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class person
//{
//public:
//	int _tel=111110;
//	int _age=90;
//	int _address = 888888;
//};
//
//class teacher:public person
//{
//public:
//	void func()
//	{
//		cout << _age << endl;
//		cout << _tel << endl;
//		cout << person::_address << endl;
//	}
//protected:
//	int _address=2048;
//};
//
//class A
//{
//public:
//	void fun()
//	{
//		cout << "func()" << endl;
//	}
//};
//class B : public A
//{
//public:
//	void fun(int i)
//	{
//		A::fun();
//		cout << "func(int i)->" << i << endl;
//	}
//};
//
//int main()
//{
//	//teacher a;
//	//a.func();
//	//teacher a;
//	//person b;
//	//a._age = 10000;
//	//b = a;
//	//cout << b._age << endl;
//	//teacher a;
//	//person& b = a;
//	//b._age = 100000000;
//	//cout << a._age << endl;
//	//teacher a;
//	//person* b = &a;
//	//b->_age = 100000000;
//	//cout << a._age << endl;
//
//	//teacher a;
//	//a.func();
//	B b;
//	b.A::fun();
//	return 0;
//}

//class Person
//{
//public:
//	Person(const char* name = "peter")
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//
//		return *this;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//
//protected:
//	string _name; // 姓名
//};
//
//class Student:public Person
//{
//public:
//
//	Student(int age=20, string tel="111111", const char* name="张三")
//		:_age(age)
//		,_tel(_name)
//		,Person(name)
//	{}
//
//	Student(const Student& s1)
//		:_age(s1._age)
//		,_tel(s1._tel)
//		,Person(s1)
//	{}
//
//	Student& operator=(Student s)
//	{
//		_age = s._age;
//		_tel = s._tel;
//		Person::operator=(s);
//		return *this;
//	}
//
//	~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//
//	void func()
//	{
//		cout << _age <<" "<< _tel <<" "<< _name << endl;
//	}
//		
//private:
//	int _age;
//	string _tel;
//};

//class Student;
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._stuNum << endl;
//}

//class Person
//{
//public:
//	Person(const char* name = "peter")
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//
//		return *this;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//	static int a;//要放在公有，不然无法访问
//protected:
//	string _name; // 姓名
//};
//int Person::a = 10;//声明就不用写static了，而且还有指定类域
//
//class Student:public Person
//{
//public:
//	Student(int age=20, string tel="111111", const char* name="张三")
//		:_age(age)
//		,_tel(_name)
//		,Person(name)
//	{}
//
//	Student(const Student& s1)
//		:_age(s1._age)
//		,_tel(s1._tel)
//		,Person(s1)
//	{}
//
//	Student& operator=(Student s)
//	{
//		_age = s._age;
//		_tel = s._tel;
//		Person::operator=(s);
//		return *this;
//	}
//
//	~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//
//	void func()
//	{
//		cout << _age <<" "<< _tel <<" "<< _name << endl;
//	}
//		
//private:
//	int _age;
//	string _tel;
//};

class Person
{
public:
	string _name; // 姓名
};

class Student :virtual public Person
{
protected:
	int _num; //学号
};

class Teacher : virtual public Person
{
protected:
	int _id; // 职工编号
};

class Assistant : public Student, public Teacher
{
protected:
	string _majorCourse; // 主修课程
	Person p;
};
int main()
{
	// 这样会有二义性无法明确知道访问的是哪一个
	//a._name = "peter";
	// 需要显示指定访问哪个父类的成员可以解决二义性问题，但是数据冗余问题无法解决
	//Assistant a;
	//a.Student::_name = "xxx";
	//a.Teacher::_name = "yyy";
	cout << sizeof(Assistant) << endl;
	return 0;
}
////
//int main()
//{
//	//Student s(18, "1234567","1111111");
//	//s.func();
//	//Student s1;
//	//s1 = s;
//	//s1.func();
//
//	//Person p;
//	//Student s;
//	//Display(p, s);
//
//	Person p;
//	Student s;
//	cout << &Person::a << endl;
//	cout << &Student::a << endl;
//	return 0;
//}





