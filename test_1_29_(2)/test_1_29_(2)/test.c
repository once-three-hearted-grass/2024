#define _CRT_SECURE_NO_WARNINGS 1
//实现一个函数，可以左旋字符串中的k个字符。
//例如：
//ABCD左旋一个字符得到BCDA
//ABCD左旋两个字符得到CDAB
#include<stdio.h>
#include<string.h>
void Spin(char arr[], int k,int sz)
{
	static char arr2[100] = {0};
	int i = 0;
	int j = 0;
	for (i = 0; i < k; i++)
	{
		arr2[sz-k+i] = arr[i];
	}
	for (i = k; i < sz; i++)
	{
		arr2[i-k] = arr[i];//自己画图找规律分析
	}
	for (j = 0; j < sz; j++)
	{
		arr[j] = arr2[j];
	}
}
int main()
{
	int k = 0;
	printf("请输入k值:");
	scanf("%d", &k);
	char arr[100] = {0};
	printf("请输入字符串:");
	 scanf("%s", arr);
	 int sz = (int)strlen(arr);
	Spin(arr,k,sz);
	for (int i = 0; i < sz; i++)
	{
		printf("%c", arr[i]);
	}
	return 0;
}