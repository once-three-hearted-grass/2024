#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int GetNumberOfK(int* nums, int numsLen, int k) {
    int count = 0;
    for (int i = 0; i < numsLen; i++)
    {
        if (nums[i] == k)
        {
            count++;
        }
    }
    return count;
}
int main()
{
    int nums[10] = { 0 };
    for (int i = 0; i < 10; i++)
    {
        scanf("%d", &nums[i]);
    }
    printf("输入要查找的数字：");
    int k = 0;
    scanf("%d", &k);
    printf("%d在数组中出现次数为：", k);
    printf("%d", GetNumberOfK(nums, 10, k));
	return 0;
}