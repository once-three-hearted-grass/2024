#define _CRT_SECURE_NO_WARNINGS 1
#include<stdbool.h>
#include<stdio.h>
#include<stdlib.h>
//如果二叉树每个节点都具有相同的值，那么该二叉树就是单值二叉树。
//只有给定的树是单值二叉树时，才返回 true；否则返回 false。

  struct TreeNode {
      int val;
      struct TreeNode *left;
      struct TreeNode *right;
 };
 
//bool isUnivalTree(struct TreeNode* root) {
//    //如何实现，只需要判断每个节点等不等于它的左右孩子就可以了
//    //递归的话就是看每个子树满不满足单值了
//    //然后再看结束条件
//    //结束条件就是NULL return true，根不等于左或者右就返回false，但是左右肯定要不为空，不然怎么访问数据，如果根等于就递归
//    if (root == NULL)
//    {
//        return true;
//    }
//    if (root->left && root->val != root->left->val)
//    {
//        return false;
//    }
//    if (root->right && root->val != root->right->val)
//    {
//        return false;
//    }
//    return isUnivalTree(root->left) && isUnivalTree(root->right);
//}

  bool my_isUnivalTree(struct TreeNode* root, int x)
  {
      //递归的话，就是看左右子树是否同时为单值树
      //结束条件的话就是NULL返回true，不等于就返回false，等于就没事,因为就一个节点等于也说明不了什么
      if (root == NULL)
      {
          return true;
      }
      if (root->val != x)
      {
          return false;
      }
      return my_isUnivalTree(root->left,x) && my_isUnivalTree(root->right,x);
}
bool isUnivalTree(struct TreeNode* root) {
    //方法二，看每个节点等不等于那个值，为了让那个值一直存在，我们在建立一个函数
    return my_isUnivalTree(root, root->val);
  }