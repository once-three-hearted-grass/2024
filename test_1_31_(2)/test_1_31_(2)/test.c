#define _CRT_SECURE_NO_WARNINGS 1
//实现一个对整形数组的冒泡排序
#include<stdio.h>
void Bubble_sort(int* arr, int sz)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < sz - 1; i++)
	{
		int flag = 0;
		for (j = 0; j < sz-1-i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
				flag = 1;
			}
		}
		if (flag == 0)
		{
			break;
		}
	}
}
void Print(int* arr, int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", *(arr + i));
	}
}
int main()
{
	int arr[10] = { 0 };
	printf("请输入十个数:");
	for (int i = 0; i < 10; i++)
	{
		scanf("%d", arr + i);
	}
	Bubble_sort(arr, 10);
	Print(arr, 10);
	return 0;
}