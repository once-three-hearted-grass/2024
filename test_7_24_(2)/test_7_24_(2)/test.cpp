#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;

class Solution {
public:
    int firstUniqChar(string s) {
        //首先建立一个数组，相当于计数排序那样，存储每个字符出现的次数
        int arr[26] = { 0 };
        for (auto x : s)
        {
            arr[x - 'a']++;
        }
        //然后遍历串，不能遍历数组得到的第一个次数为1就是第一个单身狗
        //遍历串，第一个次数为1就是第一个单身狗
        for (int i = 0; i < s.size(); i++)
        {
            if (arr[s[i] - 'a'] == 1)
            {
                return i;
            }
        }
    }
};


int main()
{
	return 0;
}