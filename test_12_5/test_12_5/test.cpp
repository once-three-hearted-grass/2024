#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
struct Date {
    int year;
    int month;
    int day;
    int people;
    int week;
};

void process(struct Date* start, struct Date* end) {
    while (start->year != end->year || start->month != end->month || start->day != end->day) {
        int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
        if (start->year % 4 == 0 && start->year % 100 != 0 || start->year % 400 == 0) {
            arr[2] += 1;
        }
        start->day++;
        if (start->day > arr[start->month]) {
            start->day = 1;
            start->month++;
            if (start->month > 12) {
                start->month = 1;
                start->year++;
            }
        }
        start->week++;
        if (start->week > 7) {
            start->week = 1;
        }
        if (start->week != 1) {
            start->people++;
            if (start->people > 4) {
                start->people = 1;
            }
        }
    }
    end->week = start->week;
    end->people = start->people;
}

int main()
{

    struct Date start = { 2007,9,1,1,6 };
    int year = 0;
    int month = 0;
    int day = 0;
    scanf("%d%d%d", &year, &month, &day);
    struct Date end = { year,month,day,0,0 };

    process(&start, &end);
    if (end.week == 1) {
        printf("ALL\n");
        return 0;
    }
    if (end.people == 1) {
        printf("B\n");
    }
    else if (end.people == 2) {
        printf("X\n");
    }
    else if (end.people == 3) {
        printf("H\n");
    }
    else if (end.people == 4) {
        printf("P\n");
    }
    return 0;
}
