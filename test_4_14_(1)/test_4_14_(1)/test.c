#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//给你单链表的头结点 head ，请你找出并返回链表的中间结点。
//如果有两个中间结点，则返回第二个中间结点。
//输入：head = [1,2,3,4,5]
//输出：[3, 4, 5]
//解释：链表只有一个中间结点，值为 3 。
//输入：head = [1, 2, 3, 4, 5, 6]
//输出：[4, 5, 6]
//解释：该链表有两个中间结点，值分别为 3 和 4 ，返回第二个结点。

struct ListNode {
	int val;
	struct ListNode* next;
};
typedef struct ListNode ListNode;
struct ListNode* middleNode(struct ListNode* head) {
	//法一：先统计总共有多少个元素，在除以2就可以了
	//法二：定义快慢指针，一个走一步，一个走两步，当快指针走到末尾的时候，慢指针才走到了一半
	ListNode* fast = head;
	ListNode* slow = head;
	while (fast && fast->next)//这两个任意一个走到了NULL就结束了
	{
		slow = slow->next;
		fast = fast->next->next;
	}
	return slow;
}
int main()
{
	return 0;
}