#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现strcat
#include<stdio.h>
#include<string.h>
#include<assert.h>
char* my_strcat(char* des, const char* src)
{
	char* ret = des;
	assert(des && src);
	//先找到des的\0
	while (*(++des));
	//在挨个替换,包括\0
	while (*(des++) = *(src++));
	return ret;
}
int main()
{
	char arr1[] = "abcd";
	char arr2[20] = "aaaaaaa";
	//strcat(arr2, arr1);
	my_strcat(arr2, arr1);
	printf("%s", arr2);
	return 0;
}