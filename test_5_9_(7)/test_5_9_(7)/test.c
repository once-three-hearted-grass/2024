#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<stdio.h>
#include<stdbool.h>
//栈 后缀表达式计算
//请使用已定义好的栈完成后缀表达式计算：
//(1)如果是操作数，直接入栈
//(2)如果是操作符op，连续出栈两次，得到操作数x 和 y, 计算 x op y，并将结果入栈。
//后缀表达式示例如下：
//9  3  1 - 3 * +10  2 / +
//13  445 + 51 / 6 -
//操作数、操作符之间由空格隔开，操作符有 + ， - ，*, / , % 共 5 种符号，所有操作数都为整型。
//栈的定义如下：
#define Stack_Size 50
typedef int ElemType;
typedef struct {
    ElemType elem[Stack_Size];
    int top;
}Stack;
bool push(Stack* S, ElemType x);
bool pop(Stack* S, ElemType* x);
void init_stack(Stack* S);
//其中，栈初始化的实现为：
void init_stack(Stack* S) {
    S->top = -1;
}
//需要完成的函数定义为：int compute_reverse_polish_notation(char* str);
//函数接收一个字符指针，该指针指向一个字符串形式的后缀表达式，函数返回该表达式的计算结果。
int compute_reverse_polish_notation(char* str) {
    char* p = str;
    int sum = 0;
    Stack s;
    init_stack(&s);
    while (*p != '\0')
    {
        if (*p != '+' && *p != '-' && *p != '*' && *p != '/' && *p != '%' && *p != ' ')
        {
            sum = (*p - 48) + sum * 10;//将字符转换为数字
            p++;
        }
        else
        {
            if(sum!=0)//说明没有字符，就别入栈了
            push(&s, sum);
            sum = 0;
            if (*p == ' ')
            {
                p++;
            }
            else//运算了
            {
                char op = *p;
                int x = 0;
                int y = 0;
                pop(&s, &x);
                pop(&s, &y);
                switch (op) {
                case '+':
                    push(&s, y +x);
                    break;
                case '-':
                    push(&s, y-x);
                    break; 
                case '*':
                        push(&s, y * x);
                        break;
                case '/':
                    push(&s, y/x);
                    break;
                case '%':
                    push(&s, y % x);
                    break;
                }
                p++;
            }
        }
    }
    int ret = 0;
    pop(&s, &ret);
    return ret;
}
int main()
{
    char arr[] = "9 3 1 - 3 * + 10 2 / +";
    int a=compute_reverse_polish_notation(arr);
    printf("%d", a);
    return 0;
}