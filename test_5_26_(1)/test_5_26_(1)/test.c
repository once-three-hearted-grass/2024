#define _CRT_SECURE_NO_WARNINGS 1

#include<stdlib.h>
#include<stdio.h>
//给你一棵二叉树的根节点 root ，翻转这棵二叉树，并返回其根节点。

struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};
//  void my_invertTree(struct TreeNode* left, struct TreeNode* right)
//  {
//      //翻转左右子树的话，就直接先翻转两个根，再翻转左的左，右的右，，，，，左的右，右的左，先假设翻转的时候同时有数据，同时为空
//      //看来这样不行，因为不会同时为空，同时又数据，所以交换数据是不行的，要交换指针
//      if (left == NULL && right == NULL)
//      {
//          return;
//      }
//      int tmp = left->val;
//      left->val = right->val;
//      right->val = tmp;
//      my_invertTree(left->left, right->right);
//      my_invertTree(left->right, right->left);
//  }
//struct TreeNode* invertTree(struct TreeNode* root) {
//    //翻转这个树，就只需要翻转这颗树的左右子树就可以了
//    if (root == NULL)
//    {
//        return NULL;
//    }
//    my_invertTree(root->left, root->right);
//    return root;
//}

struct TreeNode* invertTree(struct TreeNode* root) {
    //先交换左右子树，在交换左右子树的左右子树就可以了
    if (root == NULL)
    {
        return NULL;
    }
    struct TreeNode* tmp = root->left;
    root->left = root->right;
    root->right = tmp;
    invertTree(root->left);
    invertTree(root->right);
    return root;
}