#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

void InsertSort(int* a, int n)
{
	//
	for (int i = 0; i < n - 1; i++)
	{
		int end = i;
		int tmp = a[i + 1];
		while (end >= 0)
		{
			if (a[end] > tmp)
			{
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}

void Print(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ",arr[i]);
	}
}

void ShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[i + gap];
			while (end >=0)
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}

void BubbleSort(int* a, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int flag = 0;
		for (int j = 0; j < n -1- i; j++)
		{
			if (a[j] > a[j + 1])
			{
				int tmp = a[j];
				a[j] = a[j + 1];
				a[j + 1] = tmp;
				flag = 1;
			}
		}
		if (flag == 0)
		{
			break;
		}
	}
}

int GetMid(int* a, int left, int right)
{
	int tmp = a[left];
	int head = left;
	int tail = right;
	while (head < tail)
	{
		while (head < tail && a[tail]>=tmp)
		{
			tail--;
		}
		while (head < tail && a[head] <= tmp)
		{
			head++;
		}
		int x = a[head];
		a[head] = a[tail];
		a[tail] = x;
	}
	int y = a[head];
	a[head] = tmp;
	a[left] = y;
	return head;
}

void My_QuickSort(int* a,int left,int right)
{
	if (left >= right)
	{
		return;
	}
	int mid = GetMid(a, left, right);
	My_QuickSort(a, left, mid - 1);
	My_QuickSort(a, mid + 1, right);
}

void QuickSort(int* a, int n)
{
	My_QuickSort(a, 0, n - 1);
}

int main()
{
	int arr[] = { 1,2,6,7,8,9,5,4,3,6,4,3,2,6,8,9,5,7,9, };
	int sz = sizeof(arr) / sizeof(arr[0]);
	QuickSort(arr, sz);
	Print(arr, sz);
	return 0;
}