#define _CRT_SECURE_NO_WARNINGS 1
//求出0～100000之间的所有“水仙花数”并输出。
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，如 : 153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”。
#include<stdio.h>
#include<math.h>
//int main()
//{
//	int i, j;
//	for (int i = 0; i <= 100000; i++)
//	{
//		if (i >= 0 && i <= 9)
//		{
//			printf("%d ", i);
//		}
//		if (i >= 10 && i <= 99)
//		{
//			int a1 = i % 10;
//			int a2 = i / 10;
//			if (a1 * a1 + a2 * a2 == i)
//			{
//				printf("%d ", i);
//			}
//		}
//		if (i >= 100 && i <= 999)
//		{
//			int a1 = i % 10;
//			int b = i / 10;
//			int a2 = b % 10;
//			int a3 = b / 10;
//			if (a1 * a1*a1 + a2 * a2*a2+a3*a3*a3 == i)
//			{
//				printf("%d ", i);
//			}
//		}
//		if (i >= 1000 && i <= 9999)
//		{
//			int a1 = i % 10;
//			int b = i / 10;
//			int a2 = b % 10;
//			int c = b / 10;
//			int a3 = c % 10;
//			int d = c / 10;
//			int a4 = d % 10;
//			if (a1 * a1 * a1 * a1 + a2 * a2 * a2 * a2 + a3 * a3 * a3 * a3 + a4*a4*a4*a4 == i)
//			{
//				printf("%d ", i);
//			}
//		}
//		if (i >= 10000 && i <= 99999)
//		{
//			int a1 = i % 10;
//			int b = i / 10;
//			int a2 = b % 10;
//			int c = b / 10;
//			int a3 = c % 10;
//			int d = c / 10;
//			int a4 = d % 10;
//			int e = d / 10;
//			int a5 = e % 10;
//			if (a1 * a1 * a1 * a1 *a1 + a2 * a2 * a2 * a2*a2 + a3 * a3 * a3 * a3*a3 + a4 * a4 * a4 * a4*a4+a5*a5*a5*a5*a5 == i)
//			{
//				printf("%d ", i);
//			}
//		}
//	}
//	return 0;
//}
int main()
{
	int i = 0;
	for (i = 0; i <= 100000; i++)
	{
		//先计算几位数
		int n = 0;
		int get = i;
		while (get)
		{
			get = get / 10;
			n++;
		}
		//计算次方和
		get = i;
		int sum = 0;
		int power = 1;
		while (get)
		{
			power = 1;
			power = pow(get % 10, n);
			//pow函数计算次方，左数是底数，右数是次方
			get = get / 10;
			sum += power;
		}
		if (sum == i)
		{
			printf("%d ",i);
		}
	}
	return 0;
}