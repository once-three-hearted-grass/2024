#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class Stack
//{
//public:
//	Stack(int a)
//	{
//		cout << "Stack()" << endl;
//	}
//private:
//	int size;
//};

//class A
//{
//public:
//	A(int a)
//		:a2(a)
//		,a3(a)
//		,s(2)
//		
//	{
//		cout << a1 << endl;
//		cout << a2 << endl;
//		cout << a3 << endl;
//	}
//private:
//	Stack s;
//	int a1=2;
//	const int a2=2;
//	int& a3=a1;
//};
//int main()
//{
//	A a(4);
//	return 0;
//}

//class A
//{
//public:
//	A(int a)
//		:a2((int*)malloc(40))
//	{
//		cout << a1 << endl;
//	}
//private:
//	Stack s;
//	int a1;
//	int* a2;
//};
//
//int main()
//{
//	A tmp(1);
//	return 0;
//}
//class A {
//public:
//    A(int a)
//        :_a1(a)
//        , _a2(_a1)
//    {}
//
//    void Print() {
//        cout << _a1 << " " << _a2 << endl;
//    }
//private:
//    int _a2;
//    int _a1;
//};
//int main() {
//    A aa(1);
//    aa.Print();
//}

//class Stack
//{
//public:
//	Stack(int a)
//	{
//		cout << "Stack()" << endl;
//	}
//	Stack(Stack& s)
//	{
//		cout << "	Stack(Stack& s)" << endl;
//	}
//private:
//	int size;
//};

//int main()
//{
//	Stack s1(2);
//	Stack s2 = 2;
//	Stack s3 = 23.23;
//	return 0;
//}

//class Stack
//{
//public:
//	int Get()
//	{
//		return _count;
//	}
//	Stack()
//	{
//		++_count;
//		cout << "Stack()" << endl;
//	}
//	Stack(const Stack& s)
//	{
//		cout << "Stack(Stack& s)" << endl;
//	}
//private:
//	int _i;
//	static int _count;
//};
//int Stack::_count = 0;
//
//int main()
//{
//	Stack s1;
//	Stack s2;
//	cout << s1.Get() << endl;
//	cout << s2.Get() << endl;
//	return 0;
//}

//class Stack1
//{
//	friend class Stack2;
//public:
//	Stack1()
//	{
//		cout << "Stack1()" << endl;
//	}
//	Stack1(const Stack1& s)
//	{
//		cout << "Stack1(Stack1& s)" << endl;
//	}
//private:
//	int _i;
//};
//
//class Stack2
//{
//public:
//	Stack2()
//	{
//		Stack1 s1;
//		s1._i;
//		cout << "Stack2()" << endl;
//	}
//	Stack2(const Stack2& s)
//	{
//		cout << "Stack2(Stack2& s)" << endl;
//	}
//private:
//	int _i;
//};


class Stack
{
public:
	Stack()
	{
		_i = 0;
		cout << "Stack()" << endl;
	}
	Stack(const Stack&s)
	{
		_i = s._i;
		cout << "Stack()" << endl;
	}
	~Stack()
	{
		cout << "~Stack()" << endl;
	}
private:
	int _i;
};

Stack f()
{
	Stack s;
	return s;
}

int main()
{
	Stack s1 = f();
	return 0;
}