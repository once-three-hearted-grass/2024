#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
//输入数字 n，按顺序打印出从 1 到最大的 n 位十进制数。比如输入 3，则打印出 1、2、3 一直到最大的 3 位数 999。
int* printNumbers(int n, int* returnSize) {
    *returnSize = 1;
    for (int i = 0; i < n; i++)
    {
        *returnSize *= 10;
    }
    (*returnSize)--;
    int* ret = (int*)malloc(sizeof(int) * (*returnSize));
    for (int i = 1; i <= (*returnSize); i++)
    {
        ret[i - 1] = i;
    }
    return ret;
}