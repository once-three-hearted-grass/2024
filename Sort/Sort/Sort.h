#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

// 插入排序
void InsertSort(int* a, int n);

//打印数组
void Print(int* a, int n);

// 希尔排序
void ShellSort(int* a, int n);

// 选择排序
void SelectSort(int* a, int n);

//向下调整算法
void AdjustDwon(int* a, int n, int root);

// 堆排序
void HeapSort(int* a, int n);

// 冒泡排序
void BubbleSort(int* a, int n);

//交换函数
void Swap(int* a, int* b);

//快速排序   hoare版本
void QuickSort1(int* a, int left, int right);

//hoare版本
int part1(int* a, int left, int right);

void QuickSort2(int* a, int left, int right);

void QuickSort3(int* a, int left, int right);

//寻找三数中间值
int GetMid(int *a,int left, int mid, int right);

// 快速排序挖坑法
void QuickSort4(int* a, int left, int right);

//挖坑法版本
int part2(int* a, int left, int right);

// 快速排序前后指针法
int Part3(int* a, int left, int right);
void QuickSort5(int* a, int left, int right);

// 快速排序 非递归实现
void QuickSortNonR(int* a, int left, int right);

//归并排序
void MergeSort(int* a, int n);

//归并排序非递归实现
void MergeSortNonR1(int* a, int n);
void MergeSortNonR2(int* a, int n);

//计数排序
void CountSort(int* a, int n);

//外部排序创建数据
void CreateData(char* arr, int n);

//两个文件归并
void SMerge_Sort(char*arr1, char*arr2,char* merge_arr);