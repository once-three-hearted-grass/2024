#include"Stack.h"

//初始化栈
void StackInit(Stack* ps)
{
	ps->a = NULL;
	ps->capacity = ps->top = 0;//top表示栈中元素个数，也表示栈顶元素的下一个位置
}

//入栈
void StackPush(Stack* ps, STDataType x)
{
	assert(ps);
	if (ps->top == ps->capacity)
	{
		int new_capacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		STDataType* p = (STDataType*)realloc(ps->a, sizeof(STDataType) * new_capacity);
		if (p == NULL)
		{
			perror("StackPush:realloc:");
			return;
		}
		ps->capacity = new_capacity;
		ps->a = p;
	}
	//入栈就是直接在top位置放数据就是了
	ps->a[ps->top] = x;
	ps->top++;
}

//出栈 
void StackPop(Stack* ps)//直接少最后一个
{
	assert(ps);
	assert(ps->top);
	ps->top--;
}

//获取栈顶元素
STDataType StackTop(Stack* ps)
{
	assert(ps);
	assert(ps->top);
	return ps->a[ps->top - 1];
}

//获取栈有效元素个数
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->top;
}

//判断栈是否为空
bool StackEmpty(Stack* ps)
{
	return ps->top == 0;
}

//销毁栈
void StackDestroy(Stack* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->top = ps->capacity = 0;
}