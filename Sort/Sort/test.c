#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include"Sort.h"
void test1()
{
	int arr[] = { 9,1,2,5,7,4,6,3,23,24};
	int sz = sizeof(arr) / sizeof(arr[0]);
	//InsertSort(arr, sz);
	//Print(arr, sz);

	//ShellSort(arr, sz);
	//Print(arr, sz);

	//SelectSort(arr, sz);

	//HeapSort(arr, sz);
	//BubbleSort(arr, sz);
	//QuickSortNonR(arr, 0, sz - 1);
	//MergeSortNonR2(arr, sz);
	CountSort(arr, sz);
	Print(arr, sz);
}
void test2()//产生随机数来排序
{
	int n = 1000000;//产生n个随机数
	srand((unsigned int)time(NULL));
	int* a1 = (int*)malloc(sizeof(int) * n);
	int* a2 = (int*)malloc(sizeof(int) * n);
	int* a3 = (int*)malloc(sizeof(int) * n);
	int* a4 = (int*)malloc(sizeof(int) * n);
	int* a5 = (int*)malloc(sizeof(int) * n);
	int* a6 = (int*)malloc(sizeof(int) * n);
	int* a7 = (int*)malloc(sizeof(int) * n);
	int* a8 = (int*)malloc(sizeof(int) * n);
	for (int i = 0; i < n; i++)
	{
		int x = (rand() + i) % n;
		a1[i] = x;
		a2[i] = x;
		a3[i] = x;
		a4[i] = x;
		a5[i] = x;
		a6[i] = x;
		a7[i] = x;
		a8[i] = x;
	}

	int head1 = clock();
	//InsertSort(a1, n);
	int tail1 = clock();

	int head2 = clock();
	ShellSort(a2, n);
	int tail2 = clock();

	int head3 = clock();
	//SelectSort(a3, n);
	int tail3 = clock();

	int head4 = clock();
	HeapSort(a4, n);
	int tail4 = clock();

	int head5 = clock();
	//BubbleSort(a5, n);
	int tail5 = clock();

	int head6 = clock();
	QuickSortNonR(a6, 0,n-1);
	int tail6 = clock();

	int head7 = clock();
	MergeSortNonR2(a7,n);
	int tail7 = clock();

	int head8 = clock();
	CountSort(a7, n);
	int tail8 = clock();

	printf("InsertSort:%dms\n", tail1 - head1);
	printf("ShellSort:%dms\n", tail2 - head2);
	printf("SelectSort:%dms\n", tail3 - head3);
	printf("HeapSort:%dms\n", tail4 - head4);
	printf("BubbleSort:%dms\n", tail5 - head5);
	printf("QuickSort:%dms\n", tail6 - head6);
	printf("MergeSort:%dms\n", tail7 - head7);
	printf("CountSort:%dms\n", tail8 - head8);

}

void test3()
{
	//先产生要排序的文件
	char a[] = "sort";
	int size = 100;//要排序的外部数据总个数
	CreateData(a,size);
	FILE* asort = fopen(a, "r");
	int m = 20;//内存支持的最多排序数据为10个
	//先将这10打入数组排序，再打入每个小文件
	int n = size / m;//小文件个数
	char arr[20] = { 0 };
	for (int i = 1; i <= n; i++)
	{
		int m1 = m;
		sprintf(arr, "%d", i);
		//创建小文件
		FILE* smallsort=fopen(arr, "w");
		int* tmp = (int*)malloc(sizeof(int) * m);
		int j = 0;//记录下标
		//将m个数据打入数组
		int num;
		int ret = fscanf(asort, "%d", &num);
		while (m1 > 1&&ret!=EOF)
		{
			tmp[j] = num;
			j++;
			ret = fscanf(asort, "%d", &num);
			m1--;
		}
		tmp[j] = num;//防止fscanf读取过多了
		QuickSort3(tmp, 0, m - 1);
		//打入小文件
		for (int w = 0; w < m; w++)
		{
			fprintf(smallsort, "%d\n", tmp[w]);
		}
		fclose(smallsort);
		free(tmp);
		//走到这里已经创建好了10个有序的小文件
	}

	//现在这里就将两个两个的文件挨着挨着归并到一个新文件中，总共要做九次这种操作
	char arr1[20]="1";
	char arr2[20]="2";
	char merge_arr[20]="1";
	for (int i = 2; i <= n; i++)//九次//就是小文件个数-1
	{
		sprintf(merge_arr, "%s%d", merge_arr, i);
		SMerge_Sort(arr1, arr2, merge_arr);
		strcpy(arr1, merge_arr);
		sprintf(arr2, "%d", i+1);
	}
	//走到这里就实现了真正的外部排序了
	fclose(asort);
}
int main()
{
	//test1();
	//test2();
	test3();
	return 0;
}
