#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<assert.h>

#define _CRT_SECURE_NO_WARNINGS 1
typedef int STDataType;
typedef struct Stack
{
	STDataType* a;
	int top;
	int capacity;
}Stack;

//初始化栈
void StackInit(Stack* ps);

//入栈
void StackPush(Stack* ps, STDataType x);

//出栈 
void StackPop(Stack* ps);

//获取栈顶元素
STDataType StackTop(Stack* ps);

//获取栈顶有效元素个数
int StackSize(Stack* ps);

//判断栈是否为空
bool StackEmpty(Stack* ps);

//销毁栈
void StackDestroy(Stack* ps);
