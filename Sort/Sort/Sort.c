
#define _CRT_SECURE_NO_WARNINGS 1

#include"Sort.h"

//交换函数
void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

// 插入排序
void InsertSort(int* a, int n)//时间复杂度为O(N^2)
{
	//这个排序的思想就是，前面0~n个数据已经有序了，把第n+1个数据插入进去
	for (int i = 0; i < n - 1; i++)
	{
		//先储存好要插入的数据
		int end = i;//假设0~end有序
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (a[end] > tmp)//升序
			{
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}

//打印数组
void Print(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
}

//// 希尔排序
// oid ShellSort(int* a, int n)
//{
//	//希尔排序主要思想就是，把每相隔gap的为一组进行插入排序，然后gap逐渐减小，减小为1，就是真正的插入排序了
//	//排序这个东西就是由内到外逐层的写，先假设gap==多少，先把一趟写好,写好一趟就写外面一趟，慢慢来
//	int gap = 4;
//	for (int j = 0; j < gap; j++)//这里就是将gap组进行插入排序
//	{
//		for (int i = j; i < n - gap; i += gap)//这一趟就和插入排序的最外面那一层差不多了，因为要保证a[end + gap]存在，所以条件那样设，end+gap<n
//		{
//			//这个处理的就是其中一个gap组的插入排序
//			int end = i;
//			int tmp = a[end + gap];
//			while (end >= 0)//最里面这一趟与插入排序差不多的
//			{
//				if (a[end] > tmp)
//				{
//					a[end + gap] = a[end];//循环外面写了a[end+gap],所以这里是存在的，不会越界
//					end -= gap;
//				}
//				else
//				{
//					break;
//				}
//			}
//			a[end + gap] = tmp;
//		}
//	}
//}

// 希尔排序
void ShellSort(int* a, int n)//时间复杂度O(N^1.3)很快的
{
	//希尔排序主要思想就是，把每相隔gap的为一组进行插入排序，然后gap逐渐减小，减小为1，就是真正的插入排序了
	//排序这个东西就是由内到外逐层的写，先假设gap==多少，先把一趟写好,写好一趟就写外面一趟，慢慢来
	int gap = n;//为什么/3，因为统计出来这样效率最高
	//这样设计的话可以保证gap最后的值一定是1，不可能是0，gap不能为0，但是一定最后要为1
	while (gap > 1)//保证最后为1执行后就退出了
	{
		gap = gap / 3 + 1;
		for (int i = 0; i < n - gap; i++)//这一趟就和插入排序的最外面那一层差不多了，因为要保证a[end + gap]存在，所以条件那样设，end+gap<n
		{
			//这里可以优化的，直接i++
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)//最里面这一趟与插入排序差不多的
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];//循环外面写了a[end+gap],所以这里是存在的，不会越界
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}

// 选择排序
void SelectSort(int* a, int n)//时间复杂度O(N^2)
{
	//假设head~tail包括头尾这个区间是无序的，然后遍历这个区间一遍，选出最小的，和最大的，放在头尾
	//因为不能直接替换，所以要定义最小最大值的下标，最后交换数据就可以了
	int head = 0;
	int tail = n - 1;
	while (head < tail)
	{
		int min = head;
		int max = head;
		for (int i = head + 1; i <= tail; i++)//因为第一个可以不用比
		{
			if (a[i] < a[min])
			{
				min = i;
			}
			if (a[i] > a[max])
			{
				max = i;
			}
		}
		////交换数据
		//int tmp = a[head];
		//a[head] = a[min];
		//a[min] = tmp;

		//tmp = a[tail];//还有一种出错的可能，就是max的位置就是一开始head的位置，本来head的位置是最大值的位置，结果现在变成了最小值还去交换了
		////所以不行
		//a[tail] = a[max];
		//a[max] = tmp;

		////处理,为了防止相互替换时把最值替换走了，就直接定义四个变量，存放四个变量，然后直接赋值
		//int tmp1 = a[head];
		//int tmp2 = a[tail];
		//int tmp3 = a[min];
		//int tmp4 = a[max];
		//a[head] = tmp3;
		//a[min] = tmp1;
		//a[tail] = tmp4;
		//a[max] = tmp2;//这样还是不行，因为他们可能会指向同一个位置就很麻烦

		//这样来，先把最大值最小值存起来，先交换头与最小值，然后看最大值位置还是不是最大值，如果不是就不用交换了
		int MIN = a[min];
		int MAX = a[max];
		a[min] = a[head];
		a[head] = MIN;
		if (a[max] == MAX)//此时若不等于的话，就说明他们指向的位置有重合，此位置就不是最大值的位置了,tail也不--了
		{
			a[max] = a[tail];
			a[tail] = MAX;
			tail--;
		}

		head++;


		////法二
		//int tmp = a[head];
		//a[head] = a[min];
		//a[min] = tmp;
		//if (max == head)
		//{
		//	max = min;//画图就可得，就可以这样处理
		//}
		//tmp = a[tail];
		//a[tail] = a[max];
		//a[max] = tmp;
	}
}

//向下调整算法
void AdjustDwon(int* a, int n, int root)
{
	//这里我们建大堆，因为要升序
	//向下调整，说明下面的是堆
	//先找出左右孩子大的那个
	int child = 2 * root + 1;
	while (child < n)//开始调整//只要孩子没有越界就可以调整了
	{
		if ((child + 1) < n && a[child + 1] > a[child])//说明右孩子更小，并且没有越界
		{
			child++;
		}
		if (a[root] < a[child])
		{
			//交换
			int tmp = a[root];
			a[root] = a[child];
			a[child] = tmp;
		}
		else
		{
			break;
		}
		root = child;
		child = child * 2 + 1;
	}
}

// 堆排序
void HeapSort(int* a, int n)//时间复杂度O(NlogN)
{
	//从最后一个元素开始向下调整建堆，因为向下调整保证的就是下面的为堆,挨着挨着来
	//可以从最后一个孩子的父亲开始建堆
	//先建堆
	int tail = (n - 1 - 1) / 2;
	while (tail >= 0)//
	{
		AdjustDwon(a, n, tail);
		tail--;
	}
	//建堆完成，现在开始排序，这里我们建立的是大堆，要升序就建大堆
	//每次把堆顶的元素与末尾元素交换，堆顶向下调整，末尾元素位置--
	tail = n - 1;
	while (tail > 0)//tail=0就不用交换了
	{
		//交换堆顶元素与数组末尾元素
		int tmp = a[0];
		a[0] = a[tail];
		a[tail] = tmp;
		tail--;
		AdjustDwon(a, tail, 0);
	}
}

// 冒泡排序
void BubbleSort(int* a, int n)
{
	//冒泡排序就不用由内到外慢慢搞了，直接从最外层开始写
	//把数组遍历一遍，把大的交换到后面，走一趟就可以把最大的弄到最后面
	//总共要走n-1趟，因为最后一个不用走了
	for (int i = 0; i < n - 1; i++)
	{
		//如果一趟一次都没交换说明了已经有序了，就不用再次冒泡了//用flag判断
		int flag = 0;
		for (int j = 0; j < n - 1 - i; j++)//从0开始冒泡,因为涉及到a[j+1],所以j<n-1,,,因为每次冒泡时，原先冒好的最大值，就不用再判断了，所以j<n-1-i
		{
			if (a[j] > a[j + 1])
			{
				Swap(&a[j], &a[j + 1]);
				flag = 1;
			}
		}
		if (flag == 0)
		{
			break;
		}
	}
}

//快速排序
void QuickSort1(int* a, int left, int right)//时间复杂度是logN*N
{
	if (left == right)
	{
		return;//只有一个元素
	}
	if (left > right)
	{
		return;//没有元素
	}
	//标记第一位置的为key，先在右寻找，找到比key小的，就暂停
	//再从左开始找，找到比key大的就停止，最后交换这两个
	//最后左和右一定会相遇，相遇的时候一定比key小（可以简单分析一下），然后这个位置和key交换就可以了
	int key = left;
	int head = left;
	int tail = right;
	while (head < tail)
	{
		while (head < tail && a[head] <= a[key])
		{
			head++;
		}//找到一个比key大的
		while (head < tail && a[tail] >= a[key])
		{
			tail--;
		}//找到一个比key小的
		//交换
		Swap(&a[head], &a[tail]);
	}
	Swap(&a[key], &a[head]);
	//这就排完一个的中间元素了
	//接下来就只需要排left~head-1和head+1~right的中间元素就可以了
	QuickSort1(a,left, head - 1);
	QuickSort1(a, head + 1, right);
}

//寻找三数中间值//这里指的是数值中间值
int GetMid(int* a, int left, int mid, int right)
{
	//这里的参数是下标，返回的也是下标
	//慢慢讨论吧
	//假装不考虑等于，因为等于的话，随便返回
	if (a[left] > a[mid])
	{
		if (a[mid] > a[right])
		{
			return mid;
		}
		else//a[mid] < a[right]
		{
			if (a[left] > a[right])
			{
				return right;
			}
			else
			{
				return left;
			}
		}
	}
	else//a[left] < a[mid]
	{
		if (a[right] < a[left])
		{
			return left;
		}
		else//a[right] > a[left]
		{
			if (a[right] > a[mid])
			{
				return mid;
			}
			else
			{
				return right;
			}
		}
	}
}

//快速排序
void QuickSort2(int* a, int left, int right)//优化：
//如果有序的话，时间复杂度就变成N^2了
//所以这样优化，在left，right，和mid中间取一个中间值，放在最前面最为key
{
	if (left >= right)
	{
		return;
	}
	//还有一个优化，就是在数组长度很短的时候，就不要递归了直接去排序

	int mid = (left + right) / 2;
	int tmp=GetMid(a, left, mid, right);
	Swap(&a[left], &a[tmp]);

	int key = left;
	int head = left;
	int tail = right;
	while (head < tail)
	{
		while (head < tail && a[head] <= a[key])
		{
			head++;
		}
		while (head < tail && a[tail] >= a[key])
		{
			tail--;
		}
		Swap(&a[head], &a[tail]);
	}
	Swap(&a[key], &a[head]);
	QuickSort2(a, left, head - 1);
	QuickSort2(a, head + 1, right);
}

//快速排序
void QuickSort3(int* a, int left, int right)//优化：
{
	if (left >= right)
	{
		return;
	}
	//还有一个优化，就是在数组长度很短的时候，就不要递归了直接去排序，因为长度短的时候，递归执行次数很多，如果能省去就很好
	//这里我们直接用插入排序，因为其他排序好像都不合适，唯一的缺点就是插入排序是N^2了，但是都不影响，因为长度很短嘛
	if ((right - left + 1) <= 10)
	{
		InsertSort(a + left, right - left + 1);
		return;
	}

	int mid = (left + right) / 2;
	int tmp = GetMid(a, left, mid, right);
	Swap(&a[left], &a[tmp]);

	int key = left;
	int head = left;
	int tail = right;
	while (head < tail)
	{
		while (head < tail && a[head] <= a[key])
		{
			head++;
		}
		while (head < tail && a[tail] >= a[key])
		{
			tail--;
		}
		Swap(&a[head], &a[tail]);
	}
	Swap(&a[key], &a[head]);
	QuickSort3(a, left, head - 1);
	QuickSort3(a, head + 1, right);
}

int part1(int* a, int left, int right)
{
	int key = left;
	int head = left;
	int tail = right;
	while (head < tail)
	{
		while (head < tail && a[head] <= a[key])
		{
			head++;
		}//找到一个比key大的
		while (head < tail && a[tail] >= a[key])
		{
			tail--;
		}//找到一个比key小的
		//交换
		Swap(&a[head], &a[tail]);
	}
	Swap(&a[key], &a[head]);
	return head;
}

//挖坑法版本
int part2(int* a, int left, int right)
{
	//一趟的版本
	int head = left;
	int tail = right;
	int key = a[left];
	//先把第一个元素放入key
	//所以现在坑就是第一个位置了
	int pit = left;
	while (left < right)
	{
		while (left<right&&a[right] >= key)
		{
			right--;
		}
		//走到这里说明比key小了，或者相等了，相等也放在坑里面,相当于没有交换，不影响，分析一下就是了
		a[pit] = a[right];
		pit = right;
		while (left < right && a[left] <= key)
		{
			left++;
		}
		a[pit] = a[left];
		pit = left;
	}
	//最后把相遇位置放key就可以了
	a[pit] = key;
	return pit;
}

// 快速排序挖坑法
void QuickSort4(int* a, int left, int right)
{
	//挖坑法的话，感觉hoare版本差不多诶
	if (left >= right)
	{
		return;
	}
	int mid = part2(a, left, right);
	QuickSort4(a, left, mid - 1);
	QuickSort4(a, mid + 1, right);
}

// 快速排序前后指针法
int part3(int* a, int left, int right)
//注意前后指针法与前面的什么挖坑法，hoare版本，基本思路没什么区别，主要的区别就是这个part部分
{
	int pre = left;
	int cur = left+1;//一定不会越界的，因为right>left
	//所谓前后指针法，肯定要定义一前一后两个指针了，不是同起点的，快慢指针才是同起点，只是速度不一样罢了

	int key = a[left];

	//主要思路是，先把第一位置存起来，cur比key小就把这个位置与pre交换，pre++,cur++，，，比key大的，就只++cur就可以了
	while (cur<=right)
	{
		if (a[cur] < key)
		{
			Swap(&a[pre], &a[cur]);
			pre++;
			cur++;
		}
		else
		{
			cur++;
		}
	}
	//最后退出来的这个位置，则pre一定小于key，或就是一开始的位置，所以直接填充
	a[pre] = key;
	return pre;
}


void QuickSort5(int* a, int left, int right)
{
	if (left >= right)
	{
		return;
	}
	int mid = part3(a, left, right);
	QuickSort5(a, left, mid - 1);
	QuickSort5(a, mid + 1, right);
}

#include"Stack.h"
// 快速排序 非递归实现
void QuickSortNonR(int* a, int left, int right)
{
	//其实我们实现的这个快速排序，可以看出是一个二叉树的先序遍历，先对总的排好中间位置，再对总的左排好中间数据，再对总的左左排好中间数据，这就可以看成一个
	//先序遍历了，而且先序遍历还有一个好处就是，可以用栈模拟实现
	//因为栈可以做到，取出一个，对它作用，然后再放入它的右左，然后取出的又是左
	//这里我们就先放入头，作用，然后放右，再放左，取出的就是左，再放右左
	//这里我们放入的是区间，所以就两个两个的放就是区间了，先放右区间，再放左区间
	Stack s;
	StackInit(&s);
	//先放第一个总区间
	StackPush(&s, right);
	StackPush(&s, left);
	//开始操作
	while (!StackEmpty(&s))
	{
		//先取出一个区间，反正取出的就是先序遍历
		int head=StackTop(&s);
		StackPop(&s);
		int tail=StackTop(&s);
		StackPop(&s);
		//取出这个区间，然后就作用这个区间，把第一个的那个值位置确定，并返回分界点
		int mid = part3(a, head, tail);
		//然后再把分开的区间入栈，先入右，再入左
		//这样就一定会实现先序的作用
		
		//但是入栈前要看一下，是否是有必要或者是不是一个区间，满足入栈，出栈就不用判断了
		if (tail > mid + 1)
		{
			StackPush(&s, tail);//右的右
			StackPush(&s, mid + 1);//右的左
		}
		if (mid - 1 > head)
		{
			StackPush(&s, mid - 1);//左的右
			StackPush(&s, head);//左的左
		}
	}
	StackDestroy(&s);
}

void my_MergeSort(int* a, int* arr, int left, int right)
{
	if (left >= right)
	{
		return;
	}
	//先用归并排序排好左一半右一半，所以先得分区间
	int mid = (left + right) / 2;
	//分区间分为[left,mid][mid+1,right]就可以了,因为除法是向下取整的，所以这样分刚刚好
	//如果这样分的话，就会变成[left,mid-1][mid,right],这样分的话，如果left与right相邻的话，那么mid又等于left了，右区间又变回来了，是不行的
	//第一个方法来分，面对相邻的就会刚好一边一个，很好
	my_MergeSort(a, arr, left, mid);
	my_MergeSort(a, arr, mid+1, right);
	//对左右区间排好序后，就开始合并
	//合并的话，就是定义三个指针，小的放进去就可以了，很简单
	int head1 = left;
	int head2 = mid+1;
	int tail1 = mid;
	int tail2 = right;
	int i = head1;//指向arr的
	while (head1 <= tail1 && head2 <= tail2)
	{
		if (a[head1] <= a[head2])
		{
			arr[i] = a[head1];
			i++;
			head1++;
		}
		else
		{
			arr[i++] = a[head2++];
		}
	}
	//走出来的话，就一定有一个越界了，直接全赋值另一个就可以了
	while (head1 <= tail1)
	{
		arr[i++] = a[head1++];
	}
	while (head2 <= tail2)
	{
		arr[i++] = a[head2++];
	}
	//别忘了还要拷贝回去原数组，不然的话，白合并了
	memcpy(a+left, arr+left, sizeof(int) * (right - left + 1));
}

//归并排序
void MergeSort(int* a,int n)
{
	//归并排序的话，这个排序的主要思想就是，有两个已经排好序的数组，然后合并为一个有序地数组
	//所以先用归并排序排好左一半，在排好右一半，最后再合并
	//因为这个要排好一半，所以肯定要分区间的，所以再用一个函数就可以了

	//而且合并的话一定会调用额外空间的，所以就在这里开辟一个额外数组吧
	int* arr = (int*)malloc(sizeof(int) * n);
	if (arr == NULL)
	{
		perror("malloc:");
		return;
	}
	my_MergeSort(a, arr, 0, n - 1);
	free(arr);
}

//归并排序非递归实现
void MergeSortNonR1(int* a, int n)
{
	int* arr = (int*)malloc(sizeof(int) * n);
	if (arr == NULL)
	{
		perror("malloc:");
		return;
	}
	//容易得到，归并排序是一个相当于后序遍历的东西，它是先递归，再作用于自己，所以是后序遍历，但是后序遍历是不能用递归实现的，因为取出来放入左右
	//它自己就没了，怎么能做到最后作用呢
	//因为递归都是可以用循环实现的，所以我们就用循环实现吧
	//怎么实现呢，就是分区间呗，先分成最小区间1，挨着挨着归并，再扩大区间，挨着挨着归并
	int gap = 1;//表示归并的两个区间，一个区间多大，或者说已经有gap个元素有序了。这gap个元素在一组，与另外gap个元素合并
	//gap增大
	while (gap < n)
	{
		for (int i = 0; i < n; i+=gap*2)
		{
			//第一个区间[i,i+gap-1]
			int head1 = i;
			int tail1 = i + gap - 1;
			//第二个区间[i+gap,i+2*gap-1]
			int head2 = i + gap;
			int tail2 = i + 2 * gap - 1;
			//开始合并
			int j = head1;
			while (head1 <= tail1 && head2 <= tail2)
			{
				if (a[head1] <= a[head2])
					arr[j++] = a[head1++];
				else
					arr[j++] = a[head2++];
			}
			while (head1 <= tail1)
				arr[j++] = a[head1++];
			while (head2 <= tail2)
				arr[j++] = a[head2++];
			memcpy(a + i, arr + i, sizeof(int) * (2 * gap));//这里我们每合并一次就拷贝一次，具体说明原因现在不说
		}
		gap = gap * 2;
	}
	free(arr);
}

//归并排序非递归实现
void MergeSortNonR2(int* a, int n)
//但是上面那种方法只能适用于数量是可以一直二等分，四等分，8等分的，比如10个数据就不行了,合并没有问题，不管两个区间数量多少，都可以合并，主要的问题是
// ，如果n不太好，区间可能越界，就有可能访问到随机值，所以现在完善一下
{
	int* arr = (int*)malloc(sizeof(int) * n);
	if (arr == NULL)
	{
		perror("malloc:");
		return;
	}
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += gap * 2)
		{
			//第一个区间[i,i+gap-1]
			int head1 = i;
			int tail1 = i + gap - 1;
			//第二个区间[i+gap,i+2*gap-1]
			int head2 = i + gap;
			int tail2 = i + 2 * gap - 1;

			//[i,i+gap-1][i+gap,i+2*gap-1]
			//现在来看，如果现在走到这个区间，假设前面的区间都没有问题，因为区间肯定是越到后面越有可能越界嘛
			//瞪眼一看，只有可能tail1，head2，tail2，越界，head1是不可能越界的，不信你就打印一下
			//如果head2越界，那tail2也越界，其实就不用合并了，因为第二个区间都不存在，而且第一个区间有序，，，而且后后后面的也不用合并了，直接break
			//如果tail1越界也是和head2一样的
			//如果只是tail2越界的话，只需要修改区间就可以了了，把tail2变为n-1，在合并就ok
			//前面两种情况可以合并为head2单独越界的情况
			if (head2 >= n)
			{
				break;
			}
			if (tail2 >= n)
			{
				tail2 = n - 1;
			}
			int j = head1;
			while (head1 <= tail1 && head2 <= tail2)
			{
				if (a[head1] <= a[head2])
					arr[j++] = a[head1++];
				else
					arr[j++] = a[head2++];
			}
			while (head1 <= tail1)
				arr[j++] = a[head1++];
			while (head2 <= tail2)
				arr[j++] = a[head2++];
			//memcpy(a + i, arr + i, sizeof(int) * (2 * gap));//这里我们每合并一次就拷贝一次，具体说明原因现在不说
			//现在说一下原因，如果放在循环外面在集体拷贝的话，比如如果第一次gap=1，n=11，那么到最后一个那里就直接break了，arr里面也没有赋值，就是初始化的0、
			//所以有毛病，要么就一开始就要赋值好，有点小麻烦
			/*memcpy(a + i, arr + i, sizeof(int) * (tail2-head1+1));*///head1会变啊，所以也不行
			//因为前面修改了一下tail2，所以可能不是2*gap个，可能会存在越界情况，所以不行那样的话，就这样改
			memcpy(a + i, arr + i, sizeof(int) * (tail2 - i + 1));
		}
		gap = gap * 2;
	}
	free(arr);
}

//计数排序
void CountSort(int* a, int n)//这个很快的，时间复杂度为o(n+sz)
{
	//计数排序的话就是额外创建一个数组，用来记住原数组的每个元素的个数
	//然后根据计数数组，把原数组进行挨着挨着赋值就可以了，次数为几个就赋几次值
	
	//这个计数数组的大小是根据原数组的最大值与最小值间的差值决定的

	//最小值对应第一个元素的数值，第二小对应第二个元素
	//所以先找原数组最大最小值
	int min = a[0];
	int max = a[0];
	for (int i = 1; i < n; i++)
	{
		if (a[i] < min)
		{
			min = a[i];
		}
		if (a[i] > max)
		{
			max = a[i];
		}
	}
	int sz = max - min + 1;//所以创建的数组大小为这么多个数据这是绝对够了的
	int* tmp = (int*)calloc(sz, sizeof(int));//因为这样可以将数组每个值大小初始化为0
	//开始计数
	for (int i = 0; i < n; i++)
	{
		//a[i]-min就对应了它要计数在原数组的位置
		tmp[a[i] - min]++;
	}
	//开始把a数组赋值成有序地
	int i = 0;
	for (int j = 0; j < sz; j++)
	{
		//j+min就是原数组的值
		while (tmp[j]--)//有几个次数，就有几个相同的数据，就赋值几次，而且这样一定是有序地
		{
			a[i] = j + min;
			i++;
		}
		//tmp[j]为0就不用赋值了
	}
}

void CreateData(char* arr,int n)
{
	FILE* pf = fopen(arr, "w");
	//产生100个数据来排序
	srand((unsigned int)time(NULL));
	while (n--)
	{
		int x = rand() % 1000;
		fprintf(pf, "%d\n", x);
	}
	fclose(pf);
}

//两个文件归并
void SMerge_Sort(char* arr1, char* arr2, char* merge_arr)
{
	FILE* f1 = fopen(arr1, "r");
	FILE* f2 = fopen(arr2, "r");
	FILE* merge_f = fopen(merge_arr, "w");
	int num1;
	int num2;
	int get1 = fscanf(f1, "%d", &num1);
	int get2 = fscanf(f2, "%d", &num2);
	while (get1 != EOF && get2 != EOF)
	{
		if (num1 < num2)
		{
			fprintf(merge_f, "%d\n", num1);
			get1 = fscanf(f1, "%d", &num1);
		}
		else
		{
			fprintf(merge_f, "%d\n", num2);
			get2 = fscanf(f2, "%d", &num2);
		}
	}
	while (get1 != EOF)
	{
		fprintf(merge_f, "%d\n", num1);
		get1 = fscanf(f1, "%d", &num1);
	}
	while (get2 != EOF)
	{
		fprintf(merge_f, "%d\n", num2);
		get2 = fscanf(f2, "%d", &num2);
	}
	fclose(f1);
	fclose(f2);
	fclose(merge_f);//关闭文件也是为了刷新缓冲区
}