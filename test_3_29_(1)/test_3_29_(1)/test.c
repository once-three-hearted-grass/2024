#define _CRT_SECURE_NO_WARNINGS 1
//写一个宏，可以将一个整数的二进制位的奇数位和偶数位交换。
#include<stdio.h>
#define exchange(a) ((a&(0xAAAA))>>1)^((a&(0x5555))<<1) 
//aaaa---->1010 1010 1010 1010得偶数位,其余为0,集体右移一位，在两者按位异或，是一就是一，是0就是0
//5555---->0101 0101 0101 0101得奇数位,其余为0，集体左移一位，
int main()
{
	int a = 1;
	int b = 3;
	printf("%d", exchange(1));
	return 0;
}