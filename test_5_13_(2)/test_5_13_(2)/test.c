#define _CRT_SECURE_NO_WARNINGS 1
//块链串
//块链串定义如下：
#include<stdbool.h>
#include<stdlib.h>
#include<stdio.h>
#define BLOCK_SIZE 4    // 可由用户定义的块大小
#define BLS_BLANK '#'   // 用于空白处的补齐字符

typedef struct _block {
    char ch[BLOCK_SIZE];    //块的数据域
    struct _block* next;    //块的指针域
} Block;

typedef struct {
    Block* head;        // 串的头指针
    Block* tail;        // 串的尾指针
    int len;            // 串的当前长度
} BLString;

//字符串初始化函数：
void blstr_init(BLString* T) {
    T->len = 0;
    T->head = NULL;
    T->tail = NULL;
}
//这些定义已包含在头文件 dsstring.h 中，请实现块链串的子串查找操作：

bool blstr_substr(BLString src, int pos, int len, BLString* sub);
//src为要查找的字符串
//pos为子串开始的下标
//len为子串的长度
//sub在函数调用运行前指向一个已经初始化好的空串，在函数返回时，sub指向串src从第pos个字符起长度为len的子串
//函数查找成功返回true，参数不正确返回 false


bool blstr_substr(BLString src, int pos, int len, BLString* sub) {
    if (len <= 0)
    {
        sub->head = sub->tail = NULL;
        sub->len = 0;
        return false;
    }
    //直接创造一个len长度的字符数组
    char* tmp = (char*)malloc(sizeof(char) * src.len);
    //先将src里的数据拷贝到tmp
    Block* cur = src.head;
    int i = 0;
    while (cur)
    {
        for (int j = 0; j < 4; j++)
        {
            tmp[i] = cur->ch[j];
            i++;
        }
        cur = cur->next;
    }
    //将tmp的内容设置为满足题意
    //将pos+len及其以后位置设为'#'
    for (int i = pos + len; i < src.len; i++)
    {
        tmp[i] = '#';
    }
    //开始拷贝
    int cur_int = pos;
    //从cur开始拷贝到最后
    Block* phead = (Block*)malloc(sizeof(Block));
    cur = phead;
    //先拷贝头
    int sub_len = 0;//记录sub有多长
    for (int i = 0; i < 4; i++)
    {
        sub_len++;
        cur->ch[i] = tmp[cur_int];
        cur_int++;
    }
    //全拷贝
    while (cur_int < src.len && sub_len < len && tmp[cur_int] != '\0')
    {
        Block* newnode = (Block*)malloc(sizeof(Block));
        for (int i = 0; i < 4; i++)
        {
            if (tmp[cur_int] == '\0')//因为不能拷贝\0
            {
                for (; i < 4; i++)
                {
                    newnode->ch[i] = '#';
                    cur_int++;
                    sub_len++;
                }
                break;
            }
            newnode->ch[i] = tmp[cur_int];
            cur_int++;
            sub_len++;
        }
        newnode->next = NULL;
        cur->next = newnode;
        cur = newnode;
    }

    sub->head = phead;
    sub->tail = cur;
    sub->len = sub_len < len ? sub_len : len;//能拷贝多少就多少，因为拷贝的#不算字符，所以取两个的较小值
    sub->len = sub->len < (src.len - pos) ? sub->len : (src.len - pos);
    free(tmp);

    return true;
}

bool blstr_substr(BLString src, int pos, int len, BLString* sub) {

    //这道题主要要画图去理解，才能知道主要思路
    //先找到pos位置吧
    //先根据pos算出过了多少个节点
    int Node_count = (pos + 1) / 4;//因为pos从0开始默认的是第一个字符，举例就知道了
    //再算pos为最后一个节点的位置
    int char_count = (pos + 1) % 4;
    //先做一个准备工作，看这个子串在src中有没有那么长
    if (Node_count * 4 + char_count + len - 1 > src.len)
    {
        (*sub).len = 0;
        (*sub).head = NULL;
        (*sub).tail = NULL;
        return false;
    }
    Block* cur = src.head;
    while (Node_count--)//够长，使劲前进
    {
        cur = cur->next;
    }
    //找到pos位置
    char* pos_position = &cur->ch[char_count - 1];
    char* cur_char_position = pos_position;
    //现在开始创建并初始化sub的头节点
    Block* head_node = (Block*)malloc(sizeof(Block));
    //先初始化前面的'#'
    for (int i = 0; i < char_count - 1; i++)//举例得出循环条件
    {
        head_node->ch[i] = '#';
    }
    //在初始化后面的几个字符
    for (int i = char_count - 1; i < 4; i++)//举例得出循环条件
    {
        head_node->ch[i] = *cur_char_position;
        cur_char_position++;
    }
    //现在开始算sub有多少个整的节点,最后一个节点剩几个字符
    int sub_node_count = (len - (4 - char_count + 1)) / 4;
    int sub_char_count = (len - (4 - char_count + 1)) % 4;
    //挨个节点挨个节点的赋值
    Block* sub_cur = head_node;
    sub_cur->next = NULL;
    while (sub_node_count--)
    {
        cur = cur->next;//去赋值的节点位置
        Block* new_node = (Block*)malloc(sizeof(Block));
        for (int i = 0; i < 4; i++)
        {
            new_node->ch[i] = cur->ch[i];
        }
        new_node->next = NULL;
        //链接新节点到sub上去
        sub_cur->next = new_node;
        sub_cur = new_node;
    }
    //走到这里就剩最后一个节点的赋值了

    cur = cur->next;//去赋值的节点位置
    Block* new_node = (Block*)malloc(sizeof(Block));
    for (int i = 0; i < sub_char_count; i++)
    {
        new_node->ch[i] = cur->ch[i];
    }
    //剩下的赋值为'#'
    for (int i = sub_char_count; i < 4; i++)
    {
        new_node->ch[i] = '#';
    }
    new_node->next = NULL;
    //链接新节点到sub上去
    sub_cur->next = new_node;
    sub_cur = new_node;
    //完善sub
    sub->head = head_node;
    sub->tail = sub_cur;
    sub->len = len;
    return true;
}