#define _CRT_SECURE_NO_WARNINGS 1
//输入描述：
//输入现在的时刻以及要睡的时长k（单位：minute），中间用空格分开。
//输入格式：hour : minute k(如hour或minute的值为1，输入为1，而不是01)
//(0 ≤ hour ≤ 23，0 ≤ minute ≤ 59，1 ≤ k ≤ 109)
//输出描述：
//对于每组输入，输出闹钟应该设定的时刻，输出格式为标准时刻表示法（即时和分都是由两位表示，位数不够用前导0补齐）。
#include <stdio.h>
int main() {
    int a, b, c;
    while (scanf("%d:%d %d", &a, &b, &c) != EOF) {
        int m, n;
        m = c / 60;
        n = c % 60;
        a = a + m;
        b = b + n;
        if (b >= 60)
        {
            a = a + 1;
            b = b % 60;
        }
        if (a >= 24)
        {
            a = a % 24;
        }
        printf("%02d:%02d", a, b);
    }
    return 0;
}