#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<stdio.h>
typedef int ElemType;
// 非零元素结点结构
typedef struct OLNode
{
    int row, col;
    ElemType value;
    struct OLNode* right, * down;
}OLNode, * OLink;
// 十字链表结构
typedef struct
{
    OLink* rowhead, * colhead;
    int rows, cols, nums;
}CrossList, * PCrossList;
//1）实现十字链表的初始化操作：

//其中 L 指向 CrossList 结构，且各成员已被初始化为0；
//A 为 ElemType 类型数组中第一个元素的地址，元素的个数为 m×n 个，按行优先存储（即A[0] 为十字链表第1行第1列的元素；
//A[1] 为第1行第2列的元素，A[n] 为第2行第1列的元素，A[n + 1] 为第2行第2个元素）；
//m 表示十字链表的行数，n 表示十字链表的列数。
//init_cross_list 函数将 ElemType 数组中非0元素保存到十字链表中，函数返回非 0 元素的个数。
//2）实现十字链表的删除操作：
int del_cross_list(PCrossList L, ElemType k);
//其中 L 指向 要处理的 CrossList 结构，k 为要删除的元素；
//del_cross_list 函数删除十字链表中所有值为 k 的结点，并返回删除结点的个数。

int init_cross_list(PCrossList L, const ElemType* A, int m, int n)
{
    L->rows = m;
    L->cols = n;
    L->nums = m * n;
    OLNode* head = (OLNode*)malloc(sizeof(OLNode));
    head->col = 1;
    head->row = 1;
    head->right = NULL;
    head->down = NULL;
    head->value = A[(head->col - 1) * n + (head->row - 1)];
    L->colhead = &head;
    L->rowhead = &head;
    OLNode*cur = head;
    OLNode* pright = NULL;
    OLNode* pdown = NULL;

    for (int i = 0; i < m; i++)
    {
        if (pright != NULL)
        {
            cur= (OLNode*)malloc(sizeof(OLNode));
            cur->col = i + 1;
            cur->row = 1;
            cur->value= A[(cur->col - 1) * n + (cur->row - 1)];
        }
        OLNode* tmp = cur;//记录行的开头
        for (int j = 0; j < n-1; j++)//要少开辟一次，因为头已经开过了
        {
            pright= (OLNode*)malloc(sizeof(OLNode));
            pright->col = cur->col;
            pright->row = cur->row + 1;
            pright->value= A[(pright->col - 1) * n + (pright->row - 1)];
            pright->right = NULL;
            pright->down = NULL;
            pdown= (OLNode*)malloc(sizeof(OLNode));
            pdown->col = cur->col+1;
            pdown->row = cur->row;
            pdown->value = A[(pdown->col - 1) * n + (pdown->row - 1)];
            pdown->right = NULL;
            pdown->down = NULL;
            cur->right = pright;
            cur->down = pdown;
            cur = cur->right;
        }
        cur = tmp->down;
    }
    return m * n;
}

int del_cross_list(PCrossList L, ElemType k)
{
    //要遍历十字链表
    //代码与初始化差不多
    OLNode* cur =*L->colhead;
    OLNode* pright = cur->right;
    OLNode* pdown = cur->down;
    OLNode* left = cur;
    OLNode* up = cur;
    for (int i = 0; i < L->cols; i++)
    {
        OLNode* tmp = cur;//记录行的开头
        for (int j = 0; j < L->rows; j++)
        {
            if (cur->value == k)//删除节点//要找到上一个和前一个
            {
                if (i == 0)
                {
                    if (L->colhead==cur)
                    {
                        L->colhead = &cur->right;
                        L->rowhead = &cur->down;
                        L->nums--;
                        free(cur);
                    }
                    else
                    {
                        left->right = pright;
                        free(cur);
                    }
                }
                else
                {
                    if (j == 0)
                    {

                    }
                }
            }
            cur = pright;
            if (pright != NULL)
            {
                pright = pright->right;
            }

            if(pdown!=NULL)
            pdown = pdown->right;
            if (i != 0)
            {
                up = up->right;
            }
            if (j != 0)
            {
                left = left->right;
            }
        }
        cur = tmp->down;
        if (pdown != NULL)
            pdown = cur->right;
        pright = cur->right;
        left = cur;
        up = cur;
    }

}