#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main() {
    int n;
    while (scanf("%d", &n) != EOF) {
        int i = 0;
        int j = 0;
        for (i = 0; i < n; i++)
        {
            for (j = 0; j < 2 * n; j++)
            {
                if (j == 0 || 2 * i == j || i == n - 1)
                {
                    printf("* ");
                    j++;
                }
                else {
                    printf(" ");
                }

            }
            printf("\n");
        }
    }
    return 0;
}