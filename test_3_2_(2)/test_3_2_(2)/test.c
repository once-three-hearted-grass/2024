#define _CRT_SECURE_NO_WARNINGS 1
//模仿qsort的功能实现一个通用的冒泡排序
#include<stdio.h>
#include<string.h>
void bubble_sort(int* arr, int sz)
{
	//先写一个冒泡排序
	int i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		int j = 0;
		int flag = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
				flag = 1;
			}
		}
		if (flag == 0)
		{
			break;
		}
	}
}
void swap(char* s1, char* s2, size_t width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tmp = *s1;
		*s1 = *s2;
		*s2 = tmp;
		s1++;
		s2++;
	}
}
void bubble_qsort(const void* arr, size_t num,size_t width,int (*cmp)(const void*,const void*))
{
	//再写一个模拟qsort的冒泡排序
	int i = 0;
	for (i = 0; i < num - 1; i++)
	{
		int j = 0;
		int flag = 0;
		for (j = 0; j < num - 1 - i; j++)
		{
			if (cmp(((char*)arr) + j * width, ((char*)arr) + (j + 1) * width)>0)
			{
				swap(((char*)arr) + j * width, ((char*)arr) + (j + 1) * width, width);
				flag = 1;
			}
		}
		if (flag == 0)
		{
			break;
		}
	}
}
void print(int* arr, int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}
int cmp_int(const void* s1, const void* s2)
{
	return *(int*)s1 - *(int*)s2;
}
struct stu
{
	char name[20];
	int age;
};
int cmp_struct_name(const void* s1, const void* s2)
{
	return strcmp(((struct stu*)s1)->name, ((struct stu*)s2)->name);
}
void print_struct(struct stu* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%-10s\t%d\t\n", arr[i].name, arr[i].age);
	}
}
int main()
{
	//int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
	//int sz = sizeof(arr) / sizeof(arr[0]);
	//bubble_sort(arr, sz);
	//bubble_qsort(arr, sz,sizeof(arr[0]),cmp_int);
	//print(arr, sz);
	struct stu arr[3] = { {"zhangsan",48},{"lisi",23},{"wangwu",46} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_qsort(arr, sz, sizeof(arr[0]), cmp_struct_name);
	print_struct(arr, sz);
	return 0;
}