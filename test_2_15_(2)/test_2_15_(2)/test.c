#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现memcpy
#include<stdio.h>
#include<string.h>
//void *memcpy( void *dest, const void *src, size_t count );
void* my_memcpy(void* dest, const void* src, size_t num)
{
	void* start = dest;
	while (num--)
	{
		*(char*)dest = *(char*)src;
		((char*)dest)++;
		((char*)src)++;//记得加括号
		//或者这样++
		/*(char*)dest = (char*)dest + 1;
		(char*)src = (char*)src + 1;
	*/
	}
	return start;
}
int main()
{
	int arr1[20] = { 0 };
	int arr2[] = { 1,2,3,4,5,6,7,8,9,0 };
	float arr3[20] = { 0 };
	float arr4[] = { 1,2,3,4,5,6,7,8,9,0 };
	/*memcpy(arr1, arr2, 20);
	memcpy(arr3, arr4,20);*/
	my_memcpy(arr1+2, arr2, 20);//缺点拷贝arr2的到arr2中去的时候会发生错误，达不到想要的结果，针对有交叉的内存就不好处理了
	//所以用memmove,功能比memcpy还要全一点，可以处理有交叉部分的内存
	//优点相较于strncpy，可以拷贝任意类型了，第三个参数是要拷贝的字节数
	for (int i = 0; i < 10; i++)
	{
		printf("%d ", arr1[i]);
	}
	return 0;
}