#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
//编号为 1 到 n 的 n 个人围成一圈。从编号为 1 的人开始报数，报到 m 的人离开。
//下一个人继续从 1 开始报数。
//n - 1 轮结束以后，只剩下一个人，问最后留下的这个人编号是多少？
//输入：
//5, 2
//复制
//返回值：
//3
//复制
//说明：
//开始5个人 1，2，3，4，5 ，从1开始报数，1->1，2->2编号为2的人离开
//1，3，4，5，从3开始报数，3->1，4->2编号为4的人离开
//1，3，5，从5开始报数，5->1，1->2编号为1的人离开
//3，5，从3开始报数，3->1，5->2编号为5的人离开
//最后留下人的编号是3

//定义结构体
typedef struct ListNode {
    int val;
    struct ListNode* next;
}ListNode;

//定义一个创建结构体的函数
ListNode* SLTBuyNode(int x)
{
    ListNode* newnode = (ListNode*)malloc(sizeof(ListNode));
    newnode->val = x;
    newnode->next = NULL;
    return newnode;
}


//先链结好，再free！！！！！！！！！！！！！！！！！！！！！！

int ysf(int n, int m) {
    //先定义一个环形链表存储1到n的数字
    //依次尾插
    ListNode* head = SLTBuyNode(1);//头节点
    ListNode* tail = head;//尾节点
    for (int i = 2; i <= n; i++)
    {
        ListNode* newnode = SLTBuyNode(i);
        //尾插
        tail->next = newnode;
        tail = tail->next;
    }
    tail->next = head;//成环

    //接下来开始报数，用计数器来表示ta们报的数
    int count = 1;
    //ListNode* prev = tail;
    //ListNode* pcur = head;//head读1
    //ListNode* next = head->next;
    //while (pcur!=next)//因为最后只剩一个人
    //{
    //    if (count == m)//free掉pcur，并把count置为1，pcur向后移,还要连接好
    //    {
    //        free(pcur);
    //        count = 1;//把count置为1
    //        pcur = next;//pcur向后移
    //        if (next == prev)//说明这时只有两个了，又free了一个，就一个了
    //        {
    //            return next->val;
    //        }
    //        next = next->next;//因为next永远不可能为NULL，所以不用判断
    //        //万一next->next==pcur?
    //        prev->next = pcur;//连接好
    //    }
    //    else//count++,pcur,next后移
    //    {
    //        count++;
    //        prev = pcur;//连接好
    //        pcur = next;
    //        next = next->next;
    //    }
    //}

    //其实只需要两个指针就可以了
    ListNode* prev = tail;
    ListNode* pcur = head;//head读1
    while (pcur != pcur->next)//因为最后只剩一个人
    {
        if (count == m)
        {
            prev->next = pcur->next;
            free(pcur);
            pcur = prev->next;
            count = 1;
        }
        else
        {
            count++;
            prev = pcur;//连接好
            pcur = pcur->next;
        }
    }
    return pcur->val;
}
int main()
{
    ysf(5, 2);
    return 0;
}