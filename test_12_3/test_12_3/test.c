#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//
//int main() {
//    int year, month, day;
//    int total = 0;
//    int person;
//    scanf("%d %d %d", &year, &month, &day);
//    for (int i = 2007; i < year; i++) {
//        total += 365;
//        if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0) {
//            total += 1;
//        }
//    }
//    int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//    for (int i = 1; i < month; i++) {
//        if (year > 2007)
//        {
//            total += arr[i-1];
//            if (i == 2 && (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)) {
//                total += 1;
//            }
//        }
//        else if (i > 9)
//        {
//            total += arr[i-1];
//        }
//        else if (i == 9)
//        {
//            total += (day-1);
//        }
//    }
//    if (year == 2007&&month==9)
//    {
//        total += day-1;
//    }
//    else {
//        total += day;
//    }
//    //先算出是星期几
//    int tmp = (total + 6) % 7;
//    printf("day=%d", day);
//    printf("total=%d", total);
//    printf("tmp=%d", tmp);
//    if (tmp == 1)
//    {
//        printf("ALL\n");
//        return 0;
//    }
//
//    person = total % 4;
//    if (person == 0) {
//        printf("B\n");
//    }
//    else if (person == 1) {
//        printf("X\n");
//    }
//    else if (person == 2) {
//        printf("H\n");
//    }
//    else if (person == 3) {
//        printf("P\n");
//    }
//    return 0;
//}


struct Date {
	int year;
	int month;
	int day;
	int people;
	int week;
};

void process(struct Date* start, struct Date* end) {
	while (start->year != end->year || start->month != end->month || start->day != end->day) {

		int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };

		     if (start->year % 4 == 0 && start->year % 100 != 0 || start->year % 400 == 0){
                arr[2] += 1;
            }
		start->day++;
		if (start->day > arr[start->month]) {
			start->day = 1;
			start->month++;
			if (start->month > 12) {
				start->month = 1;
				start->year++;
			}
		}
		if (start->week != 1) {
			start->people++;
			if (start->people > 4) {
				start->people = 1;
			}
		}
		start->week++;
		if (start->week > 7) {
			start->week = 1;
		}
	}

	end->week = start->week;
	end->people = start->people;
}

int main() {

	struct Date start={ 2007,9,1,1,6 };
	int year = 0;
	int month = 0;
	int day = 0;
	scanf("%d%d%d", &year, &month, &day);
	struct Date end = { year,month,day,0,0 };

	process(&start, &end);
	if (end.week == 1) {
		printf("ALL\n");
		return;
	}
	if (end.people == 1) {
		printf("B\n");
	}
	else if (end.people == 2) {
		printf("X\n");
	}
	else if (end.people == 3) {
		printf("H\n");
	}
	else if (end.people == 4) {
		printf("P\n");
	}

	return 0;
}