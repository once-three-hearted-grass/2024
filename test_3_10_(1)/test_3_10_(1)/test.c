#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现strlen
#include<stdio.h>
//size_t my_strlen(const char* arr)
//{
//	//法一：计数器
//	int count = 0;
//	while (*arr)
//	{
//		count++;
//		arr++;
//	}
//	return count;
//}
//size_t my_strlen(const char* arr)
//{
//	//法二：指针相减
//	char* end = arr;
//	while (*end)
//	{
//		end++;
//	}
//	return (end - arr);
//}
size_t my_strlen(const char* arr)
{
	//法三：递归
	if (*arr == '\0')
	{
		return 0;
	}
	else
		return 1 + my_strlen(arr + 1);
}
int main()
{
	char arr[] = "abcdefgh";
	size_t ret = my_strlen(arr);
	printf("%zd\n",ret);
	return 0;
}