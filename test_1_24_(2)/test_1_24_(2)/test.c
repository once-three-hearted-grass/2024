#define _CRT_SECURE_NO_WARNINGS 1
//不允许创建临时变量，交换两个整数的内容
#include<stdio.h>
int main()
{
	int a = 3;
	int b = 5;
	printf("a=%d,b=%d\n", a, b);
	//法一：创建临时变量
	/*int c = 0;
	c = a;
	a = b;
	b = c;*/
	//法二：可能溢出
	/*a = a + b;
	b = a - b;
	a = a - b;*/
	//法三：最难
	a = a ^ b;
	b = a ^ b;
	a = a ^ b;
	printf("a=%d,b=%d\n", a, b);

	return 0;
}