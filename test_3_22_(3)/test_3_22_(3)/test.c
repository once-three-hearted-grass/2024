#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{

	enum ENUM_A
	{
		X1,//0
		Y1,//1
		Z1 = 255,
		A1,//256
		B1,//257
	};
	enum ENUM_A enumA = Y1;
	enum ENUM_A enumB = B1;
	printf("%d %d\n", enumA, enumB);//1 257
	return 0;
}