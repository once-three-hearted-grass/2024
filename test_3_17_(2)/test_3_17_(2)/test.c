#define _CRT_SECURE_NO_WARNINGS 1
#include < stdio.h >
#include<stdlib.h>
#include<math.h>
int my_atoi(const char* arr)
{
	int sum = 0;
	//先找到第一个非空格字符
	while (*arr==' ')
	{
		arr++;
	}
	//将数字字符串转换为真正的数字
	//先求出最后一个数字字符位置
	while (((*arr) >= '0') && ((*arr) <= '9'))
	{
		arr++;
	}
	arr--;
	//此时arr就是最后一个数字字符
	//开始转换
	//pow(底数, 次方);
	int n = 0;
	while (((*arr) >= '0') && ((*arr) <= '9'))
	{
		int mul = pow(10, n);
		n++;
		sum += ((*arr) - 48) * mul;
		arr--;
	}
	return sum;
}
int main()
{
	//int ret = atoi("   12");
	int ret = my_atoi("   12132q");
	//主要功能是将数字字符转换为真正的数字，从第一个非空格字符开始，遇到非数字字符结束
	printf("%d\n",ret);
	return 0;
}