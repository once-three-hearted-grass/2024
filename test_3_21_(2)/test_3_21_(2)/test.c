#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
    unsigned char puc[4];
    struct tagPIM
    {
        unsigned char ucPim1;//一个字节
        unsigned char ucData0 : 1;
        unsigned char ucData1 : 2;
        unsigned char ucData2 : 3;//再一个字节
    }*pstPimData;
    pstPimData = (struct tagPIM*)puc;//将puc强制类型转换，但puc本身还是char*型
    memset(puc, 0, 4);//将四个字节设置为0
    pstPimData->ucPim1 = 2;//00 00 00 10 
    pstPimData->ucData0 = 3;//11-->截断为1
    pstPimData->ucData1 = 4;//100-->截断为00
    pstPimData->ucData2 = 5;//101
    //所以前两个字节总共为00 00 00 10 00 101 00 1 -->16进制-->02 29，后两个字节已经被初始化为0
    printf("%02x %02x %02x %02x\n", puc[0], puc[1], puc[2], puc[3]);//puc加一 ，一次跳过一个字节
    return 0;
}