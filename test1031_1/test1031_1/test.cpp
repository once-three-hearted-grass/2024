#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
void fun(char* str)
{
    char ch = 0;
    int len = strlen(str);
    int i = 0;
    int j = len - 1;
    for (; i <=j; i++,j--) {
        ch = str[i];
        str[i] = str[j];
        str[j] = ch;
    }
    printf("%s\n", str);
}
int main() {
    char str[100] = { 0 };
    scanf("%s", str);

    fun(str);

    return 0;
}