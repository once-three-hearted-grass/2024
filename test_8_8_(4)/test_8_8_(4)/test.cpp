#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<map>
#include<set>
#include<vector>
using namespace std;

class Solution {
public://
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        //我们先将其存入两个set
        //因为这样就可以去重了
        set<int> s1(nums1.begin(), nums1.end());
        set<int> s2(nums2.begin(), nums2.end());
        //set就已经排好序了
        vector<int> ret;
        auto it1 = s1.begin();
        auto it2 = s2.begin();
        while (it1 != s1.end() && it2 != s2.end())
        {
            //小的加加，相同就都加加
            if (*it1 < *it2)
            {
                it1++;
            }
            else if (*it2 < *it1)
            {
                it2++;
            }
            else
            {
                ret.push_back(*it1);
                it1++;
                it2++;
            }
        }
        return ret;
    }
};