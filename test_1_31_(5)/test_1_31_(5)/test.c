#define _CRT_SECURE_NO_WARNINGS 1
//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，
//请编写程序在这样的矩阵中查找某个数字是否存在。
//要求：时间复杂度小于O(N);
#define Row 3
#define Col 5

#include<stdio.h>
int Search(int arr[Row][Col], int row, int col, int key)
{
	int x = row - 1;
	int y = 0;
	while (x>=0&&x<Row&&y>=0&&y<Col)
	{
		if (arr[x][y] > key)//说明此排往右已经不可能有key了
		{
			x--;
		}
		else if(arr[x][y]<key)//说明此列已经不可能有key了
		{
			y++;
		}
		else
		{
			return 1;
		}
	}
	return 0;
}
int main()
{
	int arr[Row][Col] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7} };
	int key = 0;
	printf("请输入您要查找的数字:");
	scanf("%d", &key);
	
	if ((Search(arr, Row, Col, key)) == 1)
	{
		printf("%d在矩阵中存在",key);
	}
	else
	{
		printf("%d在矩阵中不存在",key);
	}
	return 0;
}