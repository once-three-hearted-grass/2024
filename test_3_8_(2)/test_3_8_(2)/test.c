#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//输入一个整数，将这个整数以字符串的形式逆序输出
//程序不考虑负数的情况，若数字含有0，则逆序形式也含有0，如输入为100，则输出为001
int main() 
{
    int a = 0;
    scanf("%d", &a);
    do//万一a刚刚好为0？所以用do.....while
    {
        int tmp = a % 10;
        a = a / 10;
        printf("%c", tmp + 48);
    } while (a != 0);
    return 0;
}