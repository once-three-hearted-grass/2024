#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

class Date
{
public:
    int GetDay(int year, int month)
    {
        int arr[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
        if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
        {
            return 29;
        }
        return arr[month];
    }
    Date(int year, int month, int day)
    {
        _year = year;
        _month = month;
        _day = day;
    }
    Date(Date& d)
    {
        _year = d._year;
        _month = d._month;
        _day = d._day;
    }
    bool operator==(Date& d)
    {
        return _year == d._year &&
            _month == d._month &&
            _day == d._day;
    }
    Date& operator++()
    {
        _day++;
        if (_day > GetDay(_year, _month))
        {
            _month++;
            _day = 1;
            if (_month > 12)
            {
                _month = 1;
                _year++;
            }
        }
        return (*this);
    }
    bool operator>(Date& d)
    {
        if (_year > d._year)
        {
            return true;
        }
        else
        {
            if (_year == d._year)
            {
                if (_month > d._month)
                {
                    return true;
                }
                else
                {
                    if (_month == d._month)
                    {
                        return _day > d._day;
                    }
                }
            }
        }
        return false;
    }
    void operator=(Date& d)
    {
        _year = d._year;
        _month = d._month;
        _day = d._day;
    }
private:
    int _year;
    int _month;
    int _day;
};



int main() {
    int a, b;
    while (cin >> a >> b) { // 注意 while 处理多个 case
        int year1 = a / 10000;
        int month1 = (a % 10000) / 100;
        int day1 = a % 100;
        int year2 = b / 10000;
        int month2 = (b % 10000) / 100;
        int day2 = b % 100;
        cout << year1 << ' ' << month1 << ' ' << day1 << endl;
        cout << year2 << ' ' << month2 << ' ' << day2;
        //定义两个日期类
        Date time1(year1, month1, day1);
        Date time2(year2, month2, day2);
        Date max = time1;
        Date min = time2;
        if (min > max)
        {
            max = time2;
            min = time1;
        }
        int gap = 0;
        while (!(min == max))
        {
            ++min;
            ++gap;
        }
        ++gap;
        cout << gap;
    }
    return 0;
}
// 64 位输出请用 printf("%lld")