#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main() {
    char anti[80];
    while (scanf("%s", anti) != EOF) {
        for (int i = 0; anti[i] != '\0'; i++) {
            if (anti[i] >= 'a' && anti[i] <= 'y') {
                anti[i] = anti[i] + 1;
                printf("%c", anti[i]);
            }
            else if (anti[i] == 'z') {
                printf("a");
            }
            else if (anti[i] >= 'A' && anti[i] <= 'Y') {
                anti[i] = anti[i] + 1;
                printf("%c", anti[i]);
            }
            else if (anti[i] == 'Z') {
                printf("A");
            }
            else {
                printf("%c", anti[i]);
            }
        }
    }

    return 0;
}