#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<stdio.h>
#include<stdbool.h>
//给定一个二叉树，判断它是否是
//平衡二叉树
//平衡二叉树 是指该树所有节点的左右子树的深度相差不超过 1。

  struct TreeNode {
     int val;
     struct TreeNode *left;
     struct TreeNode *right;
  };
 
  int Height(struct TreeNode* root)
  {
      if (root == 0)
      {
          return 0;
      }
      int h1 = Height(root->left);
      int h2 = Height(root->right);
      if (h1 > h2)
      {
          return h1 + 1;
      }
      else
      {
          return h2 + 1;
      }
  }
//bool isBalanced(struct TreeNode* root) {
//    //法一：对每一个节点的左右孩子求高度，看高度是否满足平衡二叉树
//    //每一个节点:先序遍历，求高度再来一个函数
//    
//    //一个树是平衡二叉树，要由三个保证，第一左右子树是平衡二叉树（调用本函数），第二左右子树高度差不超过一（调用height函数），
//    //而左右子树是不是平衡二叉树，就同样思路
//
//    //什么时候停止呢，==NULL就返回TRUE，除此之外判断一样
//    if (root == NULL)//因为NULL无法访问孩子了
//    {
//        return true;
//    }
//    return (abs(Height(root->left) - Height(root->right))<=1) && isBalanced(root->left) && isBalanced(root->right);
//}

  int my_isBalanced(struct TreeNode* root)
  {
      if (root == NULL)
      {
          return 0;
      }
      int h1= my_isBalanced(root->left);//这里可以看出，递归其实就是把这个函数的意义正常在函数中使用
      int h2= my_isBalanced(root->right);//我这个函数的意义就是求树的高度，那么就正常调用这个函数去表示高度就可以了

      if (h1 == -1 || h2 == -1)//看左右子树是不是平衡二叉树
      {
          return -1;
      }
      if (abs(h1 - h2) > 1)
      {
          return -1;
      }
      //走到这里就是平衡二叉树了
      //返回两个高度的最大值
      int max = h1;
      if (h2 > h1)
      {
          max = h2;
      }
      return max + 1;
  }
bool isBalanced(struct TreeNode* root) {
    //改进一下，如果是平衡二叉树就返回树的高度，而树的高度就是真
    //如果不是平衡二叉树就返回-1，因为NULL返回0，因为NULL是平衡二叉树，，，，，，但这样的话就会与bool的真值矛盾
    //所以在创建一个函数吧

    //而是不是平衡二叉树的判断就是两点，第一左右子树是平衡二叉树，第二两个高度之差不超过一
   

    //这个方法的优点就是在计算高度时顺便就判断了是不是平衡二叉树
    if (my_isBalanced(root) == -1)
    {
        return false;
    }
    return true;
}