#define _CRT_SECURE_NO_WARNINGS 1
//输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分。
#include<stdio.h>
void Qiou(int* arr1, int sz)
{
	int arr2[100] = { 0 };
	int i = 0;
	int j = 0;
	int m = sz / 2;
	for (i = 0; i < sz; i++)
	{
		if (i % 2 == 1)
		{
			arr2[j] = arr1[i];
			j++;
		}
		else
		{
			arr2[m] = arr1[i];
			m++;
		}
	}
	for (i = 0; i < sz; i++)
	{
		arr1[i] = arr2[i];
	}
}
void Print(int* arr,int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}
int main()
{
	int arr[] = { 0,1,2,3,4,5,6,7,8,9 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	Qiou(arr, sz);
	Print(arr, sz);
	return 0;
}