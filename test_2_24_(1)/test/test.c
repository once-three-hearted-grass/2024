#define _CRT_SECURE_NO_WARNINGS 1
//写一个函数，求两个整数之和，要求在函数体内不得使用 + 、 - 、 * 、 / 四则运算符号
#include<stdio.h>
int add(int a, int b)
{
	int sum = 0;
	//while (a)//因为可能会有多次进1，所以要来个循环，当为0的时候就不用算了,但是缺点a如果一开始就是0，直接就退出来了，所以先do一次，再看a是否为0
	//{
	//	sum = a ^ b;//没有进位的sum
	//	a = (a & b) << 1;//只有同为1的才会保留1，才会进一，在左移一位，就是进的1，在把这个值与上一个sum^即可
	//	b = sum;
	//}
	do
	{
		sum = a ^ b;//没有进位的sum
		a = (a & b) << 1;//只有同为1的才会保留1，才会进一，在左移一位，就是进的1，在把这个值与上一个sum^即可
		b = sum;
	} while (a);
	return sum;
}
int main()
{
	int a = 0;
	int b = -4;
	printf("a和b的和为:%d", add(a, b));
	return 0;
}