#pragma once
#include<iostream>
#include<assert.h>
using namespace std;

//实现AVL树
template<class K>
struct BSNode
{
	BSNode<K>* _left;
	BSNode<K>* _right;
	BSNode<K>* _parent;
	int _bf;
	K _key;
	typedef BSNode<K> Node;
	BSNode(const K& key)
		:_key(key)
		, _left(nullptr)
		, _right(nullptr)
		,_parent(nullptr)
		,_bf(0)//右子树高度减左子树高度
	{}
};

template<class K>
class BSTree
{
public:
	typedef BSNode<K> Node;

	bool insert(const K& key)
	{
		if (_root == nullptr)
		{
			_root = new Node(key);
			return true;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}
		cur = new Node(key);
		//但要注意_root==nullptr的时候
		if (key > parent->_key)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		//插入完成，开始检查是否平衡
		while (cur!=_root)
		{
			//对于最初的和需要调整_bf的，遵循这个规则，插入右边，_bf就加加，插入左边就减减
			if (parent->_left == cur)
			{
				parent->_bf--;
			}
			else if (parent->_right == cur)
			{
				parent->_bf++;
			}

			//如果parent的bf为0，就不用调整了//因为这个说明子树高度不变
			if (parent->_bf == 0)
			{
				break;
			}

			//如果parent的bf为1，就要调整了，而且就是最初的那样调整//因为这个说明子树长高了
			if (parent->_bf == -1 || parent->_bf == 1)
			{
				cur = parent;
				if (parent->_parent)
				{
					parent = parent->_parent;
				}
			}

			//这样就需要调整了
			if (parent->_bf == -2 || parent->_bf == 2)
			{
				if (parent->_bf == 2 && cur->_bf == 1)//RR型
				{
					RR(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == -1)//LL型
				{
					LL(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)//RL
				{
					RL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)//LR
				{
					LR(parent);
				}
				else
				{
					assert(false);
				}
				break;//因为旋转之后高度降低了，或者说，高度恢复到以前的状态了，所以就平衡了，不用在调节了
				//而删除就不一定了，因为本来就删除高度可能减了，旋转之后又减了，没有恢复到以前的状态，所以还可能多次旋转
			}
		}
		return true;
	}

	//写个中序遍历
	//_root是this指针里面的东西，所以不好搞，不好递归，所以采用调用函数的方法
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	void PreOrder()
	{
		_PreOrder(_root);
		cout << endl;
	}


	//搜索某个数据
	Node* Search(const K& key)
	{
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return cur;
			}
		}
		return nullptr;
	}

	//删除某个数据
	bool Erase(const K& key)
	{
		if (_root == nullptr)
		{
			return false;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				//找到了要删除的数据
				//删除的话就分为三种//
				//第一种就是没有孩子直接删除
				//第二种就是有一个孩子的话就直接连在父亲的后面
				//第三种就是有两个孩子
				//其中第一种和第二种可以合并。因为可以把没有孩子当做空指针的孩子
				if (cur->_left == nullptr)
				{
					if (parent == nullptr)//说明删的是根
					{
						_root = cur->_right;
						delete cur;
						return true;
					}
					//先看cur在parent的左还是右
					if (cur->_key < parent->_key)
					{
						parent->_left = cur->_right;
					}
					else
					{
						parent->_right = cur->_right;
					}
					delete cur;
					return true;
				}
				else if (cur->_right == nullptr)
				{
					if (parent == nullptr)//说明删的是根
					{
						_root = cur->_left;
						delete cur;
						return true;
					}
					if (cur->_key < parent->_key)
					{
						parent->_left = cur->_left;
					}
					else
					{
						parent->_right = cur->_left;
					}
					delete cur;
					return true;
				}
				else//现在左右孩子都不为空
				{
					//如果是头结点，也不用单独考虑，我们这样设计的话
					//我们直接找到右孩子的最小值放在cur，然后删除这个最小值节点就可以了,最小值就是一直往左走就可以了
					Node* min = cur->_right;
					Node* min_parent = cur;
					while (min->_left)
					{
						min_parent = min;
						min = min->_left;
					}
					cur->_key = min->_key;
					//删除min节点
					//因为min的左孩子一定为空，所以很好删除
					if (min_parent == cur)//说明没走
					{
						min_parent->_right = min->_right;
					}
					else
					{
						min_parent->_left = min->_right;
					}
					delete min;
					return true;
				}
			}
		}
		return false;
	}

	bool IsBalance()
	{
		return _IsBalance(_root);
	}

private:
	void RR(Node* parent)
	{
		Node* parent_parent = parent->_parent;
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		//开始链接左右孩子
		parent->_right = subRL;
		subR->_left = parent;
		if (parent_parent)
		{
			if (parent_parent->_left == parent)
			{
				parent_parent->_left = subR;
			}
			else
			{
				parent_parent->_right = subR;
			}
		}
		else//说明parent_parent是nullptr，parent就是_root了
		{
			_root = subR;
		}
		//开始链接父亲节点
		if(subRL)//防止RL的时候调用为空，因为RL的时候调用的parent不是2或-2
		subRL->_parent = parent;
		parent->_parent = subR;
		subR->_parent = parent_parent;
		//开始修改_bf
		parent->_bf = 0;
		subR->_bf = 0;
	}

	void LL(Node* parent)//就是在RR的核心逻辑上right改left，left改right
	{
		Node* parent_parent = parent->_parent;
		Node* subR = parent->_left;
		Node* subRL = subR->_right;
		//开始链接左右孩子
		parent->_left = subRL;
		subR->_right = parent;
		if (parent_parent)
		{
			if (parent_parent->_left == parent)
			{
				parent_parent->_left = subR;
			}
			else
			{
				parent_parent->_right = subR;
			}
		}
		else//说明parent_parent是nullptr，parent就是_root了
		{
			_root = subR;
		}
		//开始链接父亲节点
		if(subRL)
		subRL->_parent = parent;
		parent->_parent = subR;
		subR->_parent = parent_parent;
		//开始修改_bf
		parent->_bf = 0;
		subR->_bf = 0;
	}

	void RL(Node* parent)
	{
		//先记录三个特殊的节点
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int tmp = subRL->_bf;
		//这个的话，就是先将parent->right用LL处理，再用parent用RR处理
		LL(parent->_right);
		RR(parent);
		//但是这样还不行，因为这样处理后全部的_bf都变为了0，这样是不行的，因为parent->right的_bf不为2或-2,所以这样处理会有问题
		if (tmp == 0)//说明它没有孩子，如果有孩子的话，就插入不会不平衡
		{
			parent->_bf = 0;
			subR->_bf = 0;
			subRL->_bf = 0;
		}
		else if (tmp == -1)
		{
			parent->_bf = 0;
			subR->_bf = 1;
			subRL->_bf = 0;
		}
		else if (tmp == 1)
		{
			parent->_bf = -1;
			subR->_bf = 0;
			subRL->_bf = 0;
		}
	}

	void LR(Node* parent)//还是一样的，right改left，left改right
	{
		//先记录三个特殊的节点
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int tmp = subLR->_bf;
		//这个的话，就是先将parent->right用LL处理，再用parent用RR处理
		RR(parent->_left);
		LL(parent);
		//但是这样还不行，因为这样处理后全部的_bf都变为了0，这样是不行的，因为parent->right的_bf不为2或-2,所以这样处理会有问题
		//if (subLR->_bf == 0)//说明它没有孩子，如果有孩子的话，就插入不会不平衡
			//还不能这样讨论，因为已经改为0了，应该提前记录
		if(tmp==0)
		{
			parent->_bf = 0;
			subL->_bf = 0;
			subLR->_bf = 0;
		}
		else if (tmp == -1)
		{
			parent->_bf = 0;
			subL->_bf = 1;
			subLR->_bf = 0;
		}
		else if (tmp == 1)
		{
			parent->_bf = -1;
			subL->_bf = 0;
			subLR->_bf = 0;
		}
	}

	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_InOrder(root->_left);
		cout << root->_key << " ";
		_InOrder(root->_right);
	}

	void _PreOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		cout << root->_key << " ";
		_PreOrder(root->_left);
		_PreOrder(root->_right);
	}
	
	int GetHeight(Node*root)
	{
		if (root == nullptr)
		{
			return 0;
		}
		int h1 = GetHeight(root->_left);
		int h2 = GetHeight(root->_right);
		return h1 > h2 ? (h1 + 1) : (h2 + 1);
	}
	
	bool _IsBalance(Node* root)
	{
		if (root == nullptr)
		{
			return true;
		}
		//判断一个树是不是平衡树，先看这个节点的左右子树高度差绝对值是不是小于2，而且等于_bf
		//然后就是左右子树均为平衡树
		int h1 = GetHeight(root->_left);
		int h2 = GetHeight(root->_right);
		int dif = h2 - h1;
		int BigDif = abs(dif);
		if (BigDif >= 2 )
		{
			cout << "hahah" << endl;
			return false;
		}
		if (dif != root->_bf)
		{
			cout << root->_bf << endl;
			cout << dif << endl;
			cout << root->_key << endl;
			cout << "hhhh" << endl;
			return false;
		}
		return _IsBalance(root->_left) && _IsBalance(root->_right);
	}

	Node* _root = nullptr;
};
