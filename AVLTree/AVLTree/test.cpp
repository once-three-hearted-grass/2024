#define _CRT_SECURE_NO_WARNINGS 1

#include"AVL.h"
int main()
{
	BSTree<int> bs;
	int arr[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15,12,34,67,34 };
	//int arr[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16,14 };
	for (auto x : arr)
	{
		bs.insert(x);
		bs.PreOrder();
	}
	bs.InOrder();
	bs.PreOrder();
	//检查我们的平衡树的插入是否正常除了先序遍历的方法，我们还可以写一个程序的方法
	cout << bs.IsBalance() << endl;
	return 0;
}