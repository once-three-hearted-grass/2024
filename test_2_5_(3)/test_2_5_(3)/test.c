#define _CRT_SECURE_NO_WARNINGS 1
//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//编写一个函数找出这两个只出现一次的数字。
//例如：
//有数组的元素是：1，2，3，4，5，1，2，3，4，6
//只有5和6只出现1次，要找出5和6.
//法一：没有任何思考的逻辑
//简单粗暴
#include<stdio.h>
int main()
{
	int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	for (int i = 0; i < sz; i++)
	{
		int flag = 0;
		for (int j = 0; j < sz; j++)
		{
			if (arr[i] == arr[j] && i != j)
			{
				flag = 1;
			}
		}
		if (flag == 0)
		{
			printf("%d是单身狗\n",arr[i]);
		}
	}
	return 0;
}