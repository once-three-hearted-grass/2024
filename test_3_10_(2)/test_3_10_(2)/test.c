#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现strcpy
#include<stdio.h>
#include<string.h>
#include<assert.h>
//char* my_strcpy(char* destination, const char* source)
//{
//	char* ret = destination;
//	assert(destination && source);
//	while ((*source) != '\0')
//	{
//		*destination = *source;
//		destination++;
//		source++;
//	}
//	*destination = *source;
//	return ret;
//}
char* my_strcpy(char* destination, const char* source)
{
	char* ret = destination;
	assert(destination && source);
	while (*(destination++) = *(source++))
		;
	return ret;
}
int main()
{
	char arr1[] = "abcdef";
	char arr2[20] = "xxxxxxxxxx";
	//char* ret = strcpy(arr2, arr1);
	char* ret = my_strcpy(arr2, arr1);//连\0也要拷贝
	printf("%s", arr2);
	return 0;
}