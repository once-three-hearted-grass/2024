#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//int main()
//{
//	int* p1 = new int(1);
//	cout << (*p1) << endl;
//	delete p1;
//	return 0;
//}

//int main()
//{
//	int* p1 = new int[10] {1,2,3,4,5};
//	for (int i = 0; i < 10; i++)
//	{
//		cout << p1[i] << ' ';
//	}
//	delete[] p1;
//	return 0;
//}
//
//class A
//{
//public:
//	A(int val = 1)
//	{
//		cout << "A(int val=1)" << endl;
//	}
//	A(int a, int b)
//	{
//		cout << "A(int a, int b)" << endl;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _val;
//};

//int main()
//{
//	A* p = new A(1);
//	A* p1 = new A[10]{ 1,2,3,4,{2,3} };
//	delete p;
//	delete[] p1;
//	return 0;
//}


//int main()
//{
//	int* p = (int*)operator new(sizeof(int));
//	*p = 1;
//	cout << (*p) << endl;
//	operator delete(p);
//	return 0;
//}

//int main()
//{
//	int* p = new int;
//	free(p);
//	return 0;
//}

//int main()
//{
//	int* p = new int[10];
//	delete p;
//	return 0;
//}


class A
{
public:
	A(int val = 1)
	{
		cout << "A(int val=1)" << endl;
	}
	A(int a, int b)
	{
		cout << "A(int a, int b)" << endl;
	}
	~A()
	{
		cout << "~A()" << endl;
	}
private:
	int _val;
};
int main()
{
	A* p = (A*)operator new (sizeof(A));
	new(p)A(10,2);
	p->~A();
	operator delete(p);
	return 0;
}











