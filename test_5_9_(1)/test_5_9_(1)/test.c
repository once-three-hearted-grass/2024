#define _CRT_SECURE_NO_WARNINGS 1
//顺序表 删除重复
//编写算法，在一非递减的顺序表L中，删除所有值相等的多余元素。要求时间复杂度为O(n)，空间复杂度为O(1)。
//函数原型如下：
void del_dupnum(SeqList* L);
//相关定义如下：
struct _seqlist {
    int elem[20];
    int last;
};
typedef struct _seqlist SeqList;

void del_dupnum(SeqList* L) {
    //两个指针，一个tail（新数组末尾），一个cur（原数组末尾），初始时，cur为1，tail为0
    //如果cur==tail，则cur++，如果不相等，就把cur的值拷贝给cur的下一位，因为要保留相等元素的一个
    int last = L->last;
    int tail = 0;
    int cur = 1;
    //因为非递减，所以相同元素都在一起
    while (cur <=last)
    {
        if (L->elem[cur] == L->elem[tail])
        {
            cur++;
            L->last--;
        }
        else
        {
            L->elem[tail + 1] = L->elem[cur];
            cur++;
            tail++;
        }
    }
}
#include<stdio.h>
int main()
{
	return 0;
}