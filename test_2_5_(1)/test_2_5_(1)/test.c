#define _CRT_SECURE_NO_WARNINGS 1
//将课堂上所讲使用函数指针数组实现转移表的代码，自己实践后，并提交代码到答案窗口。
#include<stdio.h>
#include<stdlib.h>
void menu()
{
	printf("*************************\n");
	printf("*** 0. exit    1. add ***\n");
	printf("*** 2. sub     3. mul ***\n");
	printf("*** 4. dIv***************\n");
	printf("*************************\n");
}
int add(int x, int y)
{
	return x + y;
}
int sub(int x, int y)
{
	return x - y;
}
int mul(int x, int y)
{
	return x * y;
}
int dIv(int x, int y)
{
	return x / y;
}
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int get = 0;
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 0:
//			printf("退出计算机\n");
//			break;
//		case 1:
//			printf("请输入操作数:");
//			scanf("%d %d", &x, &y);
//			get=add(x, y);
//			printf("结果是:%d\n", get);
//			break;
//		case 2:
//			printf("请输入操作数:");
//			scanf("%d %d", &x, &y);
//			get=sub(x, y);
//			printf("结果是:%d\n", get);
//			break;
//		case 3:
//			printf("请输入操作数:");
//			scanf("%d %d", &x, &y);
//			get=mul(x, y);
//			printf("结果是:%d\n", get);
//			break;
//		case 4:
//			printf("请输入操作数:");
//			scanf("%d %d", &x, &y);
//			get=dIv(x, y);
//			printf("结果是:%d\n", get);
//			break;
//		}
//	} while (input);
//	return 0;
//}




//法二:定义一个函数指针数组,这就是转移表
//首先是个数组，所以arr[4]
//其次数组每个元素类型为函数指针，int(*)(int ,int)
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int get = 0;
//	int (*arr[5])(int, int) = { 0,add,sub,mul,dIv };
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 0:
//			printf("退出计算机\n");
//			break;
//		case 1:
//			printf("请输入操作数:");
//			scanf("%d %d", &x, &y);
//			get = arr[input](x, y);
//			printf("结果是:%d\n", get);
//			break;
//		case 2:
//			printf("请输入操作数:");
//			scanf("%d %d", &x, &y);
//			get = arr[input](x, y);
//			printf("结果是:%d\n", get);
//			break;
//		case 3:
//			printf("请输入操作数:");
//			scanf("%d %d", &x, &y);
//			get = arr[input](x, y);
//			printf("结果是:%d\n", get);
//			break;
//		case 4:
//			printf("请输入操作数:");
//			scanf("%d %d", &x, &y);
//			get = arr[input](x, y);
//			printf("结果是:%d\n", get);
//			break;
//		}
//	} while (input);
//	return 0;
//}

//法三：再次优化
void logic(int (*arr)(int, int))
{
	int x = 0;
	int y = 0;
	printf("请输入操作数:");
	scanf("%d %d", &x, &y);
	printf("结果是:%d\n", arr(x,y));
	system("pause");
	system("cls");

}
//int main()
//{
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 0:
//			printf("退出计算机\n");
//			break;
//		case 1:
//			logic(add);
//			break;
//		case 2:
//			logic(sub);
//			break;
//		case 3:
//			logic(mul);
//			break;
//		case 4:
//			logic(dIv);
//			break;
//		}
//	} while (input);
//	return 0;
//}

int main()
{
	int input = 0;
	do
	{
		menu();
		printf("请选择:");
		scanf("%d", &input);
		int (*arr[5])(int, int) = { 0,add,sub,mul,dIv };
		
			if (input == 0)
			{
				printf("退出计算机\n");
			}
			else if (input >= 1 && input <= 4)
			{
				logic(arr[input]);
			}
			else
			{
				printf("输入错误\n");
			}

		} while (input);
	return 0;
}
