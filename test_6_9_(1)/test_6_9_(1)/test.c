#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
 struct TreeNode {
     int val;
     struct TreeNode *left;
     struct TreeNode *right;
 };
 
 //给定一个二叉树的根节点 root ，返回 它的 中序 遍历 。
 /**
  * Note: The returned array must be malloced, assume caller calls free().
  */

 int CountTree(struct TreeNode* root)
 {
     if (root == NULL)
     {
         return 0;
     }
     return 1 + CountTree(root->left) + CountTree(root->right);
 }
void my_inorderTraversal(struct TreeNode* root, int* arr, int* pi)
 {
     if (root == NULL)
     {
         return;
     }
     my_inorderTraversal(root->left,arr,pi);
     arr[*pi] = root->val;
     (*pi)++;
     my_inorderTraversal(root->right, arr, pi);
 }
int* inorderTraversal(struct TreeNode* root, int* returnSize) {
    //先统计有多少个节点
    int n = CountTree(root);
    *returnSize = n;
    int* arr = (int*)malloc(sizeof(int)*n);
    int i = 0;
    my_inorderTraversal(root, arr, &i);
    return arr;
}