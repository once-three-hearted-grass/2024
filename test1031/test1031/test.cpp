#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int main() {
    int n = 0;
    char c = 0;
    scanf("%d", &n);//转化几个小写字母
    getchar();
    for (int i = 0; i < n; i++) {
        scanf("%c", &c);
        printf("%c\n", c - 32);
        getchar();
    }
    return 0;
}