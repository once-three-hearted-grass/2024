#define _CRT_SECURE_NO_WARNINGS 1
//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//给定s1 = abcd和s2 = ACBD，返回0.
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC
#include<stdio.h>
#include<string.h>
int Judge_spin(char* arr1, char* arr2, int sz1, int sz2)
{
	if (sz1 != sz2)
	{
		return 0;
	}
	//法二:运用strstr，strstr(arr1,arr2)在arr1中找，看arr2是否是arr1的子列，若是，则返回arr1中
	//arr2首次出现的字符所对应的地址，若不是子列，则返回NULL
	strcat(arr1, arr1);
	//这样拼接而来的arr1，若arr2真是旋转而来，则一个是它的子列
	if (strstr(arr1, arr2) != NULL)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
//int Judge_spin(char* arr1, char* arr2, int sz1, int sz2)
//{
//	if (sz1 != sz2)
//	{
//		return 0;
//	}
//	//法一:一个一个转，一个一个判断
//	int i = 0;
//	for (; i < sz1; i++)
//	{
//		char tmp = arr1[0];
//		int j = 0;
//		for (; j < sz1-1; j++)
//		{
//			arr1[j] = arr1[j + 1];
//		}
//		arr1[j] = tmp;//转了一个单位
//		if (strcmp(arr1, arr2) == 0)//字符挨个挨个比较，大于返回1，小于返回0
//		{
//			return 1;
//		}
//	}
//	return 0;
//}
int main()
{
	char arr1[256] = {0};
	char arr2[256] = { 0 };
	printf("请输入arr1字符串:");
	scanf("%s", arr1);
	getchar();
	int sz1 = strlen(arr1);
	printf("请输入arr2字符串:");
	scanf("%s", arr2);
	int sz2 = strlen(arr2);
	if (Judge_spin(arr1, arr2, sz1, sz2) == 1)
	{
		printf("arr2就是由arr1旋转而来的");
	}
	else
	{
		printf("arr2不是由arr1旋转而来的");
	}
	return 0;
}