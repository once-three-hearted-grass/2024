#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<stdbool.h>
#include<stdio.h>
typedef int STDataType;
typedef struct Stack
{
	STDataType* a;
	int top;
	int capacity;
}Stack;

//初始化栈
void StackInit(Stack* ps)
{
	ps->a = NULL;
	ps->capacity = ps->top = 0;//top表示栈中元素个数，也表示栈顶元素的下一个位置
}

//入栈
void StackPush(Stack* ps, STDataType x)
{
	assert(ps);
	if (ps->top == ps->capacity)
	{
		int new_capacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		STDataType* p = (STDataType*)realloc(ps->a, sizeof(STDataType) * new_capacity);
		if (p == NULL)
		{
			perror("StackPush:realloc:");
			return;
		}
		ps->a = p;
	}
	//入栈就是直接在top位置放数据就是了
	ps->a[ps->top] = x;
	ps->top++;
}

//出栈 
void StackPop(Stack* ps)//直接少最后一个
{
	assert(ps);
	assert(ps->top);
	ps->top--;
}

//获取栈顶元素
STDataType StackTop(Stack* ps)
{
	assert(ps);
	assert(ps->top);
	return ps->a[ps->top - 1];
}

//获取栈有效元素个数
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->top;
}

//判断栈是否为空
bool StackEmpty(Stack* ps)
{
	return ps->top == 0;
}

//销毁栈
void StackDestroy(Stack* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->top = ps->capacity = 0;
}

//如何用两个栈实现一个队列呢，一个栈放数据，取数据时，先把前n-1个放入另一个栈，取完最后一个数据就把这些n-1个数据放回第一个栈,就这样保持
typedef struct {
	Stack s1;
	Stack s2;
} MyQueue;


MyQueue* myQueueCreate() {
	MyQueue* q = (MyQueue*)malloc(sizeof(MyQueue));
	StackInit(&q->s1);
	StackInit(&q->s2);
	return q;
}

void myQueuePush(MyQueue* obj, int x) {
	//就用第一个栈放数据
	StackPush(&obj->s1, x);
}

int myQueuePop(MyQueue* obj) {
	//出数据稍微麻烦一点
	//先把第一个栈的除了最后一个数据放在第二个栈
	while (StackSize(&obj->s1) > 1)
	{
		STDataType x=StackTop(&obj->s1);
		StackPop(&obj->s1);
		StackPush(&obj->s2, x);
	}
	//将第一个栈的最后一个元素就是要返回的元素存起来
	STDataType ret = StackTop(&obj->s1);
	StackPop(&obj->s1);
	//将第二个栈的元素全部放回第一个元素
	while (StackSize(&obj->s2) > 0)
	{
		STDataType x = StackTop(&obj->s2);
		StackPop(&obj->s2);
		StackPush(&obj->s1, x);
	}
	return ret;
}

int myQueuePeek(MyQueue* obj) {//返回队列开头的元素
	//直接返回第一个栈的栈底就可以了
	//因为栈是用数组实现的，所以很简单
	//栈底就是数组的第一个元素
	return obj->s1.a[0];
}

bool myQueueEmpty(MyQueue* obj) {
	//直接看第一个栈是否为空就可以了
	return StackEmpty(&obj->s1);
}

void myQueueFree(MyQueue* obj) {
	//先free两个栈，再free所创建的obj
	StackDestroy(&obj->s1);
	StackDestroy(&obj->s2);
	free(obj);
}