#define _CRT_SECURE_NO_WARNINGS 1
//给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
//不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
//元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
//输入：nums = [3, 2, 2, 3], val = 3
//输出：2, nums = [2, 2]
#include<stdio.h>
int removeElement(int* nums, int numsSize, int val) {
	//定义两个指针，一个cur，一个tail
	//tail的值为val，就++，不为val，就把值赋给cur，cur++，tail++
	int cur = 0;
	int tail = 0;
	while (tail < numsSize)
	{
		if (nums[tail] == val)
		{
			tail++;
		}
		else
		{
			nums[cur] = nums[tail];
			cur++;
			tail++;
		}
	}
	//这时cur的大小就是数组最后的长度
	return cur;
}
int main()
{

	return 0;
}