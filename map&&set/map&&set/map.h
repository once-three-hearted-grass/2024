#pragma once

namespace bit
{
	template<class K,class V>
	class map
	{
	public:

		struct KeyOf
		{
			K operator()(const pair<const K, V>& kv)
			{
				return kv.first;
			}
		};

		//在这里也要写上const，因为k和v传过来，如果没写const，那么传进去，在传进去，里面的pair就是pair<K, V>，所以就很混乱
		typedef bit::BRIterator<pair<const K, V>, pair<const K, V>&, pair<const K, V>*> iterator;
		typedef bit::BRIterator<pair<const K, V>, const pair<const K, V>&,const pair<const K, V>*> const_iterator;

		//K KeyOf(const pair<K, V>& kv)//函数定义在后面，前面要是用的话就要提前声明，但是这里比较麻烦，因为有两个
		//{
		//	return kv.first;
		//}

		void inorder()
		{
			_t.InOrder();
		}

		pair<iterator, bool> insert(const pair<K, V>& kv)
		{
			return _t.Insert(kv);
		}

		iterator begin()
		{
			return _t.Begin();
		}

		iterator end()
		{
			return _t.End();
		}

		const_iterator begin()const
		{
			return _t.Begin();
		}

		V& operator[](const K& key)
		{
			//要调用insert
			pair<iterator, bool> it=insert(make_pair(key, V()));//如果写pair的话就要说明类型,make_pair就不用说明类型了
			//it.first这个是迭代器，在解引用就会得到pair<K, V>,或者>
			return it.first->second;//
		}

		const_iterator end()const
		{
			return _t.End();
		}
	private:
		BRTree<const K, pair<const K, V>, KeyOf> _t;//搜索树是不能修改key的
	};

	void test_map()
	{
		map<string, string> m;
		m.insert(make_pair("eeeee", "aaaa"));
		m.insert({ "cccc","aaaa" });
		m.insert({ "aaaa","aaaa" });
		m.insert({ "ddddd","aaaa" });
		m.insert({ "aaaa","aaaa" });
		m["mmmmm"] = "mmmmmm";
		m["kkkkkkk"];
		m["mmmmm"] = "2222";
		m.inorder();
		for (auto x : m)
		{
			//x.first += 'a';
			cout << x.first << ' ';
		}
		cout << endl;
		auto it = m.begin();
		while (it != m.end())
		{
			cout << it->first << " " << it->second << endl;
			++it;
		}
		cout << endl;
		/*it = m.end();
		while (it != m.begin())
		{
			--it;
			cout << (*it).first << " ";
		}*/
	}
}
