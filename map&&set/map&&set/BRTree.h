﻿
namespace bit
{
	enum color
	{
		Red,
		Black
	};

	//实现AVL树
	template<class T>//T可能是key也可能是pair
	struct BRNode
	{
		BRNode<T>* _left;
		BRNode<T>* _right;
		BRNode<T>* _parent;
		T _kv;
		color _color;
		BRNode(const T& kv)
			:_kv(kv)
			, _left(nullptr)
			, _right(nullptr)
			, _parent(nullptr)
			, _color(Red)
		{}
	};

	template<class T,class l1,class l2>
	struct BRIterator
	{
		typedef BRIterator<T,l1,l2> self;

		typedef BRNode<T> Node;

		BRIterator(Node*tmp,Node*root)//C++中，‌构造函数的名字不可以是typedef出来的。‌所以不能用self的名字来构造
			:_node(tmp)
			,_root(root)
		{}

		self&operator++()
		{
			//找任意一个节点在中序遍历中的下一个节点，分为两种情况，有右子树，和没有右子树
			if (_node->_right)
			{
				//就找右子树的最左边那个节点
				Node* cur = _node->_right;
				while (cur->_left)
				{
					cur = cur->_left;
				}
				_node = cur;
				return *this;
			}
			else
			{
				//从该节点开始定义父节点和子节点，如果子节点是父节点的右孩子就继续往上找，左孩子就退出，并返回父节点
				Node* cur = _node;
				Node* parent = _node->_parent;
				while (parent&& parent->_right == cur)//防止到了最后头结点
				{
					cur = parent;
					parent = parent->_parent;
				}
				_node = parent;
				return *this;
			}
		}

		self& operator--()//这个与++就完全相反了，因为++是左中右，那么减减就是右中左,只需要左换右，右换左就可以了
		{
			//但万一_node是nullptr呢
			if (_node == nullptr)
			{
				//直接让Node=end就可以了，就是最右边那个
				//所以还要传入root
				Node* cur = _root;
				while (cur && cur->_right)
				{
					cur = cur->_right;
				}
				_node = cur;
				return *this;
			}
			if (_node->_left)
			{
				//就找右子树的最左边那个节点
				Node* cur = _node->_left;
				while (cur&&cur->_right)
				{
					cur = cur->_right;
				}
				_node = cur;
				return *this;
			}
			else
			{
				Node* cur = _node;
				Node* parent = _node->_parent;
				while (parent && parent->_left == cur)
				{
					cur = parent;
					parent = parent->_parent;
				}
				_node = parent;
				return *this;
			}
		}

		l1 operator*()
		{
			return _node->_kv;
		}

		l2 operator->()
		{
			return &_node->_kv;
		}

		bool operator==(const self& se)
		{
			return _node == se._node;
		}

		bool operator!=(const self& se)//最好都写上const，因为end函数出来的是临时变量
		{
			return _node != se._node;
		}

		Node* _node;
		Node* _root;
	};

	template<class K, class T,class KeyOfT>
	class BRTree
	{
	public:
		typedef BRNode<T> Node;
		typedef BRIterator<T,T&,T*> self;
		typedef BRIterator<T,const T&,const T*> const_self;

		pair<self,bool> Insert(const T& kv)
		{
			if (_root == nullptr)
			{
				_root = new Node(kv);
				_root->_color = Black;
				return make_pair(self(_root,_root), true);
			}
			KeyOfT kof;
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (kof(kv) > kof(cur->_kv))//但是这里针对pair的比较又不行了，所以我们要改造函数//因为比较的是K
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (kof(kv) < kof(cur->_kv))
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return make_pair(self(cur,_root), false);
				}
			}
			cur = new Node(kv);
			Node* ret = cur;
			//但要注意_root==nullptr的时候
			if (kof(kv) > kof(parent->_kv))
			{
				parent->_right = cur;
				cur->_parent = parent;
			}
			else
			{
				parent->_left = cur;
				cur->_parent = parent;
			}

			//插入完成，开始检查是否平衡
			while (parent && parent->_color == Red)//红黑树总共有四个特性，
				//左小于根小于右（左跟右），一条路黑相同（黑路同），根为黑（根叶黑），然后就是连续的不为红（不红红）
			//插入只会不红红特性
			{
				//先找到叔叔parent->_color==Red说明一定有爷
				Node* Grandparent = parent->_parent;
				if (parent == Grandparent->_right)
				{
					//叔叔在左边
					Node* uncle = Grandparent->_left;
					//开始讨论叔叔为红还是黑//叔叔为空默认为黑
					if (uncle && uncle->_color == Red)
					{
						//只需要将叔叔父亲变黑，爷爷变红就可以了
						uncle->_color = Black;
						parent->_color = Black;
						Grandparent->_color = Red;
						//然后将Grandparent看为cur继续循环
						cur = Grandparent;
						parent = cur->_parent;
						//当parent为红就继续循环
						//如果为空，说明cur为根，所以直接退出循环变黑，并增加条件parent不为空
					}
					else if (!uncle || uncle->_color == Black)//叔叔为空或者为黑//而且叔叔为黑这种情况只可能是由叔叔为红变过来的，不可能一开始插入就变成这样了
						//因为不满足黑路同，所以cur还有子树，后面还有黑节点
					{
						//接下来就来看是什么类型的旋转
						if (cur == parent->_right)//RR
						{
							RR(Grandparent);
							//然后再让原来的爷爷变红，原来的父亲变黑，原来的父亲就变为爷爷
							Grandparent->_color = Red;
							parent->_color = Black;
							//因为原来的父亲就变为爷爷，爷爷变黑，所以黑色节点每条路还是相同的，反正就是旋转之后就已经满足了红黑树了
							//不需要在继续循环了
							break;
						}
						else//RL
						{
							LL(parent);
							RR(Grandparent);
							//cur变黑，Grandparent变红
							cur->_color = Black;
							Grandparent->_color = Red;
							break;
						}
					}
				}
				else
				{
					Node* uncle = Grandparent->_right;
					//开始讨论叔叔为红还是黑//叔叔为空默认为黑
					if (uncle && uncle->_color == Red)
					{
						//只需要将叔叔父亲变黑，爷爷变红就可以了
						uncle->_color = Black;
						parent->_color = Black;
						Grandparent->_color = Red;
						//然后将Grandparent看为cur继续循环
						cur = Grandparent;
						parent = cur->_parent;
						//当parent为红就继续循环
						//如果为空，说明cur为根，所以直接退出循环变黑，并增加条件parent不为空
					}
					else if (!uncle || uncle->_color == Black)//叔叔为空或者为黑//而且叔叔为黑这种情况只可能是由叔叔为红变过来的，不可能一开始插入就变成这样了
						//因为不满足黑路同，所以cur还有子树，后面还有黑节点
					{
						//接下来就来看是什么类型的旋转
						if (cur == parent->_left)//LL
						{
							LL(Grandparent);
							//然后再让原来的爷爷变红，原来的父亲变黑，原来的父亲就变为爷爷
							Grandparent->_color = Red;
							parent->_color = Black;
							//因为原来的父亲就变为爷爷，爷爷变黑，所以黑色节点每条路还是相同的，反正就是旋转之后就已经满足了红黑树了
							//不需要在继续循环了
							break;
						}
						else//LR
						{
							RR(parent);
							LL(Grandparent);
							//cur变黑，Grandparent变红
							cur->_color = Black;
							Grandparent->_color = Red;
							break;
						}
					}
				}
			}
			_root->_color = Black;
			return make_pair(self(ret, _root), true);
		}

		//搜索某个数据
		Node* Search(const T& key)
		{
			KeyOfT kof;
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (kof(key) > kof(cur->_kv))
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (kof(key) < kof(cur->_kv))
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return cur;
				}
			}
			return nullptr;
		}

		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}

		//迭代器
		self Begin()
		{
			//找到最左边的节点就可以了
			Node* cur = _root;
			while (cur&&cur->_left)
			{
				cur = cur->_left;
			}
			return self(cur,_root);
		}

		self End()
		{
			return self(nullptr,_root);//我们以空指针作为end
		}

		const_self Begin()const
		{
			//找到最左边的节点就可以了
			Node* cur = _root;
			while (cur && cur->_left)
			{
				cur = cur->_left;
			}
			return const_self(cur, _root);
		}

		const_self End()const
		{
			return const_self(nullptr, _root);//我们以空指针作为end
		}

	private:
		void RR(Node* parent)
		{
			Node* parent_parent = parent->_parent;
			Node* subR = parent->_right;
			Node* subRL = subR->_left;
			//开始链接左右孩子
			parent->_right = subRL;
			subR->_left = parent;
			if (parent_parent)
			{
				if (parent_parent->_left == parent)
				{
					parent_parent->_left = subR;
				}
				else
				{
					parent_parent->_right = subR;
				}
			}
			else//说明parent_parent是nullptr，parent就是_root了
			{
				_root = subR;
			}
			//开始链接父亲节点
			if (subRL)//防止RL的时候调用为空，因为RL的时候调用的parent不是2或-2
				subRL->_parent = parent;
			parent->_parent = subR;
			subR->_parent = parent_parent;
		}

		void _InOrder(Node* root)
		{
			if (root == nullptr)
			{
				return;
			}
			_InOrder(root->_left);
			KeyOfT k;
			cout << k(root->_kv)<< " ";
			//cout << root->_kv.first;//不能这样因为不知道kv是个啥
			_InOrder(root->_right);
		}

		void LL(Node* parent)//就是在RR的核心逻辑上right改left，left改right
		{
			Node* parent_parent = parent->_parent;
			Node* subR = parent->_left;
			Node* subRL = subR->_right;
			//开始链接左右孩子
			parent->_left = subRL;
			subR->_right = parent;
			if (parent_parent)
			{
				if (parent_parent->_left == parent)
				{
					parent_parent->_left = subR;
				}
				else
				{
					parent_parent->_right = subR;
				}
			}
			else//说明parent_parent是nullptr，parent就是_root了
			{
				_root = subR;
			}
			//开始链接父亲节点
			if (subRL)
				subRL->_parent = parent;
			parent->_parent = subR;
			subR->_parent = parent_parent;
		}

		Node* _root = nullptr;
	};
}