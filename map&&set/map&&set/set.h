#pragma once

namespace bit
{
	template<class K>
	class set
	{
	public:
		struct KeyOf
		{
			K operator()(const K& k)
			{
				return k;
			}
		};

		//typedef bit::BRIterator< K, const K&, const K*> iterator;
		//typedef bit::BRIterator<K,const const K&,const const K*> const_iterator;
		typedef typename bit::BRTree< K, const K, KeyOf>::const_self const_iterator;//这个要写typename，因为是在未实例化的模版里面用的变量
		typedef typename bit::BRTree< K, const K, KeyOf>::self iterator;
		void inorder()
		{
			_t.InOrder();
		}

		auto search(const K& k)
		{
			return _t.Search(k);
		}

		pair<iterator, bool> insert(const K& k)
		{
			return _t.Insert(k);
		}

		iterator begin()
		{
			return _t.Begin();
		}

		iterator end()
		{
			return _t.End();
		}

		const_iterator begin()const
		{
			return _t.Begin();
		}

		const_iterator end()const
		{
			return _t.End();
		}

	private:
		BRTree< K,const K, KeyOf> _t;
	};

	void print(const set<int> s)
	{
		auto it = s.begin();
		while (it != s.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;
	}

	void test_set()
	{
		set<int> s;
		s.insert(1);
		s.insert(9);
		s.insert(3);
		s.insert(4);
		s.insert(6);
		s.insert(2);
		s.insert(4);
		/*auto p = s.search(4);
		cout << p->_kv << endl;
		s.inorder();*/
		/*for (auto x : s)
		{
			cout << x << ' ';
		}
		cout << endl;
		auto it = s.begin();
		while (it != s.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;
		it = s.end();
		while (it != s.begin())
		{
			--it;
			cout << *it << " ";
		}*/

		print(s);
	}
}
