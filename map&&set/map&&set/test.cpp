#define _CRT_SECURE_NO_WARNINGS 1

#pragma once
#include<iostream>
#include<assert.h>
using namespace std;//因为是向上查找，所以要放在前面

#include"BRTree.h"//因为是向上查找，所以要放在前面//但是结构体定义在后面也能找到，因为好像有了声明了，命名就是声明
//结构体定义的位置并不影响其使用
#include"map.h"
#include"set.h"

int main()
{
	//bit::test_set();
	bit::test_map();
	return 0;
}

