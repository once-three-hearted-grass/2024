#pragma once


namespace bit
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		~vector()
		{
			delete[] _start;
			_start = _finish = _end_of_storage=nullptr;
		}

		size_t size() const
		{
			return _finish - _start;
		}

		size_t capacity() const
		{
			return _end_of_storage - _start;
		}

		void reserve(size_t n)
		{
			assert(n > capacity());
			//开辟新空间，拷贝原来内容，释放旧空间
			T* tmp = new T[n];//这里会调用T的构造函数
			//memcpy(tmp, _start, sizeof(T) * size());//但这里memcpy只是浅拷贝，所以还不完美
			size_t oldsize = size();//这样就可以找到新的_finish位置了
			for (int i = 0; i < size(); i++)
			{
				tmp[i] = _start[i];
			}
			delete[] _start;
			_start = tmp;
			_finish = _start + oldsize;
			_end_of_storage = _start + n;
		}

		void push_back(const T& t)
		{
			if (size() == capacity())
			{
				//扩容
				size_t new_capacity = capacity() == 0 ? 4 : 2 * capacity();
				reserve(new_capacity);
			}
			_start[size()] = t;
			_finish++;
		}

		T& operator[](size_t pos)
		{
			assert(pos < size());
			return _start[pos];
		}

		const T& operator[](size_t pos)const
		{
			assert(pos < size());
			return _start[pos];
		}

		void resize(size_t n, const T& value = T())
//			//T()
////对于自定义类型，这个返回的是默认构造
////而对于内置类型，这个也是允许的
//int b = int();//这个b就等于0；
//		cout << b << endl;
//int a1 = int();
//		cout << a1 << endl;
//		int b(8);
//		cout << b << endl;
//		cout << int(10) << endl;//匿名对象这个是
		{
			//这里我们改变size，扩大的话还要给多余的那些初始值
			if (n > capacity())
			{
				reserve(n);
			}
			size_t oldsize = size();
			_finish = _start + n;
			if (n > oldsize)
			{
				for (size_t i = oldsize; i < n; i++)
				{
					_start[i] = value;
				}
			}
		}

		void pop_back()//尾删
		{
			_finish--;
		}

		void swap(vector<T>& v)
		{
			std::swap(_finish, v._finish);
			std::swap(_start, v._start);
			std::swap(_end_of_storage, v._end_of_storage);
		}

		iterator insert(iterator pos, const T& x)
		{
			//在pos位置之前插入x
			if (_finish == _end_of_storage)
			{
				size_t gap = pos - _start;
				reserve(capacity() * 2);
				//如果开辟了空间的话，那么对应_finish那些都会跟着变
				//但是pos就指向了原来的空间，所以这个迭代器就失效了
				//所以要更新pos
				pos = _start + gap;
			}
			//挨个往后移
			iterator end = _finish - 1;
			while (end >= pos)//因为pos肯定大于0的，不是size_t，所以不用像串那样麻烦
			{
				*(end + 1) = *(end);
				end--;
			}
			*pos = x;
			_finish++;
			return pos;//把新的迭代器返回回去，这样外面的pos来接收，就不会失效了，如果不接受的话，那个迭代器就失效了,这样的迭代器是不能访问的
		}

		iterator erase(iterator pos)
		{
			iterator end = pos + 1;
			while (end < _finish)
			{
				*(end - 1) = *end;
				end++;
			}
			if (pos == _finish)
			{
				pos--;//这样pos就还是指向的_finish
			}
			_finish--;
			return pos;
		}

		vector(const vector<T>& v)
		{
			//这个是拷贝构造
			//开辟新空间，拷贝原来内容，释放旧空间
			T* tmp = new T[v.capacity()];
			for (int i = 0; i < v.size(); i++)
			{
				tmp[i] = v._start[i];
			}
			_start = tmp;
			_finish = _start+v.size();
			_end_of_storage = _start + v.capacity();
		}

		vector() = default;//这个就表示显示实现一个与编译器自动调用的构造函数一样的构造函数

		vector(int n, const T& value = T())//为什么要构造两个呢，为了防止参数都为int时，去调用vector(InputIterator first, InputIterator last)
		//的情况，因为size_t比较不像int，所以会去调用那个，在解引用就会出错
		{
			//一个构造函数，初始化为n个T
			reserve(n);
			for (int i = 0; i < n; i++)
			{
				push_back(value);
			}
		}

		vector(size_t n, const T& value = T())
		{
			//一个构造函数，初始化为n个T
			reserve(n);
			for (int i = 0; i < n; i++)
			{
				push_back(value);
			}
		}

		vector<T>& operator= (vector<T> v)
		{
			//v是临时对象，所以交换也没事,如果传的是引用的话，那么就要在建立一个变量
			swap(v);
			return (*this);
		}

		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
		{
			//这里传的两个参数其实就是不同模版的迭代器，这个可以传各种类型的迭代器，然后构造出迭代器区间的vector
			InputIterator it = first;
			while (it != last)
			{
				push_back(*it);
				it++;
			}
		}

	private:
		iterator _start=nullptr;//给默认值，这样就不用先自己实现构造函数了，这里有默认构造
		iterator _finish=nullptr;
		iterator _end_of_storage=nullptr;
	};
	void test01()
	{
		//vector<int> a;//局部优先原则，在这个命名空间中，会先访问自己定义的vector，如果自己的域没有vector，才会去全局去看有没有vector
		////如果要访问库里的vector话，就要声明域，这样就会指定去访问，std::vector<int> a;比如这样，但还是要包含特定头文件
		////因为本头文件会在main函数那里展开实现的，所以可以在这里测试
		//a.push_back(1);

		//vector<int> a;
		//a.push_back(1);
		//a.push_back(2);
		//a.push_back(3);
		//a.push_back(4);
		//a.push_back(5);
		//for (int i = 0; i < a.size(); i++)
		//{
		//	cout << a[i] << ' ';
		//}
		//cout << endl;
		//for (auto x : a)
		//{
		//	cout << x << ' ';
		//}
		//cout << endl;
		//bit::vector<int>::iterator it = a.begin();
		//while (it != a.end())
		//{
		//	cout << *it << ' ';
		//	it++;
		//}
		//cout << endl;
		//vector<string> a;
		//a.push_back("12345");//隐式类型转换
		//a.push_back("12345");
		//a.push_back("12345");
		//a.push_back("12345");
		//a.push_back("12345");
		//for (auto x : a)
		//{
		//	cout << x << ' ';
		//}
		//cout << endl;

		//vector<int> a;
		//a.push_back(5);
		//a.push_back(4);
		//a.push_back(3);
		//a.push_back(2);
		//a.push_back(1);
		//a.pop_back();
		//a.pop_back();
		//a.resize(10);
		//vector<int> b;
		//b.push_back(1);
		//b.push_back(42);
		//b.push_back(436);
		//b.push_back(547);
		//b.push_back(4634);
		//b.swap(a);
		//for (auto x : a)
		//{
		//	cout << x << ' ';
		//}
		//cout << endl;

		//vector<int> a;
		//a.push_back(1);
		//a.push_back(2);
		//a.push_back(3);
		//a.push_back(4);
		//auto it = a.begin() + 2;
		////a.insert(it, 900);//如果这个函数返回值不用iterator接受的话，那么这个it是可能会失效的，访问出来是随机值
		////cout << *it << endl;
		////it=a.insert(it, 900);
		////cout << *it << endl;
		//a.insert(a.begin() + 3, 1000);
		//a.insert(a.begin() + 4, 1900);
		//for (auto x : a)
		//{
		//	cout << x << ' ';
		//}
		//cout << endl;
		//bit::vector<int>::iterator it = a.begin() + 4;
		//it=a.erase(a.begin() + 4);//按理说，这里的迭代器也应该和insert一样来接收，防止失效，但我们实现的这个erase不会失效迭代器
		////但是编译器呢，编译器实现的erase是会失效迭代器的，如果不接受，再来访问就会出问题，但是在linux下，不会出问题，所以还是要接收一下
		//for (auto x : a)
		//{
		//	cout << x << ' ';
		//}
		//cout << endl;
		 
		//vector<int> a;//如果没有实现默认构造函数的话，还这样写是不行的，当初没有构造函数，编译器会用默认值来自动生成默认构造函数
		//但是现在我们实现了构造函数，编译器就不会生成默认构造函数，就没有默认构造函数，就没有无参的构造函数
		//所以这样定义，就说明了要用默认构造函数，所以不行，所以要定义一个默认构造函数
		//vector<int> a;
		//a.push_back(1);
		//a.push_back(2);
		//a.push_back(3);
		//a.push_back(4);
		//a.push_back(5);
		//vector<int> b(a);
		//for (auto x : a)
		//{
		//	cout << x << ' ';
		//}
		//cout << endl;

		//vector<int> a(4, 90);
		//for (auto x : a)
		//{
		//	cout << x << ' ';
		//}
		//cout << endl;

		//vector<int> b(5, 80);
		//a = b;
		//for (auto x : a)
		//{
		//	cout << x << ' ';
		//}
		//cout << endl;
		////int a1 = int();
		////cout << a1 << endl;
		////int b(8);
		////cout << b << endl;
		////cout << int(10) << endl;//匿名对象这个是

		//string s1 = "123456789";
		//vector<char> s2(s1.begin(), s1.end() - 1);
		//for (auto x : s2)
		//{
		//	cout << x << ' ';
		//}
	
		//在c++11中，{1,23,4，。。。。。}类型是initializer_list，而这个东西template<class T> class initializer_list;
		//也是一个模板，而vector的构造函数其中一个就是用initializer_list来构造的，所以vector的构造可以用{}
		//就是这样写的类型
initializer_list<int> a = { 1,2,3,4,5,67 };
//然后再用这个类型去构造，因为vector支持嘛
		std::vector<int> a1 = { 2,4 };//这个是隐式类型转换，先从initializer_list来构造vector，再拷贝，优化为直接拷贝
		//而且这个就不优先考虑两个int参数的构造函数了
		std::vector<int> a3({ 2,4 });
		std::vector<int> a4 = { 2,2,3,4,6,8,0};
		std::vector<string> a2 = {"1243","13425","0000"};
		for (auto x : a3)
		{
			cout << x << ' ';
		}
		cout << endl;
		for (auto x : a2)
		{
			cout << x << ' ';
		}

	}
}

