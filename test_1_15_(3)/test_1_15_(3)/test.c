#define _CRT_SECURE_NO_WARNINGS 1
//验证尼科彻斯定理，即：任何一个整数m的立方都可以写成m个连续奇数之和。
//例如：
//1 ^ 3 = 1
//2 ^ 3 = 3 + 5
//3 ^ 3 = 7 + 9 + 11
//4 ^ 3 = 13 + 15 + 17 + 19
//输入一个正整数m（m≤100），将m的立方写成m个连续奇数之和的形式输出。

//尼科彻斯定理是指任何一个整数的立方都可以写成一串相邻奇数之和。下面用两种方法来证明这个定理：
// 第一种方法：根据证明尼科彻斯的方法，对于任意一个数a（无论奇偶，但要求大于等于3）
//a×（a - 1） + 1必定为奇数（若a为奇数，则a - 1为偶数，奇数偶数相乘为偶数再加1必定为奇数）
//则a的三次方必定可由a个连续的奇数相加得到。
#include<stdio.h>
int main() {
    int a = 0;
    scanf("%d", &a);
    int b = a*(a - 1) + 1;
    for (int i = 1; i <= a; i++)
    {
        if (i == 1)
        {
            printf("%d", b);
            b += 2;
        }
        else
        {
            printf("+%d", b);
            b += 2;
        }
    }
    return 0;
}