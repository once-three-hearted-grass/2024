#define _CRT_SECURE_NO_WARNINGS 1

#include<stdlib.h>
#include<stdio.h>
//给定一个二叉树 root ，返回其最大深度。
//二叉树的 最大深度 是指从根节点到最远叶子节点的最长路径上的节点数。

struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};

int maxDepth(struct TreeNode* root) {

    //大事化小
    //如果为空就返回0，不为空的话就返回1+子树的长度中的那个较大值，而子树的长度就是与根相同的思路
    //停止递归的条件就是NULL
    if (root == NULL)
    {
        return 0;
    }
    //记录子树的长度，不然会多次递归
    int left_len = maxDepth(root->left);
    int right_len = maxDepth(root->right);
    if (left_len > right_len)
    {
        return 1 + left_len;
    }
    else
    {
        return 1 + right_len;
    }
}