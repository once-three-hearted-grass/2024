#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<errno.h>
//int main()
//{
//	FILE* pf = fopen("text.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//	}
//	char arr[20] = { 0 };
//	while (fread(arr,1,2,pf)==2)
//	{
//		printf("%s",arr);
//	}
//	printf("%s", arr);
//	fclose(pf);
//	return 0;
//}
int main()
{
	FILE* pf = fopen("text.txt", "wb");
	if (pf == NULL)
	{
		perror("fopen");
	}
	int arr[] = {1,2,3,4,5,5,6,7};
	fwrite(arr, 4, 5, pf);
	fclose(pf);
	pf = NULL;
	return 0;
}
#if 0
int main()
{
	FILE* pf = fopen("text.txt", "wb");
	if (pf == NULL)
	{
		perror("fopen");
	}
	char arr[] = "HELLO\nHAHAHAH\nXIXIXIXI";
	fwrite(arr, 1, 5, pf);
	fwrite(arr+5, 1, 5, pf);
	fwrite(arr+10, 1, 5, pf);
	fclose(pf);
	pf = NULL;
	return 0;
}
typedef struct stu
{
	char name[20];
	int age;
}stu;
int main()
{
	FILE* pf = fopen("text.txt", "r");
	if (pf == NULL)
	{
		perror("fopen");
	}
	stu s = { 0 };
	while (fscanf(pf, "%s %d", s.name, &s.age)==2)
	{
		printf("%s %d\n", s.name,s.age);
	}
	fclose(pf);
	return 0;
}
int main()
{
	FILE* pf = fopen("text.txt", "w");
	if (pf == NULL)
	{
		perror("fopen");
	}
	stu s1 = { "zhangsan",20 };
	stu s2 = { "lisi",30 };
	fprintf(pf, "%s %d\n", s1.name, s1.age);
	fprintf(pf, "%s %d\n", s2.name, s2.age);
	fclose(pf);
	return 0;
}

int main()
{
	FILE* pf = fopen("text.txt", "r");
	if (pf == NULL)
	{
		perror("fopen");
	}
	char arr[20] = { 0 };
	while (fgets(arr, 5, pf) != NULL)
	{
		printf("%s", arr);
	}
	fclose(pf);
	return 0;
}
int main()
{
	FILE* pf = fopen("text.txt", "w");
	if (pf == NULL)
	{
		perror("fopen");
	}
	fputs("hello\n", pf);
	fputs("cengjingdesanxincao\n", pf);
	fputs("hahaha\n", pf);
	fputs("heheheh\n", pf);

	fclose(pf);
	return 0;
}

int main()
{
	FILE* pf = fopen("text.txt", "r");
	if (pf == NULL)
	{
		perror("fopen");
	}
	char c = 0;
	while ((c = fgetc(pf)) != EOF)
	{
		printf("%c", c);
	}
	fclose(pf);
	return 0;
}
int main()
{
	FILE*pf=fopen("text.txt", "w");
	if (pf == NULL)
	{
		perror("fopen");
	}
	for (char i = 'a'; i <= 'z'; i++)
	{
		fputc(i, pf);
	}
	fclose(pf);
	return 0;
}
#endif