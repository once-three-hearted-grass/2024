#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};

//给你二叉搜索树的根节点 root ，该树中的 恰好 两个节点的值被错误地交换。请在不改变其结构的情况下，恢复这棵树 。
 
int CountTree(struct TreeNode* root)
{
    if (root == NULL)
    {
        return 0;
    }
    return 1 + CountTree(root->left) + CountTree(root->right);
}

void my_inorderTraversal(struct TreeNode* root, int* arr, int* pi)
{
    if (root == NULL)
    {
        return;
    }
    my_inorderTraversal(root->left, arr, pi);
    arr[*pi] = root->val;
    (*pi)++;
    my_inorderTraversal(root->right, arr, pi);
}

int* inorderTraversal(struct TreeNode* root, int* returnSize) {
    //先统计有多少个节点
    int n = CountTree(root);
    *returnSize = n;
    int* arr = (int*)malloc(sizeof(int) * n);
    int i = 0;
    my_inorderTraversal(root, arr, &i);
    return arr;
}

void FindNum(int*arr,int* num1, int* num2,int n)
{
    //分析一下，如果两个数交换的话，那么就分为两种情况，第一种相邻交换，第二种就是不相邻交换
    int i = 0;
    for (; i < n-1; i++)
    {
        if (arr[i] > arr[i + 1])
        {
            *num1 = i;
            i++;
            break;
        }
    }
    for (; i < n-1; i++)
    {
        if (arr[i] > arr[i + 1])
        {
            *num2 = i+1;
            break;
        }
    }
    if((*num2)==-1)//走到这里说明相邻交换
    *num2 = *num1+1;//不能再用i了
}

void CorrectTree(struct TreeNode* root, int* arr, int* pj)
{
    if (root == NULL)
    {
        return;
    }
    CorrectTree(root->left, arr, pj);
    root->val = arr[*pj];
    (*pj)++;
    CorrectTree(root->right, arr, pj);
}

void recoverTree(struct TreeNode* root) {
    //如果是一颗二叉搜索树的话，中序遍历就是一个有序的数组，所以把这个树中序遍历在一个数组中，不有序的两个一定是交换的两个
    //先把值放入数组中吧
    int n = 0;
    int* arr = inorderTraversal(root, &n);
    //接下来找出那两个交换导致无序的就可以了
    int num1=-1;
    int num2=-1;
    FindNum(arr,&num1, &num2,n);
    //在数组中交换位置
    int tmp = arr[num1];
    arr[num1] = arr[num2];
    arr[num2] = tmp;
    //挨个遍历树，挨个赋值吧
    int j = 0;
    CorrectTree(root,arr,&j);
}