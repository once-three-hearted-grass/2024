#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>

 struct TreeNode {
     int val;
    struct TreeNode *left;
    struct TreeNode *right;
 };
 
 //给你一个整数 n ，请你生成并返回所有由 n 个节点组成且节点值从 1 到 n 互不相同的不同 二叉搜索树 。可以按 任意顺序 返回答案。

 /**
  * Note: The returned array must be malloced, assume caller calls free().
  */

void InsertTrees(struct TreeNode** pp, int key)
 {
     if ((*pp) == NULL)
     {
         struct TreeNode* tmp = (struct TreeNode*)malloc(sizeof(struct TreeNode));
         if (tmp == NULL)
         {
             exit(-1);
         }
         tmp->left = NULL;
         tmp->right = NULL;
         tmp->val = key;
         (*pp) = tmp;
         return;
     }
     //如果根不为空的话，就看key比根大吗，大的话就插入右子树
     if(key>(*pp)->val)
     InsertTrees(&(*pp)->right, key);
     else if (key < (*pp)->val)
     {
         InsertTrees(&(*pp)->left, key);
     }
     else {//相等就不用管了
         return;
     }
 }
struct TreeNode** generateTrees(int n, int* returnSize) {
    struct TreeNode** pp = (struct TreeNode**)malloc(sizeof(struct TreeNode*));//这个会销毁，所以是不行的，必须要开辟
    if (pp == NULL)
    {
        exit(-1);
    }
    (*pp) = NULL;
    *returnSize = n;
    for (int i = 0; i < n; i++)
    {
        InsertTrees(pp,i+1);//用的地址，因为要改变p的值，如果只是改变p所指向的的值就不用传地址
    }
    return pp;
}

int main()
{
    int n = 3;
    int ret;
    generateTrees(n, &ret);
    return 0;
}