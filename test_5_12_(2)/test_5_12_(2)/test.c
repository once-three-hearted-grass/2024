#define _CRT_SECURE_NO_WARNINGS 1
#include<stdbool.h>
#include<stdio.h>
#include<stdlib.h>
//队列 循环链表表示队列
//假设以带头结点的循环链表表示队列，并且只设一个指针指向队尾元素结点（注意不设头指针），请完成下列任务：
//1: 队列初始化，成功返回真，否则返回假： bool init_queue(LinkQueue * LQ);
//2: 入队列，成功返回真，否则返回假： bool enter_queue(LinkQueue * LQ, ElemType x);
//3: 出队列，成功返回真，且 * x为出队的值，否则返回假 bool leave_queue(LinkQueue * LQ, ElemType * x);
//相关定义如下：
typedef int ElemType;
typedef struct _QueueNode {
    ElemType data;          // 数据域
    struct _QueueNode* next;      // 指针域
}LinkQueueNode, * LinkQueue;


bool init_queue(LinkQueue* LQ)//
{
    
}

bool enter_queue(LinkQueue* LQ, ElemType x)
{

}

bool leave_queue(LinkQueue* LQ, ElemType* x)
{

}
