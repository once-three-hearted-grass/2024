#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现strstr
//strstr在arr1中寻找字串arr2，如果有，则返回arr2在arr1中字串的起始位置
//如果没有字串，则返回NULL
#include<stdio.h>
#include<string.h>
#include<assert.h>
char* my_strstr(const char* arr1, const char* arr2)
{
	assert(arr1 && arr2);
	//从arr1挨着挨着找，如果相等就继续循环判断，不等就退出循环,还要记录起始位置，相等判断的位置
	char* start1 = (char*)arr1;//记录起始位置
	char* start2 = (char*)arr2;
	char* cur1 = (char*)arr1;//记录相等的位置
	char* cur2 = (char*)arr2;
	while (*start1 != '\0')//把arr1挨个找完
	{
		cur1 = start1;
		cur2 = start2;
		if (*cur1 == *cur2)
		{
			//看是不是字串
			while (*cur1 == *cur2&&*cur1!='\0'&&cur2!='\0')//以防同时为\0.有一个为\0就可以退出来了
			{
				cur1++;
				cur2++;
			}
			//退出循环有三种可能
			//不相等，cur1为\0,cur2为\0,同时为\0
			if (*cur2 == '\0')//是字串
			{
				return start1;
			}
			if (*cur1 == '\0')//已经排除*cur1为\0的情况了，所以不可能同时为\0,那就是arr1现在太短了呗,就是没有字串呗
			{
				return NULL;
			}
			//走到这里就是不相等了，就退出这个if呗
		}
		//不相等，就继续前进呗
		start2 = (char*)arr2;
		start1++;
	}
	//可以走到了这里，比如一次都没有相等
	return NULL;

}
int main()
{
	char arr1[] = "abcddddefghj";
	char arr2[] = "ddef";
	char* ret = my_strstr(arr1, arr2);
	printf("%s\n", ret);
	return 0;
}