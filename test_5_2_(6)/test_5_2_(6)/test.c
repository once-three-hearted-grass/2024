#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//给定一个链表的头节点  head ，返回链表开始入环的第一个节点。 如果链表无环，则返回 null
//如果链表中有某个节点，可以通过连续跟踪 next 指针再次到达，则链表中存在环。
//为了表示给定链表中的环，评测系统内部使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。
//如果 pos 是 - 1，则在该链表中没有环。注意：pos 不作为参数进行传递，仅仅是为了标识链表的实际情况。
//不允许修改 链表。

 struct ListNode {
    int val;
    struct ListNode *next;
 };

 typedef struct ListNode ListNode;

struct ListNode* detectCycle(struct ListNode* head) {
    //一个快指针走两步，一个慢指针走一步，因为相对速度为一，所以两个都进环时，快指针一定能追上慢指针，追上了说明带环
//快慢指针相等说明带环，若不带环，则一定走到了NULL
    ListNode* fast = head;
    ListNode* slow = head;
    //先找到相交结点
    while (fast && fast->next)
    {
        fast = fast->next->next;
        slow = slow->next;
        if (fast == slow)
        {
            break;
        }
    }
    //判断是否是没有循环导致NULL而跳出循环
    if (fast == NULL || fast->next == NULL)
    {
        return NULL;
    }
    //来个指针记录相交节点按照1的速度前行，再来个指针从head按照速度为1前行
    ListNode* meet = fast;
    ListNode* cur = head;
    while (1)
    {
        if (meet == cur)
        {
            return meet;
        }
        meet = meet->next;
        cur = cur->next;
    }
}