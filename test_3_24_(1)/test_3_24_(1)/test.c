﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<ctype.h>
#include<string.h>
#if 0
int main()
{
	char arr1[20] = "xxxxxxxxxxx";
	char arr2[] = "abcdef";
	strncat(arr1, arr2, 8);
	printf("arr1 = \"%s\"", arr1);
	return 0;
}
int main()
{
	char arr1[20] = "xxxxxxxxxxx";
	char arr2[] = "abcdef";
	strncpy(arr1, arr2,8);
	printf("arr1 = \"%s\"", arr1);
	return 0;
}
int main()
{
	char arr1[20] = "abcdg";
	char arr2[] = "abcdef";
	int ret=strcmp(arr1, arr2);
	if (ret > 0)
	{
		printf("arr1>arr2");
	}
	else if(ret<0)
	{
		printf("arr1<arr2");
	}
	else
	{
		printf("arr1=arr2");
	}
	return 0;
}
int main()
{
	char arr1[20] = "xxxxxxxx";
	char arr2[] = "abcdef";
	strcat(arr1, arr2);
	printf("arr1 = %s", arr1);
	return 0;
}
int main()
{
	char arr1[20] = "xxxxxxxxxxxxxxxxxx";
	char arr2[] = "abcdef";
	strcpy(arr1, arr2);
	printf("%s", arr1);
	return 0;
}
int main()
{
	char arr1[] = "aufhjfhnKJDADJdcnajk";
	int ret = strlen(arr1);
	printf("arr1长为%d\n", ret);
	char arr2[] = "aufhjfhnK\0JDADJdcnajk";
	ret = strlen(arr2);
	printf("arr2长为%d\n", ret);
	char arr3[] = {'w','w','2','q'};//末尾没有\0
	ret = strlen(arr3);
	printf("arr3长为%d\n", ret);
	return 0;
}
//练习：写⼀个代码，将字符串中的⼩写字⺟转⼤写，其他字符不变。
int main()
{
	char arr[] = "aufhjfhnKJDADJdcnajk";
	int i = 0;
	while (arr[i] != '\0')
	{
		if (islower(arr[i]))//是小写返回真，就进入转换为大写
		{
			arr[i] = toupper(arr[i]);
		}
		i++;
	}
	printf("%s", arr);
	return 0;
}
int main()
{
	//int isdigit(int c);
	int ret = isdigit('0');
	if (ret != 0)
	{
		printf("是数字字符\n");
	}
	else
	{
		printf("不是数字字符\n");
	}
	printf("%d", ret);
	return 0;
}
#endif