#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
//队列 循环链表表示队列
//假设以带头结点的循环链表表示队列，并且只设一个指针指向队尾元素结点（注意不设头指针），请完成下列任务：
//1: 队列初始化，成功返回真，否则返回假： bool init_queue(LinkQueue * LQ);
//2: 入队列，成功返回真，否则返回假： bool enter_queue(LinkQueue * LQ, ElemType x);
//3: 出列，成功返回真，且 * x为出队的值，否则返回假 bool leave_queue(LinkQueue * LQ, ElemType * x);
//相关定义如下：
typedef int ElemType;
typedef struct _QueueNode {
    ElemType data;          // 数据域
    struct _QueueNode* next;      // 指针域
}LinkQueueNode, * LinkQueue;

//这道题该如何实现入队和出队呢，首先初始化一个头结点，这个头结点是循环的
//然后这个传入参数的指针是尾节点的
//入队就尾插
//出队的话，就是删去尾节点的下一个的下一个就找到队头了
bool init_queue(LinkQueue* LQ) {
    //初始化一个头，而且是循环的
    LinkQueue head = (LinkQueue)malloc(sizeof(LinkQueueNode));
    if (head == NULL)
    {
        return false;
    }
    head->next = head;//循环
    *LQ = head;
    return true;
}

bool enter_queue(LinkQueue* LQ, ElemType x) {
    LinkQueue new_node = (LinkQueue)malloc(sizeof(LinkQueueNode));
    if (new_node == NULL)
    {
        return false;
    }
    new_node->data = x;
    LinkQueue tail = *LQ;
    //插入newnde
    //先让newnode指向头节点
    new_node->next = tail->next;
    tail->next = new_node;
    *LQ = new_node;
    return true;
}

bool leave_queue(LinkQueue* LQ, ElemType* x) {
    //出对的话，就找到头节点就可以了
    LinkQueue head = (*LQ)->next;
    if (head->next == head)//没有数据
    {
        return false;
    }
    
    LinkQueue del = head->next;
    //如果只有两个节点呢，就会影响LQ了
    if (del->next == head)
    {
        *LQ = head;
    }
    head->next = del->next;
    *x = del->data;
    free(del);
    return true;
}