#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现memmove
#include<stdio.h>
#include<string.h>
//void *memmove( void *dest, const void *src, size_t count );
//memmove不仅具有memcpy的功能，还具有处理有交叉部分的功能
void* my_memmove(void* dest, const void* src, size_t num)
{
	//只需要分类讨论就可以了
	//src在dest后面，则从src的第一个进行挨个替换就可以了,src>dest
	//src在dest的前面，则从src的最后一个开始替换，就不会出矛盾,src<dest
	//内存没有交叉部分的，随便替换都可以
	if (src > dest)
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			((char*)dest)++;
			((char*)src)++;
		}
	}
	else
	{
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}
}
int main()
{
	int arr1[] = { 0,1,2,3,4,5,6,7,8,9 };
	//memmove(arr1 + 2, arr1,20);
	my_memmove(arr1+2 , arr1, 20);
	for (int i = 0; i < 10; i++)
	{
		printf("%d ",arr1[i]);
	}
	return 0;
}