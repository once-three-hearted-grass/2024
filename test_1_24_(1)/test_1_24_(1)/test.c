#define _CRT_SECURE_NO_WARNINGS 1
//递归求斐波那契数列之优化
//即用数组储存斐波那契数这样就不用盲目递归了，只需用数组就可以了
#include<stdio.h>
int Feb(int n)
{
	static int arr[100000000] = { 0 };
	//因为不能使用变长数组,所以只能这样了,而且我用static修饰,这样的话arr的作用域就会变大了
	if (n <= 1)
	{
		arr[n] = n;//这样的话arr[0]=0,arr[1]=1
		return arr[n];
	}
	if (arr[n - 1] == 0)
	{
		arr[n - 1] = Feb(n - 1);//就这一个递归，递归n次，就能把所有的斐波那契数给存到数列中
	}
	return arr[n - 1] + arr[n - 2];

}
int main()
{
	int n = 0;
	while ((scanf("%d", &n)) != EOF)
	{
		int ret = Feb(n);
		printf("第%d个斐波那契数列是%d\n",n,ret);
	}
	return 0;
}