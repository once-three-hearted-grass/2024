#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stack>
#include<vector>
using namespace std;

class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param pushV int整型vector
     * @param popV int整型vector
     * @return bool布尔型
     */
    bool IsPopOrder(vector<int>& pushV, vector<int>& popV) {
        // write code here
        //stack<int> s1;
        //int i = 0;
        //int j = 0;
        //s1.push(pushV[i]);
        //i++;
        //while (i < pushV.size())//如果这样设计的话，那么最后一个元素入栈，后就直接跳出来了，还没有判断后面的是否对应
    //    while (i < pushV.size())//但取了等于就永远死循环了，太麻烦了
    //    {
    //        while(!s1.empty()&&popV[j] == s1.top())//因为为空无法访问
    //        {
    //            s1.pop();
    //            j++;
    //        }
    //        if(s1.empty()|| popV[j] != s1.top())
    //        {
    //            s1.push(pushV[i]);
    //            i++;
    //        }
    //    }
    //    if (!s1.empty())
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        return true;
    //    }
    //}
        stack<int> s1;
        int i = 0;
        int j = 0;
        while (i < pushV.size())//交换一下位置呢//先入栈在判断是否对应
        {
            if (s1.empty() || popV[j] != s1.top())
            {
                s1.push(pushV[i]);
                i++;
            }
            while (!s1.empty() && popV[j] == s1.top())//因为为空无法访问
            {
                s1.pop();
                j++;
            }
        }
        if (!s1.empty())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
};
int main()
{
    vector<int> v1 = { 1,2,3,4,5 };
    vector<int> v2 = { 4,5,3,2,1 };
    Solution a;
    cout << a.IsPopOrder(v1, v2);
    return 0;
}