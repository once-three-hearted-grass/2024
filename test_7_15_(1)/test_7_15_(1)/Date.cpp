#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"

// 全缺省的构造函数
Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;
}

// 拷贝构造函数
// d2(d1)
Date::Date(const Date& d)
{
		_year = d._year;
		_month = d._month;
		_day = d._day;
}

// 赋值运算符重载
// d2 = d3 -> d2.operator=(&d2, d
Date& Date::operator=(const Date& d)
{
	if (this != &d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	return (*this);
}
// 析构函数
Date::~Date()
{
	//cout << "Date::~Date()" << endl;
}
// >运算符重载
bool Date::operator>(const Date& d)
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year == d._year)
	{
		if (_month > d._month)
		{
			return true;
		}
		else if (_month == d._month)
		{
			return _day > d._day;
		}
	}
	return false;
}
// ==运算符重载
bool Date::operator==(const Date& d)
{
	return _year == d._year && _month == d._month && _day == d._day;
}
// >=运算符重载
bool Date::operator >= (const Date& d)
{
	return (*this) > d || (*this) == d;
}
// <运算符重载
bool Date::operator < (const Date& d)
{
	return !((*this) >= d);
}
// <=运算符重载
bool Date::operator <= (const Date& d)
{
	return !((*this) > d);
}
// !=运算符重载
bool Date::operator != (const Date& d)
{
	return !((*this) == d);
}

//日期 += 天数
Date& Date::operator+=(int day)
{
	_day += day;
	while (_day > GetMonthDay(_year,_month))
	{
		_day -= GetMonthDay(_year, _month);
		++_month;
		if (_month == 13)
		{
			_month = 1;
			_year++;
		}
	}
	return (*this);
}
// 日期+天数
Date Date::operator+(int day)
{
	Date d = (*this);
	d += day;
	return d;
}
// 日期-=天数
Date& Date::operator-=(int day)
{
	_day -= day;
	while (_day <= 0)
	{
		if (_month == 1)
		{
			_month = 12;
			_year--;
		}
		_day += GetMonthDay(_year, _month);
	}
	return (*this);
}
// 日期-天数
Date Date::operator-(int day)
{
	Date d = (*this);
	d -= day;
	return d;
}

// 前置++
Date& Date::operator++()
{
	(*this) = (*this) + 1;
	return (*this);
}
// 后置++
Date Date::operator++(int)
{
	Date tmp = (*this);
	(*this) = (*this) + 1;
	return tmp;
}
// 后置--
Date Date::operator--(int)
{
	Date tmp = (*this);
	(*this) = (*this) - 1;
	return tmp;
}
// 前置--
Date& Date::operator--()
{
	(*this) = (*this) - 1;
	return (*this);
}

// 日期-日期 返回天数
int Date::operator-(const Date& d)
{
	//先找出大的日期
	Date max = (*this);
	Date min = d;
	int flag = 1;//用来标志左大还是右大
	if (max < min)
	{
		max = d;
		min = (*this);
		flag = -1;
	}
	int ret = 0;
	while (min != max)
	{
		ret++;
		++min;
	}
	return ret*(flag);
}

ostream& operator<<(ostream& out, Date& d)
{
	out << d._year << "年" << d._month << "月" << d._day << "号" << endl;
	return out;
}
//
istream& operator>>(istream& in, Date& d)
{
	//
	in >> d._year >> d._month >> d._day;
	if (!d.CheckDate())
	{
		cout << "输入非法" << endl;
	}
	return in;
}