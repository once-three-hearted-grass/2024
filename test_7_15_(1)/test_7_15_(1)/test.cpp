#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"


int main()
{
	Date d1(2024, 4, 3);
	Date d2(2024, 4, 4);
	const Date d3 = d2;
	d3.print();
	return 0;
}