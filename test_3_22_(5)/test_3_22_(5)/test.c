#define _CRT_SECURE_NO_WARNINGS 1
//使用malloc函数模拟开辟一个3 * 5的整型二维数组，开辟好后，使用二维数组的下标访问形式，访问空间。
#include<stdio.h>
#include<stdlib.h>
int main()
{
	//int arr[3]
	// int*arr=(int*)malloc(3*sizeof(int)),其中arr*就是数组名的类型
	//int arr[3][5]数组名为首元素地址，为第一行的地址，为int(*)[5]型
	int(*arr)[5] = (int(*)[5])malloc(3 * 5 * sizeof(int));
	if (arr == NULL)
	{
		return 1;
	}
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			arr[i][j] = i + j;
		}
	}
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	free(arr);
	arr = NULL;
	return 0;
}