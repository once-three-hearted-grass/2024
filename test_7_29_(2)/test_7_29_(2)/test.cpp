#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        //先定义一个数组，我们来计数排序
        //先遍历一遍，找到最小的和最大的
        int min = nums[0];
        int max = nums[0];
        for (auto x : nums)
        {
            if (x > max)
            {
                max = x;
            }
            if (x < min)
            {
                min = x;
            }
        }
        int* arr = (int*)malloc(sizeof(int) * (max-min+1));
        memset(arr, 0, sizeof(int) * (max - min + 1));//清为0
        //开始计数
        for (auto x : nums)
        {
            arr[x - min]++;
        }
        //开始清理重复数据
        auto it = nums.begin();
        while (it != nums.end())
        {
            if (arr[*it - min] > 1)
            {
                it=nums.erase(it);//it就相当于指向下一个了、
                arr[*it - min]--;
            }
            else
            {
                it++;
            }
        }
        return nums.size();
    }
};