#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<stdio.h>
#include<stdbool.h>
typedef int QDataType;
// 链式结构：表示队列 
typedef struct QListNode
{
	struct QListNode* next;
	QDataType data;
}QNode;

// 队列的结构 
typedef struct Queue
{
	QNode* front;
	QNode* tail;
	int size;
}Queue;

// 初始化队列 
void QueueInit(Queue* q)
{
	assert(q);
	q->front = q->tail = NULL;
	q->size = 0;
}

// 队尾入队列 
void QueuePush(Queue* q, QDataType data)
{
	assert(q);
	QNode* NewNode = (QNode*)malloc(sizeof(QNode));
	if (NewNode == NULL)
	{
		perror("QueuePush");
		return;
	}
	NewNode->data = data;
	NewNode->next = NULL;
	if (q->tail == NULL)
	{
		q->front = q->tail = NewNode;
		q->size++;
	}
	else
	{
		q->tail->next = NewNode;
		q->tail = NewNode;
		q->size++;
	}
}

// 队头出队列 
void QueuePop(Queue* q)
{
	assert(q);
	assert(q->front);
	if (q->front == q->tail)//说明指向同一个节点,而且是最后一个节点
	{
		free(q->front);
		q->front = q->tail = NULL;
		q->size--;
	}
	else
	{
		QNode* del = q->front;
		q->front = q->front->next;
		free(del);
		q->size--;
	}
}

// 获取队列头部元素 
QDataType QueueFront(Queue* q)
{
	assert(q);
	assert(q->size);
	return q->front->data;
}

// 获取队列队尾元素 
QDataType QueueBack(Queue* q)
{
	assert(q);
	assert(q->size);
	return q->tail->data;
}

// 获取队列中有效元素个数 
int QueueSize(Queue* q)
{
	assert(q);
	return q->size;
}

// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
int QueueEmpty(Queue* q)
{
	assert(q);
	return q->size == 0;
}

// 销毁队列 
void QueueDestroy(Queue* q)
{
	assert(q);
	QNode* cur = q->front;
	while (cur)
	{
		QNode* next = cur->next;
		free(cur);
		cur = next;
	}
	q->front = q->tail = NULL;
	q->size = 0;
}


typedef struct {
	Queue q1;
	Queue q2;
} MyStack;


MyStack* myStackCreate() {
	MyStack* ST = (MyStack*)malloc(sizeof(MyStack));
	QueueInit(&(ST->q1));
	QueueInit(&(ST->q2));
	return ST;
}

void myStackPush(MyStack* obj, int x) {
	//入栈的话，就只需要把数据放在不为空的队列里就可以了
	//这里用假设法求出不为空的队列
	Queue* Empty = &(obj->q1);
	Queue* NonEmpty = &(obj->q2);
	if (!QueueEmpty(Empty))
	{
		Empty = &(obj->q2);
		NonEmpty = &(obj->q1);
	}
	//现在把数据放在不为空的队列里
	QueuePush(NonEmpty, x);
}

int myStackPop(MyStack* obj) {//这个要移除并返回栈顶元素
	//出栈的话，就要把不为空的队列里的数据除了最后一个都放在空队列里，最后一个数据就作为栈的栈顶元素出去
	//这里用假设法求出不为空的队列
	Queue* Empty = &(obj->q1);
	Queue* NonEmpty = &(obj->q2);
	if (!QueueEmpty(Empty))
	{
		Empty = &(obj->q2);
		NonEmpty = &(obj->q1);
	}
	//开始放元素，最后一个不要放
	while (QueueSize(NonEmpty) > 1)
	{
		QueuePush(Empty, QueueFront(NonEmpty));//放的是非空队列的第一个元素，放了之后还要出队
		QueuePop(NonEmpty);
	}
	//现在以前那个非空队列就只剩一个元素了，这个元素就是所求栈顶元素
	QDataType ret = QueueFront(NonEmpty);
	QueuePop(NonEmpty);
	return ret;
}

int myStackTop(MyStack* obj) {
	//这个只需返回栈顶元素
	assert(obj);
	//这里用假设法求出不为空的队列
	Queue* Empty = &(obj->q1);
	Queue* NonEmpty = &(obj->q2);
	if (!QueueEmpty(Empty))
	{
		Empty = &(obj->q2);
		NonEmpty = &(obj->q1);
	}
	assert(NonEmpty->size);
	//直接返回非空队列最后一个就可以了
	return NonEmpty->tail->data;
}

bool myStackEmpty(MyStack* obj) {
	return obj->q1.size==0 && obj->q2.size==0;
}

void myStackFree(MyStack* obj) {
	QueueDestroy(&obj->q1);//箭头的优先级高哦
	QueueDestroy(&obj->q2);//箭头的优先级高哦
	free(obj);
}
