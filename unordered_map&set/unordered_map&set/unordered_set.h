#pragma once
//
#include"HashTable.h"

namespace bit
{
	template<class K,class Hash=hash<K>>
	class unordered_set
	{
	public:

		struct KeyOfset
		{
			K operator()(const K& k)
			{
				return k;
			}
		};

		typedef typename HashTable<K, const K, KeyOfset,Hash>::Iterator iterator;
		typedef typename HashTable<K,const K, KeyOfset,Hash>::constIterator const_iterator;

		pair<iterator, bool> insert(const K& key)
		{
			return ha.Insert(key);
		}

		pair<iterator, bool> find(const K& key)
		{
			return ha.Find(key);
		}

		bool erase(const K& key)
		{
			return ha.Erase(key);
		}

		iterator begin()
		{
			return ha.Begin();
		}

		iterator end()
		{
			return ha.End();
		}

		const_iterator begin()const
		{
			return ha.Begin();
		}

		const_iterator end()const
		{
			return ha.End();
		}

	private:
		HashTable<K,const K, KeyOfset,Hash> ha;
	};

	void print(const unordered_set<int>& u)//const是所有的都无法修改
	{
		auto it = u.begin();
		while (it != u.end())
		{
			cout << *it << " ";
			++it;
		}
	}

	void test_set()
	{
		//正常的map和set，key是无法修改的//把第二个模板参数改了就可以了
		unordered_set<int> u;
		int arr[] = { 1,1,2,3,4,5,79,785,4,12,34 };
		for (auto x : arr)
		{
			u.insert(x);
		}
		/*cout << u.find(1) << endl;
		for (auto x : arr)
		{
			u.erase(x);
		}*/

		auto it = u.begin();
		while (it != u.end())
		{
			/**it = 24;*/
			cout << *it << " ";
			++it;
		}
		cout << endl;
		//print(u);
	}
}
