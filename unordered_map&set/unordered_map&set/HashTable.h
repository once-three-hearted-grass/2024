#pragma once
#include<vector>
#include<iostream>
using namespace std;

namespace bit
{
	template<class K>
	struct hash
	{
		int operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	//针对string，对hash进行特化
	template<>
	struct hash<string>
	{
		int operator()(const string& key)
		{
			int i = 0;
			for (auto x : key)
			{
				i += x;
				i *= 31;//防止哈希冲突//这样不同字符串转换为相同整型的概率就小点了
			}
			return i;
		}
	};

	//开散列的实现
	template<class K, class T>
	struct HashData
	{
		T _data;
		HashData* _next;
		HashData(const T& data)
			:_next(nullptr)
			, _data(data)
		{}
	};

	template<class K, class T,class KeyOfT, class Hash>//这里缺省声明了，定义就不要写了
	class HashTable;

	template<class K, class T, class ref, class ptr, class KeyOfT, class Hash>
	struct HashIterator
	{
	
		typedef HashData<K, T> Node;
		typedef HashTable<K, T, KeyOfT,Hash> table;

		HashIterator(Node*node,const table*p)
			:_node(node)
			, _p(p)
		{}

		ref operator*()
		{
			return _node->_data;
		}

		ptr operator->()
		{
			return &_node->_data;
		}

		HashIterator operator++()
		{
			if (_node->_next)
			{
				_node = _node->_next;
			}
			else
			{
				Hash ha;
				KeyOfT kot;
				int i = ha(kot(_node->_data)) % _p->_tables.size();
				i++;
				while (i!= _p->_tables.size()&&!_p->_tables[i])
				{
					i++;
				}
				if (i == _p->_tables.size())
				{
					_node = nullptr;
				}
				else
				{
					_node = _p->_tables[i];
				}
			}
			return *this;
		}

		bool operator!=(const HashIterator& tmp)
		{
			return _node != tmp._node;
		}

		bool operator==(const HashIterator& tmp)
		{
			return _node == tmp._node;
		}

		Node* _node;
		const table* _p;
	};

	template<class K, class T, class KeyOfT,class Hash>
	class HashTable
	{
	public:
		//友元
		template<class K, class T, class ref, class ptr, class KeyOfT,class Hash>
		friend struct HashIterator;

		typedef HashData<K, T> Node;
		typedef HashIterator<K, T,T&,T*, KeyOfT,Hash> Iterator;
		typedef HashIterator<K, T,const T&,const T*, KeyOfT,Hash> constIterator;

		Iterator End()
		{
			return Iterator(nullptr, this);
		}

		Iterator Begin()
		{
			if (_n == 0)
			{
				return End();
			}
			for (int i = 0; i < _tables.size(); i++)
			{
				if (_tables[i])
				{
					return Iterator(_tables[i], this);
				}
			}
			return End();//虽然不会走到这里，但是编译器还是会根据语法报错
		}

		constIterator End()const
		{
			return constIterator(nullptr, this);
		}

		constIterator Begin()const
		{
			if (_n == 0)
			{
				return End();
			}
			for (int i = 0; i < _tables.size(); i++)
			{
				if (_tables[i])
				{
					return constIterator(_tables[i], this);//这个this是const的，所以会权限的缩小
				}
			}
			return End();//虽然不会走到这里，但是编译器还是会根据语法报错
		}

		HashTable()
		{
			_tables.resize(10, nullptr);
		}

		pair<Iterator,bool> Insert(const T& data)
		{
			KeyOfT kot;
			//防止插入重复的
			Iterator m = Find(kot(data));
			if (m._node!=nullptr)//说明找到了
			{
				return make_pair(m,false);
			}

			//检查是否需要扩容
			if (_n == _tables.size())
			{
				//扩容
				HashTable<K, T, KeyOfT,Hash> tmp;
				tmp._tables.resize(_tables.size() * 2);
				//直接现成的把原来的插过来//因为如果像闭散列的实现那样的话，就会额外开那么空间，还要释放，就浪费了，所以直接利用现成的
				for (int i = 0; i < _tables.size(); i++)
				{
					Node* cur = _tables[i];
					while (cur)
					{
						Node* next = cur->_next;
						Hash h;
						int j = h(kot(cur->_data)) % tmp._tables.size();
						cur->_next = tmp._tables[j];
						tmp._tables[j] = cur;
						cur = next;
						tmp._n++;
					}
				}
				_tables.swap(tmp._tables);
			}
			Hash h;
			int i = h(kot(data)) % _tables.size();
			Node* newnode = new Node(data);
			newnode->_next = _tables[i];//直接头插
			_tables[i] = newnode;
			_n++;
			return make_pair(Iterator(newnode, this), true);
		}

		Iterator Find(const K& key)
		{
			KeyOfT kot;
			Hash h;
			int i = h(key) % _tables.size();
			Node* cur = _tables[i];
			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					return Iterator(cur,this);
				}
				cur = cur->_next;
			}
			return Iterator(nullptr, this);
		}

		bool Erase(const K& key)
		{
			KeyOfT kot;
			Hash h;
			//这个也不能像闭散列那样，直接用find然后删除，因为有前后指针的链接嘛
			int i = h(key) % _tables.size();
			Node* cur = _tables[i];
			Node* pre = nullptr;
			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					//开始删除
					if (pre)//正常删
					{
						pre->_next = cur->_next;
					}
					else//删头结点
					{
						_tables[i] = cur->_next;
					}
					delete cur;
					return true;
				}
				pre = cur;
				cur = cur->_next;
			}
			return false;
		}

		~HashTable()//这个需要我们自己写
		{
			//vector系统会自己调用自己的析构函数，所以我们不用写它的，自定义类型有析构的都会自己调用自己的析构函数
			//关键vector自己析构还不够，要析构后面的链表
			for (int i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
					_n--;
				}
				_tables[i] = nullptr;
			}
		}

	private:
		vector<Node*> _tables;
		size_t _n=0;
	};

	//void test1()
	//{
	//	//HashTable<int, int> ha;
	//	//int arr[] = { 1,11,2,12,6,16,20 };
	//	//for (auto x : arr)
	//	//{
	//	//	ha.Insert({ x,x });
	//	//}
	//	//cout << ha.Find(13) << endl;
	//	///*for (auto x : arr)
	//	//{
	//	//	ha.Erase(x);
	//	//}*/
	//	HashTable<string, string> ha;
	//	string arr[] = {"aaa","aaa","sss","bbbb","cccc","dddd"};
	//	for (auto x : arr)
	//	{
	//		ha.Insert({ x,x });
	//	}
	//	//cout << ha.Find(13) << endl;
	//	/*for (auto x : arr)
	//	{
	//		ha.Erase(x);
	//	}*/
	//}
}
