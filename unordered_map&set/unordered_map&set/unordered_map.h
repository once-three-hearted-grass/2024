#pragma once

#include"HashTable.h"

namespace bit
{

	template<class K,class V,class Hash=hash<K>>
	class unordered_map
	{
	public:

		struct KeyOfmap
		{
			K operator()(const pair<K,V>& kv)
			{
				return kv.first;
			}
		};

		typedef typename HashTable<K, pair<const K, V>, KeyOfmap,Hash>::Iterator iterator;
		typedef typename HashTable<K, pair<const K, V>, KeyOfmap,Hash>::constIterator const_iterator;

		pair<iterator,bool> insert(const pair<K,V>& kv)
		{
			return ha.Insert(kv);
		}

		pair<iterator, bool> find(const K& key)
		{
			return ha.Find(key);
		}

		bool erase(const K& key)
		{
			return ha.Erase(key);
		}

		iterator begin()
		{
			return ha.Begin();
		}

		iterator end()
		{
			return ha.End();
		}

		const_iterator begin()const
		{
			return ha.Begin();
		}
		
		V& operator[](const K& key)
		{
			pair<iterator,bool> ret=ha.Insert(make_pair(key, V()));
			return ret.first->second;
		}

		const_iterator end()const
		{
			return ha.End();
		}
	private:
		HashTable<K, pair<const K,V>, KeyOfmap,Hash> ha;
	};

	void test_map()
	{
		/*unordered_map<int,int> u;
		int arr[] = { 1,1,2,3,4,5,79,785,4,12,34 };
		for (auto x : arr)
		{
			u.insert({x,x});
		}
		cout << u.find(1) << endl;
		for (auto x : arr)
		{
			u.erase(x);
		}

		auto it = u.begin();
		while (it != u.end())
		{
			cout << it->first << " "<<it->second;
			++it;
		}
		cout << endl;*/

		unordered_map<string, string> u;
		u.insert({ "haha","����" });
		u.insert({ "left","��" });
		u.insert({ "right","��" });
		u.insert({ "up","��" });
		u["right"]="xia";
		for (auto x : u)
		{
			cout << x.first << " " << x.second<<endl;
		}
		cout << endl;
	}
}
