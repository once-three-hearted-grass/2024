#define _CRT_SECURE_NO_WARNINGS 1
#include"Graph.h"

//初始化图
Graph* InitGraph(int vexnum)
{
	Graph* G = (Graph*)malloc(sizeof(Graph));
	G->vex = (char*)malloc(sizeof(char) * vexnum);
	//arc是一个二级指针数组，每个元素指向一个一维数组，也相当于一个二维数组
	G->arc = (int**)malloc(sizeof(int*) * vexnum);
	G->vexNUM = vexnum;
	G->arcNUM = 0;
	for (int i = 0; i < vexnum; i++)
	{
		G->arc[i] = (int*)malloc(sizeof(int) * vexnum);
	}
	return G;
}

//创造图
void CreatGraph(Graph* G, char* vex, int* arc)//第二个参数传的是顶点，第三个传的是边，强制类型转换而来的
{
	for (int i = 0; i < G->vexNUM; i++)
	{
		G->vex[i] = vex[i];//赋值顶点
		for (int j = 0; j < G->vexNUM; j++)//赋值边
		{
			G->arc[i][j] = arc[i * G->vexNUM + j];
			if (G->arc[i][j] >0&& G->arc[i][j]!=MAX)
			{
				G->arcNUM++;
			}
		}
	}
	G->arcNUM /= 2;//因为是无向图
}

//深度优先遍历图
void DFS(Graph* G, int* visited, int index)//第二个参数是看这个顶点是否被访问了,第二个参数是看从哪个顶点开始访问
{
	//先访问传入的这个顶点
	printf("v%c ", G->vex[index]);
	//标记
	visited[index] = 1;//表示被访问过了
	for (int i = 0; i < G->vexNUM; i++)
	{
		if (G->arc[index][i]>0 && G->arc[index][i] !=MAX&& visited[i] == 0)//有这条边并且这个顶点未被访问
		{
			//那就沿着这条边去访问这个顶点，这个顶点的访问都一样的
			DFS(G, visited, i);
		}
	}
}

int GetMin(int* s, int* d,Graph*G)//从没有确定最短距离的点中选出最小的距离
{
	int min = MAX;
	int index;
	for (int i = 0; i < G->vexNUM; i++)
	{
		if (s[i] == 0 && d[i] < min)
		{
			min = d[i];
			index = i;
		}
	}
	return index;
}

//接下来就是看index到任何点的最小距离和最短路径
void DWay(int* p,int end,Graph*G,int index)
{
	if (end != -1)
	{
		DWay(p, p[end],G,index);
		printf("->v%c",G->vex[end]);
	}
	else
	{
		;
	}
}

//迪杰斯特拉算法
void Dijkstra(Graph* G,int index)//从哪个点开始求到各个点的最小距离
{
	int* s = (int*)malloc(sizeof(int) * G->vexNUM);//是否求得最小距离
	int* p = (int*)malloc(sizeof(int) * G->vexNUM);//求得最小距离时的前一个节点是什么
	int* d = (int*)malloc(sizeof(int) * G->vexNUM);//最小距离是什么
	for (int i = 0; i < G->vexNUM; i++)
	{
		//初始化三个数组
		if (i == index)
		{
			s[i] = 1;
			p[i] = -1;
		}
		else
		{
			s[i] = 0;
			p[i] = index;
		}
		d[i] = G->arc[index][i];
	}
	//开始算法
	//总共要选择n-1次
	for (int i = 0; i < G->vexNUM - 1; i++)
	{
		int min = GetMin(s, d, G);
		//这个就是最小距离了
		s[min] = 1;
		//更新这个点到其他点的距离加上原来自己的距离看是不是小于原点到其他点的距离
		for (int j = 0; j < G->vexNUM; j++)
		{
			if (s[j] == 0 && d[min] + G->arc[min][j] < d[j])
			{
				d[j] = d[min] + G->arc[min][j];
				p[j] = min;
			}
		}
	}
	for (int i = 0; i < G->vexNUM; i++)
	{
		printf("%d %d %d\n", s[i], p[i], d[i]);
	}
	DWay(p, 3,G,index);//index到第六节点的最短路径
}