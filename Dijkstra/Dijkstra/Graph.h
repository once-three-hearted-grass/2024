#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

#define MAX 32326

typedef struct Graph
{
	char* vex;
	int** arc;
	int vexNUM;
	int arcNUM;
}Graph;

//初始化图
Graph* InitGraph(int vexnum);

//创造图
void CreatGraph(Graph* G, char* vex,int*arc);

//深度优先遍历图
void DFS(Graph* G, int* visited, int index);

//迪杰斯特拉算法
void Dijkstra(Graph* G,int index);
