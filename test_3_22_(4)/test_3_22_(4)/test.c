#define _CRT_SECURE_NO_WARNINGS 1
//练习使用动态内存相关的4个函数，并调试观察。
//malloc、calloc、realloc、free
#include<stdio.h>
#include<stdlib.h>
int main()
{
	//int* arr = (int*)malloc(5 * sizeof(int));
	int* arr = (int*)calloc(5 , sizeof(int));
	if (arr == NULL)
	{
		return 1;
	}
	for (int i = 0; i < 5; i++)
	{
		arr[i] = i + 1;
	}
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	int* pf = (int*)realloc(arr, 10 * sizeof(int));
	if (pf == NULL)
	{
		return 1;
	}
	else
	{
		arr = pf;
	}
	for (int i = 0; i < 10; i++)
	{
		arr[i] = i + 1;
	}
	for (int i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
	free(arr);
	arr = NULL;
	return 0;
}