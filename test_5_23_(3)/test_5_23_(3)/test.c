#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
//给你一个二叉树的根节点 root ， 检查它是否轴对称。

  struct TreeNode {
      int val;
      struct TreeNode *left;
     struct TreeNode *right;
 };
 
  bool my_isSymmetric(struct TreeNode* left, struct TreeNode* right)
  {
      //其实看是不是对称的，一定程度上就是看两个子树的某些子树是不是相同的
      //这里我们要递归的话，先看两个头相不相同
      // 
      //相同的话的话，就就看左的右，右的左两个相不相同，而这两个也可以递归，还有左的左，右的右，两者要同时相同才可以
      if (left == NULL && right == NULL)
      {
          return true;
      }
      if (left == NULL || right == NULL)
      {
          return false;
      }
      if (left->val != right->val)
      {
          return false;
      }
        return my_isSymmetric(left->right, right->left)&&my_isSymmetric(left->left, right->right);
  }
bool isSymmetric(struct TreeNode* root) {
    //递归的话就是，先看根，如果根相同，就看左子树与右子树是不是对称的//单独用个函数
    if (root == NULL)
    {
        return true;
    }
    return my_isSymmetric(root->left, root->right);
}