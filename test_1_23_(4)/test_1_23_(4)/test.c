#define _CRT_SECURE_NO_WARNINGS 1
//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
//int fac(int n)
//{
//	if (n == 0)
//	{
//		return 1;
//	}
//	else
//	{
//		return n * fac(n - 1);
//	}
//}
int fac(int n)
{
	int i = 0;
	int get = 1;
	for (i = 1; i <= n; i++)
	{
		get *= i;
	}
	return get;
}
#include<stdio.h>
int main()
{
	int n = 0;
	while ((scanf("%d", &n))!= EOF)
	{
		int ret = fac(n);
		printf("%d的阶乘是%d\n", n, ret);
	}
	return 0;
}