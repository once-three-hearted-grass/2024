#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

int main() {
    int a, b;
    while (cin >> a >> b) { // 注意 while 处理多个 case
        int year = a;
        int month = 0;
        int day = 0;
        int arr[] = { 0,31,59,90,120,151,181,212,243,273,304,334,365 };
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
        {
            for (int i = 2; i < 13; i++)
            {
                arr[i]++;
            }
        }
        for (int i = 1; i < 13; i++)
        {
            if (b < arr[i])
            {
                month = i;
                day = b - arr[i - 1];
                break;
            }
        }
        printf("%d-%02d-%02d", year, month, day);
    }
}
// 64 位输出请用 printf("%lld")