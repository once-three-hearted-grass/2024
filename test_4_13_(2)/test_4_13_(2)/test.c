#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
//给你一个链表的头节点 head 和一个整数 val ，请你删除链表中所有满足 Node.val == val 的节点，并返回 新的头节点 。
//输入：head = [1, 2, 6, 3, 4, 5, 6], val = 6
//输出：[1, 2, 3, 4, 5]

  struct ListNode {
      int val;
      struct ListNode *next;
  };
typedef struct ListNode ListNode;
//struct ListNode* removeElements(struct ListNode* head, int val)
//{
//    //法一，还是用三个指针，一次遍历，遇到val就把它free，就在原链表上修改
//    // prev  pcur  next
//    //接下来，空链表，一个节点，两个节点挨着挨着讨论
//    if (head == NULL)
//    {
//        return NULL;
//    }
//    ListNode* prev = head;
//    ListNode* pcur = prev->next;
//    while(prev&&prev->val == val)//一开始那个prev节点要看一下是否满足题意
//    {
//        free(prev);
//        head = pcur;
//        prev = pcur;
//        if (pcur != NULL)
//        {
//            pcur = pcur->next;
//        }
//    }
//    while (pcur)
//    {
//        ListNode* next = pcur->next;
//        if (pcur->val == val)
//        {
//            if (head&&head->val == val)
//            {
//                head = next;//这个处理head的方法//万一head走到了NULL?
//            }
//            free(pcur);
//            pcur = next;
//            if (next!= NULL)
//            {
//                next = next->next;
//            }
//            //然后还要连接好prev，pcur，next
//            prev->next = pcur;
//        }
//        else
//        {
//            prev = pcur;
//            pcur = next;
//            if (next!= NULL)
//            {
//                next = next->next;
//            }
//        }
//    }
//    return head;
//}
struct ListNode* removeElements(struct ListNode* head, int val)
{
    //方法二
    //创建一个带头链表，不是val的就插入到新链表中
    if (head == NULL)
    {
        return NULL;
    }
    ListNode* phead = (ListNode*)malloc(sizeof(ListNode));
    phead->next = NULL;
    ListNode* tail = phead;
    ListNode* pcur = head;
    ListNode* next = head->next;
    while (pcur)
    {
        if (pcur->val == val)
        {
            //销毁，并继续往下走
            free(pcur);
            pcur = next;
            if (next !=NULL )
            {
                next = next->next;
            }
        }
        else
        {
            //就尾插到新链表中
            tail->next = pcur;
            tail = pcur;//tail要一直在后面
            pcur = next;
            if (next != NULL)
            {
                next = next->next;
            }
        }
    }
    //最后可能最后tail的next不是NULL，所以还要设置一下
    tail->next = NULL;
    ListNode* ret = phead->next;
    free(phead);
    return ret;
}
int main()
{
    ListNode* p1 = (ListNode*)malloc(sizeof(ListNode));
    p1->val = 7;
    ListNode* p2 = (ListNode*)malloc(sizeof(ListNode));
    p2->val = 7;
    ListNode* p3 = (ListNode*)malloc(sizeof(ListNode));
    p3->val =7;
    ListNode* p4 = (ListNode*)malloc(sizeof(ListNode));
    p4->val = 7;
    p1->next = p2;
    p2->next = p3;
    p3->next = p4;
    p4->next = NULL;
    removeElements(p1, 7);
    return 0;
}