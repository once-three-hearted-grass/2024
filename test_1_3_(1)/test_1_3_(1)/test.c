#define _CRT_SECURE_NO_WARNINGS 1
//描述
//小乐乐最近接触了求和符号Σ，他想计算的结果。但是小乐乐很笨，请你帮助他解答。
//
//输入描述：
//输入一个正整数n(1 ≤ n ≤ 109)
//
//输出描述：
//输出一个值，为求和结果。
#include <stdio.h>

int main() {
    int n = 0;
    scanf("%d", &n);
    long int sum = 0;
    int i = 0;
    for (i = 0; i <= n; i++)
    {
        sum += i;
    }
    printf("%ld", sum);
    return 0;
}