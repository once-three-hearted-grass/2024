#define _CRT_SECURE_NO_WARNINGS 1
//strtok
#include<stdio.h>
#include<string.h>
int main()
{
	char arr1[] = "abcdefj%&hijk&lmn";
	char arr2[] = "%&";
	//strtok会将以arr2中的字符为分隔，第一次将%换为\0,在返回a的地址，所以会打印出abcdefj,并记录%的地址，若要继续以%的地方进行分割，就传NULL
	//则会把&改为\0,并返回h的地址，就这样可以继续打印了，继续打印hijk,继续传NULL，返回l地址，打印lmn，若再传NULL，则返回NULL，
	//所以这就可以用个循环来判断
	//若连续两个或多个分隔符在一起，就会一并变为\0？不，不会，这只会把第一个分隔符变为\0,然后跳过后面多余的分隔符，且只浪费调用一次strtok
	//char* ret = strtok(arr1, arr2);
	//printf("%s\n", ret);
	//ret = strtok(NULL, arr2);
	//printf("%s\n", ret);
	//ret = strtok(NULL, arr2);
	//printf("%s\n", ret);
	//ret = strtok(NULL, arr2);
	//printf("%s\n", ret);
	for (char* ret = strtok(arr1, arr2);ret!=NULL ;ret=strtok(NULL,arr2) )
	{
		printf("%s\n", ret);
	}
	return 0;
}