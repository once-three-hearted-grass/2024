#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//

int func(int n)
{
	if (n < 1)
	{
		return 0;
	}
	if (n == 1 || n == 2)
	{
		return 1;
	}
	else
	{
		return func(n - 1) + func(n - 2);
	}
}


int main()
{
	int n = 10;//求第n个斐波那契数
	int ret=func(n);
	printf("%d", ret);
	return 0;
}