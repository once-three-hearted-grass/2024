#define _CRT_SECURE_NO_WARNINGS 1
//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
#include<stdio.h>
//void Er(unsigned int n,int i)
//{
//	if (i < 31)
//	{
//		Er(n / 2, i + 1);
//	}
//	printf("%d", n % 2);
//}
//void Er(int n, int i)
//{
//	int j = 31;
//	while (j >= 0)
//	{
//		if ((n & (1 << j))== 0)
//		{
//			printf("0");
//		}
//		else
//		{
//			printf("1");
//		}
//		j--;
//	}
//}
void Er(int n, int i)
{
	int j = 31;
	while (j >= 0)
	{
		if ((n & (1 << j)) == 0)
		{
			printf("0");
		}
		else
		{
			printf("1");
		}
		j--;
	}
}
void Er1(int n, int i)
{
	int j = 31;
	while (j >= 0)
	{
		if ((n & (1 << j)) == 0)
		{
			printf("0");
		}
		else
		{
			printf("1");
		}
		j-=2;
	}
}
void Er2(int n, int i)
{
	int j = 30;
	while (j >= 0)
	{
		if ((n & (1 << j)) == 0)
		{
			printf("0");
		}
		else
		{
			printf("1");
		}
		j-=2;
	}
}
int main()
{
	int n = 0;
	while ((scanf("%d", &n)) != EOF)
	{
		unsigned int m = n;
		int i = 0;
		printf("%d在内存中存储的二进制位为:", n);
		Er(m,i);
		printf("\n");
		printf("%d在内存中存储的偶数二进制位为:", n);
		Er1(m,i);
		printf("\n");
		printf("%d在内存中存储的奇数二进制位为:", n);
		Er2(m,i);
		printf("\n");
	}
	return 0;
}