#pragma once
#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<errno.h>
#include<string.h>

typedef int SLDateType;

typedef struct SeqList//顺序表
{
	SLDateType* a;
	int size;//当前存了几个
	int capacity;//当前总共能存几个
}SL;

//初始化
void SLInit(SL* ps);

//销毁
void SLDestroy(SL* ps);

//尾插
void SLPushBack(SL* ps, SLDateType x);

//检查是否需要扩容
void SLCheckCapacity(SL* ps);

//打印
void SLPrint(SL* ps);

//头插
void SLPushFront(SL* ps, SLDateType x);

//头删
void SLPopFront(SL*ps);

//尾删
void SLPopBack(SL* ps);

//找到某个数据并返回这个数据的数组下标
int SLFind(SL* ps, SLDateType x);

//在指定位置之前插入
void SLInsert(SL* ps, int pos, SLDateType x);//pos是下标

//在指定位置删除
void SLErase(SL* ps, int pos);