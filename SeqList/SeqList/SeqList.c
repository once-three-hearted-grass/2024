#include"SeqList.h"
//初始化
void SLInit(SL* ps)
{
	assert(ps);
	ps->a = NULL;
	ps->capacity = ps->size = 0;
}
//销毁
void SLDestroy(SL* ps)
{
	assert(ps);
	free(ps->a);
	ps->a = NULL;
	ps->capacity = ps->size = 0;
}

//检查是否需要扩容
void SLCheckCapacity(SL* ps)
{
	assert(ps);
	if (ps->capacity == ps->size)
	{
		int newcapacity = ps->capacity == 0 ? 4 : (ps->capacity * 2);
		SLDateType* newps = realloc(ps->a, sizeof(SLDateType) * newcapacity);
		if (newps == NULL)
		{
			perror("realloc");
			exit(-1);
		}
		ps->a = newps;
		ps->capacity = newcapacity;
	}
}

//尾插
//void SLPushBack(SL* ps, SLDateType x)
//{
//	assert(ps);
//	//先检查空间够不够
//	//空间不够有两种情况，初始时capacity=0=size，还有平时的capacity=size
//	SLCheckCapacity(ps);
//	//直接将下标为size的赋值成x
//	ps->a[ps->size] = x;
//	ps->size++;
//}


//打印
void SLPrint(SL* ps)
{
	assert(ps);
	assert(ps->a);//看数组是否为空,即是否为空指针，不然是无法访问的
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ",ps->a[i]);
	}
	printf("NULL\n");
}

////头插
//void SLPushFront(SL* ps, SLDateType x)
//{
//	assert(ps);
//	//头插的话，则要把全部数据向后移动一位，把第一位留出来、
//	SLCheckCapacity(ps);
//	//从后往前移动
//	for (int i = ps->size; i>=1; i--)//初始：a[size]=a[size-1],,,,,,,末尾a[1]=a[0]
//	{
//		ps->a[i] = ps->a[i - 1];
//	}
//	ps->a[0] = x;//就算x是结构体，赋值用等号也是成立的
//	ps->size++;
//}

//头删
//void SLPopFront(SL* ps)
//{
//	assert(ps);
//	assert(ps->a);//看数组是否为空,即是否为空指针，不然是无法访问的
//	assert(ps->size > 0);//必须有元素才可以删
//	//头删就是往前覆盖
//	for (int i = 0; i <=ps->size-2 ; i++)//初始a[0]=a[1],末尾a[size-2]=a[size-1]就根据这个来设置循环的初始条件和退出条件
//	{
//		ps->a[i] = ps->a[i + 1];
//	}
//	ps->size--;
//}

////尾删
//void SLPopBack(SL* ps)
//{
//	assert(ps);
//	assert(ps->a);
//	assert(ps->size > 0);
//	ps->size--;
//}

//找到某个数据并返回这个数据的数组下标
int SLFind(SL* ps, SLDateType x)
{
	assert(ps);
	assert(ps->a);
	for (int i = 0; i < ps->size; i++)
	{
		if (ps->a[i] == x)
		{
			return i;
		}
	}
	return -1;
}

//在指定位置之前插入
void SLInsert(SL* ps, int pos, SLDateType x)//pos是下标
{
	assert(ps);
	SLCheckCapacity(ps);
	//将pos位置之后的向后移就可以了
	for (int i = ps->size;i>=pos+1; i--)//初始a[size]=a[size-1]末尾a[pos+1]=a[pos]
	{
		ps->a[i] = ps->a[i - 1];
	}
	ps->a[pos] = x;
	ps->size++;
}

//尾插2.0
void SLPushBack(SL* ps, SLDateType x)
{
	assert(ps);
	//先检查空间够不够
	//空间不够有两种情况，初始时capacity=0=size，还有平时的capacity=size
	SLCheckCapacity(ps);
	SLInsert(ps, ps->size, x);
}

//头插2.0
void SLPushFront(SL* ps, SLDateType x)
{
	assert(ps);
	//头插的话，则要把全部数据向后移动一位，把第一位留出来、
	SLCheckCapacity(ps);
	SLInsert(ps, 0, x);
}

//在指定位置删除
void SLErase(SL* ps, int pos)
{
	assert(ps);
	assert(ps->a);
	assert(ps->size > 0);
	//删除，即从pos位置开始，从前往后覆盖
	for (int i = pos; i <= ps->size - 2;i++)//初始a[pos]=a[pos+1]末a[size-2]=a[size-1]
	{
		ps->a[i] = ps->a[i+ 1];
	}
	ps->size--;
}

//头删2.0
void SLPopFront(SL* ps)
{
	assert(ps);
	assert(ps->a);//看数组是否为空,即是否为空指针，不然是无法访问的
	assert(ps->size > 0);//必须有元素才可以删
	//头删就是往前覆盖
	SLErase(ps, 0);
}

//尾删2.0
void SLPopBack(SL* ps)
{
	assert(ps);
	assert(ps->a);
	assert(ps->size > 0);
	SLErase(ps, ps->size);
}



