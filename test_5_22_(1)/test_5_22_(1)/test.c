#define _CRT_SECURE_NO_WARNINGS 1

//给你两棵二叉树的根节点 p 和 q ，编写一个函数来检验这两棵树是否相同。
//如果两个树在结构上相同，并且节点具有相同的值，则认为它们是相同的。

#include<stdbool.h>
#include<stdlib.h>

 struct TreeNode {
     int val;
     struct TreeNode *left;
     struct TreeNode *right;
};

bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
    //判断两个树是不是相同的树的话，就先判断根是不是相同的树
    //在判断两个子树是不是相同的树，而两个子树的判断的逻辑与根是相同的，这就叫大事化小

    //因为要两个子树同时为相同的树，才是真正的树，所以用&&，再加上返回又不是地址之类的，不用再设变量去保存返回值了
    
    //接下来再来看，递归停止条件，停止条件就是两个数值不相等的话就返回错误
    //而在使用两个数值之前就要看他们是不是空，一个为空都不能看数值
    //两个均为空，返回正确（说明前面的都相同了），一个空一个不为空就返回错误，这是处理空的时候，

    if (p == NULL && q == NULL)
    {
        return true;
    }
    //走到这里已经说明了两个不同时为NULL了
    //至少有一个为NULL
    if (p == NULL || q == NULL)
    {
        return false;
    }
    //根相同根本不能说明什么，要子树也相同才可以
    //不相同就说明返回错误
    if (p->val != q->val)
    {
        return false;
    }
    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
}

int main()
{
	return 0;
}