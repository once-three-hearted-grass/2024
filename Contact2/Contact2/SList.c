
#include"SList.h"

//打印
//void SLTPrint(SLTNode* phead)
//{
//	SLTNode* pcur = phead;
//	while (pcur)
//	{
//		printf("%d->", pcur->data);
//		pcur = pcur->next;
//	}
//	printf("NULL\n");
//}

////创建新的节点
SLTNode* SLTBuyNode(SLTDataType x)
{
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
	if (newnode == NULL)
	{
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;
	return newnode;
}

//尾插
void SLTPushBack(SLTNode** pphead, SLTDataType x)
{
	assert(pphead);//判断链表是否创建
	SLTNode* phead = *pphead;
	SLTNode* newnode = SLTBuyNode(x);
	if (*pphead == NULL)//这时的尾插就要改变表头//所以才要传二级指针
	{
		*pphead = newnode;
	}
	else
	{
		//这时的话就要先找到最后那个节点了
		SLTNode* tail = phead;
		while (tail->next)//这样找到最后一个节点
		{
			tail = tail->next;
		}
		tail->next = newnode;
	}
}


//头插
void SLTPushFront(SLTNode** pphead, SLTDataType x)
{
	assert(pphead);
	SLTNode* newnode = SLTBuyNode(x);
	SLTNode* phead = *pphead;
	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		newnode->next = phead;
		*pphead = newnode;//头插一定会改变头节点
	}
}


//尾删
void SLTPopBack(SLTNode** pphead)//只剩一个时，尾删就会改变头节点了
{
	assert(pphead);
	assert(*pphead);//链表为空时不能删了
	//要先找到尾部
	//删除最后一个时还要有前一个的地址哦
	SLTNode* phead = *pphead;
	SLTNode* tail = phead;
	SLTNode* pre = phead;
	if (phead->next == NULL)//要改变头节点了
	{
		free(phead);
		*pphead = NULL;//这里就结束了不在访问了，不然后面会出错
		return;
	}
	while (tail->next)
	{
		pre = tail;//保留前一个
		tail = tail->next;
	}
	free(tail);
	pre->next = NULL;
}


//头删
void SLTPopFront(SLTNode** pphead)
{
	assert(pphead);
	assert(*pphead);//链表为空时不能删了
	//一直会改变头节点、
	SLTNode* phead = *pphead;
	*pphead = phead->next;
	free(phead);
}


//查找
//SLTNode* SLTFind(SLTNode* phead, SLTDataType x)
//{
//	assert(phead);
//	SLTNode* pcur = phead;
//	while (pcur)
//	{
//		if (pcur->data == x)//
//		{
//			return pcur;
//		}
//		pcur = pcur->next;
//	}
//	return NULL;
//}


//在指定位置之前插入数据
void SLTInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x)
{
	assert(pphead);
	assert(*pphead);//链表为空的话第二个参数就不存在了
	assert(pos);
	SLTNode* phead = *pphead;
	SLTNode* pre = phead;
	SLTNode* newnode = SLTBuyNode(x);
	//第一个节点前插入，就要改变*pphead
	if (phead == pos)
	{
		*pphead = newnode;
		newnode->next = phead;
		//后面就不用进行了
		return;
	}
	while (pre->next != pos)//找到pos的前一个
	{
		pre = pre->next;
		if (pre == NULL)
		{
			printf("找不到您输入的节点\n");
			exit(-1);
		}
	}
	pre->next = newnode;
	newnode->next = pos;
}


//在指定位置之后插入数据
void SLTInsertAfter(SLTNode* pos, SLTDataType x)
{
	assert(pos);
	SLTNode* newnode = SLTBuyNode(x);
	SLTNode* next = pos->next;
	pos->next = newnode;
	newnode->next = next;
}


//删除指定节点
void SLTErase(SLTNode** pphead, SLTNode* pos)
{
	assert(pphead);
	assert(*pphead);
	assert(pos);
	SLTNode* phead = *pphead;
	SLTNode* next = pos->next;
	SLTNode* pre = phead;
	if (phead == pos)
	{
		free(pos);
		*pphead = next;
		return;
	}
	//先找到pos前一个
	while (pre->next != pos)
	{
		pre = pre->next;
	}
	free(pos);
	pre->next = next;
}


//删除pos之后的节点
void SLTEraseAfter(SLTNode* pos)
{
	assert(pos);
	assert(pos->next);
	SLTNode* next = pos->next;
	pos->next = pos->next->next;
	free(next);
	next = NULL;
}

//销毁链表
void SListDesTroy(SLTNode** pphead)
{
	assert(pphead);
	SLTNode* cur = *pphead;
	while (cur)
	{
		SLTNode* next = cur->next;
		free(cur);
		cur = next;
	}
	*pphead = NULL;
}
