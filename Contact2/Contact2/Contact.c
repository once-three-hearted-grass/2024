#include"SList.h"

//很多单链表的内容，通讯录都可以用
//单链表的打印（因为%d），查找不能用了（因为按照说明查找呢，结构体不能==）


//初始化通讯录
void InitContact(contact** con)
{
	*con = NULL;
}

//添加通讯录数据
void AddContact(contact** con)
{
	assert(con);
	PeoInfo peo = { 0 };
	printf("请输入您要添加联系人姓名：");
	scanf("%s", peo.name);
	printf("请输入您要添加联系人性别：");
	scanf("%s", peo.sex);
	printf("请输入您要添加联系人年龄：");
	scanf("%d", &peo.age);
	printf("请输入您要添加联系人电话：");
	scanf("%s", peo.tel);
	printf("请输入您要添加联系人地址：");
	scanf("%s", peo.addr);
	//尾插
	printf("\n添加成功\n");
	SLTPushBack(con, peo);
}

//展示通讯录数据
void ShowContact(contact* con)
{
	assert(con);
	contact* pcur = con;
	printf("姓名        性别        年龄        电话        地址\n");
	while (pcur)
	{
		printf("%-12s%-12s%-12d%-12s%-12s\n",
			pcur->data.name,
			pcur->data.sex,
			pcur->data.age,
			pcur->data.tel,
			pcur->data.addr);
		pcur = pcur->next;
	}
}

//找名字
contact* FindByName(contact* head, char* arr)
{
	contact* pcur = head;
	while (pcur)
	{
		if (strcmp(pcur->data.name, arr)==0)
		{
			return pcur;
		}
		pcur = pcur->next;
	}
	return NULL;
}

//删除通讯录数据
void DelContact(contact** con)
{
	assert(con);
	assert(*con);
	char arr[NAME_MAX] = { 0 };
	printf("请输入您要删除联系人的名字：");
	scanf("%s", arr);
	contact*del=FindByName(*con, arr);
	if (del == NULL)
	{
		printf("错误：无该联系人\n");
		return;
	}
	SLTErase(con, del);
}

//查找通讯录数据
void FindContact(contact* con)
{
	assert(con);
	char arr[NAME_MAX] = { 0 };
	printf("请输入您要查询联系人的名字：");
	scanf("%s", arr);
	contact* Find = FindByName(con, arr);
	if (Find == NULL)
	{
		printf("错误：无该联系人\n");
		return;
	}
	printf("姓名        性别        年龄        电话        地址\n");
	printf("%s          %s          %d          %s          %s\n",
		Find->data.name,
		Find->data.sex,
		Find->data.age,
		Find->data.tel,
		Find->data.addr);
}

//修改通讯录数据
void ModifyContact(contact** con)
{
	assert(con);
	assert(*con);
	char arr[NAME_MAX] = { 0 };
	printf("请输入您要修改的联系人的名字：");
	scanf("%s", arr);
	contact* Find = FindByName(con, arr);
	if (Find == NULL)
	{
		printf("错误：无该联系人\n");
		return;
	}
	printf("请输入您修改后的联系人姓名：");
	scanf("%s", Find->data.name);
	printf("请输入您修改后的联系人性别：");
	scanf("%s", Find->data.sex); 
	printf("请输入您修改后的联系人年龄：");
	scanf("%d", &Find->data.age);
	printf("请输入您修改后的联系人电话：");
	scanf("%s", Find->data.tel);
	printf("请输入您修改后的联系人地址：");
	scanf("%s", Find->data.addr);
}

//销毁通讯录数据
void DestroyContact(contact** con)
{
	assert(con);
	SListDesTroy(con);
}
