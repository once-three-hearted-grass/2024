#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        //两个单身狗问题，先全部异或吧
        int sum = 0;
        for (int i = 0; i < nums.size(); i++)
        {
            sum ^= nums[i];
        }
        //找到两个单身狗的二进制不同的地方，相同取0，相异取1
        int j = 0;
        while (((sum >> j) & 1) == 0)
        {
            j++;
        }
        //然后开始按照j处的不同，分别异或，就可以得到两个单身狗了
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 0; i < nums.size(); i++)
        {
            if (((nums[i] >> j) & 1) == 0)
            {
                sum1 ^= nums[i];
            }
            else
            {
                sum2 ^= nums[i];
            }
        }
        vector<int> ret= {sum1, sum2};
        return ret;
    }
};

int main()
{
    vector<int> a = { 1,2,1,3,2,5 };
    vector<int> b = Solution().singleNumber(a);//这个是匿名对象
    return 0;
}