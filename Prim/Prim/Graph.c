#define _CRT_SECURE_NO_WARNINGS 1
#include"Graph.h"

//初始化图
Graph* InitGraph(int vexnum)
{
	Graph* G = (Graph*)malloc(sizeof(Graph));
	G->vex = (char*)malloc(sizeof(char) * vexnum);
	//arc是一个二级指针数组，每个元素指向一个一维数组，也相当于一个二维数组
	G->arc = (int**)malloc(sizeof(int*) * vexnum);
	G->vexNUM = vexnum;
	G->arcNUM = 0;
	for (int i = 0; i < vexnum; i++)
	{
		G->arc[i] = (int*)malloc(sizeof(int) * vexnum);
	}
	return G;
}

//创造图
void CreatGraph(Graph* G, char* vex, int* arc)//第二个参数传的是顶点，第三个传的是边，强制类型转换而来的
{
	for (int i = 0; i < G->vexNUM; i++)
	{
		G->vex[i] = vex[i];//赋值顶点
		for (int j = 0; j < G->vexNUM; j++)//赋值边
		{
			G->arc[i][j] = arc[i * G->vexNUM + j];
			if (G->arc[i][j] >0&& G->arc[i][j]!=MAX)
			{
				G->arcNUM++;
			}
		}
	}
	G->arcNUM /= 2;//因为是无向图
}

//深度优先遍历图
void DFS(Graph* G, int* visited, int index)//第二个参数是看这个顶点是否被访问了,第二个参数是看从哪个顶点开始访问
{
	//先访问传入的这个顶点
	printf("v%c ", G->vex[index]);
	//标记
	visited[index] = 1;//表示被访问过了
	for (int i = 0; i < G->vexNUM; i++)
	{
		if (G->arc[index][i]>0 && G->arc[index][i] !=MAX&& visited[i] == 0)//有这条边并且这个顶点未被访问
		{
			//那就沿着这条边去访问这个顶点，这个顶点的访问都一样的
			DFS(G, visited, i);
		}
	}
}
//
//#include"Queue.h"
////广度优先遍历
//void BFS(Graph* G, int* visited, int index)
//{
//	//这个是先访问在入队列，再依次把相连边的的顶点先访问再入队列
//	//如果是先入队列，再出队列时在访问，就很有可能把同一个节点访问两次，不好，或者入队列前就标记好了，都一样的
//	//入队列前要标记好，这个是最重要的
//	printf("%c ", G->vex[index]);
//	visited[index] = 1;
//	Queue q;
//	QueueInit(&q);
//	QueuePush(&q, index);
//	while (!QueueEmpty(&q))
//	{
//		int i = QueueFront(&q);
//		QueuePop(&q);
//		//再挨着把相连边的顶点入队列
//		for (int j = 0; j < G->vexNUM; j++)
//		{
//			if (G->arc[i][j] == 1 && visited[j] == 0)
//			{
//				printf("%c ", G->vex[j]);
//				visited[j] = 1;
//				QueuePush(&q, j);
//			}
//		}
//	}
//	QueueDestroy(&q);
//}

Edge* InitEdge(Graph*G,int index)//index表示从哪个节点开始prim算法
{
	Edge* e = (Edge*)malloc(sizeof(Edge) * G->vexNUM);//用来存储树中节点到其他节点的最小距离，哪个距离为0说明树中的有什么节点
	//接下来把index到各个节点的距离弄进去
	for (int i = 0; i < G->vexNUM; i++)
	{
		e[i].vex = G->vex[index];//起点
		e[i].weight = G->arc[index][i];
	}
	return e;
}

int GetMinEdge(Edge* e, Graph* G)
{
	int index;//最小值的下标
	int min = MAX;
	for (int i = 0; i < G->vexNUM; i++)
	{
		if (e[i].weight != 0 && e[i].weight < min)//e[i].weight != 0因为这个是树到自己内节点的距离
		{
			min = e[i].weight;
			index = i;//这个也可以求出尾部
		}
	}
	return index;
}

void Prim(Graph* G,int index)
{
	Edge* e = InitEdge(G, index);
	//这里已经找好了一个节点，还要找n-1个
	for (int i = 0; i < G->vexNUM - 1; i++)
	{
		int min = GetMinEdge(e, G);
		//打印这条最小边
		printf("v%c->v%c,weight=%d\n", e[min].vex, G->vex[min], e[min].weight);
		//加入这个新节点到最小树中
		e[min].weight = 0;//其实也可以不用加
		//刷新最小树到各个节点的最小距离，就是看新节点都其他各个节点距离比不比原先小
		for (int j = 0; j < G->vexNUM; j++)
		{
			if (e[j].weight > G->arc[min][j])
			{
				e[j].weight = G->arc[min][j];
				e[j].vex = G->vex[min];
			}
		}
	}
}