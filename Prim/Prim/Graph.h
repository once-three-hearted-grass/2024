#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

//prim算法的话
#define MAX 32326

//没有边为MAX，自己到自己的边为0

typedef struct Edge {//定义一个结构体数组，用来记录最小生成树的到各个节点的最小距离和起始位置
	char vex;
	int weight;
}Edge;

typedef struct Graph
{
	char* vex;
	int** arc;
	int vexNUM;
	int arcNUM;
}Graph;

//初始化图
Graph* InitGraph(int vexnum);

//创造图
void CreatGraph(Graph* G, char* vex,int*arc);

//深度优先遍历图
void DFS(Graph* G, int* visited, int index);

//广度优先遍历
void BFS(Graph* G, int* visited, int index);

Edge* InitEdge(Graph* G, int index);

int GetMinEdge(Edge* e, Graph* G);

void Prim(Graph* G, int index);


