#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//输入描述：
//输入一行，每行空格分割，分别是年，月，日
//
//输出描述：
//输出是这一年的第几天
//示例1
//输入：
//2012 12 31
//复制
//输出：
//366
int main() 
{
    int year, month, day;
    while (scanf("%d %d %d", &year, &month, &day) != EOF)
    {
        int sum = 0;
        int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
        {
            arr[2] = 29;
        }
        for (int i = 1; i < month; i++)
        {
            sum += arr[i];
        }
        sum += day;
            printf("%d", sum);
    }
    return 0;
}