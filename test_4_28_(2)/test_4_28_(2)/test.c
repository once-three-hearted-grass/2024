#define _CRT_SECURE_NO_WARNINGS 1

//数组nums包含从0到n的所有整数，但其中缺了一个。请编写代码找出那个缺失的整数。你有办法在O(n)时间内完成吗？
//输入：[3, 0, 1]
//输出：2
#include<stdio.h>

int missingNumber(int* nums, int numsSize) {
	//法一：将1~n（1，2,3）的值加起来再减去数组的值（0,1，3）就是消失的数字
	//为了防止越界，所以要边加边减
	int ret = 0;
	for (int i = 0; i < numsSize; i++)
	{
		ret = ret - nums[i];
		ret = ret + i + 1;
	}
	return ret;
}
int missingNumber(int* nums, int numsSize) {
	//法二：创造一组数字（0~n）然后全部与数组的值异或，用找单身狗的方法来找消失的数字
	int ret = 0;
	for (int i = 0; i < numsSize; i++)
	{
		ret ^= nums[i];
	}
	for (int i = 0; i <= numsSize; i++)
	{
		ret ^= i;
	}
	return ret;

}
int main()
{

	return 0;
}