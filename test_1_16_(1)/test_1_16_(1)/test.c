#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() {
    int n = 0;
    scanf("%d", &n);
    getchar();
    for (int i = 0; i < n; i++)
    {
        char ch = 0;
        int flag = 0;
        while ((ch = getchar()) != '\n')
        {
            if ((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'z') || (ch
                >= 'A' && ch <= 'Z'))
            {

            }
            else
            {
                flag = 1;
            }
        }
        if (flag == 1)
        {
            printf("NO\n");
        }
        else {
            printf("YES\n");
        }
    }
    return 0;
}