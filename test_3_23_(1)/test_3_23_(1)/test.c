#define _CRT_SECURE_NO_WARNINGS 1
//下面程序的功能是什么 ?
#include<stdio.h>
int main()
{
    long num = 0;
    FILE* fp = NULL;
    if ((fp = fopen("fname.dat", "r")) == NULL)
    {
        printf("Can’t open the file!");
        exit(0);
    }
    while (fgetc(fp) != EOF)
    {
        num++;
    }
    printf("num=%d\n", num);//统计文件里有几个字符
    fclose(fp);
    return 0;
}
