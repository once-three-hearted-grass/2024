#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int main() {
    int t;
    double wage;
    double total;
    scanf("%d %lf", &t, &wage);
    if (t > 0&&t<168) {
        if (t <= 40) {
            total = t * wage;
            int tmp = (int)total;
            if ((float)tmp == total)
            {
                printf("%d\n", tmp);
            }
            else
            {
                printf("%.2lf\n", total);
            }
        }
        else if (t > 40 && t <= 60) {
            total = 40.0 * wage + (t - 40.0) * 1.5 * wage;
            int tmp = (int)total;
            if ((float)tmp == total)
            {
                printf("%d\n", tmp);
            }
            else
            {
                printf("%.2lf\n", total);
            }
        }else if(t > 60.0) {
            total = 40.0 * wage + (60.0 - 40.0) * 1.5 * wage + (t - 60.0) * 3.0 * wage;
            int tmp = (int)total;
            if ((float)tmp == total)
            {
                printf("%d\n", tmp);
            }
            else
            {
                printf("%.2lf\n", total);
            }
        }
    }
    else {
        printf("input is wrong!\n");
    }
    return 0;//
}