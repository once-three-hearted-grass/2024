#include"SeqList.h"

//初始化
void InitContact(contact* con)
{
	SLInit(con);
}

//销毁
void DestroyContact(contact* con)
{
	SLDestroy(con);
}

//添加数据//尾插
void AddContact(contact* con)
{
	PeoInfo peo;
	printf("请输入您要添加的联系人姓名:");
	scanf("%s", peo.name);
	printf("请输入您要添加的联系人性别:");
	scanf("%s", peo.sex);
	printf("请输入您要添加的联系人年龄:");
	scanf("%d", &peo.age);
	printf("请输入您要添加的联系人电话:");
	scanf("%s", peo.tel);
	printf("请输入您要添加的联系人地址(只需输入所在城市即可):");
	scanf("%s", peo.addr);
	printf("添加成功\n");
	SLPushBack(con, peo);
}

//展示通讯录数据
void ShowContact(contact* con)
{
	assert(con);
	assert(con->a);
	assert(con->size >= 0);
	if (con->size == 0)
	{
		printf("通讯录为空,请添加数据\n");
		return;
	}
	printf("____________________________________________________________________\n");
	printf("|姓名        |性别        |年龄        |电话         |地址         |\n");
	printf("|------------|------------|------------|-------------|-------------|\n");
	for (int i = 0; i < con->size; i++)
	{
		printf("|%-12s|%-12s|%-12d|%-13s|%-13s|\n", con->a[i].name, con->a[i].sex, con->a[i].age, con->a[i].tel, con->a[i].addr);
		printf("|------------|------------|------------|-------------|-------------|\n");
	}
}


//删除联系人数据、
void DelContact(contact* con)
{
	assert(con);
	assert(con->a);
	assert(con->size >= 0);
	if (con->size == 0)
	{
		printf("通讯录为空,请添加数据\n");
		return;
	}
	printf("请输入您要删除的联系人的姓名:");
	char DelName[NAME_MAX] = { 0 };
	scanf("%s", DelName);
	//接下来找到这个名字对应数组下标就可以了
	for (int i = 0; i < con->size; i++)
	{
		if (strcmp(DelName, con->a[i].name) == 0)
		{
			//直接删除
			SLErase(con, i);
			printf("删除成功!\n");
			return;
		}
	}
	printf("找不到要删除的数据!\n");
}

//查找通讯录数据
void FindContact(contact* con)
{
	assert(con);
	assert(con->a);
	assert(con->size >= 0);
	if (con->size == 0)
	{
		printf("通讯录为空,请添加数据\n");
		return ;
	}
	printf("请输入您要查找的联系人的姓名:");
	char FindName[NAME_MAX] = { 0 };
	scanf("%s", FindName);
	//接下来找到这个名字对应数组下标就可以了
	for (int i = 0; i < con->size; i++)
	{
		if (strcmp(FindName, con->a[i].name) == 0)
		{
			//直接打印
			printf("____________________________________________________________________\n");
			printf("|姓名        |性别        |年龄        |电话         |地址         |\n");
			printf("|------------|------------|------------|-------------|-------------|\n");
			printf("|%-12s|%-12s|%-12d|%-13s|%-13s|\n", con->a[i].name, con->a[i].sex, con->a[i].age, con->a[i].tel, con->a[i].addr);
			printf("|------------|------------|------------|-------------|-------------|\n");
			return;
		}
	}
	printf("找不到查找的数据!\n");
}

//修改某个通讯人的数据
void ModifyContact(contact* con)
{
	assert(con);
	assert(con->a);
	assert(con->size >= 0);
	if (con->size == 0)
	{
		printf("通讯录为空,请添加数据\n");
		return;
	}
	printf("请输入您要修改的联系人的姓名:");
	char ModifyName[NAME_MAX] = { 0 };
	scanf("%s", ModifyName);
	//接下来找到这个名字对应数组下标就可以了
	for (int i = 0; i < con->size; i++)
	{
		if (strcmp(ModifyName, con->a[i].name) == 0)
		{
			//直接修改
			printf("请输入您修改后的联系人姓名:");
			scanf("%s", con->a[i].name);
			printf("请输入您修改后的联系人性别:");
			scanf("%s", con->a[i].sex);
			printf("请输入您修改后的联系人年龄:");
			scanf("%d", &con->a[i].age);
			printf("请输入您修改后的联系人电话:");
			scanf("%s", con->a[i].tel);
			printf("请输入您修改后的联系人地址(只需输入所在城市即可):");
			scanf("%s", con->a[i].addr);
			printf("修改成功\n");
			return;
		}
	}
	printf("找不到要修改的数据!\n");
}

//实现qsort中的那个参数那个排序函数
int SortByName(const void* e1, const void* e2)
{
	//e1先转换为PeoInfo*型指针，在->访问到name数组进行比较就可以了
	return strcmp(((PeoInfo*)e1)->name, ((PeoInfo*)e2)->name);
}

//void qsort( void *base, size_t num, size_t width, int (*compare )(const void *elem1, const void *elem2 ) );
//给通讯录的联系人按照名字拼音来排序
void SortContact(contact* con)
{
	qsort(con->a, con->size, sizeof(con->a[0]),SortByName );
}


//提取文件到数据中
void LoadContact(contact* con)
{
	FILE* pf = fopen("Contact.txt", "r");
	if (pf == NULL)
	{
		perror("fopen");
		return;
	}
	//读入结构体
		//int fscanf( FILE *stream, const char *format [, argument ]... );
		PeoInfo peo = { 0 };
		//for (int i = 0; fscanf(pf, "%-12s%-12s%-12d%-13s%-13s\n", peo.name, peo.sex, &peo.age, peo.tel, peo.addr) == 5; i++)
		//不能这样写，因为这样的话，sscanf会以为那些空格也是字符串的内容，实际上不是，那只是多余的而已
		//而且用scanf的时候不能用-是不能用%-s中的-的
		for (int i = 0; fscanf(pf, "%s%s%d%s%s\n", peo.name, peo.sex, &peo.age, peo.tel, peo.addr) == 5; i++)
			//for (int i = 0; fscanf(pf, "%-12s%-12s%-12d%-13s%-13s\n", peo.name, peo.sex, &peo.age, peo.tel, peo.addr) == 5; i++)
		{
			//还要考虑增容问题
			SLCheckCapacity(con);
			con->a[i] = peo;
			con->size++;
		}
	
	fclose(pf);
	pf = NULL;
}


//保存数据到文件中
void SaveContact(contact* con)
{
	FILE* pf = fopen("Contact.txt", "w");
	if (pf == NULL)
	{
		perror("fopen");
		return;
	}
	for (int i = 0;i<con->size; i++)
	{
		fprintf(pf, "%-12s%-12s%-12d%-13s%-13s\n", con->a[i].name, con->a[i].sex, con->a[i].age, con->a[i].tel, con->a[i].addr);
	}
	fclose(pf);
	pf = NULL;
}