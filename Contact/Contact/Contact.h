#pragma once
#define NAME_MAX 20
#define SEX_MAX 10
#define TEL_MAX 20
#define ADDR_MAX 20

typedef struct SeqList contact;

typedef struct PersonInfo
{
	char name[NAME_MAX];
	char sex[SEX_MAX];
	int age;
	char tel[TEL_MAX];
	char addr[ADDR_MAX];
}PeoInfo;

//初始化
void InitContact(contact* con);

//销毁
void DestroyContact(contact* con);

//添加数据
void AddContact(contact* con);

//展示通讯录数据
void ShowContact(contact* con);

//删除联系人数据、
void DelContact(contact* con);

//查找通讯录数据
void FindContact(contact* con);

//修改某个通讯人的数据
void ModifyContact(contact* con);

//给通讯录的联系人按照名字拼音来排序
//这个函数就不单独用一个操作了，每次增加会修改，顺便排序就可以了
void SortContact(contact* con);

//从文件中提取数据
void LoadContact(contact* con);

//保存数据到文件中
void SaveContact(contact*con);
