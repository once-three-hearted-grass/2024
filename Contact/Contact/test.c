#include"SeqList.h"
//SeqListTest02()
//{
//	SL s;
//	SLInit(&s);
//	SLPushBack(&s, 1);
//	SLPushBack(&s, 2);
//	SLPushBack(&s, 3);
//	/*SLPushFront(&s, 1);
//	SLPushFront(&s, 2);
//	SLPushFront(&s, 3);*/
//	/*int ret=SLFind(&s, 0);
//	if (ret == -1)
//	{
//		printf("找不到\n");
//	}
//	else
//	{
//		printf("找到了，下标是%d\n",ret);
//	}*/
//	/*SLInsert(&s, 2, 90);
//	SLInsert(&s, 2, 80)*/;
//	/*SLErase(&s,0);
//	SLErase(&s,0);*/
//	SLPopFront(&s);
//	SLPopBack(&s);
//	SLPrint(&s);
//
//}
//SeqListTest01()
//{
//	SL s;
//	SLInit(&s);
//
//
//	SLPushBack(&s, 1);
//	SLPushBack(&s, 2);
//	SLPushBack(&s, 3);
//	//SLPushBack(&s, 4);
//	//SLPrint(&s);
//	/*SLPushFront(&s, 9);
//	SLPrint(&s);
//	SLPushFront(&s, 10);
//	SLPrint(&s);*/
//	//SLPopFront(&s);
//	//SLPopFront(&s);
//	//SLPopFront(&s);
//	/*SLPopBack(&s);
//	SLPopBack(&s);
//	SLPrint(&s);*/
//	SLDestroy(&s);
//}

void ContactTets01()
{
	contact c;
	InitContact(&c);
	AddContact(&c); 
	AddContact(&c); 
	//AddContact(&c); 
	//ShowContact(&c);
	//DelContact(&c);
	ShowContact(&c);
	ModifyContact(&c);
	ShowContact(&c);
	//FindContact(&c);

	DestroyContact(&c);
}

void menu()
{
	printf("*******************************************\n");
	printf("*********** 1. 添加   2. 展示 *************\n");
	printf("*********** 3. 修改   4. 删除 *************\n");
	printf("*********** 5. 查找   0. 退出 *************\n");
	printf("*******************************************\n");
}

//如果要文件保存的话，就要一开始将文件内容读入结构体，最后在将修改后的内容读入文件
void ContactTest02()
{
	int input = 0;
	contact c;
	InitContact(&c);
	LoadContact(&c);
	do
	{
		system("pause");
		system("cls");
		menu();
		printf("请输入你要操作的对应数字:");
		scanf("%d", &input);
		switch (input)
		{
		case 0:
			printf("已退出通讯录\n");
			break;
		case 1:
			AddContact(&c);
			SortContact(&c);
			break;
		case 2:
			ShowContact(&c);
			break;
		case 3:
			ModifyContact(&c);
			SortContact(&c);
			break;
		case 4:
			DelContact(&c);
			break;
		case 5:
			FindContact(&c);
			break;
		default:
			printf("输入错误，请重新输入!\n");
				break;
		}
	} while (input);
	SaveContact(&c);
	DestroyContact(&c);
}
int main()
{
	//SeqListTest01();
	//SeqListTest02();
	//ContactTets01();
	ContactTest02();
	return 0;
}
