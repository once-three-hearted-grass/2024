

//typedef int DataType;
//struct Stack
//{
//	void init()
//	{
//		a = NULL;
//		capacity = 0;
//		size = 0;
//	}
//	int* a;
//	int capacity;
//	int size;
//};

//class Stack
//{
//public:
//	void init()
//	{
//		a = NULL;
//		capacity = 0;
//		size = 0;
//	}
//private:
//	int* a;
//	int capacity;
//	int size;
//};
//int main()
//{
//	Stack s1;
//	s1.init();
//	return 0;
//}

//class Date
//{
//public:
//	void Init(int year)
//	{
//		// 这里的year到底是成员变量，还是函数形参？
//		_year = year;
//	}
//	void print()
//	{
//		cout << _year << endl;
//	}
//private:
//	int _year;
//};
//
//int main()
//{
//	Date d;
//	d.Init(2);
//	d.print();
//}

#include"Stack.h"

//int main()
//{
//	Date d;
//	cout << sizeof(Date) << endl;
//}

//class Date
//{
//public:
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void print()
//	{
//		cout << this->_year << '-' << this->_month << "-" << this->_day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date a;
//	a.Init(2024, 7, 12);
//	a.print();
//	return 0;
//}

//class Date
//{
//public:
	//Date()
	//{
	//	_year = 1;
	//	_month = 1;
	//	_day = 1;
	//}
//	void print()
//	{
//		cout << this->_year << '-' << this->_month << "-" << this->_day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date a;
//	a.print();
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year=1,int month=2,int day=3)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//		Date()
//	{
//		_year = 1;
//		_month = 1;
//		_day = 1;
//	}
//	void print()
//	{
//		cout << this->_year << '-' << this->_month << "-" << this->_day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date a;
//	a.print();
//	return 0;
//}

//class stack
//{
//public:
//	stack()
//	{
//		cout << "	stack()" << endl;
//	}
//private:
//	int a;
//};
//
//class Date
//{
//public:
//	void print()
//	{
//		cout << this->_year << '-' << this->_month << "-" << this->_day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//	stack a;
//};
//
//int main()
//{
//	Date a;
//	a.print();
//	return 0;
//}

//class Date
//{
//public:
//	Date()
//	{
//		_year = 28547;
//		_month = 234;
//		_day = 24;
//	}
//	void print()
//	{
//		cout << this->_year << '-' << this->_month << "-" << this->_day << endl;
//	}
//private:
//	int _year=16;
//	int _month=12;
//	int _day=10;
//};
//
//int main()
//{
//	Date a;
//	a.print();
//	return 0;
//}

//class stack
//{
//public:
//	stack(int m=4)
//	{
//		_a = (int*)malloc(sizeof(int) * m);
//		_capacity = 0;
//		_size = 0;
//	}
//	void print()
//	{
//		/////
//	}
//	~stack()
//	{
//		cout << "~stack()" << endl;
//		if (_a)
//		{
//			free(_a);
//			_a = NULL;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//private:
//	int* _a;
//	int _capacity;
//	int _size;
//};
//
//int main()
//{
//	stack a;
//	return 0;
//}

//class Date
//{
//public:
//	Date()
//	{
//		_year = 28547;
//		_month = 234;
//		_day = 24;
//	}
//	Date(Date& a)
//	{
//		cout << "Date(Date& a)" << endl;
//		_year = a._year;
//		_month = a._month;
//		_day = a._day;
//	}
//	void print()
//	{
//		cout << this->_year << '-' << this->_month << "-" << this->_day << endl;
//	}
//private:
//	int _year = 16;
//	int _month = 12;
//	int _day = 10;
//};
//
//void push(Date b)
//{
//	cout << "void push(Date b)" << endl;
//}
//
//int main()
//{
//	Date a;
//	push(a);
//	return 0;
//}
//
class stack
{
public:
	stack(int m = 4)
	{
		_a = (int*)malloc(sizeof(int) * m);
		_capacity = 0;
		_size = 0;
	}
	stack(stack&s)
	{
		_a = (int*)malloc(sizeof(int) * s._capacity);
		memcpy(_a, s._a, sizeof(int) * s._size);
		_capacity = s._capacity;
		_size = s._size;
	}
	void print()
	{
		/////
	}
	~stack()
	{
		cout << "~stack()" << endl;
		if (_a)
		{
			free(_a);
			_a = NULL;
			_capacity = 0;
			_size = 0;
		}
	}
private:
	int* _a;
	int _capacity;
	int _size;
};

int main()
{
	stack a;
	return 0;
}