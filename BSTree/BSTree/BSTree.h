
#pragma once

namespace bit
{
	//实现二叉搜索树
	template<class K>
	struct BSNode
	{
		BSNode<K>* _left;
		BSNode<K>* _right;
		K _key;
		typedef BSNode<K> Node;
		BSNode(const K& key)
			:_key(key)
			, _left(nullptr)
			, _right(nullptr)
		{}
	};

	template<class K>
	class BSTree
	{
	public:
		typedef BSNode<K> Node;

		bool insert(const K& key)
		{
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (key > cur->_key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return false;
				}
			}
			//但要注意_root==nullptr的时候
			if (key > parent->_key)
			{
				parent->_right = new Node(key);
			}
			else
			{
				parent->_left = new Node(key);
			}
			return true;
		}

		//写个中序遍历
		//_root是this指针里面的东西，所以不好搞，不好递归，所以采用调用函数的方法
		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}

		//搜索某个数据
		Node* Search(const K& key)
		{
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (key > cur->_key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return cur;
				}
			}
			return nullptr;
		}

		//删除某个数据
		bool Erase(const K& key)
		{
			if (_root == nullptr)
			{
				return false;
			}
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (key > cur->_key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					//找到了要删除的数据
					//删除的话就分为三种//
					//第一种就是没有孩子直接删除
					//第二种就是有一个孩子的话就直接连在父亲的后面
					//第三种就是有两个孩子
					//其中第一种和第二种可以合并。因为可以把没有孩子当做空指针的孩子
					if (cur->_left == nullptr)
					{
						if (parent == nullptr)//说明删的是根
						{
							_root = cur->_right;
							delete cur;
							return true;
						}
						//先看cur在parent的左还是右
						if (cur->_key < parent->_key)
						{
							parent->_left = cur->_right;
						}
						else
						{
							parent->_right = cur->_right;
						}
						delete cur;
						return true;
					}
					else if (cur->_right == nullptr)
					{
						if (parent == nullptr)//说明删的是根
						{
							_root = cur->_left;
							delete cur;
							return true;
						}
						if (cur->_key < parent->_key)
						{
							parent->_left = cur->_left;
						}
						else
						{
							parent->_right = cur->_left;
						}
						delete cur;
						return true;
					}
					else//现在左右孩子都不为空
					{
						//如果是头结点，也不用单独考虑，我们这样设计的话
						//我们直接找到右孩子的最小值放在cur，然后删除这个最小值节点就可以了,最小值就是一直往左走就可以了
						Node* min = cur->_right;
						Node* min_parent = cur;
						while (min->_left)
						{
							min_parent = min;
							min = min->_left;
						}
						cur->_key = min->_key;
						//删除min节点
						//因为min的左孩子一定为空，所以很好删除
						if (min_parent == cur)//说明没走
						{
							min_parent->_right = min->_right;
						}
						else
						{
							min_parent->_left = min->_right;
						}
						delete min;
						return true;
					}
				}
			}
			return false;
		}

	private:
		void _InOrder(Node* root)
		{
			if (root == nullptr)
			{
				return;
			}
			_InOrder(root->_left);
			cout << root->_key << " ";
			_InOrder(root->_right);
		}
		Node* _root = nullptr;
	};
}

//实现二叉搜索树
template<class K,class V>
struct BSNode
{
	BSNode<K,V>* _left;
	BSNode<K, V>* _right;
	K _key;
	V _value;
	typedef BSNode<K, V> Node;
	BSNode(const K& key, const V& value)
		:_key(key)
		,_value(value)
		, _left(nullptr)
		, _right(nullptr)
	{}
};

template<class K,class V>
class BSTree
{
public:
	typedef BSNode<K,V> Node;

	bool insert(const K& key, const V& value)
	{
		if (_root == nullptr)
		{
			_root = new Node(key,value);
			return true;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}
		//但要注意_root==nullptr的时候
		if (key > parent->_key)
		{
			parent->_right = new Node(key,value);
		}
		else
		{
			parent->_left = new Node(key,value);
		}
		return true;
	}

	//写个中序遍历
	//_root是this指针里面的东西，所以不好搞，不好递归，所以采用调用函数的方法
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	//搜索某个数据
	Node* Search(const K& key)
	{
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return cur;
			}
		}
		return nullptr;
	}

	//删除某个数据
	bool Erase(const K& key)
	{
		if (_root == nullptr)
		{
			return false;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				//找到了要删除的数据
				//删除的话就分为三种//
				//第一种就是没有孩子直接删除
				//第二种就是有一个孩子的话就直接连在父亲的后面
				//第三种就是有两个孩子
				//其中第一种和第二种可以合并。因为可以把没有孩子当做空指针的孩子
				if (cur->_left == nullptr)
				{
					if (parent == nullptr)//说明删的是根
					{
						_root = cur->_right;
						delete cur;
						return true;
					}
					//先看cur在parent的左还是右
					if (cur->_key < parent->_key)
					{
						parent->_left = cur->_right;
					}
					else
					{
						parent->_right = cur->_right;
					}
					delete cur;
					return true;
				}
				else if (cur->_right == nullptr)
				{
					if (parent == nullptr)//说明删的是根
					{
						_root = cur->_left;
						delete cur;
						return true;
					}
					if (cur->_key < parent->_key)
					{
						parent->_left = cur->_left;
					}
					else
					{
						parent->_right = cur->_left;
					}
					delete cur;
					return true;
				}
				else//现在左右孩子都不为空
				{
					//如果是头结点，也不用单独考虑，我们这样设计的话
					//我们直接找到右孩子的最小值放在cur，然后删除这个最小值节点就可以了,最小值就是一直往左走就可以了
					Node* min = cur->_right;
					Node* min_parent = cur;
					while (min->_left)
					{
						min_parent = min;
						min = min->_left;
					}
					cur->_key = min->_key;
					//删除min节点
					//因为min的左孩子一定为空，所以很好删除
					if (min_parent == cur)//说明没走
					{
						min_parent->_right = min->_right;
					}
					else
					{
						min_parent->_left = min->_right;
					}
					delete min;
					return true;
				}
			}
		}
		return false;
	}

	BSTree() = default;

	BSTree(const BSTree& t)
	{
		_root = copy(t._root);
	}

	~BSTree()
	{
		destroy(_root);
	}

private:
	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_InOrder(root->_left);
		cout << root->_key << " "<<root->_value<<endl;
		_InOrder(root->_right);
	}

	Node* copy(Node* root)
	{
		if (root == nullptr)
		{
			return nullptr;
		}
		//先拷贝根，在拷贝左右子树
		Node* tmp = new Node(root->_key, root->_value);
		tmp->_left = copy(root->_left);
		tmp->_right = copy(root->_right);
		return tmp;
	}

	void destroy(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		destroy(root->_left);
		destroy(root->_right);
		delete root;
	}
	Node* _root = nullptr;
};

