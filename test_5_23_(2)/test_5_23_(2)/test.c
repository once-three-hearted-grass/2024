#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<stdio.h>

//给你二叉树的根节点 root ，返回它节点值的前序遍历。
  struct TreeNode {
     int val;
      struct TreeNode *left;
      struct TreeNode *right;
  };
 
 /**
  * Note: The returned array must be malloced, assume caller calls free().
  */

  int TreeSize(struct TreeNode* root)
  {
      if (root == NULL)
      {
          return 0;
      }
      return 1 + TreeSize(root->left) + TreeSize(root->right);
  }
  void my_preorderTraversal(struct TreeNode* root, int* arr, int* pi)
  {
      //因为先序，所以先看根
      //根如果为空的话就什么都不干，不为空的话就赋值到数组中
      //然后再赋值左右子树，左右子树和根是一样的
      if (root == NULL)
      {
          return;
      }
      arr[*pi] = root->val;
      (*pi)++;
      my_preorderTraversal(root->left, arr, pi);
      my_preorderTraversal(root->right, arr, pi);
  }

int* preorderTraversal(struct TreeNode* root, int* returnSize) {
    //按照题意，必须开辟一个数组，所以不能递归这个函数，不然一直开辟，所以在创建函数
    //开辟多大呢，还要先求有多少个
    int size = TreeSize(root);
    int* arr = (int*)malloc(sizeof(int) * size);
    //还要一个东西来记录下标，并且会一直变的，所以要取地址
    int i = 0;
    my_preorderTraversal(root, arr, &i);

    *returnSize = size;
    return arr;
}