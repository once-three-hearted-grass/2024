

//先实现一个大堆，先不管greater怎么搞的
//模版中的语法错误是不会直接用红线报出来的，比如你用的中文符号就不会直接报错来
namespace bit
{
	template<class T>
	class myless
	{
	public:
		bool operator()(const T& t1,const T& t2)
		{
			return t1 < t2;
		}
	};

	template<class T>
	class mygreater
	{
	public:
		bool operator()(const T& t1, const T& t2)
		{
			return t1 > t2;
		}
	};

	template<class T,class container=vector<T>,class compare=myless<T>>
	class priority_queue
	{
	public:

		priority_queue() = default;

		//我们先写个迭代器区间构造
		template<class my_iterator>
		priority_queue(my_iterator first, my_iterator end)
		{
			while (first != end)
			{
				_con.push_back(*first);
				first++;
			}
			//在把这个写成堆
			int child = (_con[(_con.size() - 1 - 1) / 2]);//从这个位置开始向下调整
			while (child >= 0)
			{
				Adjust_down(child);
				child--;
			}
		}

		void Adjust_down(int parent)
		{
			int child = 2 * parent + 1;
			while (child < _con.size())
			{
				if ((child + 1) < _con.size() && _con[child + 1] > _con[child])
				{
					child++;
				}
				compare a;
				if (a(_con[parent] , _con[child]))
				{
					swap(_con[parent], _con[child]);
					parent = child;
					child = 2 * child + 1;
				}
				else
				{
					break;
				}
			}
		}

		void Adjust_up(int child)
		{
			int parent = (child - 1) / 2;
			while (child > 0)//child等于0，就说明已经比较完了
			{
				compare a;
				if (a(_con[parent], _con[child]))
				{
					swap(_con[child], _con[parent]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					break;
				}
			}
		}

		void push(const T&x)
		{
			//先插入vector，然后在向上调整
			_con.push_back(x);
			Adjust_up((int)_con.size() - 1);
		}

		void pop()
		{
			swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			Adjust_down(0);
		}

		T& top()
		{
			return _con[0];
		}

		const T& top()const
		{
			return _con[0];
		}

		size_t size()const
		{
			return _con.size();
		}

		bool empty()const
		{
			return _con.empty();
		}

	private:
		container _con;
	};

	void test3()
	{
		int arr[] = { 1,2,34,5,7,8,9,7,6,5,5,4,3,3 };

		priority_queue<int,vector<int>,mygreater<int>> a(arr, arr + sizeof(arr) / sizeof(arr[0]));
		//priority_queue<int,vector<int>> a;
		//a.push(1);
		//a.push(2);
		//a.push(3);
		//a.push(4);
		//a.push(5);
		//a.push(6);
		while (!a.empty())
		{
			cout << a.top() << " ";
			a.pop();
		}
	}
	void test4()
	{
		myless<int> m;
		cout << m(1, 2) << endl;
	}
}
