


namespace bit
{
	template<class T,class container=deque<T>>//这个container是vector，或者list或者deque（后面会说），这就叫做适配器，
	//用适配器来实现stack
	//就免去了很多我们要实现的东西
	class stack
	{
	public:
		//stack();可以不用写//因为有默认构造

		void push(const T& x)
		{
			_con.push_back(x);
		}

		void pop()
		{
			_con.pop_back();
		}

		T& top()
		{
			return _con.back();
		}

		const T& top()const
		{
			return _con.back();
		}

		size_t size()const
		{
			return _con.size();
		}

		bool empty()const
		{
			return _con.empty();
		}

	private:
		container _con;
	};

	void test1()
	{
		stack<int> s;
		s.push(1);
		s.push(2);
		s.push(3);
		s.push(4);
		s.push(5);
		s.push(6);
		while (!s.empty())
		{
			cout << s.top() << endl;
			s.pop();
		}
		cout << endl;
	}
}
