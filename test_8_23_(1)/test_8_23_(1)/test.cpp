#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class HeapOnly
//{
//public:
//	HeapOnly* CreateObj()
//	{
//		return new HeapOnly;
//	}
//private:
//	HeapOnly()
//	{}
//	HeapOnly(const HeapOnly&h)
//	{}
//};

//class HeapOnly
//{
//public:
//	static HeapOnly* CreateObj()
//	{
//		return new HeapOnly;
//	}
//
//	void destroy()
//	{
//		delete this;
//	}
//private:
//	~HeapOnly()
//	{}
//};
//
//class StackOnly
//{
//public:
//	StackOnly CreateObj()
//	{
//		return StackOnly();
//	}
//private:
//	StackOnly()
//	{}
//	StackOnly(const StackOnly&h)
//	{}
//};
//
//class NonInherit
//{
//public:
//	static NonInherit GetInstance()
//	{
//		return NonInherit();
//	}
//private:
//	NonInherit()
//	{}
//};
//
//class A
//{
//public:
//	/*A() = delete;*///不能删除，因为如果删除的话，就真的一个对象都没有了
//	A(const A& a) = delete;
//	
//	static A& CreatObj()
//	{
//		if (_ptr == nullptr)
//		{
//			_ptr = new A;
//		}
//		return *_ptr;
//	}
//
//	void Destroy()
//	{
//		delete _ptr;
//	}
//
//	class B
//	{
//	public:
//		~B()
//		{
//			cout << "h" << endl;
//			delete _ptr;
//		}
//	};
//	int _num=0;
//	static A* _ptr;
//	static B b;
//	~A()
//	{
//		;
//	}
//private:
//	A()
//	{};
//};
//A* A::_ptr = nullptr;//main函数之前就创建了一个nullptr
//A::B A::b;
//
//class A
//{
//public:
//	/*A() = delete;*///不能删除，因为如果删除的话，就真的一个对象都没有了
//	A(const A& a) = delete;
//
//	static A& CreatObj()
//	{
//		static A a;
//		return a;
//	}
//	int _num = 0;
//private:
//	A()
//	{};
//};

//int main()
//{
//	A::CreatObj()._num++;
//	A::CreatObj()._num++;
//	A::CreatObj()._num++;
//	return 0;
//}

//int main()
//{
//	/*HeapOnly* h = HeapOnly::CreateObj();
//	HeapOnly h1;*/
//	return 0;
//}

//int main()
//{
//	int a = 'c';//隐式类型转换
//	int b = (int) & a;
//	return 0;
//}

//class B
//{
//public:
//	B(int a)
//		:_b1(a)
//		, _b2(a)
//	{}
//	B(int a1, int a2)
//		:_b1(a1)
//		, _b2(a2)
//	{}
//	int _b1;
//	int _b2;
//};
//
//class A
//{
//public:
//	A(const B& b)
//	{
//		_a1 = b._b1;
//		_a2 = b._b2;
//	}
//	explicit A(int a)
//		:_a1(a)
//		, _a2(a)
//	{}
//	A(int a1, int a2)
//		:_a1(a1)
//		, _a2(a2)
//	{}
//
//	operator int()
//	{
//		return _a1 + _a2;
//	}
//private:
//	int _a1;
//	int _a2;
//};

//int main()
//{
//	B b1 = { 1,2 };
//	A a1 = b1;
//	return 0;
//}

//
//int main()
//{
//	A tmp1 = (A)1;
//	const A& tmp2 = {1,2};
//	int num = tmp1;
//	cout << num << endl;
//	return 0;
//}

//int main()
//{
//	/*double d = 0.22;
//	int a = reinterpret_cast<int>(d);*/
//
//	/*int a = 0;
//	int b = static_cast<int>( & a);*/
//
//	const int a = 0;
//	int* d = const_cast<int*>(&a);
//	*d = 10;
//	cout << a << endl;
//	cout <<(*d) << endl;
//	return 0;
//}

class A
{
public:
	virtual void f() {}//、、、、、、、、////、、、、//、、
};
class B : public A
{};
void fun(A* pa)
{
	// dynamic_cast会先检查是否能转换成功，能成功则转换，不能则返回
	B* pb2 = dynamic_cast<B*>(pa);
	if (pb2)
	{
		cout << "转换成功" << endl;
		cout << "pb2:" << pb2 << endl;
	}
	else
	{
		cout << "转换失败" << endl;
	}
}
int main()
{
	A a;
	B b;
	fun(&a);
	fun(&b);
	return 0;
}
