#define _CRT_SECURE_NO_WARNINGS 1
//递归方式实现打印一个整数的每一位
#include<stdio.h>
void print(int n)
{
	if (n >= 10)
	{
		print(n / 10);
		printf("%d ", n % 10);
	}
	else if(n>=1&&n<=9)
	{
		printf("%d ", n % 10);
	}

}
int main()
{
	int n = 0;
	while ((scanf("%d", &n)) != EOF)
	{
		print(n);
		printf("\n");
	}
	return 0;
}