#include"Snake.h"

void test()
{
	char ch = 0;
	do
	{
		//system("cls");
		//本地化
		setlocale(LC_ALL, "");
		srand((unsigned int)time(NULL));
		Snake snake = { 0 };
		//打印基本菜单，完善基本信息
		Basic();
		//初始化蛇
		SnakeInit(&snake);
		//运行游戏
		SnakeRun(&snake);
		//游戏善后
		SnakeDes(&snake);
		SetPos(20, 15);
		printf("是否再来一局(Y/N):");
		ch = getchar();
		while (getchar() != '\n');
	} while (ch == 'y' || ch == 'Y');
	SetPos(0, 27);
}

int main()
{
	test();
	return 0;
}
