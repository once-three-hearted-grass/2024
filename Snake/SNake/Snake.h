#pragma once

#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
#include<windows.h>
#include<stdbool.h>
#include<locale.h>
#include<time.h>
#include<errno.h>
#include<string.h>


#define WALL L'□'
#define BODY L'●'
#define FOOD L'★'

//定义一个关于按键的宏，按了就返回1
#define KEY_PRESS(VK) (GetAsyncKeyState(VK)&0x1)?1:0

typedef struct SnakeNode
{
	short x;
	short y;
	struct SnakeNode* next;
}SnakeNode,*pSnakeNode;

enum DIRECTION
{
	UP = 1,
	DOWN,
	LEFT,
	RIGHT
};

enum STATE
{
	OK = 1,
	ESC,
	KILL_BY_WALL,
	KILL_BYSELF
};

typedef struct Snake
{
	pSnakeNode phead;
	int sleep_time;
	enum DIRECTION dir;
	enum STATE state;
	pSnakeNode food;
	int score;
	int food_weight;
}Snake,*pSnake;

//打印基本菜单，完善基本信息
void Basic();

//设置控制台鼠标等信息
void SetConsole();

//设置鼠标位置
void SetPos(short x, short y);

//打印菜单地图等内容
void PrintMenu();

//初始化蛇
void SnakeInit(pSnake ps);

//初始化蛇身
void SnakeNodeInit(pSnake ps);

//初始化食物
void FoodInit(pSnake ps);

//运行游戏
void SnakeRun(pSnake ps);

//暂停蛇的移动
void SnakePause();

//开始走一步
void SnakeMove(pSnake ps);

//吃到食物了
void EatFood(pSnakeNode newnode,pSnake ps);

//没有吃到食物
void NoFood(pSnakeNode newnode, pSnake ps);

//蛇有没有撞到墙或自己
void SnakeIsKilled(pSnake ps);

//游戏善后
void SnakeDes(pSnake ps);

