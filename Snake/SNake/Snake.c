#include"Snake.h"

//设置鼠标位置
void SetPos(short x, short y)
{
	//先来个位置型结构体
	COORD coord = { x,y };
	//先取句柄
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(handle, coord);
}

//设置控制台鼠标等信息
void SetConsole()
{
	//设置控制台名字和大小
	system("mode con cols=100 lines=30");
	system("title 贪吃蛇");
	//隐藏鼠标
	//先取句柄
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	//定义鼠标信息型结构体
	CONSOLE_CURSOR_INFO console = { 0 };
	//先取句柄信息放在结构体中
	GetConsoleCursorInfo(handle, &console);
	//再将可见度置为false
	console.bVisible = false;
	//再设置
	SetConsoleCursorInfo(handle, &console);
}

//打印菜单地图等内容
void PrintMenu()
{
	//第一个内容
	SetPos(40, 14);
	printf("欢迎来到贪吃蛇小游戏");
	SetPos(41, 22);
	system("pause");

	//第二个内容
	system("cls");
	SetPos(25, 14);
	printf("用↑ . ↓  . ← . →分别控制蛇的移动,F3为加速，F4为减速");
	SetPos(40, 15);
	printf("加速能得到更高的分数");
	SetPos(42, 22);
	system("pause");
	system("cls");

	//第三个内容，打印地图，27行，58列
	//打印上
	for (int i = 0; i < 29; i++)
	{
		wprintf(L"%lc", WALL);
	}
	//下
	SetPos(0, 26);
	for (int i = 0; i < 29; i++)
	{
		wprintf(L"%lc", WALL);
	}
	//左
	for (int i = 1; i <= 25; i++)
	{
		SetPos(0, i);
		wprintf(L"%lc", WALL);
	}
	//右
	for (int i = 1; i <= 25; i++)
	{
		SetPos(56, i);
		wprintf(L"%lc", WALL);
	}
	SetPos(60, 15);
	printf("不能穿墙，不能咬到自己");
	SetPos(60, 16);
	printf("用↑ . ↓  . ← . →分别控制蛇的移动");
	SetPos(60, 17);
	printf("F3为加速，F4为减速");
	SetPos(60, 18);
	printf("ESC:退出游戏,空格(space):暂停游戏");
	SetPos(60, 21);
	printf("陈康@版权");
}

//打印基本菜单，完善基本信息
void Basic()
{
	//设置控制台鼠标等信息
	SetConsole();
	PrintMenu();
}

//初始化蛇身
void SnakeNodeInit(pSnake ps)
{
	//初始化五个节点
	for (int i = 0; i < 5; i++)
	{
		pSnakeNode cur = (pSnakeNode)malloc(sizeof(SnakeNode));
		if (cur == NULL)
		{
			perror("SnakeMove::malloc");
			return;
		}
		cur->x = 24 + i * 2;
		cur->y = 5;
		cur->next = NULL;
		//挨个挨个头插
		if (ps->phead == NULL)
		{
			ps->phead = cur;
		}
		else
		{
			cur->next = ps->phead;
			ps->phead = cur;
		}
	}
	//再打印蛇身
	pSnakeNode cur = ps->phead;
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}
}

//初始化食物
void FoodInit(pSnake ps)
{
	//x要在2~54，y要在1~25，且x要为偶数，且不能与蛇重合了
	int x = 0;
	int y = 0;
	again:
	do
	{
		x = rand() % 53 + 2;
		y = rand() % 25 + 1;
	} while (x % 2 != 0);
	pSnakeNode cur = ps->phead;
	while (cur)
	{
		if (cur->x == x && cur->y == y)
		{
			goto again;
		}

		cur = cur->next;
	}
	SetPos(x, y);
	wprintf(L"%lc", FOOD);
	pSnakeNode newnode= (pSnakeNode)malloc(sizeof(SnakeNode));
	if (newnode == NULL)
	{
		perror("FoodInit::malloc");
		return;
	}
	newnode->x = x;
	newnode->y = y;
	newnode->next = NULL;
	ps->food = newnode;
}

//初始化蛇
void SnakeInit(pSnake ps)
{
	//初始化蛇身
	SnakeNodeInit(ps);
	//初始化食物
	FoodInit(ps);
	ps->dir = RIGHT;
	ps->food_weight = 10;
	ps->score = 0;
	ps->sleep_time = 200;
	ps->state = OK;
}

//暂停蛇的移动
void SnakePause()
{
	while (1)
	{
		Sleep(30);
 		if (KEY_PRESS(VK_SPACE))
		{
			return;
		}
	}
}

//吃到食物了
void EatFood(pSnakeNode newnode, pSnake ps)
{
	//直接头插就可以了
	newnode->next = ps->phead;
	ps->phead = newnode;
	//再打印
	pSnakeNode cur = ps->phead;
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}
	ps->score += ps->food_weight;
	free(ps->food);
	ps->food = NULL;
	//继续创建食物
	FoodInit(ps);
}

//没有吃到食物
void NoFood(pSnakeNode newnode, pSnake ps)
{
	//还是要头插，只不过要释放最后一个节点
	newnode->next = ps->phead;
	ps->phead = newnode;
	//先找到倒数第二个节点
	pSnakeNode cur = ps->phead;
	while (cur->next->next)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}
	//最好还要打印两个空格，不然会一直显示
	SetPos(cur->next->x, cur->next->y);
	printf("  ");
	free(cur->next);
	cur->next = NULL;
}

//蛇有没有撞到墙或自己
void SnakeIsKilled(pSnake ps)
{
	//撞到墙了吗
	if (ps->phead->x == 0 || ps->phead->x == 56 || ps->phead->y == 0 || ps->phead->y == 26)
	{
		ps->state = KILL_BY_WALL;
	}
	//撞到自己了吗
	pSnakeNode cur = ps->phead->next;
	while (cur)
	{
		if (cur->x == ps->phead->x && cur->y == ps->phead->y)
		{
			ps->state = KILL_BYSELF;
			break;
		}
		cur = cur->next;
	}
}

//开始走一步
void SnakeMove(pSnake ps)
{
	//就是在下一步创建一个节点罢了
	pSnakeNode newnode = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (newnode == NULL)
	{
		perror("SnakeMove::malloc");
		return;
	}
	newnode->next = NULL;
	switch (ps->dir)
	{
	case UP:
		newnode->x = ps->phead->x;
		newnode->y = ps->phead->y - 1;
		break;
	case DOWN:
		newnode->x = ps->phead->x;
		newnode->y = ps->phead->y + 1;
		break;
	case LEFT:
		newnode->x = ps->phead->x-2;
		newnode->y = ps->phead->y;
		break;
	case RIGHT:
		newnode->x = ps->phead->x+2;
		newnode->y = ps->phead->y;
		break;
	}
	if (newnode->x == ps->food->x && newnode->y == ps->food->y)//吃到食物了
	{
		EatFood(newnode, ps);
	}
	else
	{
		NoFood(newnode, ps);
	}
	//判断是否撞墙，或撞到自己
	SnakeIsKilled(ps);
}

//运行游戏
void SnakeRun(pSnake ps)
{
	do
	{
		SetPos(60, 7);
		printf("得分:%d",ps->score);
		SetPos(60, 8);
		printf("每个食物得分:%2d", ps->food_weight);
		if (KEY_PRESS(VK_UP) && ps->state != DOWN)
		{
			ps->dir = UP;
		}
		else if (KEY_PRESS(VK_DOWN) && ps->state != UP)
		{
			ps->dir = DOWN;
		}
		else if (KEY_PRESS(VK_RIGHT) && ps->state != LEFT)
		{
			ps->dir = RIGHT;
		}
		else if (KEY_PRESS(VK_LEFT) && ps->state != RIGHT)
		{
			ps->dir = LEFT;
		}
		else if (KEY_PRESS(VK_ESCAPE))//ESC
		{
			ps->state = ESC;
		}
		else if (KEY_PRESS(VK_SPACE))//空格
		{
			//暂停蛇的移动
			SnakePause();
		}
		else if (KEY_PRESS(VK_F3))//F3
		{
			if (ps->sleep_time > 20)
			{
				ps->sleep_time -= 10;
				ps->food_weight += 5;
			}
		}
		else if (KEY_PRESS(VK_F4))//F4
		{
			if (ps->sleep_time < 300)
			{
				ps->sleep_time += 10;
				if(ps->food_weight>=5)
				ps->food_weight -= 5;
			}
		}
		//开始走一步
		SnakeMove(ps);
		Sleep(ps->sleep_time);
	} while (ps->state == OK);
}

//游戏善后
void SnakeDes(pSnake ps)
{
	if (ps->state == ESC)
	{
		SetPos(23, 14);
		printf("退出游戏成功");
	}
	else if(ps->state == KILL_BY_WALL)
	{
		SetPos(24, 14);

		printf("您撞墙身亡");
	}
	else if (ps->state == KILL_BYSELF)
	{
		SetPos(23, 14);

		printf("您撞到自己了");
	}
	//销毁链表
	free(ps->food);
	ps->food = NULL;
	pSnakeNode cur = ps->phead;
	while (cur)
	{
		pSnakeNode del = cur;
		cur = cur->next;
		free(del);
	}
}