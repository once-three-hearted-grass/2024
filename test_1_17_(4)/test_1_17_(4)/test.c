#define _CRT_SECURE_NO_WARNINGS 1
//输入描述：
//一行，一个字符串，字符串中的每个字符表示一张Lily使用的图片。
//输出描述：
//Lily的所有图片按照从小到大的顺序输出
#include <stdio.h>
#include<string.h>
int main() {
    char arr[1000] = { 0 };
    scanf("%s", arr);
    int i, j;
    int sz = strlen(arr);
    for (i = 0; i < sz - 1; i++)
    {
        int flag = 0;
        for (j = 0; j < sz - 1 - i; j++)
        {
            int ret;
            if (arr[j] > arr[j + 1])
            {
                flag = 1;
                ret = arr[j + 1];
                arr[j + 1] = arr[j];
                arr[j] = ret;
            }
        }
        if (flag == 0)
        {
            break;
        }
    }
    printf("%s", arr);
    return 0;
}