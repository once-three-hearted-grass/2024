#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
//void qsort (void* base, size_t num, size_t size,int (*compar)(const void*, const void*));//传一个函数地址，而且这个函数地址是这样的
int cmp_int(const void*s1,const void*s2)
{
	return *(int*)s1 - *(int*)s2;
}
void print_int(int arr[], int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ",arr[i]);
	}
}
void test1()
{
	//排序整型
	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), &cmp_int);
	print_int(arr, sz);
}
struct stu
{
	char name[20];
	int age;
};
int cmp_struct_age(const void* s1, const void* s2)
{
	return (((struct stu*)s1)->age - ((struct stu*)s2)->age);
}
void print_struct(struct stu arr[], int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%-10s\t%d\t\n", arr[i].name, arr[i].age);
	}
}
void test2()
{
	//排序结构体，按照年龄来排
	struct stu arr[5] = { {"zhangsan",38},{"lisi",98},{"chenkang",18},{"liusijia",13},{"dingxiaotong",12} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), cmp_struct_age);
	print_struct(arr, sz);
}
int cmp_struct_name(const void* s1, const void* s2)
{
	return strcmp(((struct stu*)s1)->name, ((struct stu*)s2)->name);
}
void test3()
{
	//排序结构体，按照年龄来排
	struct stu arr[5] = { {"zhangsan",38},{"lisi",98},{"chenkang",18},{"liusijia",13},{"dingxiaotong",12} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), cmp_struct_name);
	print_struct(arr, sz);
}
int main()
{
	//test1();
	//test2();
	test3();
	return 0;
}