#define _CRT_SECURE_NO_WARNINGS 1
//strstr
#include<stdio.h>
#include<string.h>
int main()
{
	char arr1[] = "abcdefghijklmn";
	char arr2[] = "efghijk";
	char* ret = strstr(arr1, arr2);//若找不到arr1中arr2的字串，那么返回NULL，若找得到，就返回arr2首字符在arr1中的地址
	if (ret == NULL)
	{
		printf("找不到\n");
	}
	else
	{
		printf("%s\n", ret);
	}
	return 0;
}