#pragma once
#include<vector>
#include<iostream>
using namespace std;

namespace bit1//闭散列的实现
{
	enum state
	{
		DELETE,
		EMPTY,
		EXIST
	};

	template<class K, class V>
	struct HashData
	{
		pair<K, V> _kv;
		state _state=EMPTY;
	};

	//开始写Hash函数，就是将数据转换为整型的东西
	template<class K>
	class hash
	{
		int operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	//针对string，对hash进行特化
	template<>
	struct hash<string>
	{
		int operator()(const string& key)
		{
			int i = 0;
			for (auto x : key)
			{
				i += x;
				i *= 31;//防止哈希冲突//这样不同字符串转换为相同整型的概率就小点了
			}
			return i;
		}
	};

	template<class K, class V,class Hash=hash<K>>
	class HashTable
	{
	public:
		HashTable()
		{
			_ls.resize(10);
		}

		bool insert(pair<K, V> kv)
		{
			//当插入的数量达到70%时，就要扩容
			if ((_n*10) / _ls.size() >= 7)
			{
				//扩容的话还要更改那些Hash值的位置
				HashTable<K,V> tmp;
				tmp._ls.resize(_ls.size() * 2);
				for (auto x : _ls)
				{
					tmp.insert(x._kv);
				}
				_ls.swap(tmp._ls);
			}
			Hash h;
			//先算出它的哈希值,然后在插入
			int i = h(kv.first) % _ls.size();
			while(_ls[i]._state == EXIST)
			{
				if (_ls[i]._kv.first == kv.first)
				{
					return false;
				}
				i++;
				i = i % _ls.size();
			}
			_ls[i]._kv = kv;
			_ls[i]._state = EXIST;
			_n++;
			return true;
		}

		HashData<K, V>* Find(const K& key)
		{
			Hash h;
			int i = h(key) % _ls.size();
			while ((_ls[i]._state == EXIST) || (_ls[i]._state == DELETE))
			{
				if (_ls[i]._state == EXIST)
				{
					if (_ls[i]._kv.first == key)
					{
						return &_ls[i];
					}
				}
				i++;
				i = i % _ls.size();
			}
			return nullptr;
		}

		bool erase(const K& key)
		{
			HashData<K, V>* era = Find(key);
			if (era)
			{
				era->_state = DELETE;
				_n--;
				return true;
			}
			return false;
		}

	private:
		//不用写析构和构造函数
		vector<HashData<K, V>> _ls;
		size_t _n = 0;
	};

	void test()
	{
		HashTable<string,string> h;
		string arr[] = { "eee","bbb","abc","eee","abc","def","fff","bcd"};
		for (auto e : arr)
		{
			h.insert({ e,e });
		}
		for (auto e : arr)
		{
			h.erase(e);
		}
	}
}


namespace bit2
{
	template<class K>
	class hash
	{
		int operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	//针对string，对hash进行特化
	template<>
	struct hash<string>
	{
		int operator()(const string& key)
		{
			int i = 0;
			for (auto x : key)
			{
				i += x;
				i *= 31;//防止哈希冲突//这样不同字符串转换为相同整型的概率就小点了
			}
			return i;
		}
	};

	//开散列的实现
	template<class K, class T>
	struct HashData
	{
		pair<K, T> _data;
		HashData* _next;
		HashData(const pair<K, T>& t)
			:_next(nullptr)
			, _data(t)
		{}
	};

	template<class K, class T, class Hash = hash<K>>
	class HashTable
	{
		typedef HashData<K, T> Node;
	public:

		HashTable()
		{
			_tables.resize(10, nullptr);
		}

		bool Insert(const pair<K, T>& data)
		{
			//检查是否需要扩容
			if (_n == _tables.size())
			{
				//扩容
				HashTable<K, T> tmp;
				tmp._tables.resize(_tables.size() * 2);
				//直接现成的把原来的插过来//因为如果像闭散列的实现那样的话，就会额外开那么空间，还要释放，就浪费了，所以直接利用现成的
				for (int i = 0; i < _tables.size(); i++)
				{
					Node* cur = _tables[i];
					while (cur)
					{
						Node* next = cur->_next;
						Hash h;
						int i = h(cur->_data.first) % tmp._tables.size();
						cur->_next = tmp._tables[i];
						tmp._tables[i] = cur;
						cur = next;
						tmp._n++;
					}
				}
				_tables.swap(tmp._tables);
			}
			Hash h;
			int i = h(data.first) % _tables.size();
			Node* newnode = new Node(data);
			newnode->_next = _tables[i];//直接头插
			_tables[i] = newnode;
			_n++;
			return true;
		}

		bool Find(const K& key)
		{
			Hash h;
			int i = h(key) % _tables.size();
			Node* cur = _tables[i];
			while (cur)
			{
				if (cur->_data.first == key)
				{
					return true;
				}
				cur = cur->_next;
			}
			return false;
		}

		bool Erase(const K& key)
		{
			Hash h;
			//这个也不能像闭散列那样，直接用find然后删除，因为有前后指针的链接嘛
			int i = h(key) % _tables.size();
			Node* cur = _tables[i];
			Node* pre = nullptr;
			while (cur)
			{
				if (cur->_data.first == key)
				{
					//开始删除
					if (pre)//正常删
					{
						pre->_next = cur->_next;
					}
					else//删头结点
					{
						_tables[i] = cur->_next;
					}
					delete cur;
					return true;
				}
				pre = cur;
				cur = cur->_next;
			}
			return false;
		}

		~HashTable()//这个需要我们自己写
		{
			//vector系统会自己调用自己的析构函数，所以我们不用写它的，自定义类型有析构的都会自己调用自己的析构函数
			//关键vector自己析构还不够，要析构后面的链表
			for (int i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
					_n--;
				}
				_tables[i] = nullptr;
			}
		}

	private:
		vector<Node*> _tables;
		size_t _n;
	};

	void test1()
	{
		//HashTable<int, int> ha;
		//int arr[] = { 1,11,2,12,6,16,20 };
		//for (auto x : arr)
		//{
		//	ha.Insert({ x,x });
		//}
		//cout << ha.Find(13) << endl;
		///*for (auto x : arr)
		//{
		//	ha.Erase(x);
		//}*/
		HashTable<string, string> ha;
		string arr[] = {"aaa","aaa","sss","bbbb","cccc","dddd"};
		for (auto x : arr)
		{
			ha.Insert({ x,x });
		}
		//cout << ha.Find(13) << endl;
		/*for (auto x : arr)
		{
			ha.Erase(x);
		}*/
	}
}
