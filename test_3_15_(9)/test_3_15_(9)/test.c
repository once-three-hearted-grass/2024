#define _CRT_SECURE_NO_WARNINGS 1
//写一个函数判断当前机器是大端还是小端，如果是小端返回1，如果是大端返回0.
#include<stdio.h>
int big_or_small()
{
	int a = 1;
	return *((char*)&a);
}
int main()
{
	int ret = big_or_small();
	if (ret == 1)
	{
		printf("小端\n");
	}
	else
	{
		printf("大端\n");
	}
	return 0;
}