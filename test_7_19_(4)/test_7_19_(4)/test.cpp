#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
int GetDay(int year, int month)
{
    int arr[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
    if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
    {
        return 29;
    }
    return arr[month];
}

int main() {
    int year, month, day, count;
    int n;
    cin >> n;
    while (n--)
    {
        cin >> year >> month >> day >> count;
        while (count--)
        {
            day++;
            if (day > GetDay(year, month))
            {
                month++;
                day = 1;
                if (month > 12)
                {
                    month = 1;
                    year++;
                }
            }
        }
        printf("%d-%02d-%02d\n", year, month, day);
    }
}