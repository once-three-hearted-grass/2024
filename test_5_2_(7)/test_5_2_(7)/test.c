#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void rotate(int* nums, int numsSize, int k) {
    //法二:创建一个新数组，依次先拷贝后面k个，在拷贝前面的就可以了,最后再将新数组拷贝给原数组
    int copy[numsSize];
    int n = numsSize;
    //首先k必须为有效值
    k %= numsSize;
    for (int i = 0; i < k; i++)
    {
        copy[i] = nums[n - k + i];//将原数组的后面k个拷贝到新数组的前面k个
    }
    for (int i = k; i < n; i++)
    {
        //再将原数组前面n-k个拷贝到新数组后面n-k个
        copy[i] = nums[i - k];
    }
    //最后新数组拷贝给原数组
    for (int i = 0; i < n; i++)
    {
        nums[i] = copy[i];
    }
}