#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<map>
using namespace std;

class Node {
public:
    int val;
    Node* next;
    Node* random;
    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
};

class Solution {
public:
    Node* copyRandomList(Node* head) {
        //这里我们还是用map来解决问题
        //先浅浅的拷贝一个链表，不拷贝random
        map<Node*, Node*> m;
        Node* cur = head;
        Node* copy_head = nullptr;
        Node* copy_tail = nullptr;
        while (cur)
        {
            if (copy_tail == nullptr)
            {
                copy_head = copy_tail = new Node(cur->val);
            }
            else
            {
                Node* new_node = new Node(cur->val);
                copy_tail->next = new_node;
                copy_tail = new_node;
            }
            m.insert({ cur ,copy_tail });
            cur = cur->next;
        }
        //开始链接random
        Node*cur1 = copy_head;
        Node*cur2 = head;
        while (cur1&&cur2)
        {
            if (cur2->random == nullptr)
            {
                cur1->random = nullptr;
            }
            else
            {
                cur1->random = m[cur2->random];//就这样就连接好random了
            }
            cur1 = cur1->next;
            cur2 = cur2->next;
        }
        return copy_head;
    }
};