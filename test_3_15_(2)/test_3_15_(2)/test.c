#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现strncat
#include<stdio.h>
#include<string.h>
#include<assert.h>
char* my_strncat(char* dest, const char* src, size_t num)
{
	assert(dest && src);
	char* ret = dest;
	//先找到dest的\0
	while (*dest != '\0')
	{
		dest++;
	}
	//从\0开始拼接，且最多拼接到src的\0,不然会越界，所以还要求长度
	int len = strlen(src) + 1;
	for (int i = 0; i < num && i < len; i++)
	{
		*dest = *src;
		dest++;
		src++;
	}
	return ret;
}
int main()
{
	char arr1[] = "ab";
	char arr2[20] = "aaaass\0sssssss";
	printf("%s", my_strncat(arr2, arr1, 4));//从\0开始拷贝,除了正常拷贝外，还会额外多拷贝一个\0,如果不够也不会再次补充\0了，最多补一个\0
	return 0;
}