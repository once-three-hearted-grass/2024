#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现memmove
//与memcpy一样，都是拷贝的，不同的是，memmove可以处理有重复地址的拷贝
#include<stdio.h>
#include<string.h>
#include<assert.h>
void* my_memmove(void* dest, const void* src, size_t count)
{
	void* ret = dest;
	assert(dest && src);
	if (dest < src)//从前往后拷贝
	{
		while (count--)
		{
			*((char*)dest) = *((char*)src);
			((char*)dest)++;
			((char*)src)++;
		}
	}
	else//从后往前拷贝
	{
		while (count--)
		{
			*(((char*)dest) + count) = *(((char*)src) + count);
		}
	}
	return ret;

}
int main()
{
	int arr1[] = {1,2,3,4,5,6,7,8,9,10};
	my_memmove(arr1+2,arr1,20);
	for (int i = 0; i < 10; i++)
	{
		printf("%d ",arr1[i]);
	}
	return 0;
}