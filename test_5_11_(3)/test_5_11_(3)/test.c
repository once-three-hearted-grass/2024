#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
//设计你的循环队列实现。 循环队列是一种线性数据结构，其操作表现基于 FIFO（先进先出）原则并且队尾被连接在队首之后以形成一个循环。它也被称为“环形缓冲器”。
//循环队列的一个好处是我们可以利用这个队列之前用过的空间。在一个普通队列里，一旦一个队列满了，我们就不能插入下一个元素
//，即使在队列前面仍有空间。但是使用循环队列，我们能使用这些空间去存储新的值。
//你的实现应该支持如下操作：
//MyCircularQueue(k) : 构造器，设置队列长度为 k 。
//Front : 从队首获取元素。如果队列为空，返回 - 1 。
//Rear : 获取队尾元素。如果队列为空，返回 - 1 。
//enQueue(value) : 向循环队列插入一个元素。如果成功插入则返回真。
//deQueue() : 从循环队列中删除一个元素。如果成功删除则返回真。
//isEmpty() : 检查循环队列是否为空。
//isFull() : 检查循环队列是否已满。
typedef int QDataType;
//链表节点
typedef struct QNode
{
	QDataType data;
	struct QNode* next;
}QNode;
//这里我们使用链表实现循环队列
typedef struct {
	QNode* front;
	QNode* tail;
} MyCircularQueue;

//这里就只需要创建k个节点就可以了，如果像数组一样的话，就不好找最后一个数据，算了还是像数组一样，k+1个数据吧
MyCircularQueue* myCircularQueueCreate(int k) {
	//先创建队列
	MyCircularQueue* obj = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
	obj->front = NULL;
	obj->tail = NULL;
	//再创建k个节点//依次链接起来
	int count = k + 1;
	while (count--)
	{
		QNode* new_node = (QNode*)malloc(sizeof(QNode));
		if (obj->front == NULL)
		{
			obj->front = obj->tail = new_node;
		}
		else//依次尾插
		{
			obj->tail->next = new_node;
			obj->tail = new_node;
		}
	}
	//最后变为一个循环链表
	obj->tail->next = obj->front;
	obj->tail = obj->front;
	return obj;
}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {//向循环队列插入一个元素。如果成功插入则返回真。
	//在这之前先判断满没满
	//和数组相似
	if (obj->tail->next == obj->front)//满了
	{
		return false;
	}
	//插入元素的话，就放在tail处，tail在向前走
	obj->tail->data = value;
	obj->tail = obj->tail->next;
	return true;
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {//从循环队列中删除一个元素。如果成功删除则返回真。
	//删除的话，就是front向前走了
	if (obj->front == obj->tail)//为空
	{
		return false;
	}
	obj->front = obj->front->next;
	return true;
}

int myCircularQueueFront(MyCircularQueue* obj) {//从队首获取元素。如果队列为空，返回 - 1 。
	if (obj->front == obj->tail)//为空
	{
		return -1;
	}
	return obj->front->data;
}

int myCircularQueueRear(MyCircularQueue* obj) {
	//获取队尾元素。如果队列为空，返回 - 1 。
	if (obj->front == obj->tail)//为空
	{
		return -1;
	}
	//这个稍稍复杂一点，要找到最后一个元素
	QNode* cur = obj->front;
	while (cur->next != obj->tail)
	{
		cur = cur->next;
	}
	return cur->data;
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
	if (obj->front == obj->tail)//为空
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
	if (obj->tail->next == obj->front)//满了
	{
		return true;
	}
	else
	{
		return false;
	}
}

void myCircularQueueFree(MyCircularQueue* obj) {
	//先free链表
	QNode* cur = obj->front;
	while (cur->next != obj->front)
	{
		QNode* next = cur->next;
		free(cur);
		cur = next;
	}
	//再free队列
	free(obj);
}