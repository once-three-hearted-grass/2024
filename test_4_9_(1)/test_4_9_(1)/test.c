#define _CRT_SECURE_NO_WARNINGS 1
//输入：nums1 = [1, 2, 3, 0, 0, 0], m = 3, nums2 = [2, 5, 6], n = 3
//输出：[1, 2, 2, 3, 5, 6]
//解释：需要合并[1, 2, 3] 和[2, 5, 6] 。
//合并结果是[1, 2, 2, 3, 5, 6] ，其中斜体加粗标注的为 nums1 中的元素。
#include<stdio.h>
void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
	//m表示nums1的数据个数，n表示nums2的数据个数
	//如果从前往后比，小的就放nums1前面，则有可能会覆盖nums1原来数据，导致数据丢失
	//所以从后往前比，大的就放后面
	int s1 = m - 1;
	int s2 = n - 1;
	int tail = n + m - 1;
	while(s1>=0 && s2>=0)
	{
		if (nums1[s1] > nums2[s2])
		{
			nums1[tail] = nums1[s1];
			tail--;
			s1--;
		}
		else
		{
			nums1[tail] = nums2[s2];
			tail--;
			s2--;
		}
	}
	//如果nums1还有剩余，那就这样了，不管了
	//如果nums2还有剩余，那还要挨个赋值过去
	while (s2 >= 0)
	{
		nums1[tail] = nums2[s2];
		tail--;
		s2--;
	}
}
int main()
{
	int nums1[] = { 1,2,3,0,0,0 };
	int nums2[] = { 2,5,6};
	merge(nums1, 6, 3, nums2, 3, 3);
	return 0;
}