#define _CRT_SECURE_NO_WARNINGS 1
//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//编写一个函数找出这两个只出现一次的数字。
//例如：
//有数组的元素是：1，2，3，4，5，1，2，3，4，6
//只有5和6只出现1次，要找出5和6.
#include<stdio.h>
//void sort(int* arr, int sz)
//{
//	int i = 0;
//	
//	for (i = 0; i < sz - 1; i++)
//	{
//		int flag = 0;
//		for (int j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//				flag = 1;
//			}
//		}
//		if (flag = 0)
//		{
//			break;
//		}
//	}
//}
//void Find_number(int* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		if (arr[i] == arr[i + 1])
//		{
//			i++;
//		}
//		else
//		{
//			printf("单身狗是%d\n", arr[i]);
//		}
//	}
//}
//int main()
//{
//	//法一：先排序，在两个两个的比较,缺点：修改了数组
//	int arr[] = { 1,2,3,4,5,1,2,3,4,6, };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	sort(arr, sz);
//	Find_number(arr, sz);
//	return 0;
//}




void Find_number(int* arr, int sz,int* number1,int* number2)
{
	//先全部异或起来,异或得到的最终结果的二进制有1的，说明两个单身狗在这个二进制上不一样，一个为1，一个为0
	//就可以在这个二进制上将全部数字分开，分别全部异或就可以求出两个单生狗了
	//再将这两个单身狗分别储存到number1和number2上


	//全部异或
	int sum = 0;
	for (int i = 0; i < sz; i++)
	{
		sum = sum ^ arr[i];
	}
	//找到sum二进制的那个1，是第几位
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		if ((sum & (1 << i)) != 0)
		{
			//找到了
			break;
		}
	}
	//按照第i位是不是1，是不是0进行分组
	*number1 = *number2 = 0;
	for (int j = 0; j < sz; j++)
	{
		//是1，&(1<<i)!=0
		if ((arr[j] & (1 << i)) != 0)
		{
			*number1 = *number1 ^ arr[j];
		}
		//是0，&(1<<i)==0
		else
		{
			*number2 ^= arr[j];
		}
	}
}
int main()
{
	//法一:继续巧用按位异或
	int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int number1 = 0;
	int number2 = 0;
	Find_number(arr, sz,&number1,&number2);
	//因为要传过来，所以要&
	printf("单身狗是%d和%d", number1, number2);
	return 0;
}