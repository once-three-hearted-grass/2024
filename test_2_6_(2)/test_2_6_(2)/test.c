#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct stu
{
	int age;
	char name[20];
};

void bubble_sort1(int* arr, int sz)
{
	int i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		int j = 0;
		int flag = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
				flag = 1;
			}
		}
		if (flag == 0)
		{
			break;
		}
	}
}

void print_int(int* arr, int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}

void print_char(char* arr, int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%c", arr[i]);
	}
}

void test1()
{
	//冒泡排序
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort1(arr, sz);
	print_int(arr, sz);
}

int intcmp(const void* e1, const void* e2)
{
	//void*什么类型的指针都可以接收，但就是不能解引用，所以要强制类型转换
	/*if (*(int*)e1 > *(int*)e2)
	{
		return 1;
	}
	else if (*(int*)e1 < *(int*)e2)
	{
		return -1;
	}
	else
	{
		return 0;
	}*/
	//改善
	return *(int*)e1 - *(int*)e2;
}

int charcmp(const void* e1, const void* e2)
{
	
	return *(char*)e1 - *(char*)e2;
}

void test2()
{
	//使用qsort
	/*void qsort(void* base,
		       size_t num,
		       size_t width,
		       int(* cmp)(const void* e1, const void* e2));*/
	//base:起始地址
	//num：排序的个数
	//width：排的每个元素的宽度，单位是字节
	//自己要创建一个函数，这个函数实现*e1与*e2是怎样比较大小的，*e1>*e2则返回正数，*e1=*e2返回0，*e1<*e2返回负数
	//但qsort具体怎么实现的就不用管了

	//用qsort排整型数组
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), intcmp);
	print_int(arr, sz);
}

void test3()
{
	//用qsort排字符型数组
	char arr[] = "wejighajk";
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), charcmp);
	print_char(arr, sz);
}

int stu_age_cmp(const void* e1, const void* e2)
{
	return ((struct stu*)e1)->age - ((struct stu*)e2)->age;
}

void print_stu(struct stu* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("age:%d\tname:%s\t\n", (arr + i)->age, (arr + i)->name);
	}
}

void test4()
{
	//用qsort排结构体型数组,按照年龄来排
	struct stu arr[] = { {25,"ck"},{35,"hrt"},{15,"ym"} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), stu_age_cmp);
	print_stu(arr, sz);
}

int stu_name_cmp(const void* e1, const void* e2)//传的是数组的元素即结构体类型的
{
	strcmp(((struct stu*)e1)->name, ((struct stu*)e2)->name);
}

void test5()
{
	//用qsort排结构体型数组,按照名字来排
	//即按照名字的首字母来排
	//而且如果是中文的话会转换成拼音来排
	struct stu arr[] = { {25,"ck"},{15,"ym"} ,{35,"hrt"},{46,"hhj"}};
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), stu_name_cmp);
	print_stu(arr, sz);
}

void swop(void*e1,void*e2,int width)//因为不知道传入的是什么类型的值，所以用void*，但只知道了要交换的起始地址，还不知道要交换的多少字节，所以传入width
{
	int i = 0;
	//以char*为单位，一个字节一个字节的交换，所以还要强制类型转换(char*)
	//而且是交换数值，而不是交换地址
	for (i = 0; i < width; i++)
	{
		char tmp = *(char*)e1;
		*(char*)e1 = *(char*)e2;
		*(char*)e2 = tmp;
		((char*)e1)++;
		//强制类型转换的优先级比较低，所以最好用()给括起来，不然会出错，e1会先去与++结合，从而出错
		((char*)e2)++;
	}
}

void bubble_qsort1(void* arr, int sz,int width,int(*cmp)(const void*e1,const void*e2))//因为void*能够接收任何类型指针，所以用void*
{
	int i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		int j = 0;
		int flag = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			//e1:--->(char*)arr+j*width---->arr[j]的地址,因为不知道传入什么类型，且void*不能[],所以只能这样了
			//e2:--->(char*)arr+(j+1)*width---->arr[j+1]的地址
			if (cmp((char*)arr + j * width, (char*)arr + (j+1) * width)>0)
				//将要比较的两个值地址传入cmp比较函数，返回值>0,说明不是升序，所以交换值
				//这个cmp函数也是要传入的函数地址里的，所以是不用在bubble_sort1中实现的，是在外面实现的
			{
				swop((char*)arr + j * width, (char*)arr + (j + 1) * width,width);
				flag = 1;
			}
		}
		if (flag == 0)
		{
			break;
		}
	}
}

void test6()
{
	//冒泡排序模拟实现qsort
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_qsort1(arr, sz,sizeof(arr[0]),intcmp);
	print_int(arr, sz);
}

void test7()
{
	//冒泡排序模拟实现qsort,结构体版
	struct stu arr[] = { {25,"ck"},{35,"hrt"},{15,"ym"} ,{2,"lyp"} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_qsort1(arr, sz, sizeof(arr[0]), stu_name_cmp);
	print_stu(arr, sz);
}

int main()
{
	//test1();
	//test2();
	//test3();
	//test4();
	//test5();
	//test6();
	test7();
	return 0;
}