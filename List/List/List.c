#include"List.h"

//创建节点
LTNode* LTBuyNode(LTDataType x)
{
	LTNode* newnode = (LTNode*)malloc(sizeof(LTNode));
	newnode->next = newnode;
	newnode->prev = newnode;//因为要循环嘛
	newnode->data = x;
	return newnode;
}

//初始化
LTNode* LTInit()
{
	LTNode* ret = LTBuyNode(-1);
	return ret;
}

void LTPrint(LTNode* phead)
{
	assert(phead);
	LTNode* pcur = phead->next;
	while (pcur != phead)
	{
		printf("%d->", pcur->data);
		pcur = pcur->next;
	}
	printf("\n");
}

//尾插，就是插在phead之前
void LTPushBack(LTNode* phead, LTDataType x)
{
	assert(phead);
	//phead->prev  newnode phead
	LTNode* newnode = LTBuyNode(x);
	newnode->next = phead;
	newnode->prev = phead->prev;
	phead->prev->next = newnode;
	phead->prev = newnode;
}

void LTDestroy(LTNode* phead)
{
	assert(phead);
	LTNode* pcur = phead->next;
	LTNode* next = pcur->next;
	while (pcur != phead)
	{
		//pcur->prev->next = pcur->next;
		//pcur->next->prev = pcur->prev;//连接好再释放//可以不写
		free(pcur);
		pcur = next;
		next = next->next;
	}
	free(phead);
	phead = NULL;
}

bool LTEmpty(LTNode* phead)//取值为true和false
{
	assert(phead);//只有头指针就是空
	if (phead->next == phead)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//头插//就是插在phead之后
void LTPushFront(LTNode* phead, LTDataType x)
{
	assert(phead);
	//phead newnode phead->next
	LTNode* newnode = LTBuyNode(x);
	newnode->prev = phead;//优先对newnode进行更改，因为这样对原链表是没有改变的
	newnode->next = phead->next;
	phead->next->prev = newnode;
	phead->next = newnode;
}

//尾删
void LTPopBack(LTNode* phead)
{
	assert(phead);
	assert(phead != phead->next);//链表不能为空
	//先链接，再删
	//phead->prev->prev  phead->prev  phead
	//多定义几个变量就不用用很多个->了
	//还有有些时候画出来也还不错
	LTNode* del_prev = phead->prev->prev;
	LTNode* del = phead->prev;
	del_prev->next = phead;
	phead->prev = del_prev;
	free(del);
	del = NULL;
}

//头删
void LTPopFront(LTNode* phead)
{
	//这次我们只多定义一个变量
	assert(phead);
	assert(phead != phead->next);
	//phead phead->next  phead->next->next
	LTNode* del = phead->next;
	del->next->prev = phead;
	phead->next = del->next;
	free(del);
	del = NULL;
}



//查找某个节点
LTNode* LTFind(LTNode* phead, LTDataType x)
{
	assert(phead);
	LTNode* pcur = phead->next;
	while (pcur != phead)
	{
		if (pcur->data == x)
		{
			return pcur;
		}
		pcur = pcur->next;
	}
	return NULL;
}

//删除指定节点
void LTErase(LTNode* pos)
{
	assert(pos);
	//pos-prev pos pos->next
	pos->prev->next = pos->next;
	pos->next->prev = pos->prev;
	free(pos);
}

//在pos位置之后插入数据
void LTInsert(LTNode* pos, LTDataType x)
{
	assert(pos);
	LTNode* newnode = LTBuyNode(x);
	//pos newnode pos->next
	newnode->prev = pos;
	newnode->next = pos->next;
	pos->next->prev = newnode;
	pos->next = newnode;
}
