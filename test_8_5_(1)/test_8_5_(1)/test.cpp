#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

//class A
//{};
//
//class B:public A
//{};

//class person
//{
//public:
//	virtual void buy()=0
//	{
//		cout << "买票全价" << endl;
//	}
//	virtual ~person()
//	{
//		cout << "~person()" << endl;
//	}
//};
//
//class student:public person
//{
//public:
//	virtual void buy()
//	{
//		cout << "买票半价" << endl;
//	}
//	virtual ~student()
//	{
//		cout << "~student()" << endl;
//	}
//};
//
//class military :public person
//{
//public:
//	virtual void buy()
//	{
//		cout << "买票优先" << endl;
//	}
//	virtual ~military()
//	{
//		cout << "~military()" << endl;
//	}
//};

//void func(person& x)
//{
//	x.buy();
//}
//
//class person
//{
//public:
//	virtual void buy1()
//	{
//		cout << "person:virtual void buy1()" << endl;
//	}
//	virtual void buy2()
//	{
//		cout << "virtual void buy2()" << endl;
//	}
//	void buy3()
//	{
//		cout << "void buy3()" << endl;
//	}
//};
//
//class student :public person
//{
//private:
//	virtual void buy1()
//	{
//		cout << "student:virtual void buy1()" << endl;
//	}
//};
//
//class military :public person
//{
//public:
//	virtual void buy1()
//	{
//		cout << "virtual void buy1()" << endl;
//	}
//};
//
//int main()
//{
//	//person a;
//	//student b;
//	//func(a);
//	//func(b);
//	//func(c);
//	//military c;
//	//person a;
//	//student s;
//	//person* x =new military;
//	//delete x;
//	//military a;
//
//	//cout << sizeof(person) << endl;
//
//	//person s;
//	//student s1;
//	//int a;
//	//static int b;
//	//int* c = new int;
//	//const char* d = "qqqqq";
//	//cout << "栈区:" << &a<<endl;
//	//cout << "静态区:" << &b<<endl;
//	//cout << "堆区:" << c<<endl;
//	//cout << "常量区:" << (void*)d<<endl;
//	//person p;
//	//cout << (void*)*((int*)&p) << endl;
//	//printf("函数：%p\n", &person::buy1);
//	//printf("函数：%p\n", &person::buy2);
//	//printf("函数：%p\n", &person::buy3);
//	//cout <<  & person::buy1 << endl;
//
//
//	person* s = (person*)new student;
//	s->buy1();
//
//	//printf("%p\n", person::buy1);
//	return 0;
//}
//
class A
{
public:
  A()
      :m_iVal(0)
  { test(); }
  virtual void func() { std::cout << m_iVal << " "; }
  void test() { func(); }
public:
  int m_iVal;
};



class B : public A
{
public:
    B() { test(); }
    virtual void func()
    {
        ++m_iVal;
        std::cout << m_iVal << ' ';
    }
};

int main()
{
    A* p = new B;
    p->test();
    return 0;

}