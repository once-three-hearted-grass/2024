#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现strcmp
#include<stdio.h>
#include<string.h>
#include<assert.h>
int my_strcmp(const char* arr1, const char* arr2)
{
	assert(arr1 && arr2);
	while (*arr1 == *arr2)//如果同时为\0,则还++,所以不行，所以到\0就停止了
	{
		if (*arr1 == '\0')//两个都为\0了，结束了
		{
			return 0;
		}
		arr1++;
		arr2++;
	}
	//说明arr1不等于arr2了
	return *arr1 - *arr2;

}
int main()
{
	char arr1[] = "abcde";
	char arr2[] = "abcd";
	int ret = my_strcmp(arr1, arr2);
	if (ret > 0)
	{
		printf("arr1大\n");
	}
	else if (ret == 0)
	{
		printf("一样大\n");
	}
	else
	{
		printf("arr2大\n");
	}
	return 0;
}