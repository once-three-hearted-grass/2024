#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#define NUM 5
typedef struct HashList
{
	int num;//记录哈希表中有几个元素
	char* a;
}HashList;

HashList* HashListInit()
{
	HashList* h = (HashList*)malloc(sizeof(HashList));
	h->num = 0;
	h->a = (char*)malloc(sizeof(char) * NUM);
	for (int i = 0; i < NUM; i++)
	{
		h->a[i] = 0;
	}
	return h;
}

int Hash(char a)
{
	return a % NUM;
}

void Put(HashList* h, char c)
{
	int index = Hash(c);
	if (h->a[index] == 0)
	{
		h->a[index] = c;
		h->num++;
	}
	else
	{
		int count = 1;
		index = Hash(Hash(c) + count);
		while (h->a[index] != 0&&h->num!=NUM)
		{
			count++;
			index = Hash(Hash(c) + count);
		}
		if (h->num != NUM)
		{
			h->a[index] = c;
			h->num++;
		}
	}
}

int main()
{
	HashList* h = HashListInit();
	Put(h, 'A');
	Put(h, 'F');
	printf("%c\n", h->a[0]);
	printf("%c\n", h->a[1]);
	return 0;
}
