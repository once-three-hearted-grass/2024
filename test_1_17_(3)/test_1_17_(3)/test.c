#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//给你一个整数数组 nums ，其中总是存在 唯一的 一个最大整数 。
//请你找出数组中的最大元素并检查它是否 至少是数组中每个其他数字的两倍 。如果是，则返回 最大元素的下标 ，否则返回 - 1 。
int dominantIndex(int* nums, int numsSize) {
    int a = 0;
    int i = 0;
    a = nums[1] > nums[0] ? nums[1] : nums[0];
    //找出最大值
    for (i = 0; i < numsSize; i++)
    {
        a = nums[i] > a ? nums[i] : a;
    }
    //记录最大值
    for (i = 0; i < numsSize; i++)
    {
        if (a == nums[i])
        {
            break;
        }
    }
    //判断是不是两倍
    int flag = 0;
    for (int j = 0; j < numsSize; j++)
    {
        if (i == j)
        {
            continue;
        }
        if (a < (nums[j] * 2))
        {
            flag = 1;
            break;
        }
    }
    if (flag == 1)
    {
        return -1;
    }
    else
    {
        return i;
    }
}
int main()
{
    int nums[10] = { 0 };
    for (int i = 0; i < 10; i++)
    {
        scanf("%d", &nums[i]);
    }
    printf("%d", dominantIndex(nums, 10));
	return 0;
}