#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        vector<vector<int>> arr;//这就相当于一个二维数组
        arr.resize(numRows);//先开辟好空间//而且可以访问
        //在开辟好每一列的空间
        for (int i = 0; i < numRows; i++)
        {
            arr[i].resize(i + 1);
        }
        //初始化好二维数组
        for (int i = 0; i < arr.size(); i++)
        {
            for (int j = 0; j < arr[i].size(); j++)
            {
                //只需要arr[i][j]就可以访问二维数组的每个元素了
                if (j == 0 || j == (arr[i].size() - 1))
                {
                    arr[i][j] = 1;
                }
                else
                {
                    arr[i][j] = 0;
                }
            }
        }
        //开始实现杨辉三角
        for (int i = 0; i < arr.size(); i++)
        {
            for (int j = 0; j < arr[i].size(); j++)
            {
                if (arr[i][j] == 0)
                {
                    arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
                }
            }
        }
        return arr;
    }
};