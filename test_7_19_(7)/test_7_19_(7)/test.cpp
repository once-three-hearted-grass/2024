#define _CRT_SECURE_NO_WARNINGS 1
//描述
//求1 + 2 + 3 + ... + n，要求不能使用乘除法、for、while、if、else、switch、case等关键字及条件判断语句（A ? B : C）。
//
//	示例1
//	输入：
//	5
//	复制
//	返回值：
//	15
//	复制
//	示例2
//	输入：
//	1
//	复制
//	返回值：
//	1
class Solution {
public:
    class A
    {
    public:
        A()
        {
            sum += i;
            i++;
        }
    };
    int Sum_Solution(int n) {
        A arr[n];
        return sum;
    }
private:
    static int sum;
    static int i;
};
int Solution::sum = 0;
int Solution::i = 1;