#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	unsigned char a = 200;
	//200 1100 1000
	unsigned char b = 100;
	//100 0110 0100
	unsigned char c = 0;
	//0   0000 0000
	c = a + b;
	//相加整型提升补0
	//a+c 300 0001 0010 1100
	//c 0010 1100
	printf("%d %d", a + b, c);//300 44
	//c提升 还是一样的  44
	return 0;
}