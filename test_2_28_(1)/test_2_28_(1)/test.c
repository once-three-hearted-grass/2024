#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	char c = 'A';
	if ('0' <= c <= '9')
		printf("YES");
	else printf("NO");
	//<=和>=都是从左到右运算，'0'<='A'表达式为真，所以返回1，1<='9'所以又返回1为真，所以打印YES
	return 0;
}

