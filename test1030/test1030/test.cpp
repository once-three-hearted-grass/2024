#define _CRT_SECURE_NO_WARNINGS 1

#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>

int main() {
    int year, month, day;
    int total = 0;
    int all;

    scanf("%d %d %d", &year, &month, &day);
    for (int i = 1; i < year; i++) {
        total += 365;
        if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0) {//应该是i去判断是不是闰年
            total += 1;
        }
    }
    //到去年的天数
    int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };//数组存储每个月的天数
    for (int i = 1; i < month; i++)
    {
        total += arr[i];
        if ((i==2)&&((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)) {//到了二月且为闰年
            total += 1;
        }
    }
        
    //到上个月的天数
    total += day;//到今天的天数
    all = total % 7;
    switch (all) {
    case 1:
        printf("Monday");
        break;
    case 2:
        printf("Tuesday");
        break;
    case 3:
        printf("Wednesday");
        break;
    case 4:
        printf("Thursday");
        break;
    case 5:
        printf("Friday");
        break;
    case 6:
        printf("Saturday");
        break;
    case 0:
        printf("Sunday");
        break;
    }
    return 0;
}