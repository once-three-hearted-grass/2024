#define _CRT_SECURE_NO_WARNINGS 1

//给定一个整数数组 nums，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。
//输入: nums = [1, 2, 3, 4, 5, 6, 7], k = 3
//输出 : [5, 6, 7, 1, 2, 3, 4]
//解释 :
//	向右轮转 1 步 : [7, 1, 2, 3, 4, 5, 6]
//	向右轮转 2 步 : [6, 7, 1, 2, 3, 4, 5]
//	向右轮转 3 步 : [5, 6, 7, 1, 2, 3, 4]

void rotate(int* nums, int numsSize, int k) {
	//法一：一步一步地移动，每次都把最后一个数据拿到最前面去，要移动数组
	//但时间复杂度高
	int n = numsSize;
	while (k--)//移动k次
	{
		int tmp = nums[numsSize - 1];
		//数组整体向后移一步,倒着移
		for (int i = n - 1; i >=1 ; i--)//记录好初始与末尾，循环就好写了   arr[n-1]=arr[n-2]    arr[1]=arr[0]
		{
			nums[i] = nums[i - 1];
		}
		nums[0] = tmp;
	}
}

//void rotate(int* nums, int numsSize, int k) {
//	//法二:创建一个新数组，依次先拷贝后面k个，在拷贝前面的就可以了,但是VS不支持变长数组，所以算了
//	
//}


void Reverse(int* arr, int left, int right)
{
	while (left < right)
	{
		int tmp = arr[left];
		arr[left] = arr[right];
		arr[right] = tmp;
		left++;
		right--;
	}
}
void rotate(int* nums, int numsSize, int k) {
	//法三：旋转三次，先旋转后面k个
	//在旋转前面，最后旋转整体就可以了
	//万一k值太大了呢,所以要处理一下
	int n = numsSize;
	k %= n;
	Reverse(nums, n - k, n - 1);
	Reverse(nums,0, n - k - 1);
	Reverse(nums,0, n - 1);
}
#include<stdio.h>
int main()
{

	return 0;
}