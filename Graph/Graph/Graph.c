#define _CRT_SECURE_NO_WARNINGS 1
#include"Graph.h"

//初始化图
Graph* InitGraph(int vexnum)
{
	Graph* G = (Graph*)malloc(sizeof(Graph));
	G->vex = (char*)malloc(sizeof(char) * vexnum);
	//arc是一个二级指针数组，每个元素指向一个一维数组，也相当于一个二维数组
	G->arc = (int**)malloc(sizeof(int*) * vexnum);
	G->vexNUM = vexnum;
	G->arcNUM = 0;
	for (int i = 0; i < vexnum; i++)
	{
		G->arc[i] = (int*)malloc(sizeof(int) * vexnum);
	}
	return G;
}

//创造图
void CreatGraph(Graph* G, char* vex, int* arc)//第二个参数传的是顶点，第三个传的是边，强制类型转换而来的
{
	for (int i = 0; i < G->vexNUM; i++)
	{
		G->vex[i] = vex[i];//赋值顶点
		for (int j = 0; j < G->vexNUM; j++)//赋值边
		{
			G->arc[i][j] = arc[i * G->vexNUM + j];
			if (G->arc[i][j] == 1)
			{
				G->arcNUM++;
			}
		}
	}
	G->arcNUM /= 2;//因为是无向图
}

//深度优先遍历图
void DFS(Graph* G, int* visited, int index)//第二个参数是看这个顶点是否被访问了,第二个参数是看从哪个顶点开始访问
{
	//先访问传入的这个顶点
	printf("%c ", G->vex[index]);
	//标记
	visited[index] = 1;//表示被访问过了
	for (int i = 0; i < G->vexNUM; i++)
	{
		if (G->arc[index][i] == 1 && visited[i] == 0)//有这条边并且这个顶点未被访问
		{
			//那就沿着这条边去访问这个顶点，这个顶点的访问都一样的
			DFS(G, visited, i);
		}
	}
}

#include"Queue.h"
//广度优先遍历
void BFS(Graph* G, int* visited, int index)
{
	//这个是先访问在入队列，再依次把相连边的的顶点先访问再入队列
	//如果是先入队列，再出队列时在访问，就很有可能把同一个节点访问两次，不好，或者入队列前就标记好了，都一样的
	//入队列前要标记好，这个是最重要的
	printf("%c ", G->vex[index]);
	visited[index] = 1;
	Queue q;
	QueueInit(&q);
	QueuePush(&q, index);
	while (!QueueEmpty(&q))
	{
		int i = QueueFront(&q);
		QueuePop(&q);
		//再挨着把相连边的顶点入队列
		for (int j = 0; j < G->vexNUM; j++)
		{
			if (G->arc[i][j] == 1 && visited[j] == 0)
			{
				printf("%c ", G->vex[j]);
				visited[j] = 1;
				QueuePush(&q, j);
			}
		}
	}
	QueueDestroy(&q);
}