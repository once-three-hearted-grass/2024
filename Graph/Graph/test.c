
#include"Graph.h"
int main()
{
	Graph* G = InitGraph(5);
	int arc[5][5] = {
		0,1,1,0,0,
		1,0,0,1,1,
		1,0,0,1,1,
		0,1,1,0,1,
		0,1,1,1,0
	};
	CreatGraph(G, "ABCDE", (int*)arc);
	int* visited = (int*)malloc(sizeof(int) * 5);
	for (int i = 0; i < 5; i++)
	{
		visited[i] = 0;
	}
	//DFS(G, visited, 0);
	BFS(G, visited, 0);
	return 0;
}