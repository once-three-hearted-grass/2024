#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stack>
using namespace std;


class MinStack {
public:
    MinStack() {
        //不用写
    }

    void push(int val) {
        stack1.push(val);
        if (stack2.empty() || stack2.top() >= val)//为空，或者val就是最小数据
        {
            stack2.push(val);
        }
    }

    void pop() {
        int tmp = stack1.top();
        stack1.pop();
        if (tmp == stack2.top())
        {
            stack2.pop();
        }
    }

    int top() {
        return stack1.top();
    }

    int getMin() {
        return stack2.top();
    }
private:
    stack<int> stack1;
    stack<int> stack2;
};
