#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<assert.h>

#define _CRT_SECURE_NO_WARNINGS 1
typedef char STDataType;
typedef struct Stack
{
	STDataType* a;
	int top;
	int capacity;
}Stack;
//初始化栈
void StackInit(Stack* ps)
{
	ps->a = NULL;
	ps->capacity = ps->top = 0;//top表示栈中元素个数，也表示栈顶元素的下一个位置
}

//入栈
void StackPush(Stack* ps, STDataType x)
{
	assert(ps);
	if (ps->top == ps->capacity)
	{
		int new_capacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		STDataType* p = (STDataType*)realloc(ps->a, sizeof(STDataType) * new_capacity);
		if (p == NULL)
		{
			perror("StackPush:realloc:");
			return;
		}
		ps->a = p;
		ps->capacity = new_capacity;
	}
	//入栈就是直接在top位置放数据就是了
	ps->a[ps->top] = x;
	ps->top++;
}

//出栈 
void StackPop(Stack* ps)//直接少最后一个
{
	assert(ps);
	assert(ps->top);
	ps->top--;
}

//获取栈顶元素
STDataType StackTop(Stack* ps)
{
	assert(ps);
	assert(ps->top);
	return ps->a[(ps->top) - 1];
}

//获取栈有效元素个数
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->top;
}

//判断栈是否为空
bool StackEmpty(Stack* ps)
{
	return ps->top == 0;
}

//销毁栈
void StackDestroy(Stack* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->top = ps->capacity = 0;
}

//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
//有效字符串需满足：
//左括号必须用相同类型的右括号闭合。
//左括号必须以正确的顺序闭合。
//每个右括号都有一个对应的相同类型的左括号。
//示例 1：
//输入：s = "()"
//输出：true
//示例 2：
//输入：s = "()[]{}"
//输出：true
//示例 3：
//输入：s = "(]"
//输出：false

bool isValid(char* arr) {
	//左括号就入栈，右括号的话，就出一个栈，与之匹配，看满不满足
	char* c = arr;
	Stack s;
	StackInit(&s);
	while (*c != '\0')
	{
		if (*c == '{' || *c == '[' || *c == '(')
		{
			StackPush(&s, *c);
		}
		else//说明为右括号,这是就要看匹不匹配，不匹配的话就return
		{
			if (StackEmpty(&s))//如果栈为空，直接就不匹配了，不用出栈了，说明右括号多了
			{
				StackDestroy(&s);
				return false;
			}
			char tmp = StackTop(&s);
			StackPop(&s);
			if ((*c == '}' && tmp != '{') || (*c == ']' && tmp != '[') || (*c == ')' && tmp != '('))
			{
				StackDestroy(&s);
				return false;
			}
		}
		c++;
	}
	//这是走到这里，如果栈不为空，说明左括号多了
	if (!StackEmpty(&s))
	{
		StackDestroy(&s);
		return false;
	}
	else//成功了
	{
		//别忘了销毁
		StackDestroy(&s);
		return true;
	}
}
int main()
{
	char arr[] = "({{{{}}}))";
	isValid(arr);
	return 0;
}