#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
//链表 合并
//设线性表A = (a1, a2, …, am)，B = (b1, b2, …, bn)，试写一个按下列规则合并A、B为线性表C的算法，使得：
//C = (a1, b1, …, am, bm, bm + 1, …, bn) 当m≤n时；
//或者
//C = (a1, b1, …, an, bn, an + 1, …, am) 当m > n时。
//线性表A、B、C均以单链表作为存储结构，且C表利用A表和B表中的结点空间构成。注意：单链表的长度值m和n均未显式存储。
//函数的原型如下：
//即将A和B合并为C，其中 C 已经被初始化为空单链表
//相关定义如下：
struct _lnklist {
    int data;
    struct _lnklist* next;
};
typedef struct _lnklist Node;
typedef struct _lnklist* LinkList;
void lnk_merge(LinkList A, LinkList B, LinkList C) {
    //思路：定义三个指针，curA,curB,tail,依次尾插到C就可以了
    LinkList curA = A->next;//因为自带头结点
    LinkList curB = B->next;
    LinkList tail = C;
    while (curA && curB)
    {
        LinkList nextA = curA->next;
        LinkList nextB = curB->next;
        tail->next = curA;
        curA->next = curB;
        tail = curB;
        curA = nextA;
        curB = nextB;
    }
    if (curB == NULL)
    {
        tail->next = curA;
    }
    else if(curA==NULL)//什么都不用干
    {
        ;
    }
}