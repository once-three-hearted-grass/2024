#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

#define MAX 32767

typedef struct Graph
{
	char* vex;
	int** arc;
	int vexNUM;
	int arcNUM;
}Graph;

//初始化图
Graph* InitGraph(int vexnum);

//创造图
void CreatGraph(Graph* G, char* vex, int* arc);

//深度优先遍历图
void DFS(Graph* G, int* visited, int index);

//首先先写一个函数来求各个顶点的入度
int* GetIn(Graph* G);

//开始拓扑排序
int* TopSort(Graph* G);

//关键路径算法
void KeyWay(Graph* G);