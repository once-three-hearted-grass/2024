#define _CRT_SECURE_NO_WARNINGS 1
#include"KeyWay.h"

//初始化图
Graph* InitGraph(int vexnum)
{
	Graph* G = (Graph*)malloc(sizeof(Graph));
	G->vex = (char*)malloc(sizeof(char) * vexnum);
	//arc是一个二级指针数组，每个元素指向一个一维数组，也相当于一个二维数组
	G->arc = (int**)malloc(sizeof(int*) * vexnum);
	G->vexNUM = vexnum;
	G->arcNUM = 0;
	for (int i = 0; i < vexnum; i++)
	{
		G->arc[i] = (int*)malloc(sizeof(int) * vexnum);
	}
	return G;
}

//创造图
void CreatGraph(Graph* G, char* vex, int* arc)//第二个参数传的是顶点，第三个传的是边，强制类型转换而来的
{
	for (int i = 0; i < G->vexNUM; i++)
	{
		G->vex[i] = vex[i];//赋值顶点
		for (int j = 0; j < G->vexNUM; j++)//赋值边
		{
			G->arc[i][j] = arc[i * G->vexNUM + j];
			if (G->arc[i][j]>0&&G->arc[i][j]!=MAX)
			{
				G->arcNUM++;
			}
		}
	}
}

//深度优先遍历图
void DFS(Graph* G, int* visited, int index)//第二个参数是看这个顶点是否被访问了,第二个参数是看从哪个顶点开始访问
{
	//先访问传入的这个顶点
	printf("%c ", G->vex[index]);
	//标记
	visited[index] = 1;//表示被访问过了
	for (int i = 0; i < G->vexNUM; i++)
	{
		if (G->arc[index][i]>0&&G->arc[index][i]!=MAX && visited[i] == 0)//有这条边并且这个顶点未被访问
		{
			//那就沿着这条边去访问这个顶点，这个顶点的访问都一样的
			DFS(G, visited, i);
		}
	}
}

//首先先写一个函数来求各个顶点的入度
int* GetIn(Graph* G)//这次这个图是有向图
{
	int* arr = (int*)malloc(sizeof(int) * G->vexNUM);
	for (int i = 0; i < G->vexNUM; i++)
	{
		arr[i] = 0;
	}
	for (int i = 0; i < G->vexNUM; i++)
	{
		for (int j = 0; j < G->vexNUM; j++)
		{
			if (G->arc[i][j]>0&&G->arc[i][j]!=MAX)
			{
				arr[j]++;
			}
		}
	}
	return arr;
}

typedef struct Stack
{
	int a;
	struct Stack* next;
}Stack;

Stack* Init()
{
	Stack* s = (Stack*)malloc(sizeof(Stack));
	s->a = 0;
	s->next = NULL;
	return s;
}

void Push(Stack* s, int x)//头插头删就是栈
{
	Stack* tmp = (Stack*)malloc(sizeof(Stack));
	tmp->a = x;
	tmp->next = s->next;
	s->next = tmp;
	s->a++;
}

//判断栈是否为空
int IsEmpty(Stack* s)
{
	if (s->a)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

int Pop(Stack* s)
{
	if (!IsEmpty(s))
	{
		Stack* tmp = s->next;
		s->next = tmp->next;
		int a = tmp->a;
		free(tmp);
		s->a--;
		return a;
	}
	else
	{
		return -1;
	}
}

//开始拓扑排序
int* TopSort(Graph* G)
{
	int* InDe = GetIn(G);
	//再用一个数组来记录top序列
	int* Way = (int*)malloc(sizeof(int) * G->vexNUM);
	int index = 0;
	//从中找出入度为0的，入栈
	Stack* s = Init();
	for (int i = 0; i < G->vexNUM; i++)
	{
		if (InDe[i] == 0)
		{
			Push(s, i);
		}
	}
	//开始挨着挨着出栈
	while (!IsEmpty(s))
	{
		//出栈
		int tmp = Pop(s);
		//打入路径
		Way[index] = tmp;
		index++;
		//以tmp为起点的边的终点入度减一
		for (int i = 0; i < G->vexNUM; i++)
		{
			if (G->arc[tmp][i]>0&&G->arc[tmp][i]!=MAX)
			{
				InDe[i]--;
				if (InDe[i] == 0)
				{
					Push(s, i);
				}
			}
		}
	}
	for (int i = 0; i < index; i++)
	{
		printf("v%c", G->vex[Way[i]]);
	}
	if (index < G->vexNUM)
	{
		printf("\n该图有环\n");
	}
	return Way;
}

int GetIndex(Graph* G, int* top, int j)
{
	int i;
	for (i = 0; i < G->vexNUM; i++)
	{
		if (top[i] == j)
		{
			return i;
		}
	}
	return -1;
}

//关键路径算法
void KeyWay(Graph* G)
{
	int* top = TopSort(G);//先得到拓扑序列
	//先弄节点的最早发生时间
	//从拓扑序列的第一节点开始弄
	int* Early = (int*)malloc(sizeof(int) * G->vexNUM);
	int* Late = (int*)malloc(sizeof(int) * G->vexNUM);
	for (int i = 0; i < G->vexNUM; i++)
	{
		Early[i] = 0;
		Late[i] = 0;
	}
	//开始求early
	Early[0] = 0;
	for (int i = 0; i < G->vexNUM; i++)//每个i都对应一个top[i]
	{
		//求一个节点的early的话，就是前一个节点的early+边长这些中的最大值
		int max = 0;
		for (int j = 0; j < G->vexNUM; j++)//找到以top[i]为终点的所有边
		{
			if (G->arc[j][top[i]] > 0 && G->arc[j][top[i]]!= MAX)
			{
				int index = GetIndex(G, top, j);//找到j这个节点在top的位置，这个位置也是j节点在early中的位置
				if (Early[index] + G->arc[j][top[i]] > max)
				{
					max = Early[index] + G->arc[j][top[i]];
				}
			}
		}
		//这就求出一个top[i]节点的最早发生时间了，为MAX,top[i]的位置就是i
		Early[i] = max;
	}
	printf("\n");
	for (int i = 0; i < G->vexNUM; i++)
	{
		printf("%d ", Early[i]);
	}
	//求最晚发生时间
	Late[G->vexNUM - 1] = Early[G->vexNUM - 1];//top序列中最后一个节点的early与late是一样的
	//这个要按着拓扑序列的倒序倒着来求
	for (int i = G->vexNUM - 2; i >= 0; i--)
	{
		int min = MAX;//要以top[i]为起点的边，top[i]的late等于终点的late-边的最小值
		for (int j = 0; j < G->vexNUM; j++)
		{
			if (G->arc[top[i]][j] > 0 && G->arc[top[i]][j] != MAX)
			{
				//求出终点节点的拓扑序列位置
				//late和early都是以拓扑序列的位置来的
				int index = GetIndex(G, top, j);
				if (Late[index] - G->arc[top[i]][j] < min)
				{
					min = Late[index] - G->arc[top[i]][j];
				}
			}
		}
		Late[i] = min;
	}
	printf("\n");
	for (int i = 0; i < G->vexNUM; i++)
	{
		printf("%d ", Late[i]);
	}
	//现在开始求关键路径和关键事件
	//就是一个活动的最早开始时间和最晚开始时间之差为0就是关键事件
	//最早开始时间就是起点事件的最早开始时间
	//最晚开始时间就是终点事件的最晚开始时间
	//那就挨着挨着看每个活动也就是每条边符不符合了
	printf("\n");
	for (int i = 0; i < G->vexNUM; i++)
	{
		for (int j = 0; j < G->vexNUM; j++)
		{
			if (G->arc[i][j] > 0 && G->arc[i][j] != MAX)
			{
				//但在判断之前还要找到这两个节点的拓扑序列位置
				int start = GetIndex(G, top, i);
				int end = GetIndex(G, top, j);
				if (Late[end] - G->arc[i][j] - Early[start] == 0)
				{
					//打印路径
					printf("v%c->v%c:%d\n", G->vex[i], G->vex[j], G->arc[i][j]);
				}
			}
		}
	}
}