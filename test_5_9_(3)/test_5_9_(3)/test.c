#define _CRT_SECURE_NO_WARNINGS 1
//链表 删除范围内结点
//已知线性表中的元素（整数）以值递增有序排列，并以单链表作存储结构。试写一高效算法，
//删除表中所有大于mink且小于maxk的元素（若表中存在这样的元素），分析你的算法的时间复杂度。
//链表结点定义如下：
struct _lnklist {
    int data;
    struct _lnklist* next;
};
typedef struct _lnklist Node;
typedef struct _lnklist* LinkList;
//其中L指向链表的头结点。
void lnk_del_x2y(LinkList L, int mink, int maxk) {
    //自带头结点的,所以不用考虑开始是否为NULL
    //画图分析，定义两个指针，一个pre，一个cur，满足条件就删除
    LinkList pre = L;
    LinkList cur = L->next;
    while (cur)
    {
        if (cur->data > mink && cur->data < maxk)//删除
        {
            pre->next = cur->next;
            free(cur);
            cur = pre->next;
        }
        else
        {
            pre = cur;
            cur = cur->next;
        }
    }
}