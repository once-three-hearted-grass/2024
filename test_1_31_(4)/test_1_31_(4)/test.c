#define _CRT_SECURE_NO_WARNINGS 1
//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//给定s1 = abcd和s2 = ACBD，返回0
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC
#include<stdio.h>
#include<string.h>
void Spin(char* arr1, int k, int sz)
{
	//法一：创建临时数组
	char arr2[100] = { 0 };
	for (int i = 0; i < sz; i++)
	{
		arr2[i] = arr1[i];
	}
	for (int i = 0; i < sz; i++)
	{
		arr1[i] = arr2[(i + k) % sz];
	}
}
int Judge(char arr[], char arr1[], char arr2[],int sz)
{
	int k = 0;
	int flag = 0;
	for (k = 0; k < sz; k++)
	{
		Spin(arr, k, sz);
		if (strcmp(arr, arr2) == 0)
		{
			flag = 1;
			break;
		}
		strcpy(arr, arr1);
	}
	if (flag == 1)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
int main()
{
	char arr1[100] = { 0 };
	printf("请输入字符串arr1:");
	scanf("%s", arr1);
	char arr2[100] = { 0 };
	printf("请输入字符串arr2:");
	scanf("%s", arr2);
	char arr[100] = { 0 };
	strcpy(arr, arr1);
	int sz = strlen(arr);
	if (Judge(arr, arr1, arr2, sz) == 1)
	{
		printf("arr2是由arr1旋转而来的\n");
	}
	else
	{
		printf("arr2不是由arr1旋转而来的\n");
	}
	return 0;
}