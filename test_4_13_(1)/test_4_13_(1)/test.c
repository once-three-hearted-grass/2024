#define _CRT_SECURE_NO_WARNINGS 1

//给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
//输入：head = [1, 2, 3, 4, 5]
//输出：[5, 4, 3, 2, 1]
#include<stdio.h>
struct ListNode {
	int val;
	struct ListNode* next;
};
typedef struct ListNode ListNode;
struct ListNode* reverseList(struct ListNode* head) {
    ListNode* pre = head;
    if (pre == NULL)
    {
        return NULL;
    }
    //pre不为空,就可以next了
    ListNode* cur = pre->next;
    pre->next = NULL;//第一个节点设置为尾节点
    while (cur)
    {
        ListNode* next = cur->next;
        cur->next = pre;//改变方向
        //接下来三个指针全部向后移
        pre = cur;
        cur = next;
        if (next!= NULL)
        {
            next = next->next;
        }
    }
    return pre;
}