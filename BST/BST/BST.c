#define _CRT_SECURE_NO_WARNINGS 1

#include"BST.h"
Tree* BSTSerch(Tree* root, int key)
{
	//在搜索二叉树中查找某个数据
	//如果root为空说明找不到，值一样说明找到了
	//值不一样那就往下找
	if (root == NULL)
	{
		return NULL;
	}
	if (root->data == key)
	{
		return root;
	}
	if (key < root->data)
	{
		return BSTSerch(root->left, key);
	}
	else
	{
		return BSTSerch(root->right, key);
	}
}

void BSTInsert(Tree** T, int key)
{
	//如果这个节点为空，说明可以创建节点了，而且传的是地址，可以改变外部
	//不为空的话，就往下创建
	if ((*T) == NULL)
	{
		Tree* tmp = (Tree*)malloc(sizeof(Tree));
		if (tmp == NULL)
		{
			perror("BSTInsert:malloc:");
			return;
		}
		tmp->left = NULL;
		tmp->right = NULL;
		tmp->data = key;
		(*T) = tmp;
	}
	if (key < (*T)->data)
	{
		BSTInsert(&(*T)->left, key);
	}
	else if (key == (*T)->data)
	{
		return;
	}
	else
	{
		BSTInsert(&(*T)->right, key);
	}
}

//写个中序遍历，因为中序遍历出的二叉搜索树是有序地
void InOrder(Tree* root)
{
	if (root == NULL)
	{
		return;
	}
	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}