#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<stdio.h>
typedef struct BST//这一次主要实现的就是搜索二叉树
{
	int data;
	struct BST* left;
	struct BST* right;
}Tree;

//在二叉搜索树中查找数据
Tree* BSTSerch(Tree* root, int key);

//在二叉搜索树中插入数据
void BSTInsert(Tree** T, int key);

//写个中序遍历，因为中序遍历出的二叉搜索树是有序地
void InOrder(Tree* root);
