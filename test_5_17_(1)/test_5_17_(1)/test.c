#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//参数说明:
//in， 原始字符串，保持不变
//out, 存放替换结果的字符串
//outlen，out空间的大小
//oldstr，要替换的旧字符串
//newstr，替换成的新字符串
//函数返回成功替换的次数，即有多少个子串被成功替换
//在替换过程中，任何情况下所得字符串（及结束符）不应该超过 outlen，如果某次替换所得字符串的长度超过 outlen，则不进行这次替换操作，整个替换操作结束。如：
//原始串为 "aaabbbccc"，outlen 为14, oldstr 为 "c"，newstr 为 "333" 时，两次替换后得 "aaabbb333333c"，此时字符串占用空间为14字节。
//如果再进行替换，则会超出 out 所占用的空间，所以停止替换操作。此时函数应该返回 2, out指向的串为 "aaabbb333333c"
//再如：原始串为 "aaabbbccc"，outlen 为10, oldstr 为 "b"，newstr 为 "123456"，进行替换后所得的串长度为14
//，与结束符一共占 15 个字节，超过outlen的10字节，此时不进行替换，函数应该返回 0。
char* my_strstr(const char* arr1, const char* arr2)
{
	//从arr1挨着挨着找，如果相等就继续循环判断，不等就退出循环,还要记录起始位置，相等判断的位置
	char* start1 = (char*)arr1;//记录起始位置
	char* start2 = (char*)arr2;
	char* cur1 = (char*)arr1;//记录相等的位置
	char* cur2 = (char*)arr2;
	while (*start1 != '\0')//把arr1挨个找完
	{
		cur1 = start1;
		cur2 = start2;
		if (*cur1 == *cur2)
		{
			//看是不是字串
			while (*cur1 == *cur2 && *cur1 != '\0' && *cur2 != '\0')//以防同时为\0.有一个为\0就可以退出来了
			{
				cur1++;
				cur2++;
			}
			//退出循环有三种可能
			//不相等，cur1为\0,cur2为\0,同时为\0
			if (*cur2 == '\0')//是字串
			{
				return start1;
			}
			if (*cur1 == '\0')//已经排除*cur1为\0的情况了，所以不可能同时为\0,那就是arr1现在太短了呗,就是没有字串呗
			{
				return NULL;
			}
			//走到这里就是不相等了，就退出这个if呗
		}
		//不相等，就继续前进呗
		start2 = (char*)arr2;
		start1++;
	}
	//可以走到了这里，比如一次都没有相等
	return NULL;
}

int my_strlen(const char* str)
{
	int count = 0;
	while (*str)
	{
		count++;
		str++;
	}
	return count;
}

char* my_strcat(char* des, const char* src)
{
	char* ret = des;
	//先找到des的\0
	while (*(++des));
	//在挨个替换,包括\0
	while (*(des++) = *(src++));
	return ret;
}

int str_replace(const char* in, char* out, int outlen, const char* oldstr, const char* newstr) {
	int oldlen = my_strlen(oldstr);
	int newlen = my_strlen(newstr);
	int in_len = my_strlen(in);
	int cur_out_len = 0;
	char* cur_in = (char*)in;
	char* cur_out = (char*)out;
	int count = 0;
	for (int i = 0; i < outlen; i++)
	{
		*cur_out = '\0';
		cur_out++;
	}
	cur_out = (char*)out;
	while ((*cur_in)!='\0')
	{
		char* new_in = my_strstr(cur_in, oldstr);
		if (new_in == NULL)//就挨着挨着拷贝
		{
			for (; cur_in < in + in_len; cur_in++)
			{
				cur_out_len++;
				*(cur_out) = *(cur_in);
				cur_out++;
			}
			break;
		}
		//找到了字串，就先把这个字串之前的内容开始拷贝
		for (; cur_in < new_in; cur_in++)
		{
			cur_out_len++;
			*(cur_out) = *(cur_in);
			cur_out++;
		}
		//cur_in就直接从new_in跳过oldlen就可以了
		//然后再替换老字符串内容,直接拷贝新字符串就可以了
		if (in_len+ (newlen-1)*(count+1) > outlen - 1)
		{
			//继续拷贝
			for (; cur_in < in + in_len; cur_in++)
			{
				cur_out_len++;
				*(cur_out) = *(cur_in);
				cur_out++;
			}
			break;
		}
		else
		{
			count++;
			my_strcat(out, newstr);
			cur_out = cur_out + newlen;
			cur_out_len = cur_out_len + newlen;
		}
		cur_in = new_in + oldlen;
	}
	return count;
}
int main()
{
	char *in = "aaabbbccc";
	char* oldstr = "c";
	char* newstr = "123";
	char out[14] = { 0 };
	str_replace(in, out, 14, oldstr, newstr);
}
