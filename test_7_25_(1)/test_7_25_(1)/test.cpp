#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

//class Solution {
//public:
//    string addStrings(string num1, string num2) {
//        string s2;//用来储存相加后的串
//        //s2.resize(max(num1.size(), num2.size()));//先扩容好，免得待会一直扩容//不能用size扩容，因为头插的话，会认为size里面的都是有效数据，不会处理
//        //所以用reserve
//        s2.reserve(max(num1.size(), num2.size()));
//        int tail1 = num1.size() - 1;
//        int tail2 = num2.size() - 1;
//        int gap = 0;
//        while (tail1 >= 0 || tail2 >= 0)//只要两个有一个大于0，就要继续计算
//        {
//            int a = tail1 >= 0 ? (num1[tail1] - '0') : 0;
//            int b = tail2 >= 0 ? (num2[tail2] - '0') : 0;
//            int sum = a + b + gap;
//            gap = sum / 10;
//            sum = sum % 10;
//            //然后就是头插s2//也可以先尾插，然后再逆转
//            s2.insert(0, 1, sum + '0');
//            tail1--;
//            tail2--;
//        }
//        if (gap == 1)
//        {
//            s2.insert(0, 1,  '1');
//        }
//        return s2;
//    }
//};

class Solution {
public:
    string addStrings(string num1, string num2) {
        string s2;//用来储存相加后的串
        //s2.resize(max(num1.size(), num2.size()));//先扩容好，免得待会一直扩容//不能用size扩容，因为头插的话，会认为size里面的都是有效数据，不会处理
        //所以用reserve
        s2.reserve(max(num1.size(), num2.size()));
        int tail1 = num1.size() - 1;
        int tail2 = num2.size() - 1;
        int gap = 0;
        while (tail1 >= 0 || tail2 >= 0)//只要两个有一个大于0，就要继续计算
        {
            int a = tail1 >= 0 ? (num1[tail1] - '0') : 0;
            int b = tail2 >= 0 ? (num2[tail2] - '0') : 0;
            int sum = a + b + gap;
            gap = sum / 10;
            sum = sum % 10;
            //然后就是头插s2//也可以先尾插，然后再逆转
            s2.push_back(sum + '0');
            tail1--;
            tail2--;
        }
        if (gap == 1)
        {
            s2.push_back('1');
        }
        reverse(s2.begin(), s2.end());
        return s2;
    }
};

int main()
{
    string s1("11");
    string s2("123");

    return 0;
}