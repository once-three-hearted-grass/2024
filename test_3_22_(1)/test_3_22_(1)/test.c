#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//32位
int main()
{
    union
    {
        short k;
        char i[2];
    }*s, a;
    s = &a;
    s->i[0] = 0x39;//0x39
    s->i[1] = 0x38;//
    //39 38
    // 小端所以short为38 39 
    printf("%x\n", a.k);//3839
    return 0;
}