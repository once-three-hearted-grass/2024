#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int main() {
    int n, sum = 0;
    scanf("%d", &n);
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            for (int k = 1; k <= n; k++) {
                if (i != j && j != k && i != k) {
                    printf("A:%d B:%d C:%d\n",i,j,k);
                    sum++;
                }
            }
        }
    }
    printf("%d", sum);

    return 0;
}