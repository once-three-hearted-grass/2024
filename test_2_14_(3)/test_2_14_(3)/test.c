#define _CRT_SECURE_NO_WARNINGS 1
//strerror:0~9的错误码
#include<stdio.h>
#include<string.h>
#include<errno.h>
int main()
{
	/*printf("%s\n", strerror(0));
	printf("%s\n", strerror(1));
	printf("%s\n", strerror(2));
	printf("%s\n", strerror(3));
	printf("%s\n", strerror(4));
	printf("%s\n", strerror(5));
	printf("%s\n", strerror(6));
	printf("%s\n", strerror(7));
	printf("%s\n", strerror(8));
	printf("%s\n", strerror(9));*/
	//FILE* pf = fopen("test.txt", "r");//但fopen具体怎么用，我还不会
	// //这个text.txt必须和text.c在同一个文件中才能找到
	//if (pf == NULL)
	//{
	//	printf("%s\n",strerror(errno));//用errno要引用errno.h
	//}
	//else
	//{
	//	printf("存在\n"); //因为文本文档自带.txt所以命名只需text即可
	//}
	FILE* pf = fopen("C:\\Users\\zjdsx\\Desktop\\test.txt", "r");//但fopen具体怎么用，我还不会
	//找桌面上的文件，属性中的地址+文件名+文件类型
	if (pf == NULL)
	{
		printf("%s\n", strerror(errno));//用errno要引用errno.h
	}
	else
	{
		printf("存在\n"); //因为文本文档自带.txt所以命名只需text即可
	}
	return 0;
}