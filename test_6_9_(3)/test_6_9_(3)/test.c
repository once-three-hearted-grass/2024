#define _CRT_SECURE_NO_WARNINGS 1
#include<stdbool.h>
#include<stdlib.h>
struct TreeNode {
      int val;
     struct TreeNode *left;
      struct TreeNode *right;
};
 
//bool isValidBST(struct TreeNode* root) {
//    //是不是二叉搜索树，先看第一个头节点满不满足，然后就是要保证左右子树同时是二叉搜索树
//    if (root == NULL)
//   {
//        return true;
//    }
//    if (root->left && root->left->val >= root->val)
//    {
//        return false;
//    }
//    if (root->right && root->right->val <= root->val)
//    {
//        return false;
//    }//满足并不能说明什么，不满足才有话说
//    return isValidBST(root->left) && isValidBST(root->right);//但是这个方法只能判断局部是不是满足，以后的节点满不满足不好判断//所以完善
//    //的话就要传入上下界
//} 


//bool my_isValidBST(struct TreeNode* root, struct TreeNode* min, struct TreeNode* max)
//{
//    //先看头
//    if (root == NULL)
//    {
//        return true;
//    }
//    if (min == NULL)
//    {
//        min->val = root->val;
//    }
//    if (root->left && root->left->val >= root->val)
//    {
//        return false;
//    }
//    if (root->right && root->right->val <= root->val)
//    {
//        return false;
//    }//满足并不能说明什么，不满足才有话说
//}
//bool isValidBST(struct TreeNode* root) {
//    //先看头
//    if (root == NULL)
//    {
//        return true;
//    }
//    if (root->left && root->left->val >= root->val)
//    {
//        return false;
//    }
//    if (root->right && root->right->val <= root->val)
//    {
//        return false;
//    }//满足并不能说明什么，不满足才有话说
//    return my_isValidBST(root->left, NULL,root) && isValidBST(root->right,root, NULL);//左就是上界，右的话就是下界//因为如果用0的话不好，空指针好控制一点
//    //因为这个0可能就真的作为下界，但这个0是我们随便乱写的，所以可能与val混乱，还是空指针吧
//}

//正解
bool my_isValidBST(struct TreeNode* root, struct TreeNode* min, struct TreeNode* max)
{
    //挨个判断每个节点是不是在范围内
    if (root == NULL)
    {
        return true;
    }
    if (min && root->val <= min->val)
    {
        return false;
    }
    if (max && root->val >= max->val)
    {
        return false;
    }
    return my_isValidBST(root->left, min, root) && my_isValidBST(root->right, root, max);//往左走，有最大值，，，往右走，有最小值，就这样定范围就可以了
}

bool isValidBST(struct TreeNode* root) {
    return my_isValidBST(root, NULL, NULL);//不能传0，不能传int，因为0是一个具体的值了，因为这样你初始化的范围不一定对，而且头没有范围
}