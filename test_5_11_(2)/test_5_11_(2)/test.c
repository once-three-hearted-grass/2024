#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
//设计你的循环队列实现。 循环队列是一种线性数据结构，其操作表现基于 FIFO（先进先出）原则并且队尾被连接在队首之后以形成一个循环。它也被称为“环形缓冲器”。
//循环队列的一个好处是我们可以利用这个队列之前用过的空间。在一个普通队列里，一旦一个队列满了，我们就不能插入下一个元素，
//即使在队列前面仍有空间。但是使用循环队列，我们能使用这些空间去存储新的值。
//你的实现应该支持如下操作：
//MyCircularQueue(k) : 构造器，设置队列长度为 k 。
//Front : 从队首获取元素。如果队列为空，返回 - 1 。
//Rear : 获取队尾元素。如果队列为空，返回 - 1 。
//enQueue(value) : 向循环队列插入一个元素。如果成功插入则返回真。
//deQueue() : 从循环队列中删除一个元素。如果成功删除则返回真。
//isEmpty() : 检查循环队列是否为空。
//isFull() : 检查循环队列是否已满。


//这里用数组实现循环队列
typedef int QDataType;
typedef struct {
	int front;
	int tail;
	int size;//表示数组长度
	QDataType* a;//指向数据的数组
} MyCircularQueue;


MyCircularQueue* myCircularQueueCreate(int k) {
	MyCircularQueue* ret = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
	//这里就不判断是否开辟失败了，因为OJ题一般不会开辟失败
	ret->a = (QDataType*)malloc(sizeof(QDataType) * (k+1));//这里让队列长度为k，那就开创k+1
	ret->front = 0;
	ret->tail = 0;//表示没有数据存进来
	ret->size = k + 1;
	return ret;
}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {//向循环队列插入一个元素。如果成功插入则返回真。
	if ((obj->tail + 1) % (obj->size) == obj->front)//说明满了
	{
		return false;
	}
	obj->a[obj->tail] = value;
	obj->tail=(obj->tail+1)%obj->size;//因为可能跑到开头去了
	return true;
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {//从循环队列中删除一个元素。如果成功删除则返回真。
	if (obj->front == obj->tail)
	{
		//说明为空
		return false;
	}
	obj->front = (obj->front + 1) % obj->size;
	return true;
}

int myCircularQueueFront(MyCircularQueue* obj) {//从队首获取元素。如果队列为空，返回 - 1 。
	if (obj->front == obj->tail)
	{
		//说明为空
		return -1;
	}
	return obj->a[obj->front];
}

int myCircularQueueRear(MyCircularQueue* obj) {//获取队尾元素。如果队列为空，返回 - 1 。
	if (obj->front == obj->tail)
	{
		//说明为空
		return -1;
	}
	return obj->a[(obj->tail + obj->size - 1)%(obj->size)];//因为如果为0的话，就变为-1了，因为变为最后一个元素的
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
	if (obj->front == obj->tail)
	{
		//说明为空
		return true;
	}
	else
	{
		return false;
	}
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
	if ((obj->tail + 1) % (obj->size) == obj->front)//说明满了
	{
		return true;
	}
	else
	{
		return false;
	}
}

void myCircularQueueFree(MyCircularQueue* obj) {
	//先free数组
	free(obj->a);
	//再free队列
	free(obj);
}