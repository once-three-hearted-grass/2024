#pragma once
#include<iostream>
#include<algorithm>
#include<assert.h>
using namespace std;
//我们实现的链表是带头双向循环链表
#include<list>

namespace bit
{
	template<class T>//一个定义的这种模版只能适用于一次函数，一次模版，下一次使用就要再写一次
	struct ListNode//也可以定义class，但是这个要全部访问，struct刚刚好默认公开
	{
		ListNode<T>* _prev;
		ListNode<T>* _next;
		T _val;//这三个就是成员变量

		//构造函数//也是一个节点的构造
		ListNode(const T& t=T())
			:_val(t)
			, _prev(nullptr)
			,_next(nullptr)
		{}

		ListNode(T&& t)
			:_val(move(t))
			, _prev(nullptr)
			, _next(nullptr)
		{}

		template<class...Args>
		ListNode(Args...args)
			:_val(forward<Args>(args)...)
			, _prev(nullptr)
			, _next(nullptr)
		{}
	};

	//template<class T>
	//struct listIterator//因为这些都是分装，用户不会访问到这个类，只会在list的类访问，所以要对list的成员变量private
	//{
	//	//这里可以不用保护，而且到了后面还会访问
	//	typedef ListNode<T> Node;
	//	//再来个普通构造
	//	listIterator(Node* tmp)
	//		:_node(tmp)
	//	{}
	//	listIterator(const listIterator& tmp)
	//		:_node(tmp._node)//这里只是浅拷贝就可以了，因为都指向那个链表就可以了
	//	{}
	//	//这个类就是迭代器，现在我们开始实现对应迭代器的运算符重载
	//	listIterator& operator++()//前置加加
	//	{
	//		_node = _node->_next;
	//		return (*this);
	//	}
	//	listIterator operator++(int)//后置加加
	//	{
	//		listIterator tmp(*this);//这里要有拷贝构造
	//		_node = _node->_next;
	//		return tmp;
	//	}
	//	listIterator& operator--()//前置减减
	//	{
	//		_node = _node->_prev;
	//		return (*this);
	//	}
	//	listIterator operator--(int)//后置减减
	//	{
	//		listIterator tmp(*this);//这里要有拷贝构造
	//		_node = _node->_prev;
	//		return tmp;
	//	}
	//	//链表的迭代器不支持+和-一个数，因为这样的效率不高，库里面的也是不支持的
	//	bool operator!=(const listIterator& tmp)
	//	{
	//		return _node != tmp._node;
	//	}
	//	bool operator==(const listIterator& tmp)
	//	{
	//		return _node == tmp._node;
	//	}
	//	T& operator*()//为了能够改变值，所以传&
	//	{
	//		return _node->_val;
	//	}
	//	T* operator->()//有些时候T是一个类，要访问里面的成员变量，就要->。。待会用例子说明
	//	{
	//		return &_node->_val;
	//	}
	//	listIterator operator=(const listIterator& tmp)
	//	{
	//		_node = tmp._node;
	//		return *this;
	//	}
	//	Node* _node;
	//};
	
	//template<class T>
	//struct constlistIterator//因为这些都是分装，用户不会访问到这个类，只会在list的类访问，所以要对list的成员变量private
	//{
	//	//这里可以不用保护，而且到了后面还会访问
	//	typedef ListNode<T> Node;
	//	//再来个普通构造
	//	constlistIterator(Node* tmp)
	//		:_node(tmp)
	//	{}
	//	constlistIterator(const constlistIterator& tmp)
	//		:_node(tmp._node)//这里只是浅拷贝就可以了，因为都指向那个链表就可以了
	//	{}
	//	//这个类就是迭代器，现在我们开始实现对应迭代器的运算符重载
	//	constlistIterator& operator++()//前置加加
	//	{
	//		_node = _node->_next;
	//		return (*this);
	//	}
	//	constlistIterator operator++(int)//后置加加
	//	{
	//		listIterator tmp(*this);//这里要有拷贝构造
	//		_node = _node->_next;
	//		return tmp;
	//	}
	//	constlistIterator& operator--()//前置减减
	//	{
	//		_node = _node->_prev;
	//		return (*this);
	//	}
	//	constlistIterator operator--(int)//后置减减
	//	{
	//		listIterator tmp(*this);//这里要有拷贝构造
	//		_node = _node->_prev;
	//		return tmp;
	//	}
	//	//链表的迭代器不支持+和-一个数，因为这样的效率不高，库里面的也是不支持的
	//	bool operator!=(const constlistIterator& tmp)
	//	{
	//		return _node != tmp._node;
	//	}
	//	bool operator==(const constlistIterator& tmp)
	//	{
	//		return _node == tmp._node;
	//	}
	//	const T& operator*()
	//	{
	//		return _node->_val;
	//	}
	//	const T* operator->()//要实现const的迭代器，就只需要在*和->的前面加上const，就表示他们指向的val不可修改了
	//	{
	//		return &_node->_val;
	//	}
	//	constlistIterator operator=(const constlistIterator& tmp)
	//	{
	//		_node = tmp._node;
	//		return *this;
	//	}
	//	Node* _node;
	//};

	template<class T,class a1,class a2>
	struct listIterator//因为这些都是分装，用户不会访问到这个类，只会在list的类访问，所以要对list的成员变量private
	{
		//这里可以不用保护，而且到了后面还会访问
		typedef ListNode<T> Node;

		//再来个普通构造
		listIterator(Node* tmp)
			:_node(tmp)
		{}

		listIterator(const listIterator& tmp)
			:_node(tmp._node)//这里只是浅拷贝就可以了，因为都指向那个链表就可以了
		{}

		//这个类就是迭代器，现在我们开始实现对应迭代器的运算符重载
		listIterator& operator++()//前置加加
		{
			_node = _node->_next;
			return (*this);
		}

		listIterator operator++(int)//后置加加
		{
			listIterator tmp(*this);//这里要有拷贝构造
			_node = _node->_next;
			return tmp;
		}

		listIterator& operator--()//前置减减
		{
			_node = _node->_prev;
			return (*this);
		}

		listIterator operator--(int)//后置减减
		{
			listIterator tmp(*this);//这里要有拷贝构造
			_node = _node->_prev;
			return tmp;
		}

		//链表的迭代器不支持+和-一个数，因为这样的效率不高，库里面的也是不支持的

		bool operator!=(const listIterator& tmp)
		{
			return _node != tmp._node;
		}

		bool operator==(const listIterator& tmp)
		{
			return _node == tmp._node;
		}

		a1& operator*()//为了能够改变值，所以传&
		{
			return _node->_val;
		}

		a2* operator->()//有些时候T是一个类，要访问里面的成员变量，就要->。。待会用例子说明
		{
			return &_node->_val;
		}

		listIterator operator=(const listIterator& tmp)
		{
			_node = tmp._node;
			return *this;
		}
		Node* _node;
	};

	template<class T, class a1, class a2>
	struct reverselistIterator//因为这些都是分装，用户不会访问到这个类，只会在list的类访问，所以要对list的成员变量private
	{
		//这里可以不用保护，而且到了后面还会访问
		typedef ListNode<T> Node;

		//再来个普通构造
		reverselistIterator(Node* tmp)
			:_node(tmp)
		{}

		reverselistIterator(const reverselistIterator& tmp)
			:_node(tmp._node)//这里只是浅拷贝就可以了，因为都指向那个链表就可以了
		{}

		//这个类就是迭代器，现在我们开始实现对应迭代器的运算符重载
		reverselistIterator& operator++()//前置加加
		{
			_node = _node->_prev;
			return (*this);
		}

		reverselistIterator operator++(int)//后置加加
		{
			reverselistIterator tmp(*this);//这里要有拷贝构造
			_node = _node->_prev;
			return tmp;
		}

		reverselistIterator& operator--()//前置减减
		{
			_node = _node->_next;
			return (*this);
		}

		reverselistIterator operator--(int)//后置减减
		{
			reverselistIterator tmp(*this);//这里要有拷贝构造
			_node = _node->_next;
			return tmp;
		}

		//链表的迭代器不支持+和-一个数，因为这样的效率不高，库里面的也是不支持的

		bool operator!=(const reverselistIterator& tmp)
		{
			return _node != tmp._node;
		}

		bool operator==(const reverselistIterator& tmp)
		{
			return _node == tmp._node;
		}

		a1& operator*()//为了能够改变值，所以传&
		{
			return _node->_val;//
		}

		a2* operator->()//有些时候T是一个类，要访问里面的成员变量，就要->。。待会用例子说明
		{
			return &_node->_val;//
		}

		reverselistIterator operator=(const reverselistIterator& tmp)
		{
			_node = tmp._node;
			return *this;
		}
		Node* _node;
	};

	template<class T>
	class list
	{
	public:
		typedef ListNode<T> Node;

		//typedef listIterator<T> iterator;//把这个我们分装的迭代器统一命名为我们库里面的那个名字
		////实现const迭代器，一个方法就是，在实现一个类，这个类是针对const list的
		//typedef constlistIterator<T> const_iterator;//const_iterator这个类型就是针对const的
		////typedef listIterator<const T> const_iterator;//但是不要写这个，因为这样的话，里外的T就不是同一个T,这样就会很混乱

		//除了我们显示实现两个类的这种方法，还有一个方法就是，我们只实现一个的方法，定义多个变量的名字
		typedef listIterator<T,T,T> iterator;
		typedef listIterator<T,const T,const T> const_iterator;

		typedef reverselistIterator<T, T, T> Reverse_iterator;
		typedef reverselistIterator<T, const T, const T> const_Reverse_iterator;

		Reverse_iterator rbegin()
		{
			//return _head->_next;//这只是个指针//当然这样也可以，隐式类型转换
			return Reverse_iterator(_head->_prev);
		}

		const_Reverse_iterator rbegin()const//因为list是const，所以this也是const
		{
			//return _head->_next;//这只是个指针//当然这样也可以，隐式类型转换
			return const_Reverse_iterator(_head->_prev);
		}

		Reverse_iterator rend()
		{
			return Reverse_iterator(_head);//因为是最后一个位置的下一个嘛
		}

		const_Reverse_iterator rend()const
		{
			return const_Reverse_iterator(_head);//因为是最后一个位置的下一个嘛
		}


		//开始实现迭代器的begin和end
		iterator begin()
		{
			//return _head->_next;//这只是个指针//当然这样也可以，隐式类型转换
			return iterator(_head->_next);
		}

		const_iterator begin()const//因为list是const，所以this也是const
		{
			//return _head->_next;//这只是个指针//当然这样也可以，隐式类型转换
			return const_iterator(_head->_next);
		}

		iterator end()
		{
			return iterator(_head);//因为是最后一个位置的下一个嘛
		}

		const_iterator end()const
		{
			return const_iterator(_head);//因为是最后一个位置的下一个嘛
		}

		void buynode()
		{
			//直接创建一个头结点
			_head = new Node;//调用node的默认构造
			_head->_prev = _head;
			_head->_next = _head;
		}

		//先写一个list的默认构造函数
		list()
		{
			buynode();//this指针传进去，是相互联通的
		}

		list(int n, const T& value = T())//会优先匹配int int，因为有一个int了嘛,,就是为了防止两个int去调用list(elseIterator first, elseIterator last)了
		{
			buynode();//这样就有头了
			for (int i = 0; i < n; i++)
			{
				push_back(value);
			}
		}

		list(list<T>& l)//没写拷贝构造就是浅拷贝，就会出问题//因为会析构同一个地方两次
		{
			buynode();
			iterator it = l.begin();
			while (it != l.end())
			{
				push_back(*it);
				it++;
			}
		}

		template <class elseIterator>
		list(elseIterator first, elseIterator last)
		{
			buynode();
			elseIterator it = first;
			while (it != last)
			{
				push_back(*it);
				it++;
			}
		}

		list<T>& operator=(list<T> l)//l是拷贝构造而来的，临时对象，没什么用
		{
			std::swap(_head, l._head);
			return (*this);
		}

		list(initializer_list<T> tmp)//initializer_list也是一个模版，有自己的begin那些东西
			//数组类型就是initializer_list类型的
		{
			buynode();
			auto it = tmp.begin();
			while (it != tmp.end())
			{
				push_back(*it);
				it++;
			}
		}

		size_t size()const
		{
			auto it = begin();
			size_t count = 0;
			while (it != end())
			{
				count++;
				it++;
			}
			return count;
		}

		bool empty()const//普通对象可以匹配const，但是const只能匹配const
		{
			return begin() == end();
		}

		//void push_back(const T& t)
		//{
		//	Node* NewNode = new Node(t);//用T去初始化node,那么Node应该具有构造函数，node就是listnode，就是listnode具有构造函数
		//	Node* tail = _head->_prev;
		//	NewNode->_prev = tail;
		//	NewNode->_next = _head;
		//	tail->_next = NewNode;
		//	_head->_prev = NewNode;
		//}

		T& front()
		{
			return *begin();
		}

		const T& front()const
		{
			return *begin();
		}

		T& back()
		{
			return *(--end());
		}

		const T& back()const
		{
			return *(--end());
		}

		// 在pos位置前插入值为val的节点
		iterator insert(iterator pos, const T& val)
		{
			Node* NewNode = new Node(val);
			Node* prev = pos._node->_prev;//这就是为什么要给node设置struct，还有迭代器也设置struct
			Node* next = pos._node;//next就是他本身
			NewNode->_prev = prev;
			NewNode->_next = next;
			next->_prev = NewNode;
			prev->_next = NewNode;
			return iterator(NewNode);//返回插入位置的迭代器，然后这个迭代器不会失效，但还是返回吧
		}

		void push_back(const T& t)
		{
			insert(end(), t);
		}

		/*template<class T>
		void push_back(T&& t)
		{
			insert(end(), forward<T>(t));
		}*/

		void push_back(T&& t)
		{
			insert(end(), move(t));
		}

		iterator insert(iterator pos, T&& val)
		{
			Node* NewNode = new Node(move(val));
			Node* prev = pos._node->_prev;//这就是为什么要给node设置struct，还有迭代器也设置struct
			Node* next = pos._node;//next就是他本身
			NewNode->_prev = prev;
			NewNode->_next = next;
			next->_prev = NewNode;
			prev->_next = NewNode;
			return iterator(NewNode);//返回插入位置的迭代器，然后这个迭代器不会失效，但还是返回吧
		}

		// 删除pos位置的节点，返回该节点的下一个位置
		iterator erase(iterator pos)
		{
			assert(!empty());
			Node* prev = pos._node->_prev;
			Node* next = pos._node->_next;
			prev->_next = next;
			next->_prev = prev;
			delete pos._node;
			return iterator(next);//返回下一个的迭代器
		}

		void pop_back()
		{ erase(--end()); }
		void push_front(const T& val) 
		{ insert(begin(), val); }
		void pop_front()
		{ erase(begin()); }

		void clear()
		{
			auto it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}

		~list()//析构函数和clear不同，clear是清除所有的节点，只剩头结点，析构函数是还要干掉头结点
		{
			cout << "~list()" << endl;
			clear();
			delete _head;
			_head = nullptr;
		}

		template<class...Args>
		iterator insert(iterator pos, Args&&...args)
		{

			Node* NewNode = new Node(forward<Args>(args)...);
			Node* prev = pos._node->_prev;
			Node* next = pos._node;
			NewNode->_prev = prev;
			NewNode->_next = next;
			next->_prev = NewNode;
			prev->_next = NewNode;
			return iterator(NewNode);
		}

		template<class...Args>
		void emplace_back(Args&&...args)
		{
			insert(end(), forward<Args>(args)...);
		}
	private:
		Node* _head;
	};

	//下面我们来实现一个打印函数
	template<class T>
	void print(const list<T> tmp)//这个是只访问不修改list，所以用const
		//但是有const和没有const是两个完全不同的类型，所以还得实现一个const list的迭代器
	{
		/*list<int>::iterator it = a.begin();
		while (it != a.end())
		{
			cout << *it << ' ';
			++it;
		}
		cout << endl;*/
		//tmp.push_back(1);这里写了const，就已经表示不能插入删除了，val也不能改变，但是还差迭代器，因为迭代器实现的*，->那些是可以修改值的
		//不是const类型的,还有一个原因就是没有写针对const的push_back,怎么可能const还push_back
		list<int>::const_iterator it = tmp.begin();
		while (it != tmp.end())
		{
			cout << *it << ' ';
			++it;
		}
		cout << endl;
	}

	void test1()
	{
		//list<int> a;
		//a.push_back(1);
		//a.push_back(2);
		//a.push_back(3);
		//a.push_back(4);
		////接下来实现迭代器
		////这里的迭代器就不能是个指针了，比如node*，对node*++或者*得到的并不是我们想要的数据
		////这里的迭代器是一个类，主要成员变量包含node*就可以了，在对这个类进行运算符重载之类的
		//list<int>::iterator it = a.begin();
		//while (it != a.end())
		//{
		//	cout << *it << ' ';
		//	++it;
		//}
		//cout << endl;
		//struct A
		//{
		//	int left;
		//	int right;
		//};
		//list<A> a;
		//a.push_back({ 1,2 });//因为可以这样赋值嘛，struct A tmp={1,2};
		//a.push_back({ 3,4 });
		//a.push_back({ 5,6 });
		//a.push_back({ 7,8 });
		//list<A>::iterator it = a.begin();
		//while (it != a.end())
		//{
		//	//cout << *it << ' ';//如果是这样就不行了，因为你解引用的不知道是什么东西啊，所以要用->,不像以前解引用就是int,因为-》得到的是地址啊
		//	cout << it->left << ':' << it->right << endl;
		//	//其实是这样的，
		//	cout << it.operator->()->left << ':' << it.operator->()->right << endl;//这个是显示调用
		//	//就会变成这样
		//	//cout << it->->left << ':' << it->->right << endl;//但是为了代码的可读性，两个->就优化成了一个，写两个还是错的
		//	++it;
		//}
		//cout << endl;
		//list<int> a;
		//a.push_back(1);
		//a.push_back(2);
		//a.push_back(3);
		//a.push_back(4);

		//int a = 0;
		//int b = 3;
		//std::swap(a, b);
		//cout << a;

		//list<int> a(3, 5);
		//list<int> b(5,10);
		//a = b;
		//cout << a.size() << endl;
		//cout << a.empty() << endl;
		list<int> a = {1,2,3,4,6,7,8};
		//a.push_back(1);
		//a.push_back(2);
		//a.push_back(3);
		//a.push_back(4);
		//a.push_back(5);
		//cout << a.back() << endl;
		//cout << a.front() << endl;
		//a.insert(a.begin(), 0);
		//a.erase(a.begin());
		//print(a);
		//a.erase(a.begin());
		//print(a);
		//a.erase(a.begin());
		//print(a);
		//a.erase(a.begin());
		//print(a);
		//a.erase(a.begin());
		//a.clear();
		list<int>::Reverse_iterator it = a.rbegin();
		while (it != a.rend())
		{
			cout << *it << ' ';
			it++;
		}
		cout << endl;
		print(a);
	}
}