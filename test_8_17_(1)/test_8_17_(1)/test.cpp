#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<list>
#include<vector>
#include<map>
#include<string>
#include<assert.h>
using namespace std;

struct Point
{
	int _x;
	int _y;
};

class Date
{
public:
	Date(int year, int month, int day)
		:_year(year)
		, _month(month)
		, _day(day)
	{
		cout << "Date(int year, int month, int day)" << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

//int main()
//{
//	int arr[] = { 1,1,1,1,1 };
//	int a = { 2 };
//	int b { 2 };
//	Point p1 = { 1,2 };
//	Point p2  { 1,2 };
//	int* pa = new int[4] { 0 };
//	Date a(1, 1, 1);
//	Date b = { 2,2,2 };
//
//	list<int> l = { 1,2,3,4,5,6,7,8 };
//	map<string, string> m = { {"left","左"},{"right","右"},{"up","上"}};
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	decltype(a) d = 9;
//	decltype(&a) p =&a;
//	decltype(a + d) c = 10;
//	cout << d << endl;
//	return 0;
//}


//int main()
//{
//	//int a = 0;
//	//const int b = 0;
//	//10;
//	//a + b;
//	//string("111");
//
//	//int&& x1 = 10;
//	//int&& x2= a + b;
//	//string&& x3= string("111");
//	//x3 += "qq";
//	//cout << x3 << endl;
//
//	//int m = 0;
//	//int& pm = m;
//
//	/*int a = 0;
//	int b = 0;
//	10;
//	a + b;
//	string("111");*/
//
//	/*const int& p1 = 10;
//	const int& p2 = a+b;
//	const string& p2 = string("111");
//
//	p1 = 2;*/
//
//	int a = 1;
//	/*int&& p = move(a);*/
//	int&& p = (int&&)a;
//	p = 10;
//	cout << p << endl;
//
//	return 0;
//}

namespace bit
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		const_iterator begin()const
		{
			return _str;
		}

		const_iterator end()const
		{
			return _str + _size;
		}

		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
		{
			cout << "string(char* str)普通构造" << endl;
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		string(int num,char c)
		{
			cout << "string(char* str)普通构造" << endl;
			reserve(num);
			int i = 0;
			for (; i < num; i++)
			{
				_str[i] = c;
			}
			_str[i] = '\0';
		}

		// s1.swap(s2)
		void swap(string& s)
		{
			::swap(_str, s._str);
			::swap(_size, s._size);
			::swap(_capacity, s._capacity);
		}

		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}

		// 拷贝构造
		string(const string& s)
		{
			cout << "string(const string& s) -- 拷贝构造" << endl;
			reserve(s._size);
			for (auto x : s)
			{
				push_back(x);
			}
		}

		// 赋值重载
		string& operator=(const string& s)
		{
			cout << "string& operator=(string s) -- 赋值拷贝" << endl;
			reserve(s._size);
			for (auto x : s)
			{
				push_back(x);
			}
			return *this;
		}

		// 移动构造
		string(string&& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			cout << "string(string&& s) -- 移动构造" << endl;
			swap(s);
		}

		// 移动赋值
		string& operator=(string&& s)
		{
			cout << "string& operator=(string&& s) -- 移动赋值" << endl;
			swap(s);
			return *this;
		}

		~string()
		{
			delete[] _str;
			_str = nullptr;
		}

		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				if (_str)
				{
					strcpy(tmp, _str);
					delete[] _str;
				}
				_str = tmp;
				_capacity = n;
			}
		}

		void push_back(char ch)
		{
			if (_size >= _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}
			if (_str)
			{
				_str[_size] = ch;
				++_size;
				_str[_size] = '\0';
			}
		}

		//string operator+=(char ch)
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		const char* c_str() const
		{
			return _str;
		}
	private:
		char* _str=nullptr;
		size_t _size=0;
		size_t _capacity=0; // 不包含最后做标识的\0
	};
}

//bit::string func()
//{
//	bit::string s("123456789");
//	return s;
//}

//int main()
//{
//	/*bit::string ret;
//	ret = func();*/
//	//bit::string s;
//	//s.push_back("111111");
//
//	list<bit::string> l;
//	//l.push_back("11111111");
//	bit::string&& s = "111";
//	return 0;
//}

#include"list.h"

//int main()
//{
//	bit::list<bit::string> l;
//	l.push_back("1111");
//	return 0;
//}

//void func(int &a)
//{
//	cout << "void fund(int &a)" << endl;
//}
//
//void func(int&& a)
//{
//	cout << "void fund(int &&a)" << endl;
//}
//
//template<class T>
//void PerfectForward(T&&t)
//{
//	func(forward<T>(t));
//}
//
//int main()
//{
//	PerfectForward(1);
//	int a = 0;
//	PerfectForward(a);
//	return 0;
//}

//class Person
//{
//public:
//	Person(const char* name = "11111111", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//
//	Person(Person&& s) = delete;
//
//	~Person()
//	{}
//
//private:
//	Person(Person& s);
//	bit::string _name;
//	int _age;
//};
//
//int main()
//{
//	Person s1;
//	Person s2 = s1;
//	Person s3 = move(s1);
//	return 0;
//}

//template<class T, class...Args>
//void print(T t,Args...args)
//{
//	cout << t << " ";
//	if (sizeof...(args) == 0)
//	{
//		return;
//	}
//	print(args);
//}


//void print()
//{
//	cout << endl;
//}
//
//template<class T, class...Args>
//void print(T t, Args...args)
//{
//	cout << t << " ";
//	print(args...);
//}
//
//template<class...Args>
//void ShowList(Args...args)
//{
//	print(args...);
//}

//
//
//template<class T>
//void print(T t)
//{
//	cout << t << " ";
//}
//
//template<class...Args>
//void ShowList(Args...args)
//{
//	int arr[] = {(print(args),0)...};
//	//int sz = sizeof(arr) / sizeof(arr[0]);
//	//cout << sz<<endl;
//	cout << endl;
//}
//
//int arr[] = { (print("222"),0),(print(1.2),0),(print(34),0) };
//
//int main()
//{
//	//ShowList();
//	ShowList(1,1,1,1);
//	ShowList("222", 1.2, 34);
//	return 0;
//}

#include<list>
//
//int main()
//{
//	/*list<bit::string> l;
//	bit::string s1("11111");
//	l.emplace_back(s1);
//	l.emplace_back(move(s1));*/
//
//	/*list<bit::string> l;
//	l.emplace_back("11111");*/
//
//	/*list<pair<bit::string,int>> l;
//	pair<bit::string, int > s("2222", 2);
//	l.emplace_back(s);
//	l.emplace_back(make_pair("1111", 1));
//	l.emplace_back("3333", 3);*/
//
//	//list<bit::string> l;
//	//l.emplace_back(3, 'k');
//
//	bit::list<bit::string> l;
//	bit::string s1("11111");
//	l.emplace_back(s1);
//	l.emplace_back(move(s1));
//	l.emplace_back(3, 'k');
// 	return 0;
//}

#include<vector>
#include<algorithm>

struct Goods
{
	Goods(const char* str, double price, int evaluate)
		:_name(str)
		,_price(price)
		, _evaluate(evaluate)
	{}
	string _name; //名字
	double _price; // 价格
	int _evaluate; // 评价
};

struct CmpPriceLess
{
	bool operator()(Goods g1, Goods g2)
	{
		return g1._price < g2._price;
	}
};

//int main()
//{
//	/*vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,3 }, { "菠萝", 1.5, 4 } };
//	sort(v.begin(), v.end(), CmpPriceLess());*/
//	/*auto func1 = [](int a, int b)->int {
//		return a + b;
//	};
//	cout << func1(1, 34) << endl;*/
//	/*auto func2 = [] {
//		cout << "hahahhha" << endl;
//		return 0;
//		};
//	cout<<func2()<<endl;*/
//
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,3 }, { "菠萝", 1.5, 4 } };
//	sort(v.begin(), v.end(), [](Goods g1, Goods g2)->bool
//		{
//			return g1._price < g2._price;
//		});
//	return 0;
//}

//
//int c = 0;
//int main()
//{
//	/*int a = 10;
//	int b = 8;
//	auto swap1 = [&a, &b]()mutable {
//		int tmp = a;
//		a = b;
//		b = tmp;
//		cout << a << " " << b << endl;
//		};
//	swap1();
//	cout << a << " " << b << endl;*/
//
//	/*auto swap1 = [](int a, int b)mutable {
//		int tmp = a;
//		a = b;
//		b = tmp;
//		cout << a << " " << b << endl;
//		};
//	swap1(a,b);*/
//
//	//int a = 10;
//	//int b = 8;
//	//int c = 0;
//	//auto swap1 = [&]()mutable {
//	//	int tmp = a;
//	//	a = b;
//	//	b = tmp;
//	//	c = 0;
//	//	cout << a << " " << b << endl;
//	//	};
//	//swap1();
//	//cout << a << " " << b << endl;
//
//	/*int a = 10;
//	int b = 8;
//	int c = 0;
//	auto swap1 = [&,&c]() {
//		int tmp = a;
//		a = b;
//		b = tmp;
//		c = 0;
//		cout << a << " " << b << endl;
//		};*/
//
//	auto func1 = [](int a, int b)->int {
//		return a + b;
//	};
//
//	auto func2 = [](int a, int b)->int {
//		return a + b;
//		};
//	return 0;
//}


#include<functional>
using namespace std;
int f(int a, int b)
{
	return a + b;
}

struct Functor
{
public:
	int operator() (int a, int b)
	{
		return a + b;
	}
};

//int main()
//{
//	function<int(int, int)> t1 = f;
//	function<int(int, int)> t2 = Functor();
//	function<int(int, int)> t3 = [](int a, int b)->int {return a + b; };
//	cout << t1(1, 2) << endl;
//	cout << t2(1, 2) << endl;
//	cout << t3(1, 2) << endl;
//	return 0;
//}

//class Plus
//{
//public:
//	static int plusi(int a, int b)
//	{
//		return a + b;
//	}
//
//	double plusd(double a, double b)
//	{
//		return a + b;
//	}
//};
//
//int main()
//{
//	function<int(int, int)> f1 = Plus::plusi;
//	cout << f1(1, 2) << endl;
//
//	function<double(Plus, double, double)> f2 = &Plus::plusd;
//	Plus p;
//	cout << f2(p,2, 3) << endl;
//	return 0;
//}


using namespace placeholders;

//using placeholders::_1;
//using placeholders::_2;
//using placeholders::_3;
//
//int Plus(int a, int b)
//{
//	return a - b;
//}
//
//int SubX(int a, int b, int c)
//{
//	return (a - b - c) * 10;
//}
//
//
//int main()
//{
//	/*auto f1 = bind(Plus, 100, _1);
//	cout << f1(10) << endl;*/
//
//	auto f1 = bind(SubX,_1, 100, _2);
//	cout << f1(10,2) << endl;
//	return 0;
//}


class Plus
{
public:
	static int plusi(int a, int b)
	{
		return a + b;
	}

	double plusd(double a, double b)
	{
		return a + b;
	}
};

auto add()
{
	return 0;
}

#include<stack>
using namespace std;

class A { public:
	A() 
{}
A(int) = delete; };

int main()
{
	function<double(double, double)>  f1 = bind(&Plus::plusd, Plus(), _1, _2);
	cout << f1(1.1, 1.1) << endl;

	A a(1);

	/*stack<int> s;
	s.push(1);
	s.push(3);
	s.push(6);
	s.push(9);
	s.push(2);
	s.push(4);
	for (auto x : s)
	{
		cout << x << endl;
	}
	cout << add() << endl;*/
	return 0;
}