#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode
{
	int weight;
	int parent;
	int left;
	int right;
}TreeNode;

//这里哈夫曼树的结构是一个数组，数组每个元素含有父亲左右孩子权值
typedef struct HFTree
{
	struct TreeNode* a;
	int length;
}HFTree;

//初始化哈夫曼树
HFTree* InitHFTree(int*arr,int sz)//权值数组和它的数组长度
{
	//哈夫曼树的数组长度是这个数组长度的2倍-1
	HFTree* hf = (HFTree*)malloc(sizeof(HFTree));//直接假设不会创建失误
	hf->a = (TreeNode*)malloc(sizeof(TreeNode) * (2 * sz - 1));
	hf->length = sz;//这个长度记录的是实际上的哈夫曼树的长度，合并出一个父节点的话就++
	//初始化哈夫曼树组
	for (int i = 0; i < 2*hf->length-1; i++)
	{
		hf->a[i].weight = arr[i];
		hf->a[i].left = -1;
		hf->a[i].right = -1;//因为孩子父亲都不存在
		hf->a[i].parent = -1;
	}
	return hf;
}

//选出哈夫曼树中没有父亲的节点中的最小的两个的下标
int* SelectMin(HFTree* pf)
{
	int min=INT_MAX;
	int second_min= INT_MAX;
	int MIN;//这个是下标
	int SECOND_MIN;
	for (int i = 0; i < pf->length; i++)//先选出最小值
	{
		if (pf->a[i].parent == -1 && pf->a[i].weight < min)
		{
			min = pf->a[i].weight;
			MIN = i;
		}
	}
	for (int i = 0; i < pf->length; i++)//先选出第二小值
	{
		if (pf->a[i].parent == -1 && pf->a[i].weight < second_min &&i!=MIN)
		{
			second_min = pf->a[i].weight;
			SECOND_MIN = i;
		}
	}
	int* ret = (int*)malloc(sizeof(int) * 2);//数组存放下标并返回
	ret[0] = MIN;
	ret[1] = SECOND_MIN;
	return ret;
}

//直接创建哈夫曼树
void CreatHFTree(HFTree* hf)
{
	//先定义变量
	int* ret;
	int length=hf->length;
	for (int i = hf->length; i < (2 *length - 1); i++)//因为哈夫曼树的创建就是从这里开始的//不能用这个i < (2 * hf->length - 1)，因为有变量
	{
		//先选出那两个最小的
		ret = SelectMin(hf);
		hf->a[i].weight = hf->a[ret[0]].weight + hf->a[ret[1]].weight;//权值为细小的两个相加
		hf->a[i].left = ret[0];
		hf->a[i].right = ret[1];
		hf->a[ret[0]].parent = i;
		hf->a[ret[1]].parent = i;
		//注意这时哈夫曼树的长度要增加了
		hf->length++;
	}
}

void PreOrder(HFTree* hf ,int i)//先序遍历，因为这个树不是链式树，所以要精准访问的话就要传入下标
{
	if (i == -1)
	{
		return;
	}
	printf("%d ", hf->a[i].weight);
	PreOrder(hf, hf->a[i].left);
	PreOrder(hf, hf->a[i].right);
}

int main()
{
	//传入一个权值数组，然后实现这个哈夫曼树
	int arr[] = { 1,2,3,4 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	HFTree* hf = InitHFTree(arr, sz);
	CreatHFTree(hf);
	PreOrder(hf,hf->length-1);//因为这个点就是根
	return 0;
}