#define _CRT_SECURE_NO_WARNINGS 1
#include"BinaryTree.h"
#include"Queue.h"

// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树
BTNode* BinaryTreeCreate(BTDataType* a, int n, int* pi)
{
	BTNode* A = (BTNode*)malloc(sizeof(BTNode));BTNode* B = (BTNode*)malloc(sizeof(BTNode));BTNode* C = (BTNode*)malloc(sizeof(BTNode));
	BTNode* D = (BTNode*)malloc(sizeof(BTNode));BTNode* E = (BTNode*)malloc(sizeof(BTNode));BTNode* F = (BTNode*)malloc(sizeof(BTNode));
	BTNode* G = (BTNode*)malloc(sizeof(BTNode));BTNode* H = (BTNode*)malloc(sizeof(BTNode));

	A->data = a[0];B->data = a[1];C->data = a[2];D->data = a[3];E->data = a[4];F->data = a[5];G->data = a[6];H->data = a[7];

	A->left = B; B->left = D; B->right = E; D->left = NULL; D->right = NULL;
	E->left = NULL;E->right = H;H->left = H->right = NULL;
	A->right = C;C->left = F;C->right = G;F->left = F->right = NULL;G->left = G->right = NULL;
	*pi = n;
	return A;
}

// 二叉树销毁
void BinaryTreeDestory(BTNode** root)
{
	assert(root);
	BTNode* node = *root;
	if (node == NULL)
	{
		return;
	}
	BinaryTreeDestory(&node->left);
	BinaryTreeDestory(&node->right);
	free(node);
	//printf("free一次");
	node = NULL;
}

// 二叉树节点个数
int BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	return 1+BinaryTreeSize(root->left) + BinaryTreeSize(root->right);
}

// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}
	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}

// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	//求第k层的话，递归的话，就是求子树第k-1层的节点个数，如果是第k层的，就返回1
	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}
	return BinaryTreeLevelKSize(root->left, k - 1) + BinaryTreeLevelKSize(root->right, k - 1);
}

// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	//找节点为x的话，递归的话，就相当于返回子树不为NULL的某个返回值
	if (root == NULL)
	{
		return NULL;
	}
	if (root->data == x)
	{
		return root;
	}
	//先存下左右子树的返回值，不然会递归多次
	BTNode* root_left = BinaryTreeFind(root->left, x);
	BTNode* root_right = BinaryTreeFind(root->right, x);
	if (root_left == NULL)//说明左子树没有x的节点
	{
		return root_right;
	}
	else//两个都没有x的节点。就随便返回一个吧
	{
		return root_left;
	}
}

// 二叉树前序遍历 
void BinaryTreePrevOrder(BTNode* root)
{
	//先访问根，就是先把根的东西写出来，最后再递归
	if (root == NULL)
	{
		printf("N ");
		return;
	}
	printf("%c ", root->data);
	BinaryTreePrevOrder(root->left);
	BinaryTreePrevOrder(root->right);
}

// 二叉树中序遍历
void BinaryTreeInOrder(BTNode* root)
{
	//中间访问根，就是再中间把根的东西写出来，递归放两边
	if (root == NULL)
	{
		printf("N ");
		return;
	}
	BinaryTreeInOrder(root->left);
	printf("%c ", root->data);
	BinaryTreeInOrder(root->right);
}

// 二叉树后序遍历
void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("N ");
		return;
	}
	BinaryTreePostOrder(root->left);
	BinaryTreePostOrder(root->right);
	printf("%c ", root->data);
}

// 层序遍历
void BinaryTreeLevelOrder(BTNode* root)
{
	//先把根入队列
	//然后出一个队列就入其孩子队列
	Queue q;
	QueueInit(&q);
	if (root == NULL)
	{
		return;
	}
	QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		//出一个
		BTNode* ret = QueueFront(&q);
		QueuePop(&q);
		printf("%c ", ret->data);
		//如果孩子不为空就入队
		if (ret->left)
		{
			QueuePush(&q, ret->left);
		}
		if (ret->right)
		{
			QueuePush(&q, ret->right);
		}
	}
	QueueDestroy(&q);
}

// 判断二叉树是否是完全二叉树
bool BinaryTreeComplete(BTNode* root)
{
	//这个都不能用递归了了
	//这个也是和层序遍历是一样的，但不同的是，孩子为空指针也会入队，当出队的为空指针时，队列中还有
	//非空指针，那就不是，如果后面全是空指针，那就是完全二叉树
	Queue q;
	QueueInit(&q);
	if (root == NULL)
		return true;
	QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		//出一个
		BTNode* ret = QueueFront(&q);
		QueuePop(&q);
		if (ret == NULL)
		{
			break;
		}
		QueuePush(&q, ret->left);
		QueuePush(&q, ret->right);
	}
	while (!QueueEmpty(&q))
	{
		//出一个
		BTNode* ret = QueueFront(&q);
		QueuePop(&q);
		if (ret != NULL)
		{
			QueueDestroy(&q);
			return false;
		}
	}
	QueueDestroy(&q);
	return true;
}