#define _CRT_SECURE_NO_WARNINGS 1
#include"BinaryTree.h"
int main()
{
	BTNode* tree = NULL;
	BTDataType a[8] = "ABCDEFGH";
	int pi=0;
	tree = BinaryTreeCreate(a, 8, &pi);

	int ret=BinaryTreeSize(tree);
	printf("树的节点个数为：%d\n", ret);

	ret = BinaryTreeLeafSize(tree);
	printf("树的叶子节点个数为：%d\n", ret);

	int k = 3;
	ret = BinaryTreeLevelKSize(tree,k);
	printf("树的第%d层节点个数为：%d\n",k, ret);

	BTNode*r=BinaryTreeFind(tree, 'p');
	if (r != NULL)
	{
		printf("找到了找到%c了\n", r->data);
	}
	else
	{
		printf("没有找到\n");
	}

	printf("先序遍历：");
	BinaryTreePrevOrder(tree);
	printf("\n");

	printf("中序遍历：");
	BinaryTreeInOrder(tree);
	printf("\n");

	printf("后序遍历：");
	BinaryTreePostOrder(tree);
	printf("\n");

	printf("层序遍历：");
	BinaryTreeLevelOrder(tree);

	if (BinaryTreeComplete(tree))
	{
		printf("\n是完全二叉树\n");
	}
	else
	{
		printf("\n不是完全二叉树\n");
	}

	BinaryTreeDestory(&tree);

	return 0;
}