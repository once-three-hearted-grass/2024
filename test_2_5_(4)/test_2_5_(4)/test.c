#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//1  2  3  4  5  6  7  8  9  10  11  12
//31 ?  31 30 31 30 31 31 30 31  30  31
//
int is_run(int year, int month)
{
    switch (month)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;
    case 4:
    case 6:
    case 11:
    case 9:
        return 30;
    case 2:
        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
            return 29;
        else
            return 28;
    default:
        return 0;
    }

}

int main() {
    int a, b;
    printf("请输入年份与月份(年 月):");
    while (scanf("%d %d", &a, &b) != EOF) {
        printf("%d年%d月总共有%d天\n",a,b, is_run(a, b));
    }
    return 0;
}