#define _CRT_SECURE_NO_WARNINGS 1

//实现一种算法，找出单向链表中倒数第 k 个节点。返回该节点的值。
//示例：
//输入： 1->2->3->4->5 和 k = 2
//输出： 4
#include<stdio.h>


struct ListNode {
	int val;
	struct ListNode* next;
};
typedef struct ListNode ListNode;
int kthToLast(struct ListNode* head, int k) {
	ListNode* slow = head;
	ListNode* fast = head;
	//先让fast走k步//k是有效的
	while (k--)
	{
		fast = fast->next;
	}
	//这次两个同时一步一步的走，fast为空的时候slow就到了
	while (fast)
	{
		fast = fast->next;
		slow = slow->next;
	}
	return slow->val;
}

int main()
{

	return 0;
}