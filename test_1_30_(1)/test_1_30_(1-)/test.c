#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现库函数strlen
//size_t strlen(const char* string);
#include<stdio.h>
#include<assert.h>
//法一：计数器
//size_t My_strlen(const char* arr)
//{
//	assert(arr != NULL);
//	size_t count = 0;
//	while (*arr != '\0')
//	{
//		count++;
//		arr++;
//	}
//	return count;
//}
//法二：指针相减
//size_t My_strlen(const char* arr)
//{
//	assert(arr != NULL);
//	const char* start = arr;
//	while (*arr != '\0')
//	{
//		arr++;
//	}
//	return arr-start;
//}
//法三：递归
size_t My_strlen(const char* arr)
{
	assert(arr != NULL);
	if (*arr == '\0')
	{
		return 0;
	}
	else
	{
		return 1 + My_strlen(arr + 1);
	}
}
int main()
{
	int arr[100] = { 0 };
	printf("请输入字符串:");
	scanf("%s", arr);
	printf("您输入的字符串长度是%zd.",My_strlen(arr));

}