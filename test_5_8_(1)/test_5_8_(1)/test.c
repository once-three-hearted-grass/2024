#define _CRT_SECURE_NO_WARNINGS 1
#define MAXSIZE 29
#include <stdio.h>
#include <stdlib.h>
//顺序表 删除指定范围
//设计一个高效的算法，从顺序表L中删除所有值介于x和y之间(包括x和y)的所有元素（假设y >= x），要求时间复杂度为O(n)，空间复杂度为O(1)。
//函数原型如下：
//void del_x2y(SeqList * L, ElemType x, ElemType y);
//相关定义如下：
typedef int ElemType;

struct _seqlist {
    ElemType elem[MAXSIZE];
    int last;
};

typedef struct _seqlist SeqList;

void del_x2y(SeqList* L, ElemType x, ElemType y) {
    //思想：一个指针tail保留新的数据，一个指针cur往前走，cur每次拷贝给tail，如果cur不满足，就向前走，满足就拷贝给tail
    int tail = 0;
    int cur = 0;
    int last = L->last;//因为last要变，所以存好
    while (cur <= last)//last是最后一个元素的下标，所以<=
    {
        if (L->elem[cur] >= x && L->elem[cur] <= y)
        {
            cur++;
            //数组要变短
            L->last--;
        }
        else
        {
            L->elem[tail] = L->elem[cur];
            cur++;
            tail++;
        }
    }
}