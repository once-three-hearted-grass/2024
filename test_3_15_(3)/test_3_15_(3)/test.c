#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现memcpy
//void *memcpy( void *dest, const void *src, size_t count );
//按照字节拷贝,count表示拷贝多少字节，地址是指从哪里开始拷贝
#include<stdio.h>
#include<string.h>
#include<assert.h>
void* my_memcpy(void* dest, const void* src, size_t count)
{
	assert(dest && src);
	void* ret = dest;
	while (count--)//循环count次
	{
		*((char*)dest) = *((char*)src);
		((char*)dest)++;
		((char*)src)++;
	}
	return ret;

}
int main()
{
	int arr1[] = { 1,2,3,4,5,6,7 };
	int arr2[20] = { 0 };
	my_memcpy(arr2, arr1, 20);//但不能处理有交叉地址的拷贝
	for (int i = 0; i < 20; i++)
	{
		printf("%d ", arr2[i]);
	}
	return 0;
}