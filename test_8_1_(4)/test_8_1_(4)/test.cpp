#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<array>
#include<vector>
using namespace std;

template<class T, double N>
class Array
{
public:
	int func()
	{
		N--;
	}
private:
	//T arr[N];
};
//
//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{}
//
//	bool operator<(const Date& d)const
//	{
//		return (_year < d._year) ||
//			(_year == d._year && _month < d._month) ||
//			(_year == d._year && _month == d._month && _day < d._day);
//	}
//
//	bool operator>(const Date& d)const
//	{
//		return (_year > d._year) ||
//			(_year == d._year && _month > d._month) ||
//			(_year == d._year && _month == d._month && _day > d._day);
//	}
//
//	friend ostream& operator<<(ostream& _cout, const Date& d);
//private:
//	int _year;
//	int _month;
//	int _day;
//};

//ostream& operator<<(ostream& _cout, const Date& d)
//{
//	_cout << d._year << "-" << d._month << "-" << d._day;
//	return _cout;
//}

template<class T>
class myless
{
public:
	bool operator()(const T& t1, const T& t2)
	{
		return t1 < t2;
	}
};

template<class T>
class myless1
{
public:
	bool operator()(const T& t1, const T& t2)
	{
		return *t1 < *t2;
	}
};

//template<>
//bool Less<int*>(int* left, int* right)
//{
//	return *left < *right;
//}

void PrintVector(const vector<int>& v)
{
	vector<int>::const_iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

template<class T>
void PrintVector(const vector<T>& v)
{
	typename vector<T>::const_iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

template<class T>
bool Less(const T& left,const T& right)
{
	return left < right;
}

template<>
bool Less<int*>(int* const& left,int* const& right)
{
	return *left < *right;
}

template<class T1,class T2>
class Date
{
public:
	Date()
	{
		cout << "template<class T1,class T2>" << endl;
	}
private:
};

template<>
class Date<int,double>
{
public:
	Date()
	{
		cout << "class Date<int,double>" << endl;
	}
private:
};

template<class T>
class Date<T ,double>
{
public:
	Date()
	{
		cout << "class Date<T ,double>" << endl;
	}
private:
};

template<class T1,class T2>
class Date<T1*, T2*>
{
public:
	Date()
	{
		cout << "class Date<T1*, T2*>" << endl;
		cout << typeid(T1).name() << endl;
	}
private:
};

template<class T1, class T2>
class Date<T1&, T2&>
{
public:
	Date()
	{
		cout << "class Date<T1&, T2&>" << endl;
		cout << typeid(T1).name() << endl;
	}
private:
};

int main()
{
	//Array<int, 100> a1;
	//a1.func();
	////这个就表示建立一个100个整型的数组
	//Array<int, 10> a1;
	//array<int,10> a;
	//int arr[10] = { 0 };
	//arr[100] = 10;
	//Array<int, 12.2> a;

	//myless1<Date*> a;
	//cout<<a(new Date(23, 2, 3),new Date(22, 3, 4));
	//cout << Less(new int(1), new int(2));
	//vector<double> a = { 1.1,2.3,3.4,5.3 };
	//PrintVector(a);
	//Date<double, double> a;
	Date<int&,int&> a;
	return 0;
}











