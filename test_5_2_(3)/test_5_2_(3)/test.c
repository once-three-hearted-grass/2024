#define _CRT_SECURE_NO_WARNINGS 1
//给你两个单链表的头节点 headA 和 headB ，请你找出并返回两个单链表相交的起始节点。
//如果两个链表不存在相交节点，返回 null 。
//题目数据 保证 整个链式结构中不存在环。
//
//注意，函数返回结果后，链表必须 保持其原始结构 。


#include<stdio.h>


struct ListNode {
   int val;
   struct ListNode *next;
 };
typedef struct ListNode ListNode;

struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
	//先计算两个链表的长度，那么长的链表就先走它们的差距步，等到链表相等时就是相交点
	ListNode* curA = headA;
	ListNode* curB = headB;
	int countA = 0;
	int countB = 0;
	//先找到两个链表的长度
	while (curA)
	{
		countA++;
		curA = curA->next;
	}
	while (curB)
	{
		countB++;
		curB = curB->next;
	}
	int dif = abs(countA - countB);
	//找出长链表
	ListNode* LongListNode = headA;
	ListNode* ShortListNode = headB;
	if (countB > countA)
	{
		LongListNode = headB;
		ShortListNode = headA;
	}
	//长链表先走dif步
	while (dif--)
	{
		LongListNode = LongListNode->next;
	}
	//两个一起走
	while (LongListNode)
	{
		if (LongListNode == ShortListNode)
		{
			return LongListNode;
		}
		LongListNode = LongListNode->next;
		ShortListNode = ShortListNode->next;
	}
	return NULL;
}
int main()
{

	return 0;
}