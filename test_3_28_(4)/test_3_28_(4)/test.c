#define _CRT_SECURE_NO_WARNINGS 1
//写一个宏，计算结构体中某变量相对于首地址的偏移，并给出说明
//考察：offsetof宏的实现
//#include<stddef.h>
#define my_offsetof(a,b) (char*)&b-(char*)&a
struct S
{
	char a;
	int b;
	char c;
	int d;
};
//size_t offsetof(structName, memberName);
#include<stdio.h>
int main()
{
	struct S s = { 0 };
	int ret = my_offsetof(s, s.c);
	printf("%d ",ret);
	return 0;
}