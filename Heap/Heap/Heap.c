#define _CRT_SECURE_NO_WARNINGS 1
#include"heap.h"

//堆的初始化
void HeapInit(HP* php)
{
	assert(php);
	php->a = NULL;
	php->capacity = php->size = 0;
}

// 堆的销毁
void HeapDestory(HP* php)
{
	assert(php);
	free(php->a);
	php->size = php->capacity = 0;
}

Swap(HPDataType* child, HPDataType* parent)
{
	HPDataType tmp = *child;
	*child = *parent;
	*parent = tmp;
}

AdjustUp(HPDataType* a, int child)
{
	//这里我们建立小堆
		//能这样干的前提，向上的都是小堆
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] < a[parent])
		{
			Swap(&a[child],&a[parent]);
		}
		else
		{
			break;
		}
		child = parent;
		parent = (child - 1) / 2;
	}
}

// 堆的插入
void HeapPush(HP* php, HPDataType x)
{
	assert(php);
	if (php->capacity == php->size)
	{
		int new_capacity = php->capacity == 0 ? 4 : php->capacity * 2;
		HPDataType*p = realloc(php->a, sizeof(HPDataType) * new_capacity);
		if (p == NULL)
		{
			perror("HeapPush:");
			return;
		}
		php->a = p;
		php->capacity = new_capacity;
	}
	php->a[php->size] = x;
	php->size++;
	AdjustUp(php->a, php->size-1);
}

void AdjustDown(HPDataType*a, int n, int parent)
{
	int child = parent * 2 + 1;//先默认小的孩子为左孩子
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] < a[child])
		{
			child++;
		}
		//开始调整
		if (a[parent] > a[child])
		{
			Swap(&a[parent], &a[child]);
		}
		else
		{
			break;
		}
		parent = child;
		child = child * 2 + 1;
	}
}

// 堆的删除
void HeapPop(HP* php)//堆的删除就是删除堆顶元素
{
	assert(php);
	assert(php->size > 0);
	//先交换第一个元素与最后一个元素
	Swap(&php->a[0], &php->a[php->size - 1]);
	//再删除最后一个元素
	php->size--;
	AdjustDown(php->a, php->size, 0);//传入数组长度和要向下调整的元素下标
	//能这样干的前提，向下的都是小堆
}

// 取堆顶的数据
HPDataType HeapTop(HP* php)
{
	assert(php);
	assert(php->size > 0);
	return php->a[0];
}

// 堆的数据个数
int HeapSize(HP* php)
{
	assert(php);
	return php->size;
}

// 堆的判空
int HeapEmpty(HP* php)
{
	assert(php);
	return php->size == 0;
}

//堆排序
void HeapSort1(HPDataType* a, int n)//小堆就降序
{
	//首先要把这个数组变成堆才行
	//则从第一个元素开始就向上调整就可以了，因为向上调整的要求就是上面的为堆，所以从开头开始，或者从第二个开始，因为第一个已经是堆了
	for (int i = 2; i < n; i++)
	{
		AdjustUp(a, i);
	}
	//这样的话就变成堆了，这样的话第一个元素就是最小的了
	while (n > 0)
	{
		Swap(&a[0], &a[n - 1]);
		n--;//因为这样的话下一步向下调整就不会涉及到我的最小元素了
		//再对第一个元素进行向下调整
		AdjustDown(a, n, 0);//这样的话第二小的元素就跑到堆顶了
	}
}

//堆排序
void HeapSort2(HPDataType* a, int n)//小堆就降序
{
	//第二个方法就是建堆的方法不一样罢了
	//倒着建堆，每一个都向下调整，因为向下调整的前提就是下面的为堆
	//而且从第一个非叶子节点开始建堆，因为一个叶子节点就已经是堆了
	for (int i =(n-1-1)/2; i >= 0; i--)
	{
		AdjustDown(a,n, i);
	}
	//这样的话就变成堆了，这样的话第一个元素就是最小的了
	while (n > 0)
	{
		Swap(&a[0], &a[n - 1]);
		n--;//因为这样的话下一步向下调整就不会涉及到我的最小元素了
		//再对第一个元素进行向下调整
		AdjustDown(a, n, 0);//这样的话第二小的元素就跑到堆顶了
	}
}

//创造10万个数据
void CreatData()
{
	FILE* pf = fopen("text.txt", "w");
	int n = 100000;
	srand((unsigned int)time(NULL));
	for(int i=0;i<n;i++)
	{
		int x = (rand() + i) % 100000;
		fprintf(pf, "%d\n", x);
	}
	fclose(pf);
}

//topk算法
void PrintTopK(int k)
{
	//找出前k个大的，而且空间复杂度为1
	//建立大小为k的堆，因为k远小于10万，所以视为常数
	//向下调整建立堆
	int* arr = (int*)malloc(sizeof(int)*k);
	//将前k个数据放入数组中
	FILE* pf = fopen("text.txt", "r");
	for (int i = 0; i < k; i++)
	{
		fscanf(pf, "%d", &arr[i]);
	}
	//将前k个数据建堆
	for (int i = (k - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(arr, k, i);
	}
	//现在堆最上面放的肯定是最小的
	//现在开始访问十万个数据，比堆顶大的，就放上去，并向下调整
	int x = 0;
	while (fscanf(pf, "%d", &x) != EOF)
	{
		if (x > arr[0])
		{
			arr[0] = x;
			AdjustDown(arr, k, 0);
		}
	}
	//打印出前k个最大的
	for (int i = 0; i < k; i++)
	{
		printf("%d ", arr[i]);
	}
}