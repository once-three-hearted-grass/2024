#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<assert.h>
#include<errno.h>
#include<time.h>
typedef int HPDataType;
typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}HP;

//堆的初始化
void HeapInit(HP* php);

// 堆的销毁
void HeapDestory(HP* php);

// 堆的插入
void HeapPush(HP* php, HPDataType x);

// 堆的删除
void HeapPop(HP* php);

// 取堆顶的数据
HPDataType HeapTop(HP* php);

// 堆的数据个数
int HeapSize(HP* php);

// 堆的判空
int HeapEmpty(HP* php);

//向上调整
AdjustUp(HPDataType* a, int child);

//交换
Swap(HPDataType* child, HPDataType* parent);

//堆排序
void HeapSort1(HPDataType* a, int n);

//在文件中创造数据
void CreatData();

//topk算法
void PrintTopK(int k);
