#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<map>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

//class my_compare
//{
//public:
//    bool operator()(pair<string, int> p1, pair<string, int> p2)
//    {
//        return p1.second > p2.second;//因为要降序
//    }
//};
//
//class Solution {
//public:
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        //先把每个string统计好次数
//        map<string, int> m;
//        for (auto x : words)
//        {
//            ++m[x];
//        }
//        //这样就统计好次数了
//        // 先存入vector中再排序//因为sort只能排连续的//所以传入pair
//        vector<pair<string,int>> tmp;
//        for (auto x : m)
//        {
//            tmp.push_back(x);
//        }
//        //接下来按照次数来排序//我们用库里面的排序算法
//        //sort(tmp.begin(), tmp.end());//但这样不行，因为这样默认排的序是pair的first，如果first相同就比second，所以我们要自己写仿函数
//        sort(tmp.begin(), tmp.end(), my_compare());//传入对象进去就可以了，这里是匿名对象
//        //排序排好了，现在只需要取前k个就可以了
//        vector<string> ret;
//        for (int i = 0; i < k; i++)
//        {
//            ret.push_back(tmp[i].first);
//        }
//        return ret;
//    }
//};
//但是这样还不够，因为要求string次数相同的也要按照字节序来比较
//虽然我们的map插入的时候就是按照字节序比较的，相同次数的，字节序前的在前面，但是我们sort是快排，是不稳定的，
//所以可以考虑stable_sort，这个是稳定的
//class my_compare
//{
//public:
//    bool operator()(pair<string, int> p1, pair<string, int> p2)
//    {
//        return p1.second > p2.second;//因为要降序
//    }
//};
//
//class Solution {
//public:
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        //先把每个string统计好次数
//        map<string, int> m;
//        for (auto x : words)
//        {
//            ++m[x];
//        }
//        //这样就统计好次数了
//        // 先存入vector中再排序//因为sort只能排连续的//所以传入pair
//        vector<pair<string, int>> tmp;
//        for (auto x : m)
//        {
//            tmp.push_back(x);
//        }
//        //接下来按照次数来排序//我们用库里面的排序算法
//        //sort(tmp.begin(), tmp.end());//但这样不行，因为这样默认排的序是pair的first，如果first相同就比second，所以我们要自己写仿函数
//        stable_sort(tmp.begin(), tmp.end(), my_compare());//传入对象进去就可以了，这里是匿名对象
//        //排序排好了，现在只需要取前k个就可以了
//        vector<string> ret;
//        for (int i = 0; i < k; i++)
//        {
//            ret.push_back(tmp[i].first);
//        }
//        return ret;
//    }
//};
//或者我们更改比较规则，让次数相同的还要比较字节序
class my_compare
{
public:
    bool operator()(pair<string, int> p1, pair<string, int> p2)
    {
        return (p1.second > p2.second)||(p1.second == p2.second&&p1.first<p2.first);//因为要降序//而且字节序小的在前面
        //这样写就是次数大的在前面，字节序小的在前面
    }
};

class Solution {
public:
    vector<string> topKFrequent(vector<string>& words, int k) {
        //先把每个string统计好次数
        map<string, int> m;
        for (auto x : words)
        {
            ++m[x];
        }
        //这样就统计好次数了
        // 先存入vector中再排序//因为sort只能排连续的//所以传入pair
        vector<pair<string, int>> tmp;
        for (auto x : m)
        {
            tmp.push_back(x);
        }
        //接下来按照次数来排序//我们用库里面的排序算法
        //sort(tmp.begin(), tmp.end());//但这样不行，因为这样默认排的序是pair的first，如果first相同就比second，所以我们要自己写仿函数
        sort(tmp.begin(), tmp.end(), my_compare());//传入对象进去就可以了，这里是匿名对象
        //排序排好了，现在只需要取前k个就可以了
        vector<string> ret;
        for (int i = 0; i < k; i++)
        {
            ret.push_back(tmp[i].first);
        }
        return ret;
    }
};

int main()
{
    return 0;
}
