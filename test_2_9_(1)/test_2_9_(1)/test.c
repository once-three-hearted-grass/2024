#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现strcpy
#include<stdio.h>
#include<string.h>
//char* my_strcpy(char* destination, const char* source)
//{
//	char* start = destination;
//	while (*source!='\0')
//	{
//		*destination = *source;
//		destination++;
//		source++;
//	}
//	*destination = '\0';
//	return start;
//}
//完善一下
#include<assert.h>
char* my_strcpy(char* destination, const char* source)
{
	assert(destination && source);
	char* start = destination;
	while (*destination++ = *source++);
	return start;
}
int main()
{
	char arr1[100] = "ckckyyds";
	char arr2[20] = "hrtsb";
	//strcpy(arr1, arr2);
	//strcpy拷贝是将arr2的内容覆盖到arr1的内容，将arr2的内容直到\0(所以必须要有\0)，但也只包括一个\0,覆盖过去
	//%s打印也是打印到\0就停止了，所以本来是hrtsb\0yds的，只打印了hrtsb
	//char *strcpy( char *strDestination, const char *strSource );
	my_strcpy(arr1, arr2);
	printf("%s", arr1);
	return 0;
}