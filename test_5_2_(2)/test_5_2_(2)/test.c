#define _CRT_SECURE_NO_WARNINGS 1
//对于一个链表，请设计一个时间复杂度为O(n), 额外空间复杂度为O(1)的算法，判断其是否为回文结构。
//给定一个链表的头指针A，请返回一个bool值，代表其是否为回文结构。保证链表长度小于等于900。
//测试样例：
//1->2->2->1
//返回：true

#include<stdbool.h>
#include<stdio.h>
struct ListNode {
    int val;
    struct ListNode* next;
}; 
//判断是否对称的
typedef struct ListNode ListNode;

ListNode* reverse(ListNode* head)
{
    ListNode* cur = head;
    if (head == NULL)
    {
        return NULL;
    }
    ListNode* next =cur->next ;
    cur->next = NULL;
    while (next)
    {
        ListNode* next_next = next->next;
        next->next = cur;
        cur = next;
        next = next_next;
    }
    return cur;//返回头节点
}

ListNode* find_mid(ListNode* head)
{
    ListNode* slow = head;
    ListNode* fast = head;
    if (head == NULL)
    {
        return NULL;
    }
    while (fast&&fast->next)
        {
        fast = fast->next->next;
        slow = slow->next;
        }
    return slow;
}
bool chkPalindrome(ListNode* head) {
    // write code here
    //先找到中间节点
    ListNode* mid = find_mid(head);
    //再逆转mid后面链表
    ListNode* head2 = reverse(mid);
    //现在开始挨着挨着比较
    //但mid的前一个还连着mid要注意一下
    //但是也不影响
    ListNode* cur1 = head;
    ListNode* cur2 = head2;
    while (cur2)
    {
        if (cur1->val != cur2->val)
        {
            return false;
        }
        cur2 = cur2->next;
        cur1 = cur1->next;
    }
    return true;
}
int main()
{
    ListNode a1;
    a1.val = 1;
    ListNode a2;
    a2.val = 2;
    ListNode a3;
    a3.val = 2;
    ListNode a4;
    a4.val = 1;
    a1.next = &a2;
    a2.next = &a3;
    a3.next = &a4;
    a4.next =NULL;
    chkPalindrome(&a1);


    return 0;
}