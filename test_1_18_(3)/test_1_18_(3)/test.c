#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//给定一个大小为 n 的数组 nums ，返回其中的多数元素。多数元素是指在数组中出现次数 大于 ? n / 2 ? 的元素。
//你可以假设数组是非空的，并且给定的数组总是存在多数元素。
// int majorityElement(int* nums, int numsSize)
// {
//     //制造另一个相同的数组，重复几次就加几次，两个数组之差就是个数
//     int arr[numsSize];
//     int i=0;
//     for(i=0;i<numsSize;i++)
//     {
//         arr[i]=nums[i];
//     }
//     for(i=0;i<numsSize;i++)
//     {
//         int sum=0;
//         sum=0;
//         for(int j=0;j<numsSize;j++)
//         {
//             if(arr[j]==nums[i])
//             {
//                 sum++;
//             }
//         }
//         for(int j=0;j<numsSize;j++)
//         {
//             if(arr[j]==nums[i])
//             {
//                 arr[j]+=sum;
//             }
//         }
//     }
//     for(i=0;i<numsSize;i++)
//     {
//         if((arr[i]-nums[i])>(numsSize)/2)
//         {
//             return nums[i];
//         }
//     }
//     return 0;
// }

int majorityElement(int* nums, int numsSize)
{
    //制造另一个相同的数组，重复几次就加几次，加的次数够多就返回
    int arr[numsSize];
    int i = 0;
    for (i = 0; i < numsSize; i++)
    {
        arr[i] = nums[i];
    }
    for (i = 0; i < numsSize; i++)
    {
        int sum = 0;
        sum = 0;
        for (int j = 0; j < numsSize; j++)
        {
            if (arr[j] == nums[i])
            {
                sum++;
            }
            if (sum > (numsSize) / 2)
            {
                return arr[i];
            }
        }

    }
    return 0;
}
int main()
{
    
    return 0;
}