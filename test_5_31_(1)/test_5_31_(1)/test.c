#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include"Sort.h"
void test1()
{
	int arr[] = {9,1,2,5,7,4,6,3,232,8};
	int sz = sizeof(arr) / sizeof(arr[0]);
	//InsertSort(arr, sz);
	//Print(arr, sz);

	//ShellSort(arr, sz);
	//Print(arr, sz);

	//SelectSort(arr, sz);

	//HeapSort(arr, sz);
	BubbleSort(arr, sz);
	Print(arr, sz);
}
void test2()//产生随机数来排序
{
	int n = 100000;//产生n个随机数
	srand((unsigned int)time(NULL));
	int* a1 = (int*)malloc(sizeof(int) * n);
	int* a2 = (int*)malloc(sizeof(int) * n);
	int* a3 = (int*)malloc(sizeof(int) * n);
	int* a4 = (int*)malloc(sizeof(int) * n);
	int* a5 = (int*)malloc(sizeof(int) * n);
	for (int i = 0; i < n; i++)
	{
		int x = (rand() + i) % n;
		a1[i] = x;
		a2[i] = x;
		a3[i] = x;
		a4[i] = x;
		a5[i] = x;
	}

	int head1 = clock();
	InsertSort(a1, n);
	int tail1 = clock();

	int head2 = clock();
	ShellSort(a2, n);
	int tail2 = clock();

	int head3 = clock();
	SelectSort(a3, n);
	int tail3 = clock();
	
	int head4 = clock();
	HeapSort(a4, n);
	int tail4 = clock();

	int head5 = clock();
	BubbleSort(a5, n);
	int tail5 = clock();

	printf("InsertSort:%dms\n", tail1 - head1);
	printf("ShellSort:%dms\n", tail2 - head2);
	printf("SelectSort:%dms\n", tail3 - head3);
	printf("HeapSort:%dms\n", tail4 - head4);
	printf("BubbleSort:%dms\n", tail5 - head5);

}
int main()
{
	//test1();
	test2();
	return 0;
}