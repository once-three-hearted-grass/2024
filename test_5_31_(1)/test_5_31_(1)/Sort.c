#define _CRT_SECURE_NO_WARNINGS 1

#include"Sort.h"

//交换函数
void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

// 插入排序
void InsertSort(int* a, int n)//时间复杂度为O(N^2)
{
	//这个排序的思想就是，前面0~n个数据已经有序了，把第n+1个数据插入进去
	for (int i = 0; i < n - 1; i++)
	{
		//先储存好要插入的数据
		int end = i;//假设0~end有序
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (a[end] > tmp)//升序
			{
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}

//打印数组
void Print(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
}

//// 希尔排序
//void ShellSort(int* a, int n)
//{
//	//希尔排序主要思想就是，把每相隔gap的为一组进行插入排序，然后gap逐渐减小，减小为1，就是真正的插入排序了
//	//排序这个东西就是由内到外逐层的写，先假设gap==多少，先把一趟写好,写好一趟就写外面一趟，慢慢来
//	int gap = 4;
//	for (int j = 0; j < gap; j++)//这里就是将gap组进行插入排序
//	{
//		for (int i = j; i < n - gap; i += gap)//这一趟就和插入排序的最外面那一层差不多了，因为要保证a[end + gap]存在，所以条件那样设，end+gap<n
//		{
//			//这个处理的就是其中一个gap组的插入排序
//			int end = i;
//			int tmp = a[end + gap];
//			while (end >= 0)//最里面这一趟与插入排序差不多的
//			{
//				if (a[end] > tmp)
//				{
//					a[end + gap] = a[end];//循环外面写了a[end+gap],所以这里是存在的，不会越界
//					end -= gap;
//				}
//				else
//				{
//					break;
//				}
//			}
//			a[end + gap] = tmp;
//		}
//	}
//}

// 希尔排序
void ShellSort(int* a, int n)//时间复杂度O(N^1.3)很快的
{
	//希尔排序主要思想就是，把每相隔gap的为一组进行插入排序，然后gap逐渐减小，减小为1，就是真正的插入排序了
	//排序这个东西就是由内到外逐层的写，先假设gap==多少，先把一趟写好,写好一趟就写外面一趟，慢慢来
	int gap = n;//为什么/3，因为统计出来这样效率最高
	//这样设计的话可以保证gap最后的值一定是1，不可能是0，gap不能为0，但是一定最后要为1
	while (gap > 1)//保证最后为1执行后就退出了
	{
		gap = gap / 3 + 1;
		for (int i = 0; i < n - gap; i++)//这一趟就和插入排序的最外面那一层差不多了，因为要保证a[end + gap]存在，所以条件那样设，end+gap<n
		{
			//这里可以优化的，直接i++
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)//最里面这一趟与插入排序差不多的
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];//循环外面写了a[end+gap],所以这里是存在的，不会越界
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}

// 选择排序
void SelectSort(int* a, int n)//时间复杂度O(N^2)
{
	//假设head~tail包括头尾这个区间是无序的，然后遍历这个区间一遍，选出最小的，和最大的，放在头尾
	//因为不能直接替换，所以要定义最小最大值的下标，最后交换数据就可以了
	int head = 0;
	int tail = n - 1;
	while (head<tail)
	{
		int min = head;
		int max = head;
		for (int i = head+1; i <= tail; i++)//因为第一个可以不用比
		{
			if (a[i] < a[min])
			{
				min = i;
			}
			if (a[i] > a[max])
			{
				max = i;
			}
		}
		////交换数据
		//int tmp = a[head];
		//a[head] = a[min];
		//a[min] = tmp;

		//tmp = a[tail];//还有一种出错的可能，就是max的位置就是一开始head的位置，本来head的位置是最大值的位置，结果现在变成了最小值还去交换了
		////所以不行
		//a[tail] = a[max];
		//a[max] = tmp;
		
		////处理,为了防止相互替换时把最值替换走了，就直接定义四个变量，存放四个变量，然后直接赋值
		//int tmp1 = a[head];
		//int tmp2 = a[tail];
		//int tmp3 = a[min];
		//int tmp4 = a[max];
		//a[head] = tmp3;
		//a[min] = tmp1;
		//a[tail] = tmp4;
		//a[max] = tmp2;//这样还是不行，因为他们可能会指向同一个位置就很麻烦

		//这样来，先把最大值最小值存起来，先交换头与最小值，然后看最大值位置还是不是最大值，如果不是就不用交换了
		int MIN = a[min];
		int MAX = a[max];

		a[min] = a[head];
		a[head] = MIN;

		if (a[max] == MAX)//此时若不等于的话，就说明他们指向的位置有重合，此位置就不是最大值的位置了,tail也不--了
		{
			a[max] = a[tail];
			a[tail] = MAX;
			tail--;
		}

		head++;


		////法二
		//int tmp = a[head];
		//a[head] = a[min];
		//a[min] = tmp;
		//if (max == head)
		//{
		//	max = min;//画图就可得，就可以这样处理
		//}
		//tmp = a[tail];
		//a[tail] = a[max];
		//a[max] = tmp;
	}
}

//向下调整算法
void AdjustDwon(int* a, int n, int root)
{
	//这里我们建大堆，因为要升序
	//向下调整，说明下面的是堆
	//先找出左右孩子大的那个
	int child = 2 * root + 1;
	while (child < n)//开始调整//只要孩子没有越界就可以调整了
	{
		if ((child + 1) < n && a[child + 1] > a[child])//说明右孩子更小，并且没有越界
		{
			child++;
		}
		if (a[root] < a[child])
		{
			//交换
			int tmp = a[root];
			a[root] = a[child];
			a[child] = tmp;
		}
		else
		{
			break;
		}
		root = child;
		child = child * 2 + 1;
	}
}

// 堆排序
void HeapSort(int* a, int n)//时间复杂度O(NlogN)
{
	//从最后一个元素开始向下调整建堆，因为向下调整保证的就是下面的为堆,挨着挨着来
	//可以从最后一个孩子的父亲开始建堆
	//先建堆
	int tail = (n-1- 1) / 2;
	while (tail >= 0)//
	{
		AdjustDwon(a, n, tail);
		tail--;
	}
	//建堆完成，现在开始排序，这里我们建立的是大堆，要升序就建大堆
	//每次把堆顶的元素与末尾元素交换，堆顶向下调整，末尾元素位置--
	tail = n - 1;
	while (tail > 0)//tail=0就不用交换了
	{
		//交换堆顶元素与数组末尾元素
		int tmp = a[0];
		a[0] = a[tail];
		a[tail] = tmp;
		tail--;
		AdjustDwon(a, tail, 0);
	}
}

// 冒泡排序
void BubbleSort(int* a, int n)
{
	//冒泡排序就不用由内到外慢慢搞了，直接从最外层开始写
	//把数组遍历一遍，把大的交换到后面，走一趟就可以把最大的弄到最后面
	//总共要走n-1趟，因为最后一个不用走了
	for (int i = 0; i < n - 1; i++)
	{
		//如果一趟一次都没交换说明了已经有序了，就不用再次冒泡了//用flag判断
		int flag = 0;
		for (int j = 0; j < n - 1 - i;j++ )//从0开始冒泡,因为涉及到a[j+1],所以j<n-1,,,因为每次冒泡时，原先冒好的最大值，就不用再判断了，所以j<n-1-i
		{
			if (a[j] > a[j + 1])
			{
				Swap(&a[j], &a[j + 1]);
				flag = 1;
			}
		}
		if (flag == 0)
		{
			break;
		}
	}
}