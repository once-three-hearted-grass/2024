#define _CRT_SECURE_NO_WARNINGS 1
//找出单身狗1
//一个数组中只有一个数字是出现一次，其他所有数字都出现了两次。
//编写一个函数找出这一个只出现一次的数字。
#include<stdio.h>
int main()
{
	//利用a^a=0;a^0=a;且满足交换律
	int arr[] = { 1,2,3,2,1 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int sum = 0;
	for (int i = 0; i < sz; i++)
	{
		sum ^= arr[i];
	}
	printf("单身狗是%d", sum);
	return 0;
}