#define _CRT_SECURE_NO_WARNINGS 1
#include<stdbool.h>
#include<stdio.h>

//给你两棵二叉树 root 和 subRoot 。检验 root 中是否包含和 subRoot 具有相同结构和节点值的子树。如果存在，返回 true ；否则，返回 false 。
//二叉树 tree 的一棵子树包括 tree 的某个节点和这个节点的所有后代节点。tree 也可以看做它自身的一棵子树。

  struct TreeNode {
      int val;
     struct TreeNode *left;
      struct TreeNode *right;
  };
  bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
      //判断两个树是不是相同的树的话，就先判断根是不是相同的树
      //在判断两个子树是不是相同的树，而两个子树的判断的逻辑与根是相同的，这就叫大事化小

      //因为要两个子树同时为相同的树，才是真正的树，所以用&&，再加上返回又不是地址之类的，不用再设变量去保存返回值了

      //接下来再来看，递归停止条件，停止条件就是两个数值不相等的话就返回错误
      //而在使用两个数值之前就要看他们是不是空，一个为空都不能看数值
      //两个均为空，返回正确（说明前面的都相同了），一个空一个不为空就返回错误，这是处理空的时候，

      if (p == NULL && q == NULL)
      {
          return true;
      }
      //走到这里已经说明了两个不同时为NULL了
      //至少有一个为NULL
      if (p == NULL || q == NULL)
      {
          return false;
      }
      //根相同根本不能说明什么，要子树也相同才可以
      //不相同就说明返回错误
      if (p->val != q->val)
      {
          return false;
      }
      return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
  }
bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot) {
    //这道题递归的话就是看以这个节点为根的话，这个树与subroot是不是相同的树，相同的话就ok，不相同的话就继续看左右孩子为根的树是不是，有一个就可以了，所以||
    if (root == NULL)
    {
        return false;
    }
    if (root->val==subRoot->val&&isSameTree(root, subRoot))//增加第一个条件的话，一定程度上可以减少好多次第二个条件的执行
    {
        return true;
    }
    return isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
}