#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;

//double Division(int a, int b)
//{
//	// 当b == 0时抛出异常
//	if (b == 0)
//		throw "Division by zero condition!";
//	else
//		return ((double)a / (double)b);
//}
//
//void Func()
//{
//	int len, time;
//	cin >> len >> time;
//	try
//	{
//		cout << Division(len, time) << endl;
//	}
//	catch (const char* errmsg)
//	{
//		cout << 12345 << endl;
//		cout << errmsg << endl;
//	}
//}
//
//int main()
//{
//	try 
//	{
//		Func();
//	}
//	catch (const char* errmsg)
//	{
//	cout << errmsg << endl;
//	}
//	catch(...)
//	{
//		cout << "unkown exception" << endl;
//	}
//	return 0;
//}


// 服务器开发中通常使用的异常继承体系
class Exception
{
public:
	Exception(const string& errmsg, int id)
		:_errmsg(errmsg)
		, _id(id)
	{}
	virtual string what() const
	{
		return _errmsg;
	}

	int GetId()const
	{
		return _id;
	}
protected:
	string _errmsg;
	int _id;
};

class SqlException : public Exception
{
public:
	SqlException(const string& errmsg, int id, const string& sql)
		:Exception(errmsg, id)
		, _sql(sql)
	{}
	virtual string what() const
	{
		string str = "SqlException:";
		str += _errmsg;
		str += "->";
		str += _sql;
		return str;
	}
private:
	const string _sql;
};

class CacheException : public Exception
{
public:
	CacheException(const string& errmsg, int id)
		:Exception(errmsg, id)
	{}
	virtual string what() const
	{
		string str = "CacheException:";
		str += _errmsg;
		return str;
	}
};

class HttpServerException : public Exception
{
public:
	HttpServerException(const string& errmsg, int id, const string& type)
		:Exception(errmsg, id)
		, _type(type)
	{}
	virtual string what() const
	{
		string str = "HttpServerException:";
		str += _type;
		str += ":";
		str += _errmsg;
		return str;
	}
private:
	const string _type;
};

void CacheMgr()
{
	if (rand() % 2 == 0)
	{
		throw CacheException("网络不佳", 100);
	}
	else if (rand() % 3 == 0)
	{
		throw CacheException("对方已删除你的好友", 101);
	}
	throw CacheException("发送成功", 103);
}
void HttpServer()
{
	//网络不佳，就重新发送三次
	for (int i = 0; i < 4; i++)
	{
		try
		{
			CacheMgr();
		}
		catch (const Exception& e)
		{
			if (e.GetId() == 100)
			{
				if (i == 3)
				{
					cout << "发送失败" << endl;
					break;
				}
				cout << "网络不佳，开始第" << i + 1 << "次发送" << endl;
			}
			else if(e.GetId() == 101)
			{
				//break;//break跳出的是catch，所以不用break
				throw e;
			}
			else
			{
				throw e;
			}
		}
	}
}
#include<chrono>
#include <thread>         
using namespace std;
//int main()
//{
//	srand(time(0));
//	int n = 10;
//	while (1)
//	{
//		this_thread::sleep_for(chrono::seconds(1));
//		try {
//			HttpServer();
//		}
//		catch (const Exception& e)
//		{
//			cout << e.what()<< endl;
//		}
//		cout << endl;
//	}
//	return 0;
//}


double Division(int a, int b)
{
	// 当b == 0时抛出异常
	if (b == 0)
	{
		throw "Division by zero condition!";
	}
	return (double)a / (double)b;
}
void Func()
{
	// 这里可以看到如果发生除0错误抛出异常，另外下面的array没有得到释放。
	// 所以这里捕获异常后并不处理异常，异常还是交给外面处理，这里捕获了再
	// 重新抛出去。
	int* array1 = new int[10];
	int* array2 = new int[10];
	try {
		int len, time;
		cin >> len >> time;
		cout << Division(len, time) << endl;
	}
	catch (const char* arr)
	{
		cout << "delete []" << array1 << endl;
		delete[] array1;
		throw;
	}
	// ...
	cout << "delete []" << array1 << endl;
	delete[] array1;
}
int main()
{
	try
	{
		Func();
	}
	catch (const char* errmsg)
	{
		cout << errmsg << endl;
	}
	return 0;
}
