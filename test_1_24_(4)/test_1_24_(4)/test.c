#define _CRT_SECURE_NO_WARNINGS 1
//求2个数二进制有几个不同
#include <stdio.h>
int main() {
    int a, b;
    while (scanf("%d %d", &a, &b) != EOF) {

        int n = a ^ b;
        //^相异取一，这样求出n有多少个1就知道有多少位不同了
        int count = 0;
        while (n)
        {
            n = n & (n - 1);
            count++;
        }
        printf("%d与%d二进制有%d个不同\n", a,b,count);

    }
    return 0;
}