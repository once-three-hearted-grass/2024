#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<map>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode(int x) : val(x), next(NULL) {}
};
typedef ListNode Node;
class Solution {
public:
    bool hasCycle(ListNode* head) {
        //这里我们用map和set解决
        //用 map来统计节点次数
        map<Node*, int> m;
        Node* cur = head;
        while (cur)
        {
            m[cur]++;
            if (m[cur] == 2)//同一个节点有两次说明有环，因为节点地址都不一样嘛
            {
                return true;
            }
            cur = cur->next;
        }
        //cur为空也说明不为环
        return false;
    }
};





