#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现strstr
//char *strstr( const char *string, const char *strCharSet );
#include<stdio.h>
#include<string.h>
#include<assert.h>
char* my_strstr(const char* dest, const char* src)
{
	assert(dest && src);
  	char* s1 = (char*)dest;
	char* ret = (char*)dest;
	while (*ret!='\0')
	{
		char* s1 = ret;
		char* s2 = (char*)src;
		while (*s1 == *s2 && s1 != '\0' && s2 != '\0')
		{
			s1++;
			s2++;
		}
		if (*s2 == '\0')
		{
			return ret;
		}
		if (*s1 == '\0')//走到这里说明*s2！='\0',且s1太短了
		{
			return NULL;
		}
		//走到这里说明*s2！='\0'&&*s1!='\0'
		//只有*s1!=*s2了
		ret++;
	}
	return NULL;
}
int main()
{
	char arr1[] = "abccdefj";
	char* arr2 = "cdef";
	char* ret = my_strstr(arr1, arr2);
	printf("%s", ret);
	return 0;
}