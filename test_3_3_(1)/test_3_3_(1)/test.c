#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int n = 1001;
	int ans = 0;
	for (int i = 1; i <= n; ++i)
	{
		ans ^= i % 3;
	}
	printf("%d", ans);
	return 0;
}