#define _CRT_SECURE_NO_WARNINGS 1
#include<string>
#include<iostream>
using namespace std;
bool Is(char c)
{
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
    {
        return true;
    }
    else
    {
        return false;
    }
}
string reverseOnlyLetters(string s) {
    int head = 0;
    int tail = s.size()-1;
    while (head < tail)
    {
        while ((!Is(s[head])) && (head < tail))
        {
            head++;
        }
        while ((!Is(s[tail])) && (head < tail))
        {
            tail--;
        }
        swap(s[head], s[tail]);
        head++;
        tail--;
    }
    return s;
}
int main()
{
    string s1 = "ab-cd";
    string s2=reverseOnlyLetters(s1);
    cout << s2 << endl;
    return 0;
}