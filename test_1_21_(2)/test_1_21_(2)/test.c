#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int n = 0;
	
	while (scanf("%d",&n)!=EOF)
	{
		int i, j;
		for (i = 0; i < 2 * n; i++)
		{
			for (j = 0; j < 2 * n; j++)
			{
				if ((j <= i + n-1) && (j >= -i + n-1) && (j >= i - (n-1)) && (j <= -i + n-1+2*(n-1)))
				{
					printf("* ");
					j++;
				}
				else
				{
					printf(" ");
				}
			}
			printf("\n");
		}
	}
	return 0;
}