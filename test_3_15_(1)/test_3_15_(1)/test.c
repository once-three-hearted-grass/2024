#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现strncpy
//char *strncpy( char *strDest, const char *strSource, size_t count );
#include<stdio.h>
#include<string.h>
#include<assert.h>
char* my_strncpy(char* dest, const char* src,size_t num)
{
	assert(dest && src);
	char* ret = dest;
	int len = strlen(src);
	for (int i = 0; i < num; i++)
	{
		if (i > len)
		{
			*dest = '\0';
			dest++;
		}
		else
		{
			*dest = *src;
			dest++;
			src++;
		}
	}
	return ret;
}
int main()
{
	char arr1[] = "ab";
	char arr2[20] = "xxxxxxxx";
	strncpy(arr2, arr1,3);//能拷贝几个就拷贝几个，不会额外补充\0,.如果不够了全补充\0
 	printf("%s",arr2);
	
	return 0;
}