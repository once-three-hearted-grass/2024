#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//在C语言中，?% x?是?printf?函数的格式控制符，用于以十六进制格式打印输出。
//使用?% x?格式控制符打印十六进制数时，
//字母?a - f?会被转换为大写的?A - F?。如果需要带前缀?0x?的十六进制数
//，可以使用? % #x?格式控制符。

int main()
{

    //大小端问题
    int a = 0x11223344;
    char* pc = (char*)&a;
    *pc = 0;
    printf("%x\n", a);//0x11223300
    return 0;
}