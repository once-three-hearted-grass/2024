#define _CRT_SECURE_NO_WARNINGS 1
#include"Queue.h"
int main()
{
	Queue q;
	QueueInit(&q);
	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	/*QueuePop(&q);
	QueuePop(&q);*/
	int ret = QueueFront(&q);
	printf("%d", ret);
	ret = QueueBack(&q);
	printf("%d", ret);
	ret = QueueSize(&q);
	printf("%d", ret);
	ret = QueueEmpty(&q);
	printf("%d", ret);
	QueueDestroy(&q);
	return 0;
}