#define _CRT_SECURE_NO_WARNINGS 1
//求存储中的补码的1的个数
#include<stdio.h>
//int Number(int n)
//{
//	int count = 0;
//	int i = 0;
//	while (i < 32)
//	{
//		if ((n & (1 << i)) != 0)
//		{
//			count++;
//		}
//		i++;
//	}
//	return count;
//}
//int Number(int n)
//{
//	int count = 0;
//	unsigned int m = n;//转换为无符号的，这样就算是负数，这个方法也管用
//	//利用十进制的%10再/10操作对二进制同样操作
//	while(m)
//	{
//		if (m % 2 == 1)
//		{
//			count++;
//		}
//		m /= 2;
//	}
//	return count;
//}
int Number(int n)
{
	int count = 0;
	while (n)
	{
		n = n & (n - 1);
		//这个操作能把二进制的最右边的那个一变为0，变了多少次就有多少个1，而且n不为0，说明至少有一个1
		count++;
	}
	return count;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int get = Number(n);
	printf("%d存储在二进制中的补码的1的个数是%d",n,get);
	return 0;
}