#define _CRT_SECURE_NO_WARNINGS 1
//递归和非递归分别实现求第n个斐波那契数
//例如：
//输入：5  输出：5
//输入：10， 输出：55
//输入：2， 输出：1
//int Fact(int n)
//{
//	if (n <= 2)
//	{
//		return 1;
//	}
//	else
//	{
//		return Fact(n - 1) + Fact(n - 2);
//	}
//}
int Fact(int n)
{
	int a = 1;
	int b = 1;
	int c = 1;
	while (n>=3)
	{
		c = a + b;
		a = b;
		b = c;
		n--;
	}
	return c;
}
#include<stdio.h>
int main()
{
	int n = 0;
	scanf("%d", &n);
	int ret = Fact(n);
	printf("第%d个斐波那契数是%d", n, ret);
	return 0;
}