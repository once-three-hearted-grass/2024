#include"Snake.h"

//设置鼠标位置
void SetPos(short x, short y)
{
	//先来个位置型结构体
	COORD coord = { x,y };
	//先取句柄
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(handle, coord);
}

//设置控制台鼠标等信息
void SetConsole()
{
	//设置控制台名字和大小
	system("mode con cols=100 lines=30");
	system("title 贪吃蛇");
	//隐藏鼠标
	//先取句柄
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	//定义鼠标信息型结构体
	CONSOLE_CURSOR_INFO console = { 0 };
	//先取句柄信息放在结构体中
	GetConsoleCursorInfo(handle, &console);
	//再将可见度置为false
	console.bVisible = false;
	//再设置
	SetConsoleCursorInfo(handle, &console);
}

//打印菜单地图等内容
void PrintMenu()
{
	//第一个内容
	SetPos(40, 14);
	printf("欢迎来到贪吃蛇小游戏");
	SetPos(41, 22);
	system("pause");

	//第二个内容
	system("cls");
	SetPos(25, 14);
	printf("用↑ . ↓  . ← . →分别控制蛇的移动，Shift键为超级加速");
	SetPos(40, 15);
	printf("加速可以得到更高的分数哦,但不会很高的哦");
	SetPos(42, 22);
	system("pause");
	system("cls");

	//第三个内容，打印地图，27行，58列
	//打印上
	for (int i = 0; i < 29; i++)
	{
		wprintf(L"%lc", WALL);
	}
	//下
	SetPos(0, 26);
	for (int i = 0; i < 29; i++)
	{
		wprintf(L"%lc", WALL);
	}
	//左
	for (int i = 1; i <= 25; i++)
	{
		SetPos(0, i);
		wprintf(L"%lc", WALL);
	}
	//右
	for (int i = 1; i <= 25; i++)
	{
		SetPos(56, i);
		wprintf(L"%lc", WALL);
	}
	SetPos(60, 15);
	printf("不能穿墙，不能咬到自己");
	SetPos(60, 16);
	printf("用↑ . ↓  . ← . →分别控制蛇的移动");
	SetPos(60, 17);
	printf("Shift键为超级加速");
	SetPos(60, 18);
	printf("ESC:退出游戏,空格(space):暂停游戏");
	SetPos(60, 21);
	printf("M.与L.@版权");
	SetPos(60, 22);
	printf("版权声明：此游戏由M.与L.独家冠名制造");
	SetPos(60, 23);
	printf("严禁伪造");
}

//打印基本菜单，完善基本信息
void Basic()
{
	//设置控制台鼠标等信息
	SetConsole();
	PrintMenu();
}

//初始化蛇身
void SnakeNodeInit(pSnake ps)
{
	//初始化五个节点
	for (int i = 0; i < 5; i++)
	{
		pSnakeNode cur = (pSnakeNode)malloc(sizeof(SnakeNode));
		if (cur == NULL)
		{
			perror("SnakeMove::malloc");
			return;
		}
		cur->x = 24 + i * 2;
		cur->y = 5;
		cur->next = NULL;
		//挨个挨个头插
		if (ps->phead == NULL)
		{
			ps->phead = cur;
		}
		else
		{
			cur->next = ps->phead;
			ps->phead = cur;
		}
	}
	//再打印蛇身
	pSnakeNode cur = ps->phead;
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}
}

//初始化食物
void FoodInit(pSnake ps)
{
	//x要在2~54，y要在1~25，且x要为偶数，且不能与蛇重合了
	int x = 0;
	int y = 0;
again:
	do
	{
		x = rand() % 53 + 2;
		y = rand() % 25 + 1;
	} while (x % 2 != 0);
	pSnakeNode cur = ps->phead;
	while (cur)
	{
		if (cur->x == x && cur->y == y)
		{
			goto again;
		}

		cur = cur->next;
	}
	SetPos(x, y);
	wprintf(L"%lc", FOOD);
	pSnakeNode newnode = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (newnode == NULL)
	{
		perror("FoodInit::malloc");
		return;
	}
	newnode->x = x;
	newnode->y = y;
	newnode->next = NULL;
	ps->food = newnode;
}

//初始化食物
void FoodInit2(pSnake ps)//创造多个食物
{
	if (ps->food == NULL)
	{
		int count = rand() % FOOD_COUNT + 1;
		while (count--)
		{
			//x要在2~54，y要在1~25，且x要为偶数，且不能与蛇重合了
			int x = 0;
			int y = 0;
		again:
			do
			{
				x = rand() % 53 + 2;
				y = rand() % 25 + 1;
			} while (x % 2 != 0);
			pSnakeNode cur = ps->phead;
			while (cur)
			{
				if (cur->x == x && cur->y == y)
				{
					goto again;
				}

				cur = cur->next;
			}
			SetPos(x, y);
			wprintf(L"%lc", FOOD);
			pSnakeNode newnode = (pSnakeNode)malloc(sizeof(SnakeNode));
			if (newnode == NULL)
			{
				perror("FoodInit::malloc");
				return;
			}
			newnode->x = x;
			newnode->y = y;
			newnode->next = NULL;
			if (ps->food == NULL)
			{
				ps->food = newnode;
			}
			else
			{
				newnode->next = ps->food;
				ps->food = newnode;
			}
		}
	}
}

//初始化蛇
void SnakeInit(pSnake ps)
{
	//初始化蛇身
	SnakeNodeInit(ps);
	//初始化食物
	FoodInit2(ps);
	ps->dir = RIGHT;
	ps->food_weight = 5;
	ps->score = 0;
	ps->sleep_time = 500;
	ps->state = OK;
	ps->length = 5;
	ps->speed = 1;
}

//暂停蛇的移动
void SnakePause()
{
	while (1)
	{
		Sleep(30);
		if (KEY_PRESS(VK_SPACE))
		{
			return;
		}
	}
}

//吃到食物了,吃一个食物就加速
void EatFood(pSnakeNode newnode, pSnake ps)
{
	//直接头插就可以了
	newnode->next = ps->phead;
	ps->phead = newnode;
	//再打印
	pSnakeNode cur = ps->phead;
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}
	ps->score += ps->food_weight;
	free(ps->food);
	ps->food = NULL;
	//继续创建食物
	FoodInit(ps);
	//加速
	if(ps->sleep_time>=10)
	ps->sleep_time -= 10;
	//单次得分增加
	ps->food_weight += 5;
	//长度增加
	ps->length++;
	//速度
	ps->speed += 1;
}

//没有吃到食物
void NoFood(pSnakeNode newnode, pSnake ps)
{
	//还是要头插，只不过要释放最后一个节点
	newnode->next = ps->phead;
	ps->phead = newnode;
	//先找到倒数第二个节点
	pSnakeNode cur = ps->phead;
	while (cur->next->next)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}
	//最好还要打印两个空格，不然会一直显示
	SetPos(cur->next->x, cur->next->y);
	printf("  ");
	free(cur->next);
	cur->next = NULL;
}

//蛇有没有撞到墙或自己
void SnakeIsKilled(pSnake ps)
{
	//撞到墙了吗
	if (ps->phead->x == 0 || ps->phead->x == 56 || ps->phead->y == 0 || ps->phead->y == 26)
	{
		ps->state = KILL_BY_WALL;
	}
	//撞到自己了吗
	pSnakeNode cur = ps->phead->next;
	while (cur)
	{
		if (cur->x == ps->phead->x && cur->y == ps->phead->y)
		{
			ps->state = KILL_BYSELF;
			break;
		}
		cur = cur->next;
	}
}

//下一个是不是食物
int NextIsFood(pSnakeNode newnode, pSnake ps)
{
	pSnakeNode cur = ps->food;
	while (cur)
	{
		if (cur->x == newnode->x && cur->y == newnode->y)
		{
			return 1;
		}
		cur = cur->next;
	}
	return 0;
}

//吃到食物了,吃一个食物就加速
void EatFood2(pSnakeNode newnode, pSnake ps)//要删除那个食物节点
{
	assert(ps->food);
	//找到那个食物节点
	pSnakeNode cur = ps->food;
	while (cur)
	{
		if (cur->x == newnode->x && cur->y == newnode->y)//如果cur==food呢//后面处理
		{
			break;
		}
		cur = cur->next;
	}
	assert(cur);//cur不可能为NULL，因为已经检测过了，因为有警告，所以才assert一下
	//直接头插就可以了
	newnode->next = ps->phead;
	ps->phead = newnode;

	//再打印
	pSnakeNode curphead = ps->phead;
	while (curphead)
	{
		SetPos(curphead->x, curphead->y);
		wprintf(L"%lc", BODY);
		curphead = curphead->next;
	}
	ps->score += ps->food_weight;

	//删除食物节点
	if (cur == ps->food)//这里处理//而且两种情况不同，要分开处理
	{
		ps->food = cur->next;
	}
	else {
		pSnakeNode prev = ps->food;
		while (prev->next != cur)
		{
			prev = prev->next;
		}
		prev->next = cur->next;
	}
	
	free(cur);
	cur = NULL;

	//继续创建食物
	FoodInit2(ps);

	//加速
	if (ps->sleep_time >= 200)
		ps->sleep_time -= 10;
	else if(ps->sleep_time >= 100)
	{
		ps->sleep_time -= 5;
	}
	else if (ps->sleep_time >= 50)
	{
		ps->sleep_time -= 2;
	}
	else if (ps->sleep_time >= 1)
	{
		ps->sleep_time -= 1;
	}

	//单次得分增加
	ps->food_weight += 5;

	//长度增加
	ps->length++;

	//速度
	ps->speed += 1;
}

//开始走一步
void SnakeMove(pSnake ps)
{
	//就是在下一步创建一个节点罢了
	pSnakeNode newnode = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (newnode == NULL)
	{
		perror("SnakeMove::malloc");
		return;
	}
	newnode->next = NULL;
	switch (ps->dir)
	{
	case UP:
		newnode->x = ps->phead->x;
		newnode->y = ps->phead->y - 1;
		break;
	case DOWN:
		newnode->x = ps->phead->x;
		newnode->y = ps->phead->y + 1;
		break;
	case LEFT:
		newnode->x = ps->phead->x - 2;
		newnode->y = ps->phead->y;
		break;
	case RIGHT:
		newnode->x = ps->phead->x + 2;
		newnode->y = ps->phead->y;
		break;
	}
	if (NextIsFood(newnode,ps)) //吃到食物了
	{
		EatFood2(newnode, ps);
	}
	else
	{
		NoFood(newnode, ps);
	}
	//判断是否撞墙，或撞到自己
	SnakeIsKilled(ps);
}

//运行游戏
void SnakeRun(pSnake ps)
{
	int time1 = clock();
	int time2 = 0;
	int score = 0;
	int time = 0;
	do
	{
		//打开最高成绩
		FILE* pf = fopen("text.txt", "r");
		if (pf == NULL)
		{
			SetPos(60, 2);
			printf("历史最优成绩：");
			SetPos(60, 3);
			printf("分数：0，时间：0s");
		}
		else
		{
			fscanf(pf, "%d %d", &score, &time);
			fflush(pf);
			SetPos(60, 2);
			printf("历史最优成绩：");
			SetPos(60, 3);
			printf("分数：%d  时间：%ds",score,time);
		}
		time2 = clock();
		ps->time = (time2 - time1) / 1000;
		SetPos(60, 6);
		printf("所用时间:%ds", ps->time);
		SetPos(60, 7);
		printf("得分:%d", ps->score);
		SetPos(60, 8);
		printf("每个食物得分:%3d", ps->food_weight);
		SetPos(60, 9);
		printf("蛇长:%2d", ps->length);
		SetPos(60, 10);
		printf("速度:%2dm/s", ps->speed);
		if(KEY_PRESS(VK_UP) && ps->dir != DOWN)
		{
			ps->dir = UP;
		}
		else if (KEY_PRESS(VK_DOWN) && ((ps->dir) != UP))
		{
			ps->dir = DOWN;
		}
		else if (KEY_PRESS(VK_RIGHT))
		{
			if(ps->dir != LEFT)
			ps->dir = RIGHT;
		}
		else if (KEY_PRESS(VK_LEFT) )
		{
			if(ps->dir != RIGHT)
			ps->dir = LEFT;
		}
		else if (KEY_PRESS(VK_ESCAPE))//ESC
		{
			ps->state = ESC;
		}
		else if (KEY_PRESS(VK_SPACE))//空格
		{
			//暂停蛇的移动
			SnakePause();
		}
		else if (KEY_PRESS(VK_SHIFT))//这个宏会读取按键，读取你的按键，你的按键按入了，
			//是会被读取的（但要读取的话，也只会读取可显示的字符，比如加号等，SHIFT就不会被读取），
			// 在系统运行期间，你输入的东西是会被放入屏幕显示流的（我自己编的），会放在一个流中，等到可以用的时候在拿出来
			// 所以你在程序运行期间输入的
			//东西会在printf的时候显示出来，而且会被getchar读走了，影响判断，就算多读几次getchar把1读走，也不好处理y到ch中
			//而且打印出来也不好看，还是用不可显示字符吧
		{
			if (ps->sleep_time >= 1)
			{
				if (ps->sleep_time > 100)
				{
					ps->speed += 10;
					ps->sleep_time -= 100;
					ps->food_weight += 5;
				}
				else if(ps->sleep_time>10)
				{
					ps->sleep_time -= 10;
					ps->speed += 1;
					ps->food_weight += 5;
				}
				else
				{
					ps->sleep_time -= 1;
					ps->speed += 1;
					ps->food_weight += 5;
				}
			}
		}
		//开始走一步
		SnakeMove(ps);
		Sleep(ps->sleep_time);
	} while (ps->state == OK);
	//保存成绩
	FILE*pf = fopen("text.txt", "w");
	int scoremax = ps->score > score ? ps->score : score;
	int timemin;
	if (ps->score == score)
	{
		timemin = ps->time > time ? time : ps->time;
	}
	else if(ps->score<score)
	{
		timemin = time;
	}
	else
	{
		timemin = ps->time;
	}
	fprintf(pf, "%d %d", scoremax, timemin);
	fflush(pf);
}

//游戏善后
void SnakeDes(pSnake ps)
{
	if (ps->state == ESC)
	{
		SetPos(23, 14);
		printf("退出游戏成功");
	}
	else if (ps->state == KILL_BY_WALL)
	{
		SetPos(24, 14);

		printf("您撞墙身亡");
	}
	else if (ps->state == KILL_BYSELF)
	{
		SetPos(23, 14);

		printf("您撞到自己了");
	}
	//销毁链表
	free(ps->food);
	ps->food = NULL;
	pSnakeNode cur = ps->phead;
	while (cur)
	{
		pSnakeNode del = cur;
		cur = cur->next;
		free(del);
	}
}

