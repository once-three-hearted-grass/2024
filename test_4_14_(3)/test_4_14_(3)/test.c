#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
//输入：l1 = [1, 2, 4], l2 = [1, 3, 4]
//输出：[1, 1, 2, 3, 4, 4]


struct ListNode {
    int val;
    struct ListNode* next;
};
typedef struct ListNode ListNode;
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
    //创建一个新链表，挨个挨个比较大小尾插
    ListNode* head = (ListNode*)malloc(sizeof(ListNode));
    ListNode* pcur = head;
    while (list1 && list2)
    {
        if (list1->val < list2->val)//尾插list1
        {
            pcur->next = list1;
            pcur = pcur->next;
            list1 = list1->next;
        }
        else
        {
            pcur->next = list2;
            pcur = pcur->next;
            list2 = list2->next;
        }
    }
    if (list1)//说明list1还有剩
    {
        pcur->next = list1;
    }
    else
    {
        pcur->next = list2;
    }
    ListNode* ret = head->next;
    free(head);
    return ret;
}