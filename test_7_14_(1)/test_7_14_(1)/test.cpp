#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//
//class Date
//{
//public:
//	Date(int year, int month,int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	bool operator==(Date & d)
//	{
//		return _year == d._year &&
//			_month == d._month &&
//			_day == d._day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1(2024, 7, 14);
//	Date d2(2024, 7, 14);
//	if (d1.operator==(d2))
//	{
//		cout << "等于"<<endl;
//	}
//	else
//	{
//		cout << "不等于" << endl;
//	}
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	int GetYear()
//	{
//		return _year;
//	}
////private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//bool operator==(Date&d1,Date& d2)
//{
//	return d1._year == d2._year &&
//		d1._month == d2._month &&
//		d1._day == d2._day;
//}
//
//int main()
//{
//	Date d1(2024, 7, 14);
//	Date d2(2024, 7, 14);
//	if (operator==(d1,d2))
//	{
//		cout << "等于" << endl;
//	}
//	else
//	{
//		cout << "不等于" << endl;
//	}
//	if (d1==d2)
//	{
//		cout << "等于" << endl;
//	}
//	else
//	{
//		cout << "不等于" << endl;
//	}
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	bool operator==(Date& d)
//	{
//		return _year == d._year &&
//			_month == d._month &&
//			_day == d._day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1(2024, 7, 14);
//	Date d2(2024, 7, 14);
//	if (d1.operator==(d2))
//	{
//		cout << "等于" << endl;
//	}
//	else
//	{
//		cout << "不等于" << endl;
//	}
//	return 0;
//}

//int add(int a, int b)
//{
//	return a + b;
//}
//
//int main()
//{
//	int c = add(1, 3);
//}


//Date func()
//{
//	Date d;
//	return d;
//}
//
//int main()
//{
//	Date a = func();
//	return 0;
//}

//Date& func()
//{
//	static Date d(2,2,3);
//	return d;
//}
//
//int main()
//{
//	Date& a = func();
//	a.print();
//	return 0;
//}


//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	Date(const Date& d)
//	{
//		cout << "Date(Date& d)" << endl;
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//	~Date()
//	{
//		cout << "~Date()" << endl;
//	}
//	Date& operator=(Date& b)
//	{
//		_year = b._year;
//		_month = b._month;
//		_day = b._day;
//		return (*this);
//	}
//	void print()
//	{
//		cout << _year << "年" << _month << "月" << _day << "号" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date a(1, 1, 1);
//	Date b(2,2,2);
//	Date c(3,3,3);
//	a = b = c;
//	a.print();
//	c.print();
//	b.print();
//	return 0;
//}

class Date
{
public:
	Date(int year = 1, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	~Date()
	{
		cout << "~Date()" << endl;
	}
	Date operator++(int)//后置加加
	{
		Date tmp = (*this);
		_day++;
		return tmp;
	}
	Date& operator++()//前置加加
	{
		_day++;
		return (*this);
	}
	void print()
	{
		cout << _year << "年" << _month << "月" << _day << "号" << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date a(1, 1, 1);
	Date c(1, 1, 1);
	Date b=a++;
	Date d = ++c;
	b.print();
	d.print();
	a.operator++(1);
	a.operator++();
	return 0;
}









