#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;

class Solution {
public:
    bool Is(char c)
    {
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')||(c>='0'&&c<='9'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    bool isPalindrome(string s) {
        //这道题最简单的方法就是将所有的大写字母转换成小写字母，题上都说了
        for (auto& x : s)
        {
            if (x >= 'A' && x <= 'Z')
            {
                x += 32;
            }
        }
        //开始判断是不是回文
        int head = 0;
        int tail = s.size() - 1;
        while (head < tail)
        {
            while (!Is(s[head]) && (head < tail))
            {
                head++;
            }
            while (!Is(s[tail]) && (head < tail))
            {
                tail--;
            }
            if (s[head] != s[tail])
            {
                return false;
            }
            head++;
            tail--;
        }
        return true;
    }
};

int main()
{
    string s1 = "0P";
    Solution s;
    s.isPalindrome(s1);
    return 0;
}