#define _CRT_SECURE_NO_WARNINGS 1
//有一个长度为 n 的非降序数组，比如[1, 2, 3, 4, 5]，将它进行旋转，即把一个数组最开始的若干个元素搬到数组的末尾，
//变成一个旋转数组，比如变成了[3, 4, 5, 1, 2]，或者[4, 5, 1, 2, 3]这样的。
//请问，给定这样一个旋转数组，求数组中的最小值。
//输入：
//[3, 4, 5, 1, 2]
//返回值：
//1
//输入：
//[3, 100, 200, 3]
//返回值：
//3
#include<stdio.h>
int minNumberInRotateArray(int* nums, int numsLen) {
    // write code here
    //旋转数组是将升序数组的前几个放在后面去
    //所以最小值一定偏右
    for (int i = 0; i < numsLen; i++)
    {
        scanf("%d", &nums[i]);
    }
    int left1 = 0;
    int right1 = numsLen - 1;
    int mid1 = (left1 + right1) / 2;
    while (left1 != right1)
    {
        if (nums[mid1] < nums[right1])//说明一直升序，所以最小值在左边
        {
            right1 = mid1;
            mid1 = (left1 + right1) / 2;
        }
        else if (nums[mid1] >= nums[right1])
        {
            left1 = mid1 + 1;
            mid1 = (left1 + right1) / 2;
        }
    }
    int left2 = 0;
    int right2 = numsLen - 1;
    int mid2 = (left2 + right2) / 2;
    while (left2 != right2)
    {
        if (nums[mid2] <= nums[right2])//说明一直升序，所以最小值在左边
        {
            right2 = mid2;
            mid2 = (left2 + right2) / 2;
        }
        else if (nums[mid2] >= nums[right2])
        {
            left2 = mid2 + 1;
            mid2 = (left2 + right2) / 2;
        }
    }
    //因为有重复数组的，不好处理，最小值可能在相等元素之间，也可能在两个想等元素之外
    //所以侦察两次，两边都找一下，找出相等元素下的两边的最小值，再来比较
    return nums[mid1] >= nums[mid2] ? nums[mid2] : nums[mid1];
}
int main()
{
    int arr[5];
    printf("%d", minNumberInRotateArray(arr, 5));
	return 0;
}