#define _CRT_SECURE_NO_WARNINGS 1
//使用所学文件操作，在当前目录下放一个文件data.txt，写一个程序，将data.txt文件拷贝一份，生成data_copy.txt文件。
//基本思路：
//打开文件data.txt，读取数据
//打开文件data_copy.txt，写数据
//从data.txt中读取数据存放到data_copy.txt文件中，直到文件结束。
//关闭两个文件
#include<stdio.h>
int main()
{
	//先创建data.txt,data_copy.txt
	FILE* data_w = fopen("data.txt", "w");
	FILE* copy_w = fopen("data_copy.txt", "w");
	//向data.txt中放入数据
	fputs("hahahahah yyds\n", data_w);
	fputc('a', data_w);
	fputc('b', data_w);
	fputc('c', data_w);
	//拷贝数据，这样data读，copy写
	FILE*data_r = fopen("data.txt", "r");
	//fgets可以完完全全一模一样的读完,格式一样的都
	char tmp[20] = { 0 };//媒介
	fflush(data_w);//刷新一下，因为数据这时不会马上拷贝到磁盘上，数据会放在缓冲区中，你看到了放在文件上，是因为程序已经结束了，fclose会刷新
	//，（如果没有fflush）所以才读不到数据这时，所以才无法拷贝到tmp中，因为文件这时是空的
	while (fgets(tmp, 5, data_r) != NULL)
	{
		//把data的数据读到tmp中
		fputs(tmp, copy_w);//再把tmp中数据放在copy中
	}
	//最后打印copy内容
	//相应的这里也要刷新，因为文件上还没有内容，无法转移
	fflush(copy_w);
	FILE*copy_r = fopen("data_copy.txt", "r");
	while (fgets(tmp, 5, copy_r) != NULL)
	{
		printf("%s", tmp);
	}
	fclose(data_w);
	fclose(data_r);
	fclose(copy_w);
	fclose(copy_r);
	data_w = NULL;
	data_r = NULL;
	copy_w = NULL;
	copy_r = NULL;
	return 0;
}