#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<stdbool.h>
//给你二叉树的根节点 root ，返回其节点值的 层序遍历 。（即逐层地，从左到右访问所有节点）。

  struct TreeNode {
      int val;
      struct TreeNode *left;
      struct TreeNode *right;
  };
  typedef struct TreeNode* QDataType;
  // 链式结构：表示队列 
  typedef struct QListNode
  {
	  struct QListNode* next;
	  QDataType data;
  }QNode;
  typedef struct TreeNode* STDataType;
  // 队列的结构 
  typedef struct Queue
  {
	  QNode* front;
	  QNode* tail;
	  int size;
  }Queue;

  // 初始化队列 
  void QueueInit(Queue* q)
  {
	  assert(q);
	  q->front = q->tail = NULL;
	  q->size = 0;
  }

  // 队尾入队列 
  void QueuePush(Queue* q, QDataType data)
  {
	  assert(q);
	  QNode* NewNode = (QNode*)malloc(sizeof(QNode));
	  if (NewNode == NULL)
	  {
		  perror("QueuePush");
		  return;
	  }
	  NewNode->data = data;
	  NewNode->next = NULL;
	  if (q->tail == NULL)
	  {
		  q->front = q->tail = NewNode;
		  q->size++;
	  }
	  else
	  {
		  q->tail->next = NewNode;
		  q->tail = NewNode;
		  q->size++;
	  }
  }

  // 队头出队列 
  void QueuePop(Queue* q)
  {
	  assert(q);
	  assert(q->front);
	  if (q->front == q->tail)//说明指向同一个节点,而且是最后一个节点
	  {
		  free(q->front);
		  q->front = q->tail = NULL;
		  q->size--;
	  }
	  else
	  {
		  QNode* del = q->front;
		  q->front = q->front->next;
		  free(del);
		  q->size--;
	  }
  }

  // 获取队列头部元素 
  QDataType QueueFront(Queue* q)
  {
	  assert(q);
	  assert(q->size);
	  return q->front->data;
  }

  // 获取队列队尾元素 
  QDataType QueueBack(Queue* q)
  {
	  assert(q);
	  assert(q->size);
	  return q->tail->data;
  }

  // 获取队列中有效元素个数 
  int QueueSize(Queue* q)
  {
	  assert(q);
	  return q->size;
  }

  // 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
  int QueueEmpty(Queue* q)
  {
	  assert(q);
	  return q->size == 0;
  }

  // 销毁队列 
  void QueueDestroy(Queue* q)
  {
	  assert(q);
	  QNode* cur = q->front;
	  while (cur)
	  {
		  QNode* next = cur->next;
		  free(cur);
		  cur = next;
	  }
	  q->front = q->tail = NULL;
	  q->size = 0;
  }
  int CountTree(struct TreeNode* root)
  {
	  if (root == NULL)
	  {
		  return 0;
	  }
	  return 1 + CountTree(root->left) + CountTree(root->right);
  }
int** levelOrder(struct TreeNode* root, int* returnSize, int** returnColumnSizes) {
    //返回int**，那就要开辟malloc,,,,,int*
    int** ret = (int**)malloc(sizeof(int*));
	//层序遍历的话就要用队列了了//队列储存的元素就是每个节点
	Queue s;
	QueueInit(&s);
	//创建一个数组来保存数组的值
	int n = CountTree(root);
	int* arr = (int*)malloc(sizeof(int) * n);
	*ret = arr;
	*returnColumnSizes = ret;
	*returnSize = n;
	int i = 0;
	//开始放入队列
	if (root)
	{
		QueuePush(&s, root);
	}
	else
	{
		return NULL;
	}
	while (!QueueEmpty(&s))
	{
		//先出队列
		struct TreeNode* tmp= QueueFront(&s);
		QueuePop(&s);
		//再入左孩子右孩子
		if (tmp->left)
		{
			QueuePush(&s,tmp->left);
		}
		if (tmp->right)
		{
			QueuePush(&s, tmp->right);
		}
		arr[i] = tmp->val;
		i++;
	}
	QueueDestroy(&s);
	return ret;
}
int main()
{
	struct TreeNode a;
	a.val = 1;
	a.left = a.right = NULL;
	int** get = levelOrder(&a);
	return 0;
}