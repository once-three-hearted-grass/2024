#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<queue>
using namespace std;

class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        priority_queue<int> a;
        for (const auto& x : nums)
        {
            a.push(x);
        }
        int count = k - 1;
        while (count--)
        {
            a.pop();
        }
        return a.top();
    }
};